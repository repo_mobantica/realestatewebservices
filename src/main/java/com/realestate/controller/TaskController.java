package com.realestate.controller;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.Booking;
import com.realestate.bean.CustomerReceiptForm;
import com.realestate.bean.Employee;
import com.realestate.bean.Floor;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.Task;
import com.realestate.repository.*;
import com.realestate.response.TaskResponse;
import com.realestate.response.UpdateTaskResponse;
import com.realestate.response.CommanResponse;
import com.realestate.response.LoginDetails;
import com.realestate.response.TaskDetailsResponse;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.City;
import com.realestate.bean.Company;
import com.realestate.bean.EmployeeSalaryDetails;
import com.realestate.bean.Flat;
import com.realestate.bean.LocationArea;
import com.realestate.bean.Login;
import com.realestate.bean.MonthlyEmployeeSalaryReport;
import com.realestate.bean.TaskHistory;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.EmployeeRepository;
import com.realestate.repository.FlatRepository;
import com.realestate.repository.FloorRepository;
import com.realestate.repository.ProjectRepository;

@Controller
@RestController
@RequestMapping("task")
public class TaskController {


	@Autowired
	TaskRepository taskRepository;
	@Autowired
	FlatRepository flatRepository;
	@Autowired
	FloorRepository floorRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	TaskHistoryRepository taskHistoryRepository;
	@Autowired
	MongoTemplate mongoTemplate;



	@RequestMapping(value = "/getTaskList", method = RequestMethod.POST)
	public ResponseEntity<TaskResponse> getTaskList(@RequestBody LoginDetails loginDetails) {

		TaskResponse response = new TaskResponse();

		try {
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyy");  

			List<Employee> employeeList = employeeRepository.findAll();
			List<Task> taskDetails = taskRepository.findAll();

			for(int i=0; i<taskDetails.size();i++)
			{
				Query query = new Query();
				if(!taskDetails.get(i).getProjectId().equals("ALL"))
				{
					query = new Query();
					Project projectDetails = new Project();
					projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(taskDetails.get(i).getProjectId())), Project.class);
					taskDetails.get(i).setProjectName(projectDetails.getProjectName());
				}
				else
				{
					taskDetails.get(i).setProjectName("ALL");	
				}

				if(!taskDetails.get(i).getBuildingId().equals("ALL"))
				{
					query = new Query();
					ProjectBuilding projectBuilding = new ProjectBuilding();
					projectBuilding = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(taskDetails.get(i).getBuildingId())), ProjectBuilding.class);
					taskDetails.get(i).setBuildingName(projectBuilding.getBuildingName());
				}
				else
				{
					taskDetails.get(i).setBuildingName("ALL");
				}

				if(!taskDetails.get(i).getWingId().equals("ALL"))
				{
					query = new Query();
					ProjectWing projectWing = new ProjectWing();
					projectWing = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(taskDetails.get(i).getWingId())), ProjectWing.class);
					taskDetails.get(i).setWingName(projectWing.getWingName());
				}
				else
				{
					taskDetails.get(i).setWingName("ALL");
				}

				if(!taskDetails.get(i).getFloorId().equals("ALL"))
				{
					query = new Query();
					Floor floorDetails = new Floor();
					floorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("floorId").is(taskDetails.get(i).getFloorId())), Floor.class);
					taskDetails.get(i).setFloorName(floorDetails.getFloortypeName());
				}
				else
				{
					taskDetails.get(i).setFloorName("ALL");
				}

				if(!taskDetails.get(i).getFlatId().equals("ALL"))
				{
					query = new Query();
					Flat flatDetails = new Flat();
					flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(taskDetails.get(i).getFlatId())), Flat.class);
					taskDetails.get(i).setFlatNumber(flatDetails.getFlatNumber());
				}
				else
				{
					taskDetails.get(i).setFlatNumber("ALL");
				}

				for(int j=0;j<employeeList.size();j++)
				{
					if(taskDetails.get(i).getTaskassignFrom().equals(employeeList.get(j).getEmployeeId()))
					{
						taskDetails.get(i).setAssignFrom(employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeemiddleName()+" "+employeeList.get(j).getEmployeelastName());
					}

					if(taskDetails.get(i).getTaskassignTo().equals(employeeList.get(j).getEmployeeId()))
					{
						taskDetails.get(i).setAssignTo(employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeemiddleName()+" "+employeeList.get(j).getEmployeelastName());
					}
				}

				taskDetails.get(i).setTaskassingDate1(dateFormat.format(taskDetails.get(i).getTaskassingDate()));
				taskDetails.get(i).setTaskendDate1(dateFormat.format(taskDetails.get(i).getTaskendDate()));

			}
			response.setTaskList(taskDetails);
			response.setResponse("Task List");
			response.setStatus(1);

			return new ResponseEntity<TaskResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			response.setResponse("Login fail");
			response.setStatus(0);
			return new ResponseEntity<TaskResponse>(response,HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/getTaskDetails", method = RequestMethod.POST)
	public ResponseEntity<TaskDetailsResponse> getTaskDetails(@RequestBody Task task) {

		TaskDetailsResponse response = new TaskDetailsResponse();

		try {


			Query query = new Query();
			Task taskDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("taskId").is(task.getTaskId())), Task.class);

			if(!taskDetails.getProjectId().equals("ALL"))
			{
				query = new Query();
				Project projectDetails = new Project();
				projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(taskDetails.getProjectId())), Project.class);
				taskDetails.setProjectName(projectDetails.getProjectName());
			}
			else
			{
				taskDetails.setProjectName("ALL");	
			}

			if(!taskDetails.getBuildingId().equals("ALL"))
			{
				query = new Query();
				ProjectBuilding projectBuilding = new ProjectBuilding();
				projectBuilding = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(taskDetails.getBuildingId())), ProjectBuilding.class);
				taskDetails.setBuildingName(projectBuilding.getBuildingName());
			}
			else
			{
				taskDetails.setBuildingName("ALL");
			}

			if(!taskDetails.getWingId().equals("ALL"))
			{
				query = new Query();
				ProjectWing projectWing = new ProjectWing();
				projectWing = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(taskDetails.getWingId())), ProjectWing.class);
				taskDetails.setWingName(projectWing.getWingName());
			}
			else
			{
				taskDetails.setWingName("ALL");
			}

			if(!taskDetails.getFloorId().equals("ALL"))
			{
				query = new Query();
				Floor floorDetails = new Floor();
				floorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("floorId").is(taskDetails.getFloorId())), Floor.class);
				taskDetails.setFloorName(floorDetails.getFloortypeName());
			}
			else
			{
				taskDetails.setFloorName("ALL");
			}

			if(!taskDetails.getFlatId().equals("ALL"))
			{
				query = new Query();
				Flat flatDetails = new Flat();
				flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(taskDetails.getFlatId())), Flat.class);
				taskDetails.setFlatNumber(flatDetails.getFlatNumber());
			}
			else
			{
				taskDetails.setFlatNumber("ALL");
			}

			DateFormat dateFormat = new SimpleDateFormat("d/M/yyy");
			dateFormat = new SimpleDateFormat("d/M/yyy");
			taskDetails.setTaskassingDate1(dateFormat.format(taskDetails.getTaskassingDate()));
			dateFormat = new SimpleDateFormat("d/M/yyy");
			taskDetails.setTaskendDate1(dateFormat.format(taskDetails.getTaskendDate()));

			Employee employee = new Employee();
			query = new Query();
			employee = mongoTemplate.findOne(query.addCriteria(Criteria.where("employeeId").is(taskDetails.getTaskassignFrom())), Employee.class);

			taskDetails.setAssignFrom(employee.getEmployeefirstName()+" "+employee.getEmployeemiddleName()+" "+employee.getEmployeelastName());

			employee = new Employee();
			query = new Query();
			employee = mongoTemplate.findOne(query.addCriteria(Criteria.where("employeeId").is(taskDetails.getTaskassignTo())), Employee.class);

			taskDetails.setAssignTo(employee.getEmployeefirstName()+" "+employee.getEmployeemiddleName()+" "+employee.getEmployeelastName());


			query = new Query();
			List<TaskHistory> taskHistoryDetails = mongoTemplate.find(query.addCriteria(Criteria.where("taskId").is(taskDetails.getTaskId())), TaskHistory.class);

			for(int i=0;i<taskHistoryDetails.size();i++)
			{
				dateFormat = new SimpleDateFormat("d/M/yyy");
				taskHistoryDetails.get(i).setStartDate1(dateFormat.format(taskHistoryDetails.get(i).getStartDate()));

				dateFormat = new SimpleDateFormat("d/M/yyy");
				taskHistoryDetails.get(i).setExpectedEndDate1(dateFormat.format(taskHistoryDetails.get(i).getExpectedEndDate()));

				dateFormat = new SimpleDateFormat("d/M/yyy");
				taskHistoryDetails.get(i).setActualEndDate1(dateFormat.format(taskHistoryDetails.get(i).getActualEndDate()));
			}

			response.setTaskId(taskDetails.getTaskId());
			response.setProjectId(taskDetails.getProjectId());
			response.setBuildingId(taskDetails.getBuildingId());
			response.setWingId(taskDetails.getWingId());
			response.setFloorId(taskDetails.getFloorId());
			response.setFlatId(taskDetails.getFlatId());
			response.setTaskName(taskDetails.getTaskName());
			response.setTaskassignFrom(taskDetails.getTaskassignFrom());
			response.setTaskassignTo(taskDetails.getTaskassignTo());
			response.setTaskassingDate1(taskDetails.getTaskassingDate1());
			response.setTaskendDate1(taskDetails.getTaskendDate1());
			response.setTaskStatus(taskDetails.getTaskStatus());
			response.setTaskHistoryDetails(taskHistoryDetails);
			response.setResponse("Task Details");
			response.setStatus(1);

			return new ResponseEntity<TaskDetailsResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			response.setResponse("Login fail");
			response.setStatus(0);
			return new ResponseEntity<TaskDetailsResponse>(response,HttpStatus.OK);
		}
	}


	@RequestMapping(value = "/updateTaskList", method = RequestMethod.POST)
	public ResponseEntity<CommanResponse> updateTaskList(@RequestBody UpdateTaskResponse updatetaskResponse) {

		CommanResponse response = new CommanResponse();

		try {

			for(int i=0;i<updatetaskResponse.getTasklist().size();i++)
			{
				if(!updatetaskResponse.getTasklist().get(i).getOtherTaskStatus().equalsIgnoreCase(""))
				{
					Query query = new Query();
					TaskHistory taskHistoryDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("taskHistoryId").is(Long.parseLong(updatetaskResponse.getTasklist().get(i).getTaskHistoryId()))), TaskHistory.class);
					taskHistoryDetails.setOtherTaskStatus(updatetaskResponse.getTasklist().get(i).getOtherTaskStatus());
					taskHistoryDetails.setActualEndDate(new Date());
					taskHistoryRepository.save(taskHistoryDetails);
				}
			}

			response.setResponse("Task successfully Update");
			response.setStatus(1);

			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			ex.printStackTrace();
			response.setResponse("Login fail");
			response.setStatus(0);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
		}
	}

}
