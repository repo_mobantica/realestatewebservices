package com.realestate.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.Booking;
import com.realestate.bean.CustomerLoanDetails;
import com.realestate.bean.CustomerPaymentDetails;
import com.realestate.bean.Enquiry;
import com.realestate.bean.EnquiryFollowUp;
import com.realestate.bean.Flat;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.BookingRepository;
import com.realestate.repository.CustomerLoanDetailsRepository;
import com.realestate.repository.CustomerPaymentDetailsRepository;
import com.realestate.repository.EnquiryFollowUpRepository;
import com.realestate.repository.EnquiryRepository;
import com.realestate.repository.FlatRepository;
import com.realestate.repository.LoginRepository;
import com.realestate.repository.ParkingZoneRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.UserAssignedProjectRepository;
import com.realestate.response.BookingListResponse;
import com.realestate.response.CommanResponse;
import com.realestate.response.CustomerLoanDetailsResponse;
import com.realestate.response.CustomerLoanListResponse;
import com.realestate.response.EnquiryResponse;
import com.realestate.response.LoginDetails;

@Controller
@RestController
@RequestMapping("loan")
public class CustomerLoanController {


	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	EnquiryRepository enquiryRepository;
	@Autowired
	ParkingZoneRepository parkingZoneRepository;
	@Autowired
	EnquiryFollowUpRepository enquiryFollowUpRepository;
	@Autowired 
	BookingRepository bookingRepository;
	@Autowired
	CustomerPaymentDetailsRepository customerpaymentdetailsrepository;
	@Autowired
	FlatRepository flatRepository;
	@Autowired
	LoginRepository loginRepository;
	@Autowired
	UserAssignedProjectRepository userprojectListRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	CustomerLoanDetailsRepository customerLoandetailsRepository;

	String loanCode;
	private String LoanCode()
	{
		long loanCount = customerLoandetailsRepository.count();

		if(loanCount<10)
		{
			loanCode = "LN000"+(loanCount+1);
		}
		else if((loanCount>=10) && (loanCount<100))
		{
			loanCode = "LN00"+(loanCount+1);
		}
		else if((loanCount>=100) && (loanCount<1000))
		{
			loanCode = "LN0"+(loanCount+1);
		}
		else
		{
			loanCode = "LN"+(loanCount+1);
		}

		return loanCode;
	}


	@RequestMapping(value = "/getLoanBookingList", method = RequestMethod.POST)
	public ResponseEntity<BookingListResponse> getLoanBookingList(@RequestBody LoginDetails loginDetails) {

		BookingListResponse response = new BookingListResponse();

		try {

			Login login =loginRepository.findByUserNameAndPassWordAndStatus(loginDetails.getUserName(), loginDetails.getPassWord(),"Active");
			int flag =0;
			if(login!=null)
			{
				List<Booking> bookingList=new ArrayList<Booking>();
				List<UserAssignedProject> userLoginProjectList=userprojectListRepository.findByEmployeeId(login.getEmployeeId());
				Query query = new Query();
				for(int i=0;i<userLoginProjectList.size();i++)
				{
					query = new Query();
					List<Booking> bookingList2 = mongoTemplate.find(query.addCriteria(Criteria.where("bookingstatus").ne("Cancel").and("projectId").is(userLoginProjectList.get(i).getProjectId())), Booking.class);
					List<CustomerLoanDetails> loanList1 = customerLoandetailsRepository.findAll();

					List<Booking> bookingList1 = new ArrayList<Booking>();
					Booking bookingobj=new Booking();
					flag =0;
					for(int j=0;j<bookingList2.size();j++)
					{
						for(int k=0;k<loanList1.size();k++)
						{
							if(bookingList2.get(j).getBookingId().equalsIgnoreCase(loanList1.get(k).getBookingId()))
							{			flag =1;
							break;
							}
							else
							{
								flag =0;
							}
						}
						if(flag ==0)
						{
							bookingobj=new Booking();
							bookingobj.setBookingId(bookingList2.get(j).getBookingId());
							bookingobj.setEnquiryId(bookingList2.get(j).getEnquiryId());
							bookingobj.setBookingfirstname(bookingList2.get(j).getBookingfirstname());
							bookingobj.setBookingmiddlename(bookingList2.get(j).getBookingmiddlename());
							bookingobj.setBookinglastname(bookingList2.get(j).getBookinglastname());
							bookingobj.setBookingaddress(bookingList2.get(j).getBookingaddress());
							bookingobj.setCountryId(bookingList2.get(j).getCountryId());
							bookingobj.setStateId(bookingList2.get(j).getStateId());
							bookingobj.setCityId(bookingList2.get(j).getCityId());
							bookingobj.setLocationareaId(bookingList2.get(j).getLocationareaId());
							bookingobj.setBookingPincode(bookingList2.get(j).getBookingPincode());
							bookingobj.setBookingEmail(bookingList2.get(j).getBookingEmail());
							bookingobj.setBookingmobileNumber1(bookingList2.get(j).getBookingmobileNumber1());
							bookingobj.setBookingmobileNumber2(bookingList2.get(j).getBookingmobileNumber2());
							bookingobj.setBookingOccupation(bookingList2.get(j).getBookingOccupation());
							bookingobj.setPurposeOfFlat(bookingList2.get(j).getPurposeOfFlat());
							bookingobj.setFloorId(bookingList2.get(j).getFloorId());
							bookingobj.setProjectId(bookingList2.get(j).getProjectId());
							bookingobj.setBuildingId(bookingList2.get(j).getBuildingId());
							bookingobj.setWingId(bookingList2.get(j).getWingId());
							bookingobj.setFlatType(bookingList2.get(j).getFlatType());
							bookingobj.setFlatId(bookingList2.get(j).getFlatId());
							bookingobj.setFlatFacing(bookingList2.get(j).getFlatFacing());
							bookingobj.setFlatareainSqFt(bookingList2.get(j).getFlatareainSqFt());
							bookingobj.setFlatCostwithotfloorise(bookingList2.get(j).getFlatCostwithotfloorise());
							bookingobj.setFloorRise(bookingList2.get(j).getFloorRise());
							bookingobj.setFlatCost(bookingList2.get(j).getFlatCost());
							bookingobj.setFlatbasicCost(bookingList2.get(j).getFlatbasicCost());
							bookingobj.setParkingFloorId(bookingList2.get(j).getParkingFloorId());
							bookingobj.setParkingZoneId(bookingList2.get(j).getParkingZoneId());
							bookingobj.setAgentId(bookingList2.get(j).getAgentId());
							bookingobj.setInfrastructureCharge(bookingList2.get(j).getInfrastructureCharge());
							bookingobj.setAggreementValue1(bookingList2.get(j).getAggreementValue1());
							bookingobj.setHandlingCharges(bookingList2.get(j).getHandlingCharges());
							bookingobj.setStampDuty1(bookingList2.get(j).getStampDuty1());
							bookingobj.setStampDutyPer(bookingList2.get(j).getStampDutyPer());
							bookingobj.setRegistrationPer(bookingList2.get(j).getRegistrationPer());
							bookingobj.setRegistrationCost1(bookingList2.get(j).getRegistrationCost1());
							bookingobj.setGstCost(bookingList2.get(j).getGstCost());
							bookingobj.setGstAmount1(bookingList2.get(j).getGstAmount1());
							bookingobj.setGrandTotal1(bookingList2.get(j).getGrandTotal1());
							bookingobj.setTds(bookingList2.get(j).getTds());
							bookingobj.setBookingstatus(bookingList2.get(j).getBookingstatus());
							bookingobj.setUserName(bookingList2.get(j).getUserName());

							try {
								//System.out.println("bookingList2.get(j).Booking() = "+bookingList2.get(j).getBookingId());
								//System.out.println("bookingList2.get(j).getProjectId() = "+bookingList2.get(j).getProjectId());
								query = new Query();
								Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(bookingList2.get(j).getProjectId())),Project.class);
								bookingobj.setProjectName(projectDetails.getProjectName());
								query = new Query();
								ProjectBuilding projectbuildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(bookingList2.get(j).getBuildingId())),ProjectBuilding.class);
								bookingobj.setBuildingName(projectbuildingDetails.getBuildingName());
								query = new Query();
								ProjectWing projectwingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(bookingList2.get(j).getWingId())),ProjectWing.class);
								bookingobj.setWingName(projectwingDetails.getWingName());
								query = new Query();
								try {
									Flat flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(bookingList2.get(j).getFlatId())),Flat.class);
									bookingobj.setFlatNumber(flatDetails.getFlatNumber());
								}catch (Exception e) {
									bookingobj.setFlatNumber("");
									// TODO: handle exception
								}
								bookingobj.setBookingType("Booking");

							}catch (Exception e) {
								e.printStackTrace();
								// TODO: handle exception
							}
							bookingList1.add(bookingobj);
						}
					}
					bookingList.addAll(bookingList1);
				}

				response.setBookingList(bookingList);
				response.setResponse("Booking List");
				response.setStatus(1);
				return new ResponseEntity<BookingListResponse>(response,HttpStatus.OK);
			}
			else
			{
				response.setResponse("Login fail");
				response.setStatus(0);
				return new ResponseEntity<BookingListResponse>(response,HttpStatus.OK);
			}

		} catch (Exception ex) {
			ex.printStackTrace();

			response.setResponse("Login fail");
			response.setStatus(0);
			return new ResponseEntity<BookingListResponse>(response,HttpStatus.OK);
		}
	}


	@RequestMapping(value = "/getCustomerLoanList", method = RequestMethod.POST)
	public ResponseEntity<CustomerLoanDetailsResponse> getCustomerLoanList(@RequestBody LoginDetails loginDetails) {

		CustomerLoanDetailsResponse response = new CustomerLoanDetailsResponse();
		try {
			Login login =loginRepository.findByUserNameAndPassWordAndStatus(loginDetails.getUserName(), loginDetails.getPassWord(),"Active");
			if(login!=null)
			{
				List<UserAssignedProject> userLoginProjectList=userprojectListRepository.findByEmployeeId(login.getEmployeeId());
				Query query = new Query();
				List<CustomerLoanDetails> loanList1 = customerLoandetailsRepository.findAll();

				List<CustomerLoanListResponse> loanList=new ArrayList<CustomerLoanListResponse>();
				CustomerLoanListResponse customerloanDetails=new CustomerLoanListResponse();
				for(int k=0;k<userLoginProjectList.size();k++)
				{
					query = new Query();
					List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("bookingstatus").ne("Cancel").and("projectId").is(userLoginProjectList.get(k).getProjectId())), Booking.class);
					customerloanDetails=new CustomerLoanListResponse();
					for(int i=0;i<bookingList.size();i++)
					{
						for(int j=0;j<loanList1.size();j++)
						{
							try {
								if(bookingList.get(i).getBookingId().equals(loanList1.get(j).getBookingId()))
								{ 
									customerloanDetails=new CustomerLoanListResponse();
									customerloanDetails.setCustomerloanId(loanList1.get(j).getCustomerloanId());
									customerloanDetails.setBookingId(loanList1.get(j).getBookingId());
									customerloanDetails.setCustomerName(loanList1.get(j).getCustomerName());
									customerloanDetails.setTotalAmount(loanList1.get(j).getTotalAmount());
									customerloanDetails.setLoanAmount(loanList1.get(j).getLoanAmount());
									customerloanDetails.setRemainingAmount(loanList1.get(j).getRemainingAmount());
									customerloanDetails.setBankName(loanList1.get(j).getBankName());
									customerloanDetails.setFileNumber(loanList1.get(j).getFileNumber());
									customerloanDetails.setBankPersonName(loanList1.get(j).getBankPersonName());
									customerloanDetails.setBankPersonEmailId(loanList1.get(j).getBankPersonEmailId());
									customerloanDetails.setBankPersonMobNo(loanList1.get(j).getBankPersonMobNo());
									customerloanDetails.setAgainstAggreement(loanList1.get(j).getAgainstAggreement());
									customerloanDetails.setAgainstGST(loanList1.get(j).getAgainstGST());
									customerloanDetails.setAgainstRegistration(loanList1.get(j).getAgainstRegistration());
									customerloanDetails.setAgainstStampDuty(loanList1.get(j).getAgainstStampDuty());

									customerloanDetails.setTotalAmount1(loanList1.get(j).getTotalAmount()+"");
									customerloanDetails.setLoanAmount1(loanList1.get(j).getLoanAmount()+"");
									customerloanDetails.setRemainingAmount1(loanList1.get(j).getRemainingAmount()+"");
									try {
										if(loanList1.get(j).getAgainstAggreement()==null)
										{
											customerloanDetails.setAgainstAggreement1("");
										}
										else
										{
											customerloanDetails.setAgainstAggreement1(loanList1.get(j).getAgainstAggreement()+"");
										}
									}catch (Exception e) {
										e.printStackTrace();
										// TODO: handle exception
									}

									try {
										if(loanList1.get(j).getAgainstGST()==null)
										{
											customerloanDetails.setAgainstGST1("");
										}
										else
										{
											customerloanDetails.setAgainstGST1(loanList1.get(j).getAgainstGST()+"");
										}
									}catch (Exception e) {
										e.printStackTrace();
										// TODO: handle exception
									}


									try {
										if(loanList1.get(j).getAgainstRegistration()==null)
										{
											customerloanDetails.setAgainstRegistration1("");
										}
										else
										{
											customerloanDetails.setAgainstRegistration1(loanList1.get(j).getAgainstRegistration()+"");
										}
									}catch (Exception e) {
										e.printStackTrace();
										// TODO: handle exception
									}
									try {
										if(loanList1.get(j).getAgainstStampDuty()==null)
										{
											customerloanDetails.setAgainstStampDuty1("");
										}
										else
										{
											customerloanDetails.setAgainstStampDuty1(loanList1.get(j).getAgainstStampDuty()+"");
										}
									}catch (Exception e) {
										e.printStackTrace();
										// TODO: handle exception
									}
									double ownAmount=loanList1.get(j).getTotalAmount()-loanList1.get(j).getLoanAmount();
									customerloanDetails.setAggreementAmount(bookingList.get(i).getAggreementValue1()+"");
									customerloanDetails.setStampDutyAmount(bookingList.get(i).getStampDuty1()+"");
									customerloanDetails.setRegistrationAmount(bookingList.get(i).getRegistrationCost1()+"");
									customerloanDetails.setGstAmount(bookingList.get(i).getGstCost()+"");
									customerloanDetails.setOwnContributionAmount(""+ownAmount);
									loanList.add(customerloanDetails);
								}
							}catch (Exception e) {
								e.printStackTrace();
								// TODO: handle exception
							}
						}

					}

				}
				response.setCustomerList(loanList);
				response.setResponse("Customer Loan List");
				response.setStatus(1);
				return new ResponseEntity<CustomerLoanDetailsResponse>(response,HttpStatus.OK);
			}
			else
			{
				response.setResponse("Login fail");
				response.setStatus(0);
				return new ResponseEntity<CustomerLoanDetailsResponse>(response,HttpStatus.OK);

			}
		}catch (Exception e) {
			e.printStackTrace();
			response.setResponse("Login fail");
			response.setStatus(0);
			return new ResponseEntity<CustomerLoanDetailsResponse>(response,HttpStatus.OK);
			// TODO: handle exception
		}
	}


	@RequestMapping(value = "/addCustomerLoanDetails", method = RequestMethod.POST)
	public ResponseEntity<CommanResponse> addCustomerLoanDetails(@RequestBody CustomerLoanDetails loan) {
System.out.println("INNNNN");
		CommanResponse response = new CommanResponse();

		try {

			try
			{
				if(loan.getCustomerloanId().equalsIgnoreCase(""))
				{
					loan = new CustomerLoanDetails(LoanCode(),loan.getBookingId(),loan.getCustomerName(),
							loan.getTotalAmount(),loan.getLoanAmount(),loan.getRemainingAmount(),
							loan.getBankName(),loan.getFileNumber(),loan.getBankPersonName(),loan.getBankPersonMobNo(),loan.getBankPersonEmailId(),
							loan.getAgainstAggreement(), loan.getAgainstStampDuty(), loan.getAgainstRegistration(), loan.getAgainstGST());
					customerLoandetailsRepository.save(loan);
				}
				else
				{
					loan = new CustomerLoanDetails(loan.getCustomerloanId(),loan.getBookingId(),loan.getCustomerName(),
							loan.getTotalAmount(),loan.getLoanAmount(),loan.getRemainingAmount(),
							loan.getBankName(),loan.getFileNumber(),loan.getBankPersonName(),loan.getBankPersonMobNo(),loan.getBankPersonEmailId(),
							loan.getAgainstAggreement(), loan.getAgainstStampDuty(), loan.getAgainstRegistration(), loan.getAgainstGST());
					customerLoandetailsRepository.save(loan);

				}
				// model.addAttribute("bankStatus","Success");
			}
			catch(Exception de)
			{
				//model.addAttribute("bankStatus","Fail");
			}


			response.setResponse("Customer Loan successfully added");
			response.setStatus(1);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			response.setResponse("Customer Loan added fail");
			response.setStatus(0);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
		}
	}


}
