package com.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.ChartofAccount;
import com.realestate.bean.CompanyBanks;
import com.realestate.bean.SubChartofAccount;
import com.realestate.repository.CompanyBanksRepository;
import com.realestate.response.CompanyBanksResponse;
import com.realestate.response.LoginDetails;

@Controller
@RestController
@RequestMapping("company")
public class CompanyAccountController {

	@Autowired
	CompanyBanksRepository companyBanksRepository;


	@RequestMapping(value = "/getCompanyAccountList", method = RequestMethod.POST)
	public ResponseEntity<CompanyBanksResponse> getCompanyAccountList(@RequestBody LoginDetails loginDetails) {

		CompanyBanksResponse response = new CompanyBanksResponse();
		
		try {
			List<CompanyBanks> companyBanksList=companyBanksRepository.findAll();
			response.setCompanyBanksList(companyBanksList);
			response.setResponse("Company Bank Account List");
			response.setStatus(1);
			
			return new ResponseEntity<CompanyBanksResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			response.setResponse("Login fail");
			response.setStatus(0);
			return new ResponseEntity<CompanyBanksResponse>(response,HttpStatus.OK);
		}
	}
	
}
