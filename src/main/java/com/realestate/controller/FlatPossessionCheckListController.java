package com.realestate.controller;
import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;    
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.Agent;
import com.realestate.bean.Booking;
import com.realestate.bean.City;
import com.realestate.bean.Country;
import com.realestate.bean.CustomerPaymentDetails;
import com.realestate.bean.Enquiry;
import com.realestate.bean.Flat;
import com.realestate.bean.FlatPossessionCheckList;
import com.realestate.bean.Floor;
import com.realestate.bean.LocationArea;
import com.realestate.bean.ParkingZone;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.State;
import com.realestate.repository.*;
import com.realestate.response.BookingIdResponse;
import com.realestate.response.BookingResponse;
import com.realestate.response.CommanResponse;
import com.realestate.response.FlatPossessionCheckListResponse;
import com.realestate.services.SmsServices;

@Controller
@RestController
@RequestMapping("flatpossession")
public class FlatPossessionCheckListController {

	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	EnquiryRepository enquiryRepository;
	@Autowired
	FlatPossessionCheckListRepository flatpossessionchecklistRepository;


	String possessionId;
	private String PossessionId()
	{
		long Count = flatpossessionchecklistRepository.count();

		if(Count<10)
		{
			possessionId = "FP000"+(Count+1);
		}
		else if((Count>=10) && (Count<100))
		{
			possessionId = "FP00"+(Count+1);
		}
		else if((Count>=100) && (Count<1000))
		{
			possessionId = "FP0"+(Count+1);
		}
		else
		{
			possessionId = "FP"+(Count+1);
		}

		return possessionId;
	}


	@RequestMapping(value = "/getFlatPossessionCheckList", method = RequestMethod.POST)
	public ResponseEntity<FlatPossessionCheckListResponse> getFlatPossessionCheckList(@RequestBody BookingIdResponse bookingidResponse) {

		FlatPossessionCheckListResponse response = new FlatPossessionCheckListResponse();

		try {

			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); 

			Query query = new Query();
			List<FlatPossessionCheckList> flatpossessionchecklistdetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingidResponse.getBookingId())), FlatPossessionCheckList.class);
			FlatPossessionCheckList flatPossessionCheckList=new FlatPossessionCheckList();
			if(flatpossessionchecklistdetails.size()==0)
			{
				flatPossessionCheckList.setPossessionId("New");
				flatPossessionCheckList.setBookingId(bookingidResponse.getBookingId());

				flatPossessionCheckList.setAllWallCracksFilledUpNeatly("Pending");
				flatPossessionCheckList.setAllWaterPatchesremovedNeatly("Pending");
				flatPossessionCheckList.setAllFanHooksInPosition("Pending");
				flatPossessionCheckList.setGoodOverallFinishingOfWall("Pending");
				flatPossessionCheckList.setFlooringInUniformLevel("Pending");
				flatPossessionCheckList.setDadoTilesAreaProperlyFinishedandJointFilled("Pending");
				flatPossessionCheckList.setDadoTilesAreProperLineAndInRightAngle("Pending");
				flatPossessionCheckList.setNoCrackedTilesOnTheWall("Pending");
				flatPossessionCheckList.setProperSlopehasbeenToBathFlooring("Pending");
				flatPossessionCheckList.setProperSlopeGivenToTerraceOutlet("Pending");
				flatPossessionCheckList.setKitchenPlatformProperlyFittedallJointsFilled("Pending");
				flatPossessionCheckList.setNoLeakagesIntheSinkOftheKitchen("Pending");
				flatPossessionCheckList.setWallBelowKitchenOttaProperlyFinished("Pending");
				flatPossessionCheckList.setAllDoorsAndWindowOpenandShutProperly("Pending");
				flatPossessionCheckList.setAllLocksTowerBoltsStoppersHingesOperateWell("Pending");
				flatPossessionCheckList.setAllGlassToWindowProperlyFixedAndOperationWellAndCleaned("Pending");
				flatPossessionCheckList.setNamePlateFixedOnMainDoor("Pending");
				flatPossessionCheckList.setMsFoldingDoorInTerraceIsOperatingProperly("Pending");
				flatPossessionCheckList.setEyePieceFittingTotheMainDoor("Pending");
				flatPossessionCheckList.setAllDoorFittingAndMortiseLocksareProperlyFixed("Pending");
				flatPossessionCheckList.setAllDoorMagneticCatcherareProperlyFixed("Pending");
				flatPossessionCheckList.setAllFittingareFreePaintSpots("Pending");
				flatPossessionCheckList.setCpFittingFixedProperly("Pending");
				flatPossessionCheckList.setNoLeakagesInCPFittings("Pending");
				flatPossessionCheckList.setHotAndColdMixerOparatesProperly("Pending");
				flatPossessionCheckList.setFlushTankcanEasilyOprate("Pending");
				flatPossessionCheckList.setEwcCoverProperlyFitted("Pending");
				flatPossessionCheckList.setGapBetwwenWallTilesandWashBasinFilled("Pending");
				flatPossessionCheckList.setAllSanitaryFittingareNeatlyCleaned("Pending");
				flatPossessionCheckList.setWashbasinIsProperlyFittedAndDoesNotMove("Pending");
				flatPossessionCheckList.setNoSanitaryFittingsareCracked("Pending");
				flatPossessionCheckList.setBothTapsInKitchenOperative("Pending");
				flatPossessionCheckList.setElectricMeterConnectedTotheFlat("Pending");
				flatPossessionCheckList.setAllSwitchesOperatedProperly("Pending");
				flatPossessionCheckList.setMcbAnddcbProperlyFixed("Pending");
				flatPossessionCheckList.setColorOfAllSwitchesIsUniform("Pending");
				flatPossessionCheckList.setAllswitchboardsareProperlyScrewed("Pending");
				flatPossessionCheckList.setPaintingOfallWallsAndCeilingsIsProperlyDone("Pending");
				flatPossessionCheckList.setOilPaintingOfWindowGrillsAndTerraceRailingDoneProperly("Pending");
				flatPossessionCheckList.setCreationDate(formatter.format(date));
				flatPossessionCheckList.setUpdateDate("");
				flatpossessionchecklistdetails.add(flatPossessionCheckList);
				response.setFlatpossessionchecklistdetails(flatpossessionchecklistdetails);
			}
			else
			{

				response.setFlatpossessionchecklistdetails(flatpossessionchecklistdetails);
			}
			response.setResponse("Flat Possession Check List Details");
			response.setStatus(1);
			return new ResponseEntity<FlatPossessionCheckListResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			ex.printStackTrace();
			response.setResponse("Login fail");
			response.setStatus(0);
			return new ResponseEntity<FlatPossessionCheckListResponse>(response,HttpStatus.OK);
		}
	}



	@RequestMapping(value = "/addFlatPossession", method = RequestMethod.POST)
	public ResponseEntity<CommanResponse> addFlatPossession(@RequestBody FlatPossessionCheckList flatpossessionchecklist) {

		CommanResponse response = new CommanResponse();

		try {
			String possessionId="";

			if(flatpossessionchecklist.getPossessionId().equalsIgnoreCase("New"))
			{
				possessionId=PossessionId();
			}
			else
			{
				possessionId=flatpossessionchecklist.getPossessionId();
			}
			flatpossessionchecklist.setPossessionId(possessionId);

			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); 
			System.out.println("Id = "+flatpossessionchecklist.getPossessionId());
			System.out.println("getBookingId = "+flatpossessionchecklist.getBookingId());
			System.out.println("getAllWallCracksFilledUpNeatly = "+flatpossessionchecklist.getAllWallCracksFilledUpNeatly());
			System.out.println("getAllWaterPatchesremovedNeatly = "+flatpossessionchecklist.getAllWaterPatchesremovedNeatly());


			flatpossessionchecklist = new FlatPossessionCheckList(flatpossessionchecklist.getPossessionId(),flatpossessionchecklist.getBookingId(),
					flatpossessionchecklist.getAllWallCracksFilledUpNeatly(),flatpossessionchecklist.getAllWaterPatchesremovedNeatly(),flatpossessionchecklist.getAllFanHooksInPosition(),
					flatpossessionchecklist.getGoodOverallFinishingOfWall(),flatpossessionchecklist.getFlooringInUniformLevel(),flatpossessionchecklist.getDadoTilesAreaProperlyFinishedandJointFilled(),
					flatpossessionchecklist.getDadoTilesAreProperLineAndInRightAngle(),flatpossessionchecklist.getNoCrackedTilesOnTheWall(),flatpossessionchecklist.getProperSlopehasbeenToBathFlooring(),
					flatpossessionchecklist.getProperSlopeGivenToTerraceOutlet(),flatpossessionchecklist.getKitchenPlatformProperlyFittedallJointsFilled(),
					flatpossessionchecklist.getNoLeakagesIntheSinkOftheKitchen(),flatpossessionchecklist.getWallBelowKitchenOttaProperlyFinished(),flatpossessionchecklist.getAllDoorsAndWindowOpenandShutProperly(),
					flatpossessionchecklist.getAllLocksTowerBoltsStoppersHingesOperateWell(),flatpossessionchecklist.getAllGlassToWindowProperlyFixedAndOperationWellAndCleaned(),
					flatpossessionchecklist.getNamePlateFixedOnMainDoor(),flatpossessionchecklist.getMsFoldingDoorInTerraceIsOperatingProperly(),flatpossessionchecklist.getEyePieceFittingTotheMainDoor(),
					flatpossessionchecklist.getAllDoorFittingAndMortiseLocksareProperlyFixed(),flatpossessionchecklist.getAllDoorMagneticCatcherareProperlyFixed(),
					flatpossessionchecklist.getAllFittingareFreePaintSpots(),flatpossessionchecklist.getCpFittingFixedProperly(),flatpossessionchecklist.getNoLeakagesInCPFittings(),flatpossessionchecklist.getHotAndColdMixerOparatesProperly(),
					flatpossessionchecklist.getFlushTankcanEasilyOprate(),flatpossessionchecklist.getEwcCoverProperlyFitted(),flatpossessionchecklist.getGapBetwwenWallTilesandWashBasinFilled(),flatpossessionchecklist.getAllSanitaryFittingareNeatlyCleaned(),
					flatpossessionchecklist.getWashbasinIsProperlyFittedAndDoesNotMove(),flatpossessionchecklist.getNoSanitaryFittingsareCracked(),flatpossessionchecklist.getBothTapsInKitchenOperative(),
					flatpossessionchecklist.getElectricMeterConnectedTotheFlat(),flatpossessionchecklist.getAllSwitchesOperatedProperly(),flatpossessionchecklist.getMcbAnddcbProperlyFixed(),
					flatpossessionchecklist.getColorOfAllSwitchesIsUniform(),flatpossessionchecklist.getAllswitchboardsareProperlyScrewed(),flatpossessionchecklist.getPaintingOfallWallsAndCeilingsIsProperlyDone(),
					flatpossessionchecklist.getOilPaintingOfWindowGrillsAndTerraceRailingDoneProperly(),
					flatpossessionchecklist.getCreationDate(),""+formatter.format(date),flatpossessionchecklist.getUserId());


			flatpossessionchecklistRepository.save(flatpossessionchecklist);
			response.setResponse("Flat Possession successfully added");
			response.setStatus(1);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);


		} catch (Exception ex) {
			response.setResponse("Flat Possession added fail");
			response.setStatus(0);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
		}
	}

}
