package com.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.Item;
import com.realestate.bean.Login;
import com.realestate.bean.SupplierType;
import com.realestate.repository.LoginRepository;
import com.realestate.repository.ItemRepository;
import com.realestate.response.ItemResponse;
import com.realestate.response.LoginDetails;
import com.realestate.response.SupplierTypeResponse;

@Controller
@RestController
@RequestMapping("item")
public class ItemController {


	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	LoginRepository loginRepository;
	@Autowired
	ItemRepository itemRepository;

	@RequestMapping(value = "/getItemList", method = RequestMethod.POST)
	public ResponseEntity<ItemResponse> getItemList(@RequestBody LoginDetails loginDetails) {

		ItemResponse response = new ItemResponse();

		try {

			Login login =loginRepository.findByUserNameAndPassWordAndStatus(loginDetails.getUserName(), loginDetails.getPassWord(),"Active");

			if(login!=null)
			{
				List<Item> itemList=itemRepository.findAll();
				response.setItemList(itemList);
				response.setResponse("Item List");
				response.setStatus(1);
				return new ResponseEntity<ItemResponse>(response,HttpStatus.OK);
			}
			else
			{
				response.setResponse("Login fail");
				response.setStatus(0);

				return new ResponseEntity<ItemResponse>(response,HttpStatus.OK);
			}
		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<ItemResponse>(response,HttpStatus.OK);
		}
	}

}
