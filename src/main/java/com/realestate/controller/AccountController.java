package com.realestate.controller;

import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.realestate.bean.CustomerReceiptForm;
import com.realestate.bean.Project;
import com.realestate.bean.SubChartofAccount;
import com.realestate.bean.BankTransactionHistoryDetails;
import com.realestate.bean.Booking;
import com.realestate.bean.ChartofAccount;
import com.realestate.bean.Company;
import com.realestate.bean.CompanyAccountPaymentDetails;
import com.realestate.bean.CompanyBanks;
import com.realestate.bean.CustomerOtherPaymentDetails;
import com.realestate.bean.CustomerPaymentDetails;
import com.realestate.bean.CustomerPaymentStatusHistory;
import com.realestate.repository.*;
import com.realestate.bean.Agent;
import com.realestate.response.ChartofAccountResponse;
import com.realestate.response.CommanResponse;
import com.realestate.response.CustomerReceiptFormResponse;
import com.realestate.response.LoginDetails;
import com.realestate.response.UpdatePaymentStatusResponse;

@Controller
@RestController
@RequestMapping("account")
public class AccountController {


	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	BookingRepository bookingRepository;
	@Autowired
	CustomerReceiptFormRepository customerReceiptFormRepository;
	@Autowired
	CustomerPaymentDetailsRepository customerPaymentDetailsRepository;
	@Autowired
	ChartofAccountRepository chartofaccountRepository;
	@Autowired
	SubChartofAccountRepository subchartofAccountRepository;
	@Autowired
	CustomerOtherPaymentDetailsRepository customerOtherPaymentDetailsRepository;
	@Autowired
	CompanyAccountPaymentDetailsRepository companyAccountPaymentDetailsRepository;
	@Autowired
	CustomerPaymentStatusHistoryRepository customerPaymentStatusHistoryRepository;
	@Autowired
	BankTransactionHistoryDetailsRepository banktransactionhistorydetailsRepository;

	//for paymentId Generation
	String  paymentHistoryId="";
	public String GeneratePaymentHistoryId()
	{
		long cnt = customerPaymentStatusHistoryRepository.count();

		if(cnt<10)
		{
			paymentHistoryId = "PHI0000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			paymentHistoryId = "PHI000"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			paymentHistoryId = "PHI00"+(cnt+1);
		}
		else if(cnt>=1000 && cnt<10000)
		{
			paymentHistoryId = "PHI0"+(cnt+1);
		}
		else
		{
			paymentHistoryId = "PHI"+(cnt+1);
		}

		return paymentHistoryId;
	}

	String paymentId="";
	public String GeneratePaymentId()
	{

		long cnt = companyAccountPaymentDetailsRepository.count();

		if(cnt<10)
		{
			paymentId = "P000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			paymentId = "P00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			paymentId = "P0"+(cnt+1);
		}
		else
		{
			paymentId = "P"+(cnt+1);
		}

		return paymentId;
	}

	@RequestMapping(value = "/getCustomerPaymentList", method = RequestMethod.POST)
	public ResponseEntity<CustomerReceiptFormResponse> getCustomerPaymentList(@RequestBody LoginDetails loginDetails) {

		CustomerReceiptFormResponse response = new CustomerReceiptFormResponse();

		try {

			Query query = new Query();
			List<CustomerReceiptForm> customerReceiptList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Not Deposited")), CustomerReceiptForm.class);

			List<Booking> bookingList = bookingRepository.findAll();

			for(int i=0;i<customerReceiptList.size();i++)
			{
				for(int j=0;j<bookingList.size();j++)
				{
					try {
						if(customerReceiptList.get(i).getBookingId().equals(bookingList.get(j).getBookingId()))
						{
							customerReceiptList.get(i).setCustomerName(bookingList.get(j).getBookingfirstname());
							customerReceiptList.get(i).setProjectId(bookingList.get(j).getProjectId());
						}
					}catch (Exception e) {
						customerReceiptList.get(i).setCustomerName("");
						customerReceiptList.get(i).setProjectId("");
						// TODO: handle exception
					}
				}
			}
			response.setCustomerReceiptList(customerReceiptList);
			response.setResponse("customer Receipt List");
			response.setStatus(1);

			return new ResponseEntity<CustomerReceiptFormResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			response.setResponse("Login fail");
			response.setStatus(0);
			return new ResponseEntity<CustomerReceiptFormResponse>(response,HttpStatus.OK);
		}
	}


	@RequestMapping(value = "/getChartofAccountList", method = RequestMethod.POST)
	public ResponseEntity<ChartofAccountResponse> getChartofAccountList(@RequestBody LoginDetails loginDetails) {

		ChartofAccountResponse response = new ChartofAccountResponse();

		try {

			List<ChartofAccount> chartofaccountList = chartofaccountRepository.findAll();
			List<SubChartofAccount> subchartofaccountList=subchartofAccountRepository.findAll();
			response.setChartofaccountList(chartofaccountList);
			response.setSubchartofaccountList(subchartofaccountList);
			response.setResponse("Chart of Account List");
			response.setStatus(1);

			return new ResponseEntity<ChartofAccountResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			response.setResponse("Login fail");
			response.setStatus(0);
			return new ResponseEntity<ChartofAccountResponse>(response,HttpStatus.OK);
		}
	}


	@RequestMapping(value = "/updatePaymentStatus", method = RequestMethod.POST)
	public ResponseEntity<CommanResponse> updatePaymentStatus(@RequestBody UpdatePaymentStatusResponse updatepaymentStatusResponse) {

		CommanResponse response = new CommanResponse();
		String transactionStatus="None";
		if(updatepaymentStatusResponse.getPaymentStatus().equals("Cleared"))
		{
			transactionStatus="Credited";
		}
		else
		{
			transactionStatus="None";
		}

		try {

			Query query = new Query();
			try
			{
				if(updatepaymentStatusResponse.getPaymentStatus().equals("Cleared"))
				{
					BankTransactionHistoryDetails banktransactionhistorydetails=new BankTransactionHistoryDetails();

					banktransactionhistorydetails.setCompanyBankId(updatepaymentStatusResponse.getCompanyBankId());
					banktransactionhistorydetails.setAmount(updatepaymentStatusResponse.getPaymentAmount());
					banktransactionhistorydetails.setPaymentType("Booking");
					banktransactionhistorydetails.setPaymentId(updatepaymentStatusResponse.getBookingId());
					banktransactionhistorydetails.setCreditOrDebiteType("Credited");
					banktransactionhistorydetails.setCreationDate(new Date());
					banktransactionhistorydetails.setUserName(updatepaymentStatusResponse.getUserId());
					banktransactionhistorydetailsRepository.save(banktransactionhistorydetails);
				}
			}
			catch (Exception e) {
				// TODO: handle exception
			}

			if(updatepaymentStatusResponse.getPaymentStatus().equals("Cleared"))
			{
				query = new Query();
				CustomerReceiptForm customerReceiptForm = mongoTemplate.findOne(query.addCriteria(Criteria.where("receiptId").is(updatepaymentStatusResponse.getReceiptId())), CustomerReceiptForm.class);
				customerReceiptForm.setStatus("Deposited");

				customerReceiptFormRepository.save(customerReceiptForm);


				//save and update balance in company bank account
				try
				{
					query = new Query();
					List<CompanyAccountPaymentDetails> companyAccountPaymentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("companyBankId").is(updatepaymentStatusResponse.getCompanyBankId())), CompanyAccountPaymentDetails.class);

					if(companyAccountPaymentDetails.size()!=0)
					{
						//companyAccountPaymentDetails.get(0).setPaymentId(GeneratePaymentId());
						//companyAccountPaymentDetails.get(0).setCompanyBankId(companyBankId);

						long currentBalance = companyAccountPaymentDetails.get(0).getCurrentBalance();
						companyAccountPaymentDetails.get(0).setCurrentBalance((currentBalance+updatepaymentStatusResponse.getPaymentAmount().longValue()));

						companyAccountPaymentDetailsRepository.save(companyAccountPaymentDetails);
					}
					else
					{
						CompanyAccountPaymentDetails companyAccountPaymentDetails1 = new CompanyAccountPaymentDetails();

						companyAccountPaymentDetails1.setPaymentId(GeneratePaymentId());
						companyAccountPaymentDetails1.setCompanyBankId(updatepaymentStatusResponse.getCompanyBankId());
						companyAccountPaymentDetails1.setCurrentBalance(updatepaymentStatusResponse.getPaymentAmount().longValue());

						companyAccountPaymentDetailsRepository.save(companyAccountPaymentDetails1);
					}
				}
				catch(Exception e)
				{
				}

			}
			else
			{
				CustomerReceiptForm customerReceiptForm = mongoTemplate.findOne(query.addCriteria(Criteria.where("receiptId").is(updatepaymentStatusResponse.getReceiptId())), CustomerReceiptForm.class);
				customerReceiptForm.setStatus("Canceled");

				customerReceiptFormRepository.save(customerReceiptForm);

				CustomerPaymentDetails customerPaymentDetails = new CustomerPaymentDetails();

				CustomerOtherPaymentDetails customerOtherPaymentDetails = new CustomerOtherPaymentDetails();

				if(updatepaymentStatusResponse.getPaymentType().equals("Agreement") || updatepaymentStatusResponse.getPaymentType().equals("Stamp Duty") || updatepaymentStatusResponse.getPaymentType().equals("Registration") || updatepaymentStatusResponse.getPaymentType().equals("GST"))
				{
					query = new Query();
					customerPaymentDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("BookingId").is(updatepaymentStatusResponse.getBookingId())), CustomerPaymentDetails.class);  
				}

				if(updatepaymentStatusResponse.getPaymentType().equals("Maintainance") || updatepaymentStatusResponse.getPaymentType().equals("Handling Charge") || updatepaymentStatusResponse.getPaymentType().equals("Extra Charge"))
				{
					query = new Query();
					customerOtherPaymentDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("BookingId").is(updatepaymentStatusResponse.getBookingId())), CustomerOtherPaymentDetails.class);
				}


				// for customerPaymentDetails table update
				if(updatepaymentStatusResponse.getPaymentType().equals("Agreement"))
				{
					long originalAggAmt = customerPaymentDetails.getTotalpayAggreement().longValue();
					long currentAggAmt  = updatepaymentStatusResponse.getPaymentAmount().longValue();
					customerPaymentDetails.setTotalpayAggreement((double)originalAggAmt-currentAggAmt);

					customerPaymentDetailsRepository.save(customerPaymentDetails);
				}

				if(updatepaymentStatusResponse.getPaymentType().equals("Stamp Duty"))
				{
					long originalStmpDutyAmt = customerPaymentDetails.getTotalpayStampDuty().longValue();
					long currentStmpDutyAmt  = updatepaymentStatusResponse.getPaymentAmount().longValue();
					customerPaymentDetails.setTotalpayStampDuty((double)originalStmpDutyAmt-currentStmpDutyAmt);

					customerPaymentDetailsRepository.save(customerPaymentDetails);
				}

				if(updatepaymentStatusResponse.getPaymentType().equals("Registration"))
				{
					long originalRegiAmt = customerPaymentDetails.getTotalpayRegistration().longValue();
					long currentRegiAmt  = updatepaymentStatusResponse.getPaymentAmount().longValue();
					customerPaymentDetails.setTotalpayRegistration((double)originalRegiAmt-currentRegiAmt);

					customerPaymentDetailsRepository.save(customerPaymentDetails); 
				}

				if(updatepaymentStatusResponse.getPaymentType().equals("GST"))
				{
					long originalGSTAmt = customerPaymentDetails.getTotalpaygstAmount().longValue();
					long currentGSTAmt  = updatepaymentStatusResponse.getPaymentAmount().longValue();
					customerPaymentDetails.setTotalpaygstAmount((double)originalGSTAmt-currentGSTAmt);

					customerPaymentDetailsRepository.save(customerPaymentDetails);
				}

				//for CustomerOtherPaymentDetails table update
				if(updatepaymentStatusResponse.getPaymentType().equals("Maintainance"))
				{
					long originalMaintAmt = customerOtherPaymentDetails.getTotalPaidMaintainanceAmt();
					long currentMaintAmt  = updatepaymentStatusResponse.getPaymentAmount().longValue();
					customerOtherPaymentDetails.setTotalPaidMaintainanceAmt(originalMaintAmt-currentMaintAmt);

					customerOtherPaymentDetailsRepository.save(customerOtherPaymentDetails);
				}

				if(updatepaymentStatusResponse.getPaymentType().equals("Handling Charge"))
				{
					long originalHandAmt = customerOtherPaymentDetails.getTotalPaidHandlingCharges();
					long currentHandAmt  = updatepaymentStatusResponse.getPaymentAmount().longValue();
					customerOtherPaymentDetails.setTotalPaidHandlingCharges(originalHandAmt-currentHandAmt);

					customerOtherPaymentDetailsRepository.save(customerOtherPaymentDetails);
				}

				if(updatepaymentStatusResponse.getPaymentType().equals("Extra Charge"))
				{
					long originalExtraAmt = customerOtherPaymentDetails.getTotalPaidExtraCharges();
					long currentExtraAmt  = updatepaymentStatusResponse.getPaymentAmount().longValue();
					customerOtherPaymentDetails.setTotalPaidExtraCharges(originalExtraAmt-currentExtraAmt);

					customerOtherPaymentDetailsRepository.save(customerOtherPaymentDetails);
				}
			}

			//code to save entry in customer payment status history
			try
			{
				CustomerPaymentStatusHistory customerPaymentStatusHistory = new CustomerPaymentStatusHistory();
				customerPaymentStatusHistory.setPaymentHistoryId(GeneratePaymentHistoryId());
				customerPaymentStatusHistory.setReceiptId(updatepaymentStatusResponse.getReceiptId());
				customerPaymentStatusHistory.setBookingId(updatepaymentStatusResponse.getBookingId());
				customerPaymentStatusHistory.setPaymentAmount(updatepaymentStatusResponse.getPaymentAmount().longValue());
				customerPaymentStatusHistory.setTransactionStatus(transactionStatus);
				customerPaymentStatusHistory.setPaymentStatus(updatepaymentStatusResponse.getPaymentStatus());
				customerPaymentStatusHistory.setPaymentDate(updatepaymentStatusResponse.getPaymentDate());
				customerPaymentStatusHistory.setRemark(updatepaymentStatusResponse.getRemark());
				customerPaymentStatusHistory.setChartaccountId(updatepaymentStatusResponse.getChartaccountId());
				customerPaymentStatusHistory.setSubchartaccountId(updatepaymentStatusResponse.getSubchartaccountId());
				Date todateDate = new Date();
				customerPaymentStatusHistory.setCreationDate(todateDate);

				customerPaymentStatusHistoryRepository.save(customerPaymentStatusHistory);
			}
			catch(Exception e)
			{
			}


			response.setResponse("Payment add successfully");
			response.setStatus(1);

			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			response.setResponse("Login fail");
			response.setStatus(0);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
		}
	}

}
