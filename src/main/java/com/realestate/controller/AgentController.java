package com.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.Agent;
import com.realestate.repository.AgentRepository;
import com.realestate.response.LoginDetails;
import com.realestate.response.AgentResponse;


@Controller
@RestController
@RequestMapping("agent")
public class AgentController {

	@Autowired
	AgentRepository agentRepository;

	@RequestMapping(value = "/getAgentList", method = RequestMethod.POST)
	public ResponseEntity<AgentResponse> getAgentList(@RequestBody LoginDetails loginDetails) {

		AgentResponse response = new AgentResponse();
		
		try {
			List<Agent> agentList=agentRepository.findAll();
			response.setAgentList(agentList);
			response.setResponse("Agent List");
			response.setStatus(1);
			
			return new ResponseEntity<AgentResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			response.setResponse("Login fail");
			response.setStatus(0);
			return new ResponseEntity<AgentResponse>(response,HttpStatus.OK);
		}
	}
	
}
