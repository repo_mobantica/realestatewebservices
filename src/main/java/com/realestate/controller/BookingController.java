package com.realestate.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.Agent;
import com.realestate.bean.Booking;
import com.realestate.bean.BookingCancelForm;
import com.realestate.bean.City;
import com.realestate.bean.Country;
import com.realestate.bean.CustomerPaymentDetails;
import com.realestate.bean.Enquiry;
import com.realestate.bean.EnquiryFollowUp;
import com.realestate.bean.EnquirySource;
import com.realestate.bean.Flat;
import com.realestate.bean.Floor;
import com.realestate.bean.LocationArea;
import com.realestate.bean.Login;
import com.realestate.bean.Occupation;
import com.realestate.bean.ParkingZone;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.State;
import com.realestate.bean.SubEnquirySource;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.*;
import com.realestate.response.*;
import com.realestate.response.BookingIdResponse;
import com.realestate.response.BookingListResponse;
import com.realestate.response.BookingResponse;
import com.realestate.response.BookingViewResponse;
import com.realestate.response.CommanResponse;
import com.realestate.response.EnquiryResponseDetails;
import com.realestate.response.LoginDetails;
import com.realestate.services.SmsServices;

@Controller
@RestController
@RequestMapping("booking")
public class BookingController {


	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	EnquiryRepository enquiryRepository;
	@Autowired
	ParkingZoneRepository parkingZoneRepository;
	@Autowired
	EnquiryFollowUpRepository enquiryFollowUpRepository;
	@Autowired 
	BookingRepository bookingRepository;
	@Autowired
	CustomerPaymentDetailsRepository customerpaymentdetailsrepository;
	@Autowired
	FlatRepository flatRepository;
	@Autowired
	LoginRepository loginRepository;
	@Autowired
	UserAssignedProjectRepository userprojectListRepository;
	@Autowired
	BookingCancelFormRepository bookingcancelformRepository;
	@Autowired
	ProjectRepository projectRepository;


	int year = Calendar.getInstance().get(Calendar.YEAR);
	int month=Calendar.getInstance().get(Calendar.MONTH)+1;
	int y1,y2;

	String cancelCode;
	String code;
	int cancelCount;
	private String CancelCode()
	{
		List<BookingCancelForm> cancelCount1;

		cancelCount1 = bookingcancelformRepository.findAll();
		if(cancelCount1.size()!=0) {
			for(int i=0;i<cancelCount1.size();i++)
			{
				code=cancelCount1.get(i).getCancelId();
			}
			String[] part = code.split("(?<=\\D)(?=\\d)");
			cancelCount=Integer.parseInt(part[1]);
		}
		else {
			cancelCount=0;
		}
		if(cancelCount<10)
		{
			cancelCode = "BK000"+(cancelCount+1);
		}
		else if((cancelCount>=10) && (cancelCount<100))
		{
			cancelCode = "BK00"+(cancelCount+1);
		}
		else if((cancelCount>=100) && (cancelCount<1000))
		{
			cancelCode = "BK0"+(cancelCount+1);
		}
		else if(cancelCount>=1000)
		{
			cancelCode = "BK"+(cancelCount+1);
		}
		return cancelCode;
	}


	String enquiryCode;
	private String EnquiryCode()
	{
		long enquiryCount = enquiryRepository.count();
		if(month>3) 
		{
			y1=year;
			y2=year+1;
			if(enquiryCount<10)
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/000"+(enquiryCount+1);
			}
			else if((enquiryCount>=10) && (enquiryCount<100))
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/00"+(enquiryCount+1);
			}
			else if((enquiryCount>=100) && (enquiryCount<1000))
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/0"+(enquiryCount+1);
			}
			else
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/"+(enquiryCount+1);
			}
		}
		else
		{
			y1=year-1;
			y2=year;
			if(enquiryCount<10)
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/000"+(enquiryCount+1);
			}
			else if((enquiryCount>=10) && (enquiryCount<100))
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/00"+(enquiryCount+1);
			}
			else if((enquiryCount>=100) && (enquiryCount<1000))
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/0"+(enquiryCount+1);
			}
			else 
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/"+(enquiryCount+1);
			}
		}
		return enquiryCode;
	}


	String payAmountCode;
	private String customerpayAmountCode()
	{
		long Count = customerpaymentdetailsrepository.count();

		if(Count<10)
		{
			payAmountCode = "TP000"+(Count+1);
		}
		else if((Count>=10) && (Count<100))
		{
			payAmountCode = "TP00"+(Count+1);
		}
		else if((Count>=100) && (Count<1000))
		{
			payAmountCode = "TP0"+(Count+1);
		}
		else
		{
			payAmountCode = "TP"+(Count+1);
		}

		return payAmountCode;
	}


	public String GetBookingId( String projectId,String buildingId, String wingId,String flatId)
	{

		String bookingId="BKG/";
		String projectName1[]=null, buildingName1[]=null, wingName1[]=null;
		Query query =new Query();
		List<Flat> flatDetails = mongoTemplate.find(query.addCriteria(Criteria.where("flatId").is(flatId)), Flat.class);

		String flatNumber=flatDetails.get(0).getFlatNumber();
		String flatNumber1 = flatNumber.replaceAll("\\s", "");

		query = new Query();
		long bookingCount =0;
		bookingCount= mongoTemplate.count(query.addCriteria(Criteria.where("projectId").is(projectId).and("buildingId").is(buildingId).and("wingId").is(wingId).and("flatId").is(flatId)), Booking.class);

		query =new Query();
		List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(projectId)), Project.class);

		query =new Query();
		List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(buildingId)), ProjectBuilding.class);

		query =new Query();
		List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(wingId)), ProjectWing.class);

		String projectName=projectDetails.get(0).getProjectName();
		String buildingName=buildingDetails.get(0).getBuildingName();
		String wingName=wingDetails.get(0).getWingName();
		projectName1  = projectName.split(" ");
		buildingName1 = buildingName.split(" ");
		wingName1 	  = wingName.split(" ");

		if(projectName1.length==1)
		{
			for(int i=0; i<projectName1[0].length() && i<4;i++)
			{
				bookingId = bookingId + projectName1[0].charAt(i);
			}
		}
		else
		{
			for(int i=0; i<projectName1[0].length() && i<2;i++)
			{
				bookingId = bookingId + projectName1[0].charAt(i);
			}

			for(int i=0; i<projectName1[1].length() && i<2;i++)
			{
				bookingId = bookingId + projectName1[1].charAt(i);
			}
		}

		bookingId = bookingId +"/";

		if(buildingName1.length==1)
		{
			for(int i=0; i<buildingName1[0].length() && i<4;i++)
			{
				bookingId = bookingId + buildingName1[0].charAt(i);
			}
		}
		else
		{
			for(int i=0; i<buildingName1[0].length() && i<2;i++)
			{
				bookingId = bookingId + buildingName1[0].charAt(i);
			}

			for(int i=0; i<buildingName1[1].length() && i<2;i++)
			{
				bookingId = bookingId + buildingName1[1].charAt(i);
			}
		}

		bookingId = bookingId +"/";

		if(wingName1.length==1)
		{
			for(int i=0; i<wingName1[0].length() && i<4;i++)
			{
				bookingId = bookingId + wingName1[0].charAt(i);
			}
		}
		else
		{
			for(int i=0; i<wingName1[0].length() && i<2;i++)
			{
				bookingId = bookingId + wingName1[0].charAt(i);
			}

			for(int i=0; i<wingName1[1].length() && i<2;i++)
			{
				bookingId = bookingId + wingName1[1].charAt(i);
			}
		}

		bookingId = bookingId +"/"+flatNumber1+"/"+bookingCount;

		return bookingId;
	}

	@RequestMapping(value = "/addBooking", method = RequestMethod.POST)
	public ResponseEntity<CommanResponse> addBooking(@RequestBody BookingResponse booking) {

		CommanResponse response = new CommanResponse();

		try {

			Query query = new Query();
			Flat flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(booking.getFlatId())), Flat.class);

			if(flatDetails.getFlatstatus().equalsIgnoreCase("Booking Remaninig"))
			{
				String bookingId=GetBookingId(booking.getProjectId(),booking.getBuildingId(),booking.getWingId(),booking.getFlatId());
				String enquiryId="";

				if(booking.getEnquiryId().equalsIgnoreCase(""))
				{
					enquiryId=EnquiryCode();

					// for add enquiry
					try {

						query = new Query();
						Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(booking.getProjectId())), Project.class);

						Enquiry enquiry=new Enquiry();
						enquiry.setEnquiryId(enquiryId);
						enquiry.setEnqfirstName(booking.getBookingName());
						enquiry.setEnqmiddleName("");
						enquiry.setEnqlastName("");
						enquiry.setEnqEmail(booking.getBookingEmail());
						enquiry.setEnqmobileNumber1(booking.getBookingmobileNumber1());
						enquiry.setEnqmobileNumber2(booking.getBookingmobileNumber2());
						enquiry.setProjectName(projectDetails.getProjectName());

						enquiry.setEnqOccupation(booking.getBookingOccupation());

						enquiry.setFlatType(booking.getFlatType());
						enquiry.setFlatBudget(booking.getFlatBudget());
						enquiry.setEnquirysourceId(booking.getEnquirysourceId());
						enquiry.setSubsourceId(booking.getSubsourceId());
						enquiry.setFollowupDate("");
						enquiry.setEnqStatus("Booking completed");
						enquiry.setEnqRemark("Booking completed");
						enquiry.setCreationDate(new Date());
						enquiry.setUpdateDate(new Date());
						enquiry.getUserName();
						enquiryRepository.save(enquiry);	
					}
					catch(Exception ee)
					{}

				}
				else
				{
					enquiryId=booking.getEnquiryId();
					try
					{

						Enquiry enquiry;
						enquiry = mongoTemplate.findOne(Query.query(Criteria.where("enquiryId").is(booking.getEnquiryId())), Enquiry.class);
						enquiry.setEnqStatus("Booking Completed");
						enquiryRepository.save(enquiry);
					}
					catch(Exception ee)
					{

					}

				}

				try {

					CustomerPaymentDetails customerpaymentdetails=new CustomerPaymentDetails();
					customerpaymentdetails.setId(customerpayAmountCode());
					customerpaymentdetails.setBookingId(bookingId);
					customerpaymentdetails.setTotalpayAggreement(0.0);
					customerpaymentdetails.setTotalpayStampDuty(0.0);
					customerpaymentdetails.setTotalpayRegistration(0.0);
					customerpaymentdetails.setTotalpaygstAmount(0.0);
					customerpaymentdetails.setAmountStatus("pending");
					customerpaymentdetails.getCreationDate();
					customerpaymentdetails.getUpdateDate();
					customerpaymentdetails.getUserName();
					customerpaymentdetailsrepository.save(customerpaymentdetails);
				}
				catch(Exception es)
				{

				}

				try
				{
					Booking booking1=new Booking();
					booking1.setBookingId(bookingId);
					booking1.setEnquiryId(enquiryId);

					booking1.setBookingfirstname(booking.getBookingName());
					booking1.setBookingmiddlename("");
					booking1.setBookinglastname("");

					booking1.setBookingaddress(booking.getBookingaddress());
					booking1.setCountryId(booking.getCountryId());
					booking1.setStateId(booking.getStateId());
					booking1.setCityId(booking.getCityId());
					booking1.setLocationareaId(booking.getLocationareaId());
					booking1.setBookingPincode(booking.getBookingPincode());

					booking1.setBookingEmail(booking.getBookingEmail());
					booking1.setBookingmobileNumber1(booking.getBookingmobileNumber1());
					booking1.setBookingmobileNumber2(booking.getBookingmobileNumber2());
					booking1.setBookingOccupation(booking.getBookingOccupation());
					booking1.setPurposeOfFlat(booking.getPurposeOfFlat());

					booking1.setProjectId(booking.getProjectId());
					booking1.setBuildingId(booking.getBuildingId());
					booking1.setWingId(booking.getWingId());
					booking1.setFloorId(booking.getFloorId());
					booking1.setFlatType(booking.getFlatType());
					booking1.setFlatId(booking.getFlatId());
					booking1.setFlatFacing(booking.getFlatFacing());
					booking1.setFlatareainSqFt(booking.getFlatareainSqFt());
					booking1.setFlatCostwithotfloorise(booking.getFlatCostwithotfloorise());
					booking1.setFloorRise(booking.getFloorRise());
					booking1.setFlatCost(booking.getFlatCost());
					booking1.setFlatbasicCost(booking.getFlatbasicCost());
					booking1.setParkingFloorId(booking.getParkingFloorId());
					booking1.setParkingZoneId(booking.getParkingZoneId());
					booking1.setAgentId(booking.getAgentId());
					booking1.setInfrastructureCharge(booking.getInfrastructureCharge());
					booking1.setAggreementValue1(booking.getAggreementValue1());
					booking1.setHandlingCharges(booking.getHandlingCharges());
					booking1.setStampDutyPer(booking.getStampDutyPer());
					booking1.setRegistrationPer(booking.getRegistrationPer());
					booking1.setStampDuty1(booking.getStampDuty1());
					booking1.setRegistrationCost1(booking.getRegistrationCost1());
					booking1.setGstCost(booking.getGstCost());
					booking1.setGstAmount1(booking.getGstAmount1());
					booking1.setGrandTotal1(booking.getGrandTotal1());
					booking1.setTds(booking.getTds());
					booking1.setBookingstatus("Booking");
					booking1.setCreationDate(new Date());
					booking1.setUpdateDate(new Date());
					booking1.setUserName(booking.getUserId());

					bookingRepository.save(booking1);

					Flat flat;
					flat = mongoTemplate.findOne(Query.query(Criteria.where("flatId").is(booking.getFlatId())), Flat.class);
					flat.setFlatstatus("Booking Completed");
					flatRepository.save(flat);

					try {   	

						ParkingZone parkingzone=new ParkingZone();
						parkingzone = mongoTemplate.findOne(Query.query(Criteria.where("parkingZoneId").is(booking.getParkingZoneId())), ParkingZone.class);
						parkingzone.setParkingZoneStatus("Assigned");
						parkingZoneRepository.save(parkingzone);
					}
					catch(Exception ee)
					{
					}

					// model.addAttribute("bankStatus","Success");
				}
				catch(Exception de)
				{
					//model.addAttribute("bankStatus","Fail");
				}


				query = new Query();
				// for Send SMS to customer
				try {
					Date date = new Date();  
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy"); 
					formatter = new SimpleDateFormat("dd MMMM yyyy");  
					String strDate = formatter.format(date);  

					query = new Query();
					Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(booking.getProjectId())), Project.class);

					query = new Query();
					//Flat flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(booking.getFlatId())), Flat.class);
					String sms="";
					sms=" Dear "+booking.getBookingName()+", Project "+projectDetails.getProjectName()+" flat no."+flatDetails.getFlatNumber()+" has been booked for you on "+strDate+". ";
					SmsServices.sendSMS(booking.getBookingmobileNumber1(),sms);
				}
				catch (Exception e) {
					e.printStackTrace();
					// TODO: handle exception
				}

				response.setResponse("booking successfully added");
				response.setStatus(1);
				return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);

			}else
			{
				response.setResponse("This Flat is already booking ");
				response.setStatus(0);
				return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);

			}
		} catch (Exception ex) {
			response.setResponse("Booking added fail");
			response.setStatus(0);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/getEnquiryListForBooking", method = RequestMethod.POST)
	public ResponseEntity<EnquiryResponseDetails> getEnquiryListForBooking(@RequestBody LoginDetails loginDetails) {

		EnquiryResponseDetails response = new EnquiryResponseDetails();

		try {

			Query query = new Query();
			List<Enquiry> enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("enqStatus").is("Booking")), Enquiry.class);

			for(int i=0;i<enquiryList.size();i++)
			{
				query = new Query();
				EnquirySource enquirysource = mongoTemplate.findOne(query.addCriteria(Criteria.where("enquirysourceId").is(enquiryList.get(i).getEnquirysourceId())), EnquirySource.class);
				query = new Query();
				SubEnquirySource subenquirySource = mongoTemplate.findOne(query.addCriteria(Criteria.where("subsourceId").is(enquiryList.get(i).getSubsourceId())), SubEnquirySource.class);
				enquiryList.get(i).setEnquirysourceName(enquirysource.getEnquirysourceName());
				enquiryList.get(i).setSubenquirysourceName(subenquirySource.getSubenquirysourceName());
				enquiryList.get(i).setBookingType("Enquiry Booking");

			}
			response.setEnquiryList(enquiryList);
			response.setResponse("Enquiry List");
			response.setStatus(1);
			return new ResponseEntity<EnquiryResponseDetails>(response,HttpStatus.OK);

		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<EnquiryResponseDetails>(response,HttpStatus.OK);
		}
	}


	@RequestMapping(value = "/getBookingList", method = RequestMethod.POST)
	public ResponseEntity<BookingListResponse> getBookingList(@RequestBody LoginDetails loginDetails) {

		BookingListResponse response = new BookingListResponse();

		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		try {

			Login login =loginRepository.findByUserNameAndPassWordAndStatus(loginDetails.getUserName(), loginDetails.getPassWord(),"Active");

			if(login!=null)
			{
				List<Booking> bookingList=new ArrayList<Booking>();
				List<UserAssignedProject> userLoginProjectList=userprojectListRepository.findByEmployeeId(login.getEmployeeId());
				Query query = new Query();
				for(int i=0;i<userLoginProjectList.size();i++)
				{
					query = new Query();
					List<Booking> bookingList1 = mongoTemplate.find(query.addCriteria(Criteria.where("bookingstatus").ne("Cancel").and("projectId").is(userLoginProjectList.get(i).getProjectId())), Booking.class);

					for(int j=0;j<bookingList1.size();j++)
					{
						try {
							query = new Query();
							CustomerPaymentDetails customerPayment = mongoTemplate.findOne(query.addCriteria(Criteria.where("bookingId").is(bookingList1.get(j).getBookingId())),CustomerPaymentDetails.class);

							if(Double.compare(bookingList1.get(j).getAggreementValue1(), customerPayment.getTotalpayAggreement())==0 || Double.compare(bookingList1.get(j).getStampDuty1(), customerPayment.getTotalpayStampDuty())==0 || Double.compare(bookingList1.get(j).getRegistrationCost1(), customerPayment.getTotalpayRegistration())==0 || Double.compare(bookingList1.get(j).getGstAmount1(), customerPayment.getTotalpaygstAmount())==0)
							{
							}
							else
							{
								try {
									query = new Query();
									Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(bookingList1.get(j).getProjectId())),Project.class);
									query = new Query();
									ProjectBuilding projectbuildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(bookingList1.get(j).getBuildingId())),ProjectBuilding.class);
									query = new Query();
									ProjectWing projectwingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(bookingList1.get(j).getWingId())),ProjectWing.class);
									query = new Query();
									Flat flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(bookingList1.get(j).getFlatId())),Flat.class);
									query = new Query();
									Floor floorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("floorId").is(flatDetails.getFloorId())),Floor.class);

									bookingList1.get(j).setProjectName(projectDetails.getProjectName());
									bookingList1.get(j).setBuildingName(projectbuildingDetails.getBuildingName());
									bookingList1.get(j).setWingName(projectwingDetails.getWingName());
									bookingList1.get(j).setFlatNumber(flatDetails.getFlatNumber());
									bookingList1.get(j).setFloorName(floorDetails.getFloortypeName());
									bookingList1.get(j).setBookingType("Booking");

									bookingList1.get(j).setBookingDate(dateFormat.format(bookingList1.get(j).getCreationDate()));

								}catch (Exception e) {
									// TODO: handle exception
								}
								bookingList.add(bookingList1.get(j));
							}
						}catch (Exception e) {
							// TODO: handle exception
						}
					}
				}

				response.setBookingList(bookingList);
				response.setResponse("Booking List");
				response.setStatus(1);
				return new ResponseEntity<BookingListResponse>(response,HttpStatus.OK);
			}
			else
			{
				response.setResponse("Login fail");
				response.setStatus(0);
				return new ResponseEntity<BookingListResponse>(response,HttpStatus.OK);
			}

		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);
			return new ResponseEntity<BookingListResponse>(response,HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/getViewBooking", method = RequestMethod.POST)
	public ResponseEntity<BookingViewResponse> getEnquirySource(@RequestBody BookingIdResponse bookingidResponse) {

		BookingViewResponse response = new BookingViewResponse();

		try {
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");  
			Query query = new Query();
			Booking bookingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("bookingId").is(bookingidResponse.getBookingId())),Booking.class);

			query = new Query();
			Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(bookingDetails.getProjectId())),Project.class);
			query = new Query();
			ProjectBuilding projectbuildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(bookingDetails.getBuildingId())),ProjectBuilding.class);
			query = new Query();
			ProjectWing projectwingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(bookingDetails.getWingId())),ProjectWing.class);
			query = new Query();
			Flat flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(bookingDetails.getFlatId())),Flat.class);
			query = new Query();
			Floor floorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("floorId").is(bookingDetails.getFloorId())),Floor.class);
			query = new Query();
			Country countryDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("countryId").is(bookingDetails.getCountryId())),Country.class);
			query = new Query();
			State stateDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("stateId").is(bookingDetails.getStateId())),State.class);
			query = new Query();
			City cityDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(bookingDetails.getCityId())),City.class);
			query = new Query();
			LocationArea locationareaDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("cityId").is(bookingDetails.getCityId())),LocationArea.class);

			try {
				query = new Query();
				Floor parkingfloorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("floorId").is(bookingDetails.getParkingFloorId())),Floor.class);
				response.setParkingZoneName(parkingfloorDetails.getFloortypeName());
				query = new Query();
				ParkingZone parkingZoneDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("parkingZoneId").is(bookingDetails.getParkingZoneId())),ParkingZone.class);

				response.setParkingFloorName(parkingZoneDetails.getParkingNumber());
			}
			catch (Exception e) {
				response.setParkingZoneName("");
				response.setParkingFloorName("");
				// TODO: handle exception
			}
			try
			{
				query = new Query();
				Agent agentDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("agentId").is(bookingDetails.getAgentId())),Agent.class);
				response.setAgentName(agentDetails.getAgentfirmName());
			}
			catch (Exception e) {
				response.setAgentName("");
				// TODO: handle exception
			}
			response.setBookingId(bookingDetails.getBookingId());
			response.setBookingName(bookingDetails.getBookingfirstname());
			response.setBookingEmail(bookingDetails.getBookingEmail());
			response.setBookingmobileNumber1(bookingDetails.getBookingmobileNumber1());
			response.setBookingaddress(bookingDetails.getBookingaddress());
			response.setCountryName(countryDetails.getCountryName());
			response.setStateName(stateDetails.getStateName());
			response.setCityName(cityDetails.getCityName());
			response.setLocationareaName(locationareaDetails.getLocationareaName());
			response.setBookingPincode(bookingDetails.getBookingPincode());
			response.setBookingOccupation(bookingDetails.getBookingOccupation());
			response.setPurposeOfFlat(bookingDetails.getPurposeOfFlat());
			response.setProjectName(projectDetails.getProjectName());
			response.setBuildingName(projectbuildingDetails.getBuildingName());
			response.setWingName(projectwingDetails.getWingName());
			response.setFloorName(floorDetails.getFloortypeName());
			response.setFlatNumber(flatDetails.getFlatNumber());
			response.setFlatType(bookingDetails.getFlatType());
			response.setFlatFacing(bookingDetails.getFlatFacing());
			response.setFlatareainSqFt(bookingDetails.getFlatareainSqFt());
			response.setFlatCostwithotfloorise(bookingDetails.getFlatCostwithotfloorise());
			response.setFloorRise(bookingDetails.getFloorRise());
			response.setFlatCost(bookingDetails.getFlatCost());
			response.setFlatbasicCost(bookingDetails.getFlatbasicCost());
			response.setInfrastructureCharge(bookingDetails.getInfrastructureCharge());
			response.setAggreementValue1(bookingDetails.getAggreementValue1());
			response.setHandlingCharges(bookingDetails.getHandlingCharges());
			response.setStampDuty1(bookingDetails.getStampDuty1());
			response.setRegistrationCost1(bookingDetails.getRegistrationCost1());
			response.setGstAmount1(bookingDetails.getGstAmount1());
			response.setGrandTotal1(bookingDetails.getGrandTotal1());
			response.setTds(bookingDetails.getTds());

			response.setBookingstatus(bookingDetails.getBookingstatus());
			response.setCreationDate(dateFormat.format(bookingDetails.getCreationDate()));

			response.setResponse("Booking Details");
			response.setStatus(1);
			return new ResponseEntity<BookingViewResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			response.setResponse("Login fail");
			response.setStatus(0);
			return new ResponseEntity<BookingViewResponse>(response,HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/bookingCancel", method = RequestMethod.POST)
	public ResponseEntity<CommanResponse> bookingCancel(@RequestBody BookingCancleResponse bookingCancleResponse) {

		CommanResponse response = new CommanResponse();

		try {

			Query query = new Query();
			Booking bookingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("bookingId").is(bookingCancleResponse.getBookingId())),Booking.class);

			Flat flat;
			Booking booking;
			try
			{
				Date date = new Date();  
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");  

				BookingCancelForm bookingcancelform =new BookingCancelForm();
				bookingcancelform.setCancelId(CancelCode());
				bookingcancelform.setBookingId(bookingCancleResponse.getBookingId());
				bookingcancelform.setBookingDate(formatter.format(bookingDetails.getCreationDate()));
				bookingcancelform.setCancelDate(formatter.format(date));
				bookingcancelform.setCustomerName(bookingDetails.getBookingfirstname());
				bookingcancelform.setCustomerEmail(bookingDetails.getBookingEmail());
				bookingcancelform.setCustomerMobile(bookingDetails.getBookingmobileNumber1());
				bookingcancelform.setProjectId(bookingDetails.getProjectId());
				bookingcancelform.setBuildingId(bookingDetails.getBuildingId());
				bookingcancelform.setWingId(bookingDetails.getWingId());
				bookingcancelform.setFloorId(bookingDetails.getFloorId());;
				bookingcancelform.setFlatType(bookingDetails.getFlatType());
				bookingcancelform.setFlatId(bookingDetails.getFlatId());
				bookingcancelform.setFlatFacing(bookingDetails.getFlatFacing());
				bookingcancelform.setFlatareainSqFt(bookingDetails.getFlatareainSqFt());
				bookingcancelform.setFlatbasicCost(bookingDetails.getFlatbasicCost());
				bookingcancelform.setAggreementValue1(bookingDetails.getAggreementValue1());
				bookingcancelform.setCancelCharge(Double.parseDouble(bookingCancleResponse.getCancellationCharges()));
				bookingcancelform.setGstTaxes(Double.parseDouble(bookingCancleResponse.getGstAmount()));
				bookingcancelform.setNetAmount(Double.parseDouble(bookingCancleResponse.getTotalAmount()));
				bookingcancelform.setCreationDate(formatter.format(date));
				bookingcancelform.setUpdateDate(formatter.format(date));
				bookingcancelform.setUserName(bookingCancleResponse.getUserId());

				bookingcancelformRepository.save(bookingcancelform);

				flat = mongoTemplate.findOne(Query.query(Criteria.where("flatId").is(bookingDetails.getFlatId())), Flat.class);
				flat.setFlatstatus("Booking Remaninig");
				flatRepository.save(flat);

				booking = mongoTemplate.findOne(Query.query(Criteria.where("bookingId").is(bookingCancleResponse.getBookingId())), Booking.class);
				booking.setBookingstatus("Cancel");
				bookingRepository.save(booking);

				try {
					List<Booking> bookingList; 
					Query query1 = new Query();
					bookingList = mongoTemplate.find(query1.addCriteria(Criteria.where("bookingId").is(bookingDetails.getBookingId())), Booking.class);

					ParkingZone parkingzone;
					parkingzone = mongoTemplate.findOne(Query.query(Criteria.where("parkingZoneId").is(bookingList.get(0).getParkingZoneId())), ParkingZone.class);
					parkingzone.setParkingZoneStatus("Not Assigned");
					parkingZoneRepository.save(parkingzone);
				}
				catch(Exception e) {}
			}
			catch(DuplicateKeyException de)
			{
				//model.addAttribute("bankStatus","Fail");
			}


			/*
			//Send sms to customer
			try {
				String sms="";
				sms="Dear "+enquiry.getEnqfirstName()+", Thanks for visiting us and enquiring a "+enquiry.getProjectName()+" project of Sonigara Group for "+enquiry.getFlatType()+" flat.\r\n" + 
						"Sales Ex Contact No:-09527023000"+".\r\n" +
						"Pls do visit again.";
				//	SmsServices.sendSMS(enquiry.getEnqmobileNumber1(),sms);
			}catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
			 */

			response.setResponse("Booking successfully Cancel");
			response.setStatus(1);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			response.setResponse("Booking Cancel fail");
			response.setStatus(0);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
		}
	}


}
