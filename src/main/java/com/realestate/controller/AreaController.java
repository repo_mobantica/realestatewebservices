package com.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.Budget;
import com.realestate.bean.*;
import com.realestate.repository.*;
import com.realestate.response.AreaResponse;
import com.realestate.response.BudgetResponse;
import com.realestate.response.LoginDetails;

@Controller
@RestController
@RequestMapping("area")
public class AreaController {


	@Autowired
	BudgetRepository budgetRepository;
	@Autowired
	CityRepository cityRepository;
	@Autowired
	LocationAreaRepository locationAreaRepository;
	@Autowired
	StateRepository stateRepository;
	@Autowired
	CountryRepository countryRepository;

	@RequestMapping(value = "/getAreaList", method = RequestMethod.POST)
	public ResponseEntity<AreaResponse> getAreaList(@RequestBody LoginDetails loginDetails) {

		AreaResponse response = new AreaResponse();

		try {
			List<Country> countryList=countryRepository.findAll();
			List<State> stateList=stateRepository.findAll();
			List<City> cityList=cityRepository.findAll();
			List<LocationArea> locationareaList=locationAreaRepository.findAll();

			response.setCountryList(countryList);
			response.setStateList(stateList);
			response.setCityList(cityList);
			response.setLocationareaList(locationareaList);

			response.setResponse("Area List");
			response.setStatus(1);
			return new ResponseEntity<AreaResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			response.setResponse("Login fail");
			response.setStatus(0);
			return new ResponseEntity<AreaResponse>(response,HttpStatus.OK);
		}
	}


}
