package com.realestate.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.Item;
import com.realestate.bean.MaterialPurchaseRequisition;
import com.realestate.bean.MaterialPurchaseRequisitionHistory;
import com.realestate.bean.MaterialsPurchased;
import com.realestate.bean.MaterialsPurchasedHistory;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.SubSupplierType;
import com.realestate.bean.SupplierQuotation;
import com.realestate.bean.SupplierQuotationHistory;
import com.realestate.bean.SupplierType;
import com.realestate.response.CommanResponse;
import com.realestate.response.PurchaseSupplierQuotationResponse;
import com.realestate.response.SupplierQuotationAddResponse;
import com.realestate.response.SupplierQuotationDetailsResponse;
import com.realestate.response.SupplierQuotationResponse;
import com.realestate.response.SupplierRequisitionItemListResponse;
import com.realestate.repository.*;

@Controller
@RestController
@RequestMapping("supplierQuotation")
public class SupplierQuotationController {

	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	SupplierQuotationRepository supplierQuotationRepository;
	@Autowired
	MaterialPurchaseRequisitionRepository materialPurchaseRequisitionRepository;
	@Autowired
	MaterialPurchaseRequisitionHistoryRepository materialpurchaseRequisitionHistoryRepository;
	@Autowired
	SupplierQuotationRepository supplierquotationRepository;
	@Autowired
	SupplierQuotationHistoryRepository supplierquotationhistoryRepository;
	@Autowired
	MaterialsPurchasedHistoryRepository materialsPurchasedHistoryRepository;
	@Autowired
	MaterialPurchaseRequisitionRepository materialpurchaserequisitionRepository;

	@Autowired
	MaterialsPurchasedRepository materialsPurchasedRepository;
	String quotationCode;

	private String SupplierQuotationCode()
	{
		long cnt  = supplierquotationRepository.count();
		if(cnt<10)
		{
			quotationCode = "SQ000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			quotationCode = "SQ00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			quotationCode = "SQ0"+(cnt+1);
		}
		else 
		{
			quotationCode = "SQ"+(cnt+1);
		}
		return quotationCode;
	}

	private long QuotationHistoriId()
	{
		long quotationHistoriId;
		long cnt=0;
		cnt  = supplierquotationhistoryRepository.count();
		quotationHistoriId=cnt+1;
		return quotationHistoriId;
	}

	private long MaterialsPurchasedHistriesId()
	{
		long count ;
		count = materialsPurchasedHistoryRepository.count();
		return count;
	}

	String purchasedId= null;
	public String GenerateMaterialPurchasedId(String projectName, String buildingName, String wingName)
	{
		long count = 0;
		SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  
		Date date = new Date();  
		String creationDate=""+formatter.format(date);

		Query query = new Query();
		List<MaterialsPurchased> materialspPurchasedList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").is(creationDate)), MaterialsPurchased.class);

		count=materialspPurchasedList.size()+1;
		String projectName1[]=null, buildingName1[]=null, wingName1[]=null;
		projectName1  = projectName.split(" ");
		buildingName1 = buildingName.split(" ");
		wingName1 	  = wingName.split(" ");

		purchasedId="PO/";
		if(projectName1.length==1)
		{
			purchasedId = purchasedId + projectName1[0].charAt(0) + projectName1[0].charAt(1);
		}
		else
		{
			purchasedId = purchasedId + projectName1[0].charAt(0) + projectName1[1].charAt(0);
		}
		/*
		purchasedId = purchasedId +"/";

		if(buildingName1.length==1)
		{
			if(buildingName1[0].length()==1)
			{
				purchasedId = purchasedId + buildingName1[0].charAt(0);
			}
			else
			{
				purchasedId = purchasedId + buildingName1[0].charAt(0) + buildingName1[0].charAt(1);
			}
		}
		else
		{
			purchasedId = purchasedId + buildingName1[0].charAt(0) + buildingName1[1].charAt(0);
		}
		 */
		if(count<10)
		{
			purchasedId = purchasedId+ "/"+creationDate+"/00"+count; 	
		}
		else if(count<100)
		{
			purchasedId = purchasedId+ "/"+creationDate+"/0"+count; 
		}
		else
		{
			purchasedId = purchasedId+ "/"+creationDate+"/"+count; 
		}

		return purchasedId;
	}

	@RequestMapping(value = "/getSupplierQuotationList", method = RequestMethod.POST)
	public ResponseEntity<SupplierQuotationResponse> getsupplierQuotationList(@RequestBody MaterialPurchaseRequisition materialpurchaseRequisition) {
		SupplierQuotationResponse response = new SupplierQuotationResponse();
		try {
			List<SupplierQuotation> supplierquotationList =supplierQuotationRepository.findByRequisitionId(materialpurchaseRequisition.getRequisitionId());
			response.setSupplierQuotationList(supplierquotationList);
			response.setResponse("Supplier Quotation List");
			response.setStatus(1);
			return new ResponseEntity<SupplierQuotationResponse>(response,HttpStatus.OK);
		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);
			return new ResponseEntity<SupplierQuotationResponse>(response,HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/getSupplierRequisitionItemList", method = RequestMethod.POST)
	public ResponseEntity<SupplierRequisitionItemListResponse> getSupplierRequisitionItemList(@RequestBody MaterialPurchaseRequisition materialpurchaseRequisition) {

		SupplierRequisitionItemListResponse response = new SupplierRequisitionItemListResponse();

		try {
			Query query = new Query();
			MaterialPurchaseRequisition materialList = materialPurchaseRequisitionRepository.findByRequisitionId(materialpurchaseRequisition.getRequisitionId());

			response.setRequisitionId(materialList.getRequisitionId());
			response.setProjectId(materialList.getProjectId());
			response.setBuildingId(materialList.getBuildingId());
			response.setWingId(materialList.getWingId());
			response.setEmployeeId(materialList.getEmployeeId());
			response.setTotalNoItem(materialList.getTotalNoItem());
			response.setTotalQuantity(materialList.getTotalQuantity());
			response.setRequisitionstatus(materialList.getStatus());
			response.setRequiredDate(materialList.getRequiredDate());
			List<MaterialPurchaseRequisitionHistory> materialpurchaserequisitionhistory = materialpurchaseRequisitionHistoryRepository.findByRequisitionId(materialpurchaseRequisition.getRequisitionId());
			List<MaterialPurchaseRequisitionHistory> materialpurchaserequisitionhistory1 = new ArrayList<MaterialPurchaseRequisitionHistory>();
			MaterialPurchaseRequisitionHistory material = new MaterialPurchaseRequisitionHistory();
			try
			{
				for(int i=0;i<materialpurchaserequisitionhistory.size();i++)
				{
					material = new MaterialPurchaseRequisitionHistory();
					query = new Query();
					Item itemDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("itemId").is(materialpurchaserequisitionhistory.get(i).getItemId())),Item.class);
					material.setRequisitionHistoryId(materialpurchaserequisitionhistory.get(i).getRequisitionHistoryId());
					material.setRequisitionId(materialpurchaserequisitionhistory.get(i).getRequisitionId()); 
					material.setItemId(materialpurchaserequisitionhistory.get(i).getItemId());
					material.setItemName(itemDetails.getItemName());
					try
					{
						query =new Query();
						SupplierType suppliertypeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("suppliertypeId").is(itemDetails.getSuppliertypeId())), SupplierType.class);
						material.setSuppliertypeId(suppliertypeDetails.getSupplierType());	

						query =new Query();
						SubSupplierType subsuppliertypeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("subsuppliertypeId").is(itemDetails.getSubsuppliertypeId())), SubSupplierType.class);
						material.setSubsuppliertypeId(subsuppliertypeDetails.getSubsupplierType());
					}
					catch (Exception e) {
						// TODO: handle exception
					}

					material.setGstPer(itemDetails.getGstPer());
					material.setItemInitialDiscount(itemDetails.getItemInitialDiscount());
					material.setItemInitialPrice(itemDetails.getIntemInitialPrice());
					material.setItemQuantity(materialpurchaserequisitionhistory.get(i).getItemQuantity());
					material.setItemSize(materialpurchaserequisitionhistory.get(i).getItemSize());
					material.setItemunitName(materialpurchaserequisitionhistory.get(i).getItemunitName());
					material.setItemSubType("");
					materialpurchaserequisitionhistory1.add(material);

				}
			}catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}

			response.setItemList(materialpurchaserequisitionhistory1);
			response.setResponse("Requisition Item List");
			response.setStatus(1);
			return new ResponseEntity<SupplierRequisitionItemListResponse>(response,HttpStatus.OK);
		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<SupplierRequisitionItemListResponse>(response,HttpStatus.OK);
		}
	}


	@RequestMapping(value = "/addSupplierQuotation", method = RequestMethod.POST)
	public ResponseEntity<CommanResponse> addSupplierQuotation(@RequestBody SupplierQuotationAddResponse supplierquotationaddResponse) {

		CommanResponse response = new CommanResponse();

		try {
			String quotationId=SupplierQuotationCode();
			SupplierQuotation supplierQuotation=new SupplierQuotation();
			SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  
			Date date = new Date();  
			Double total=0.0;
			Double discountAmount=0.0;
			Double gstAmount=0.0;
			Double grandTotal=0.0;

			SupplierQuotationHistory supplierquotation=new SupplierQuotationHistory();
			for(int i=0;i<supplierquotationaddResponse.getItemList().size();i++)
			{
				try {
					supplierquotation=new SupplierQuotationHistory();
					supplierquotation.setQuotationHistoriId(QuotationHistoriId());
					supplierquotation.setQuotationId(quotationId);
					supplierquotation.setItemId(supplierquotationaddResponse.getItemList().get(i).getItemId());
					supplierquotation.setItemSize(supplierquotationaddResponse.getItemList().get(i).getItemSize());
					supplierquotation.setItemUnit(supplierquotationaddResponse.getItemList().get(i).getItemUnit());
					supplierquotation.setItemBrandName(supplierquotationaddResponse.getItemList().get(i).getItemBrandName());
					supplierquotation.setQuantity(supplierquotationaddResponse.getItemList().get(i).getQuantity());
					supplierquotation.setRate(supplierquotationaddResponse.getItemList().get(i).getRate());
					supplierquotation.setTotal(supplierquotationaddResponse.getItemList().get(i).getTotal());
					supplierquotation.setDiscountPer(supplierquotationaddResponse.getItemList().get(i).getDiscountPer());
					supplierquotation.setDiscountAmount(supplierquotationaddResponse.getItemList().get(i).getDiscountAmount());
					supplierquotation.setGstPer(supplierquotationaddResponse.getItemList().get(i).getGstPer());
					supplierquotation.setGstAmount(supplierquotationaddResponse.getItemList().get(i).getGstAmount());
					supplierquotation.setGrandTotal(supplierquotationaddResponse.getItemList().get(i).getGrandTotal());
					supplierquotationhistoryRepository.save(supplierquotation);
					
					total=total+supplierquotationaddResponse.getItemList().get(i).getTotal();
					discountAmount=discountAmount+supplierquotationaddResponse.getItemList().get(i).getDiscountAmount();
					gstAmount=gstAmount+supplierquotationaddResponse.getItemList().get(i).getGstAmount();
					grandTotal=grandTotal+supplierquotationaddResponse.getItemList().get(i).getGrandTotal();
				}catch (Exception e) {
					e.printStackTrace();
					// TODO: handle exception
				}
			}
			supplierQuotation.setQuotationId(quotationId);
			supplierQuotation.setSupplierId(supplierquotationaddResponse.getSupplierId());
			supplierQuotation.setRequisitionId(supplierquotationaddResponse.getRequisitionId());
			supplierQuotation.setTotal(total);
			supplierQuotation.setDiscountAmount(discountAmount);
			supplierQuotation.setGstAmount(gstAmount);
			supplierQuotation.setGrandTotal(grandTotal);
			supplierQuotation.setCreationDate(formatter.format(date));
			supplierQuotation.setUpdateDate(formatter.format(date));
			supplierQuotation.setUserName(supplierquotationaddResponse.getUserId());
			supplierquotationRepository.save(supplierQuotation);
			response.setResponse("Supplier Quotation successfully added");
			response.setStatus(1);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			ex.printStackTrace();
			response.setResponse("Supplier Quotation added fail");
			response.setStatus(0);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
		}
	}


	@RequestMapping(value = "/getSupplierQuotationDetails", method = RequestMethod.POST)
	public ResponseEntity<SupplierQuotationDetailsResponse> getSupplierQuotationDetails(@RequestBody SupplierQuotation supplierQuotation) {
		SupplierQuotationDetailsResponse response = new SupplierQuotationDetailsResponse();
		try {

			Query query = new Query();
			SupplierQuotation supplierquotationDetails =supplierquotationRepository.findByQuotationId(supplierQuotation.getQuotationId());
			List<SupplierQuotationHistory> supplierquotationHistoriesList =supplierquotationhistoryRepository.findByQuotationId(supplierquotationDetails.getQuotationId());

			List<SupplierQuotationHistory> supplierquotationHistoriesList1 = new ArrayList<SupplierQuotationHistory>();

			SupplierQuotationHistory material = new SupplierQuotationHistory();

			for(int i=0;i<supplierquotationHistoriesList.size();i++)
			{
				material = new SupplierQuotationHistory();
				query = new Query();
				Item itemDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("itemId").is(supplierquotationHistoriesList.get(i).getItemId())),Item.class);

				material.setQuotationHistoriId(supplierquotationHistoriesList.get(i).getQuotationHistoriId());
				material.setQuotationId(supplierquotationHistoriesList.get(i).getQuotationId());
				material.setItemId(itemDetails.getItemId());

				material.setItemName(itemDetails.getItemName());

				try
				{
					query =new Query();
					List<SupplierType> suppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("suppliertypeId").is(itemDetails.getSuppliertypeId())), SupplierType.class);
					material.setSuppliertypeId(suppliertypeDetails.get(0).getSupplierType());	
					material.setSubsuppliertypeId(itemDetails.getSubsuppliertypeId());
					query =new Query();
					List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(itemDetails.getSubsuppliertypeId())), SubSupplierType.class);

					material.setSubsupplierType(subsuppliertypeDetails.get(0).getSubsupplierType());

				}
				catch (Exception e) {
					// TODO: handle exception
				}

				material.setItemUnit(supplierquotationHistoriesList.get(i).getItemUnit());
				material.setItemSize(supplierquotationHistoriesList.get(i).getItemSize());
				material.setItemBrandName(supplierquotationHistoriesList.get(i).getItemBrandName());
				material.setQuantity(supplierquotationHistoriesList.get(i).getQuantity());
				material.setRate(supplierquotationHistoriesList.get(i).getRate());
				material.setTotal(supplierquotationHistoriesList.get(i).getTotal());
				material.setDiscountPer(supplierquotationHistoriesList.get(i).getDiscountPer());
				material.setDiscountAmount(supplierquotationHistoriesList.get(i).getDiscountAmount());
				material.setGstPer(supplierquotationHistoriesList.get(i).getGstPer());
				material.setGstAmount(supplierquotationHistoriesList.get(i).getGstAmount());
				material.setGrandTotal(supplierquotationHistoriesList.get(i).getGrandTotal());

				supplierquotationHistoriesList1.add(material);
			}


			response.setQuotationId(supplierquotationDetails.getQuotationId());
			response.setSupplierId(supplierquotationDetails.getSupplierId());
			response.setRequisitionId(supplierquotationDetails.getRequisitionId());
			response.setTotal(supplierquotationDetails.getTotal());
			response.setGstAmount(supplierquotationDetails.getGstAmount());
			response.setDiscountAmount(supplierquotationDetails.getDiscountAmount());
			response.setGrandTotal(supplierquotationDetails.getGrandTotal());

			response.setItemList(supplierquotationHistoriesList1);

			response.setResponse("Supplier Quotation ");
			response.setStatus(1);
			return new ResponseEntity<SupplierQuotationDetailsResponse>(response,HttpStatus.OK);
		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);
			return new ResponseEntity<SupplierQuotationDetailsResponse>(response,HttpStatus.OK);
		}
	}


	@RequestMapping(value = "/purchaseSupplierQuotation", method = RequestMethod.POST)
	public ResponseEntity<CommanResponse> PurchaseSupplierQuotation(@RequestBody PurchaseSupplierQuotationResponse purchasesupplierquotationResponse) {
		CommanResponse response = new CommanResponse();
		try {

			SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  
			Date date = new Date();  

			MaterialPurchaseRequisition materialRequisition = materialPurchaseRequisitionRepository.findByRequisitionId(purchasesupplierquotationResponse.getRequisitionId());
			SupplierQuotation supplierquotationDetails =supplierquotationRepository.findByQuotationId(purchasesupplierquotationResponse.getQuotationId());
			List<SupplierQuotationHistory> supplierquotationHistoriesList =supplierquotationhistoryRepository.findByQuotationId(purchasesupplierquotationResponse.getQuotationId());

			Query query = new Query();
			Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(materialRequisition.getProjectId())),Project.class);
			query =new Query();
			ProjectBuilding buildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(materialRequisition.getBuildingId())), ProjectBuilding.class);

			query =new Query();
			ProjectWing wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(materialRequisition.getWingId())), ProjectWing.class);

			String materialsPurchasedId=GenerateMaterialPurchasedId(projectDetails.getProjectName(),buildingDetails.getBuildingName(),wingDetails.getWingName());

			MaterialsPurchased materialsPurchased=new MaterialsPurchased();

			MaterialsPurchasedHistory materialpurches = new MaterialsPurchasedHistory();
			for(int i=0;i<supplierquotationHistoriesList.size();i++)
			{
				materialpurches = new MaterialsPurchasedHistory();

				query = new Query();
				Item itemDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("itemId").is(supplierquotationHistoriesList.get(i).getItemId())),Item.class);

				materialpurches.setMaterialPurchesHistoriId(""+MaterialsPurchasedHistriesId());
				materialpurches.setMaterialsPurchasedId(materialsPurchasedId);
				materialpurches.setRequisitionId(purchasesupplierquotationResponse.getRequisitionId());
				materialpurches.setItemId(supplierquotationHistoriesList.get(i).getItemId());
				materialpurches.setItemName(itemDetails.getItemName());
				materialpurches.setSubsuppliertypeId(itemDetails.getSubsuppliertypeId());
				materialpurches.setItenSize(supplierquotationHistoriesList.get(i).getItemSize());
				materialpurches.setItemUnit(supplierquotationHistoriesList.get(i).getItemUnit());
				materialpurches.setItemBrandName(supplierquotationHistoriesList.get(i).getItemBrandName());
				materialpurches.setItemQuantity(supplierquotationHistoriesList.get(i).getQuantity());

				materialpurches.setRate(supplierquotationHistoriesList.get(i).getRate());
				materialpurches.setTotal(supplierquotationHistoriesList.get(i).getTotal());
				materialpurches.setDiscountPer(supplierquotationHistoriesList.get(i).getDiscountPer());
				materialpurches.setDiscountAmount(supplierquotationHistoriesList.get(i).getDiscountAmount());
				materialpurches.setGstPer(supplierquotationHistoriesList.get(i).getGstPer());
				materialpurches.setGstAmount(supplierquotationHistoriesList.get(i).getGstAmount());
				materialpurches.setGrandTotal(supplierquotationHistoriesList.get(i).getGrandTotal());

				materialsPurchasedHistoryRepository.save(materialpurches);
			}

			materialsPurchased.setMaterialsPurchasedId(materialsPurchasedId);
			materialsPurchased.setRequisitionId(purchasesupplierquotationResponse.getRequisitionId());

			materialsPurchased.setSupplierId(purchasesupplierquotationResponse.getSupplierId());
			materialsPurchased.setStoreId(purchasesupplierquotationResponse.getStoreId());
			materialsPurchased.setEmployeeId(materialRequisition.getEmployeeId());
			materialsPurchased.setDelivery(purchasesupplierquotationResponse.getDelivery());
			materialsPurchased.setAfterdelivery(purchasesupplierquotationResponse.getAfterdelivery());
			materialsPurchased.setPaymentterms(purchasesupplierquotationResponse.getPaymentterms());
			materialsPurchased.setTotalPrice(supplierquotationDetails.getTotal());
			materialsPurchased.setTotalDiscount(supplierquotationDetails.getDiscountAmount());
			materialsPurchased.setTotalGstAmount(supplierquotationDetails.getGstAmount());
			materialsPurchased.setTotalAmount(supplierquotationDetails.getGrandTotal());
			materialsPurchased.setStatus("Applied");
			materialsPurchased.setPaymentStatus("Pending");
			materialsPurchased.setCreationDate(formatter.format(date));
			materialsPurchased.setUpdateDate(formatter.format(date));
			materialsPurchased.setUserName(purchasesupplierquotationResponse.getUserId());

			materialsPurchasedRepository.save(materialsPurchased);

			try {
				MaterialPurchaseRequisition materialpurchaserequisition;
				materialpurchaserequisition = mongoTemplate.findOne(Query.query(Criteria.where("requisitionId").is(purchasesupplierquotationResponse.getRequisitionId())), MaterialPurchaseRequisition.class);
				materialpurchaserequisition.setStatus("Completed");
				materialpurchaserequisitionRepository.save(materialpurchaserequisition);
			}
			catch(Exception ee)
			{
			}

			response.setResponse("Material Purchase successfully added");
			response.setStatus(1);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
		} catch (Exception ex) {

			response.setResponse("Material Purchase successfully fail");
			response.setStatus(0);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
		}
	}


}
