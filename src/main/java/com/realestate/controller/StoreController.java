package com.realestate.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.Employee;
import com.realestate.bean.Item;
import com.realestate.bean.Login;
import com.realestate.bean.MaterialsPurchased;
import com.realestate.bean.MaterialsPurchasedHistory;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.Stock;
import com.realestate.bean.Store;
import com.realestate.bean.StoreStock;
import com.realestate.bean.StoreStockHistory;
import com.realestate.bean.SubSupplierType;
import com.realestate.bean.Supplier;
import com.realestate.bean.SupplierType;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.EmployeeRepository;
import com.realestate.repository.LoginRepository;
import com.realestate.repository.MaterialsPurchasedHistoryRepository;
import com.realestate.repository.MaterialsPurchasedRepository;
import com.realestate.repository.StockRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.StoreRepository;
import com.realestate.repository.StoreStockHistoryRepository;
import com.realestate.repository.StoreStockRepository;
import com.realestate.repository.UserAssignedProjectRepository;
import com.realestate.repository.UserModelRepository;
import com.realestate.response.CommanResponse;
import com.realestate.response.LoginDetails;
import com.realestate.response.MaterialsPurchasedResponse;
import com.realestate.response.PurchasedResponse;
import com.realestate.response.StockResponse;
import com.realestate.response.StoreResponse;
import com.realestate.response.StoreStockHistoryResponse;
import com.realestate.response.StoreStockResponse;

@Controller
@RestController
@RequestMapping("store")
public class StoreController {

	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	LoginRepository loginRepository;
	@Autowired
	UserAssignedProjectRepository userprojectListRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	UserModelRepository userModelRepository;
	@Autowired
	StockRepository stockRepository;
	@Autowired
	StoreRepository storeRepository;
	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	StoreStockHistoryRepository storestockhistoryRepository;
	@Autowired
	StoreStockRepository storestockRepository;
	@Autowired
	MaterialsPurchasedHistoryRepository materialspurchasedhistoryRepository;
	@Autowired
	MaterialsPurchasedRepository materialsPurchasedRepository;


	private String PurchesStoreCode()
	{
		String code;
		long cnt  = storestockRepository.count();
		if(cnt<10)
		{
			code = "SC000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			code = "SC00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			code = "SC0"+(cnt+1);
		}
		else
		{
			code = "SC"+(cnt+1);
		}
		return code;
	}

	long count;
	private long PurchesStoreHistoriCode()
	{

		/*  if(cnt<10)
	        {
	        	code = "PSC000"+(cnt+1);
	        }
	        else if((cnt<100) && (cnt>=10))
	        {
	        	code = "PSC00"+(cnt+1);
	        }
	        else if((cnt<1000) && (cnt>=100))
	        {
	        	code = "PSC0"+(cnt+1);
	        }
	        else if(cnt>=1000)
	        {
	        	code = "PSC"+(cnt+1);
	        }
		 */
		count  = storestockhistoryRepository.count();

		return count;
	}

	@RequestMapping(value = "/getStoreList", method = RequestMethod.POST)
	public ResponseEntity<StoreResponse> getStoreList(@RequestBody LoginDetails loginDetails) {

		StoreResponse response = new StoreResponse();

		try {

			Login login =loginRepository.findByUserNameAndPassWordAndStatus(loginDetails.getUserName(), loginDetails.getPassWord(),"Active");

			if(login!=null)
			{
				List<Store> storeList = storeRepository.findAll();

				response.setStoreList(storeList);
				response.setResponse("Store List");
				response.setStatus(1);

				return new ResponseEntity<StoreResponse>(response,HttpStatus.OK);
			}
			else
			{
				response.setResponse("Login fail");
				response.setStatus(0);

				return new ResponseEntity<StoreResponse>(response,HttpStatus.OK);
			}
		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<StoreResponse>(response,HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/getStoreStockList", method = RequestMethod.POST)
	public ResponseEntity<StoreStockResponse> getStoreStockList(@RequestBody Store store) {

		StoreStockResponse response = new StoreStockResponse();

		try {

			Query query= new Query();
			List<Stock> storestockList = mongoTemplate.find(query.addCriteria(Criteria.where("storeId").is(store.getStoreId())),Stock.class);

			List<StockResponse> storewisestockList = new ArrayList<StockResponse>();
			List<Item> itemDetails= new ArrayList<Item>();

			StockResponse storewisestock=new StockResponse();

			for(int i=0;i<storestockList.size();i++)
			{
				storewisestock=new StockResponse();
				query = new Query();
				itemDetails = mongoTemplate.find(query.addCriteria(Criteria.where("itemId").is(storestockList.get(i).getItemId())), Item.class);

				try
				{
					if(itemDetails.size()!=0)
					{
						storewisestock.setItemId(storestockList.get(i).getItemId());
						//storewisestock.setItemBrandName(storestockList.get(i).getBrandName());
						storewisestock.setItemName(itemDetails.get(0).getItemName());
						query =new Query();
						SupplierType suppliertypeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("suppliertypeId").is(itemDetails.get(0).getSuppliertypeId())), SupplierType.class);

						query =new Query();
						SubSupplierType subsuppliertypeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("subsuppliertypeId").is(itemDetails.get(0).getSubsuppliertypeId())), SubSupplierType.class);

						storewisestock.setStoreId(store.getStoreId());
						storewisestock.setSuppliertypeId(suppliertypeDetails.getSupplierType());
						storewisestock.setSubsuppliertypeId(subsuppliertypeDetails.getSubsupplierType());
						storewisestock.setItemQuantity(storestockList.get(i).getCurrentStock());
						storewisestock.setNoOfPieces(storestockList.get(i).getNoOfPieces());
						storewisestockList.add(storewisestock);
						itemDetails.clear();
					}
				} 
				catch (Exception e) {
					// TODO: handle exception
				}

			}

			response.setStockList(storewisestockList);
			response.setResponse("Store Stock List");
			response.setStatus(1);

			return new ResponseEntity<StoreStockResponse>(response,HttpStatus.OK);
		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<StoreStockResponse>(response,HttpStatus.OK);
		}
	}



	@RequestMapping(value = "/getMaterialsPurchasedList", method = RequestMethod.POST)
	public ResponseEntity<MaterialsPurchasedResponse> getMaterialsPurchasedList(@RequestBody LoginDetails loginDetails) {

		MaterialsPurchasedResponse response = new MaterialsPurchasedResponse();

		try {
			double totalQty=0;
			long totalItem=0;
			List<Employee> employeeList = employeeRepository.findAll();
			Query query = new Query();
			List<MaterialsPurchased> purchesedList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Applied")),MaterialsPurchased.class);

			String empName="";
			for(int i=0;i<purchesedList.size();i++)
			{
				totalQty=0;
				for(int j=0;j<employeeList.size();j++)
				{
					if(purchesedList.get(i).getEmployeeId().equals(employeeList.get(j).getEmployeeId()))
					{
						empName=employeeList.get(j).getEmployeefirstName()+" "+employeeList.get(j).getEmployeemiddleName()+" "+employeeList.get(j).getEmployeelastName();
						break;
					}
				}
				purchesedList.get(i).setEmployeeName(empName);


				query= new Query();
				Store storeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("storeId").is(purchesedList.get(i).getStoreId())),Store.class);

				query= new Query();
				Supplier supplierDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("supplierId").is(purchesedList.get(i).getSupplierId())),Supplier.class);
				purchesedList.get(i).setStoreName(storeDetails.getStoreName());
				purchesedList.get(i).setSupplierfirmName(supplierDetails.getSupplierfirmName());

				query = new Query();
				List<MaterialsPurchasedHistory> materialDetails = mongoTemplate.find(query.addCriteria(Criteria.where("materialsPurchasedId").is(purchesedList.get(i).getMaterialsPurchasedId())),MaterialsPurchasedHistory.class);
				for(int k=0;k<materialDetails.size();k++)
				{
					totalQty=(totalQty+materialDetails.get(k).getItemQuantity());
					totalItem=totalItem+1;		
				}
				purchesedList.get(i).setTotalItem(totalItem);
				purchesedList.get(i).setTotalQty(totalQty);
			}


			response.setMaterialspurchasedList(purchesedList);
			response.setResponse("Materials Purchased List");
			response.setStatus(1);

			return new ResponseEntity<MaterialsPurchasedResponse>(response,HttpStatus.OK);
		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<MaterialsPurchasedResponse>(response,HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/getMaterialsPurchasedDetails", method = RequestMethod.POST)
	public ResponseEntity<PurchasedResponse> getMaterialsPurchasedDetails(@RequestBody MaterialsPurchased materialsPurchased) {

		PurchasedResponse response = new PurchasedResponse();

		try {


			Query query = new Query();
			List<MaterialsPurchasedHistory> materialDetails = mongoTemplate.find(query.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchased.getMaterialsPurchasedId())),MaterialsPurchasedHistory.class);

			Query query1 = new Query();
			MaterialsPurchased materialpurchesDetails = mongoTemplate.findOne(query1.addCriteria(Criteria.where("materialsPurchasedId").is(materialsPurchased.getMaterialsPurchasedId())),MaterialsPurchased.class);

			for(int i=0;i<materialDetails.size();i++)
			{
				query =new Query();
				List<SubSupplierType> subsuppliertypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subsuppliertypeId").is(materialDetails.get(i).getSubsuppliertypeId())), SubSupplierType.class);
				materialDetails.get(i).setSubsuppliertypeId(subsuppliertypeDetails.get(0).getSubsupplierType());
			}

			response.setMaterialsPurchasedId(materialpurchesDetails.getMaterialsPurchasedId());
			response.setRequisitionId(materialpurchesDetails.getRequisitionId());
			response.setSupplierId(materialpurchesDetails.getSupplierId());
			response.setStoreId(materialpurchesDetails.getStoreId());
			response.setEmployeeId(materialpurchesDetails.getEmployeeId());
			response.setTotalAmount(materialpurchesDetails.getTotalAmount());
			response.setTotalDiscount(materialpurchesDetails.getTotalDiscount());
			response.setTotalGstAmount(materialpurchesDetails.getTotalGstAmount());
			response.setTotalPrice(materialpurchesDetails.getTotalPrice());

			response.setMaterialList(materialDetails);
			response.setResponse("Materials Purchased List");
			response.setStatus(1);

			return new ResponseEntity<PurchasedResponse>(response,HttpStatus.OK);
		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<PurchasedResponse>(response,HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/addStock", method = RequestMethod.POST)
	public ResponseEntity<CommanResponse> addStock(@RequestBody StoreStockHistoryResponse storestockhistoryResponse) {

		CommanResponse response = new CommanResponse();

		try {

			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  
			String strDate= formatter.format(date);

			String status1="";
			double presentStock=0.0;
			long presentnoOfPieces=0;
			for(int i=0;i<storestockhistoryResponse.getStockList().size();i++)
			{
				try
				{

					StoreStockHistory storeStockHistory = new StoreStockHistory();
					storeStockHistory.setStoreStockHistoriId(""+PurchesStoreHistoriCode());
					storeStockHistory.setMaterialsPurchasedId(storestockhistoryResponse.getMaterialsPurchasedId());
					storeStockHistory.setStoreId(storestockhistoryResponse.getStoreId());
					storeStockHistory.setItemId(storestockhistoryResponse.getStockList().get(i).getItemId());
					storeStockHistory.setItemUnit(storestockhistoryResponse.getStockList().get(i).getItemUnit());
					storeStockHistory.setItemSize(storestockhistoryResponse.getStockList().get(i).getItemSize());
					storeStockHistory.setItemBrandName(storestockhistoryResponse.getStockList().get(i).getItemBrandName());
					storeStockHistory.setItemQuantity(storestockhistoryResponse.getStockList().get(i).getActualQuantity());
					storeStockHistory.setRemark(storestockhistoryResponse.getStockList().get(i).getRemark());
					storeStockHistory.setNoOfPieces(storestockhistoryResponse.getStockList().get(i).getNoOfPieces());
					storeStockHistory.setCreationDate(strDate);

					storestockhistoryRepository.save(storeStockHistory);
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}

				Query query = new Query();
				MaterialsPurchasedHistory materialspurchasedhistory=new MaterialsPurchasedHistory();
				materialspurchasedhistory = mongoTemplate.findOne(query.addCriteria(Criteria.where("materialPurchesHistoriId").is(storestockhistoryResponse.getStockList().get(i).getMaterialPurchesHistoriId())), MaterialsPurchasedHistory.class);

				if(storestockhistoryResponse.getStockList().get(i).getActualQuantity()==storestockhistoryResponse.getStockList().get(i).getItemQuantity())
				{
					materialspurchasedhistory.setStatus("Done");
					materialspurchasedhistoryRepository.save(materialspurchasedhistory);
				}
				else
				{
					materialspurchasedhistory.setStatus("InCompleted");
					materialspurchasedhistoryRepository.save(materialspurchasedhistory);
				}
				try
				{
					Query query1 = new Query();
					List<Stock> stockList = mongoTemplate.find(query1.addCriteria(Criteria.where("itemId").is(storestockhistoryResponse.getStockList().get(i).getItemId()).and("storeId").is(storestockhistoryResponse.getStoreId())),Stock.class);
					if(stockList.size()!=0)
					{
						presentStock=stockList.get(0).getCurrentStock();
						presentnoOfPieces=stockList.get(0).getNoOfPieces();
						presentStock=presentStock+storestockhistoryResponse.getStockList().get(i).getActualQuantity();
						presentnoOfPieces=presentnoOfPieces+storestockhistoryResponse.getStockList().get(i).getNoOfPieces();
						stockList.get(0).setCurrentStock(presentStock);
						stockList.get(0).setNoOfPieces(presentnoOfPieces);
						stockList.get(0).setUpdateDate(strDate);
						stockRepository.save(stockList.get(0));
					}
					else
					{
						String stockCode="";
						long cnt  = stockRepository.count();
						if(cnt<10)
						{
							stockCode = "SK000"+(cnt+1);
						}
						else if((cnt<100) && (cnt>=10))
						{
							stockCode = "SK00"+(cnt+1);
						}
						else if((cnt<1000) && (cnt>=100))
						{
							stockCode = "SK0"+(cnt+1);
						}
						else if(cnt>=1000)
						{
							stockCode = "SK"+(cnt+1);
						}
						Stock stock1=new Stock();
						stock1.setStockId(stockCode);	
						stock1.setStoreId(storestockhistoryResponse.getStoreId());
						stock1.setItemId(storestockhistoryResponse.getStockList().get(i).getItemId());
						//stock1.setBrandName(itemBrandName);
						stock1.setCurrentStock(storestockhistoryResponse.getStockList().get(i).getActualQuantity());
						stock1.setNoOfPieces(storestockhistoryResponse.getStockList().get(i).getNoOfPieces());
						stock1.setCreationDate(strDate);
						stockRepository.save(stock1);
					}
				}
				catch (Exception e) {
					e.printStackTrace();
				}

			}


			StoreStock storeStock=new StoreStock();
			storeStock.setStoreStockId(PurchesStoreCode());
			storeStock.setStoreId(storestockhistoryResponse.getStoreId());
			storeStock.setMaterialsPurchasedId(storestockhistoryResponse.getMaterialsPurchasedId());
			storeStock.setChallanNo(storestockhistoryResponse.getChallanNo());
			storeStock.setEwayBillNo(storestockhistoryResponse.getEwayBillNo());
			storeStock.setVehicleNumber(storestockhistoryResponse.getVehicleNumber());
			storeStock.setLoadedWeight(storestockhistoryResponse.getLoadedWeight());
			storeStock.setUnloadedWeight(storestockhistoryResponse.getUnloadedWeight());
			storeStock.setTotalMaterialWeight(storestockhistoryResponse.getTotalMaterialWeight());

			storeStock.setRemark(storestockhistoryResponse.getRemark());
			storeStock.setCreationDate(strDate);
			storestockRepository.save(storeStock);


			for(int i=0;i<storestockhistoryResponse.getStockList().size();i++)
			{
				if(storestockhistoryResponse.getStockList().get(i).getActualQuantity()==storestockhistoryResponse.getStockList().get(i).getItemQuantity())
				{
					status1="Completed";
				}
				else
				{
					status1="InCompleted";
				}
			}

			try {
				MaterialsPurchased materialspurchased;

				materialspurchased = mongoTemplate.findOne(Query.query(Criteria.where("materialsPurchasedId").is(storestockhistoryResponse.getMaterialsPurchasedId())), MaterialsPurchased.class);
				materialspurchased.setStatus(status1);
				materialsPurchasedRepository.save(materialspurchased);
			}
			catch (Exception e) {
				e.printStackTrace();
			}

			response.setResponse("Stock successfully added");
			response.setStatus(1);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
		} catch (Exception ex) {
			ex.printStackTrace();

			response.setResponse("Stock added fail");
			response.setStatus(0);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
		}
	}


}
