package com.realestate.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.Agent;
import com.realestate.bean.Booking;
import com.realestate.bean.CustomerLoanDetails;
import com.realestate.bean.CustomerPaymentDetails;
import com.realestate.bean.CustomerReceiptForm;
import com.realestate.bean.Enquiry;
import com.realestate.bean.EnquiryFollowUp;
import com.realestate.bean.GeneratePaymentScheduler;
import com.realestate.repository.CustomerPaymentDetailsRepository;
import com.realestate.repository.CustomerReceiptFormRepository;
import com.realestate.response.BookingIdResponse;
import com.realestate.response.BookingPaymentRsponse;
import com.realestate.response.CommanResponse;
import com.realestate.response.CustomerPaymentResponse;
import com.realestate.response.EnquiryResponse;
import com.realestate.response.LoginDetails;
import com.realestate.services.SmsServices;

@Controller
@RestController
@RequestMapping("payment")
public class CustomerPaymentController {


	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	CustomerReceiptFormRepository customerreceiptformrepository;
	@Autowired
	CustomerPaymentDetailsRepository customerpaymentdetailsrepository;

	String payAmountCode;
	private String customerpayAmountCode()
	{
		long Count = customerpaymentdetailsrepository.count();

		if(Count<10)
		{
			payAmountCode = "TP000"+(Count+1);
		}
		else if((Count>=10) && (Count<100))
		{
			payAmountCode = "TP00"+(Count+1);
		}
		else if((Count>=100) && (Count<1000))
		{
			payAmountCode = "TP0"+(Count+1);
		}
		else
		{
			payAmountCode = "TP"+(Count+1);
		}

		return payAmountCode;
	}

	@RequestMapping(value = "/getCustomerPaymentDetails", method = RequestMethod.POST)
	public ResponseEntity<CustomerPaymentResponse> getCustomerPaymentDetails(@RequestBody BookingIdResponse bookingIdResponse) {

		CustomerPaymentResponse response = new CustomerPaymentResponse();

		String loanAmountAgainst="";
		long loanAmount=0;

		try {



			Query query = new Query();
			Booking bookingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("bookingId").is(bookingIdResponse.getBookingId())),Booking.class);
			query = new Query();
			CustomerPaymentDetails customerPayment = mongoTemplate.findOne(query.addCriteria(Criteria.where("bookingId").is(bookingIdResponse.getBookingId())),CustomerPaymentDetails.class);

			query = new Query();
			List<GeneratePaymentScheduler> generatepaymentscheduler = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingIdResponse.getBookingId())),GeneratePaymentScheduler.class);

			query = new  Query();
			List<CustomerLoanDetails> loanDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingIdResponse.getBookingId())),CustomerLoanDetails.class);


			try {
				if(loanDetails.size()!=0)
				{
					loanAmount=loanDetails.get(0).getLoanAmount().longValue();

					if(loanDetails.get(0).getAgainstAggreement().equals("AggreementAgainst"))
					{
						loanAmountAgainst=loanAmountAgainst+"Agreement  ";
					}
					if(loanDetails.get(0).getAgainstRegistration().equals("RegistrationAgainst"))
					{
						loanAmountAgainst=loanAmountAgainst+"Registration  ";
					}
					if(loanDetails.get(0).getAgainstStampDuty().equals("StampDutyAgainst"))
					{
						loanAmountAgainst=loanAmountAgainst+"Stamp Duty  ";
					}
					if(loanDetails.get(0).getAgainstGST().equals("GSTAgainst"))
					{
						loanAmountAgainst=loanAmountAgainst+"GST  ";
					}
				}
			}
			catch (Exception e) {
				// TODO: handle exception
			}

			Double totalpayAggreement=0.0;
			Double totalpayStampDuty=0.0;
			Double totalpayRegistration=0.0;
			Double totalpaygstAmount=0.0;
			try {
				if(customerPayment!=null)
				{
					totalpayAggreement=customerPayment.getTotalpayAggreement();
					totalpayStampDuty=customerPayment.getTotalpayStampDuty();
					totalpayRegistration=customerPayment.getTotalpayRegistration();
					totalpaygstAmount=customerPayment.getTotalpaygstAmount();
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}

			Double reamaingAggreement=bookingDetails.getAggreementValue1()-totalpayAggreement;
			Double reamaingstampDuty=bookingDetails.getStampDuty1()-totalpayStampDuty;
			Double reamaingRegistration=bookingDetails.getRegistrationCost1()-totalpayRegistration;
			Double reamaingGstAmount=bookingDetails.getGstAmount1()-totalpaygstAmount;
			Double totalRemainingAmount=bookingDetails.getGrandTotal1()-totalpayAggreement-totalpayStampDuty-totalpayRegistration-totalpaygstAmount;

			response.setBookingId(bookingDetails.getBookingId());
			response.setBookingName(bookingDetails.getBookingfirstname());
			response.setInfrastructureCharge(bookingDetails.getInfrastructureCharge());
			response.setAggreementValue1(bookingDetails.getAggreementValue1());
			response.setHandlingCharges(bookingDetails.getHandlingCharges());
			response.setStampDutyPer(bookingDetails.getStampDutyPer());
			response.setStampDuty1(bookingDetails.getStampDuty1());
			response.setRegistrationPer(bookingDetails.getRegistrationPer());
			response.setRegistrationCost1(bookingDetails.getRegistrationCost1());
			response.setGstCost(bookingDetails.getGstCost());
			response.setGstAmount1(bookingDetails.getGstAmount1());
			response.setGrandTotal1(bookingDetails.getGrandTotal1());
			response.setTds(bookingDetails.getTds());

			response.setLoanAmount(loanAmount);
			response.setLoanAmountAgainst(loanAmountAgainst);

			response.setTotalpayAggreement(totalpayAggreement);
			response.setTotalpaygstAmount(totalpaygstAmount);
			response.setTotalpayRegistration(totalpayRegistration);
			response.setTotalpayStampDuty(totalpayStampDuty);

			response.setReamaingAggreement(reamaingAggreement);
			response.setReamaingGstAmount(reamaingGstAmount);
			response.setReamaingRegistration(reamaingRegistration);
			response.setReamaingstampDuty(reamaingstampDuty);
			response.setTotalRemainingAmount(totalRemainingAmount);

			response.setResponse("Customer Payment Details");
			response.setStatus(1);

			return new ResponseEntity<CustomerPaymentResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			ex.printStackTrace();
			response.setResponse("Login fail");
			response.setStatus(0);
			return new ResponseEntity<CustomerPaymentResponse>(response,HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/addCustomerPayment", method = RequestMethod.POST)
	public ResponseEntity<CommanResponse> addEnquiry(@RequestBody BookingPaymentRsponse bookingpaymentRsponse) {

		CommanResponse response = new CommanResponse();

		try {
			String bookingId=bookingpaymentRsponse.getBookingId();
			double aggreementAmount=bookingpaymentRsponse.getAggreementAmount();
			double stampDutyAmount=bookingpaymentRsponse.getStampDutyAmount();
			double registrationAmount=bookingpaymentRsponse.getRegistrationAmount();
			double gstAmount=bookingpaymentRsponse.getGstAmount();
			
			Query query = new Query();
			String mobileNumber="";
			String sms="";

			try
			{
				query = new Query();
				Booking bookingList;
				bookingList = mongoTemplate.findOne(Query.query(Criteria.where("bookingId").is(bookingId)), Booking.class);
				mobileNumber=bookingList.getBookingmobileNumber1();
				sms="Dear "+bookingList.getBookingfirstname()+", thanks for paid amount under the ";
				/*
				 query =new Query();
				 List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(bookingList.getProjectId())), Project.class);

				 query =new Query();
				 List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(bookingList.getBuildingId())), ProjectBuilding.class);

				 query =new Query();
				 List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(bookingList.getWingId())), ProjectWing.class);

			     String projectName=projectDetails.get(0).getProjectName();
			     String buildingName=buildingDetails.get(0).getBuildingName();
			     String wingName=wingDetails.get(0).getWingName();
				 */
				String one = "REC";
				one = one +"/"+bookingId+"/";
				String receiptCode="";

				Date todayDate = new Date();  

				/*SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  
				 String strDate= formatter.format(date);
				 */
				CustomerReceiptForm customerreceiptform=new CustomerReceiptForm();
				Double paymentAmount;
				String paymentType;
				if(aggreementAmount!=0.0)
				{
					sms=sms+" Agreement amount "+aggreementAmount +"/- ";
					query = new Query();
					long receiptCount = mongoTemplate.count(query.addCriteria(Criteria.where("bookingId").is(bookingId)), CustomerReceiptForm.class);
					if(receiptCount<10)
					{
						receiptCode = one+"0"+(receiptCount+1);
					}
					else 
					{
						receiptCode = one+(receiptCount+1);
					}

					paymentType="Agreement";
					paymentAmount=aggreementAmount;
					customerreceiptform.setReceiptId(receiptCode);
					customerreceiptform.setBookingId(bookingId);
					customerreceiptform.setPaymentAmount(paymentAmount);
					customerreceiptform.setPaymentType(paymentType);
					customerreceiptform.setPaymentMode(bookingpaymentRsponse.getPaymentMode());
					customerreceiptform.setBankName(bookingpaymentRsponse.getBankName());
					customerreceiptform.setBranchName(bookingpaymentRsponse.getBranchName());
					customerreceiptform.setChequeNumber(bookingpaymentRsponse.getChequeNumber());
					customerreceiptform.setNarration(bookingpaymentRsponse.getNarration());
					customerreceiptform.setDate(bookingpaymentRsponse.getDate());
					customerreceiptform.setCreationDate(todayDate);
					customerreceiptform.setStatus("Not Deposited");
					customerreceiptformrepository.save(customerreceiptform);
				}

				if(stampDutyAmount!=0.0)
				{
					sms=sms+" Stamp Duty amount "+stampDutyAmount+"/- ";
					query = new Query();
					long receiptCount = mongoTemplate.count(query.addCriteria(Criteria.where("bookingId").is(bookingId)), CustomerReceiptForm.class);
					if(receiptCount<10)
					{
						receiptCode = one+"0"+(receiptCount+1);
					}
					else 
					{
						receiptCode = one+(receiptCount+1);
					}
					customerreceiptform=new CustomerReceiptForm();
					paymentType="Stamp Duty";
					paymentAmount=stampDutyAmount;
					customerreceiptform.setReceiptId(receiptCode);
					customerreceiptform.setBookingId(bookingId);
					customerreceiptform.setPaymentAmount(paymentAmount);
					customerreceiptform.setPaymentType(paymentType);
					customerreceiptform.setPaymentMode(bookingpaymentRsponse.getPaymentMode());
					customerreceiptform.setBankName(bookingpaymentRsponse.getBankName());
					customerreceiptform.setBranchName(bookingpaymentRsponse.getBranchName());
					customerreceiptform.setChequeNumber(bookingpaymentRsponse.getChequeNumber());
					customerreceiptform.setNarration(bookingpaymentRsponse.getChequeNumber());
					customerreceiptform.setDate(bookingpaymentRsponse.getDate());
					customerreceiptform.setCreationDate(todayDate);
					customerreceiptform.setStatus("Not Deposited");
					customerreceiptformrepository.save(customerreceiptform);
				}
				if(registrationAmount!=0.0)
				{
					sms=sms+" Registration amount "+registrationAmount+"/-";
					query = new Query();
					long receiptCount = mongoTemplate.count(query.addCriteria(Criteria.where("bookingId").is(bookingId)), CustomerReceiptForm.class);
					if(receiptCount<10)
					{
						receiptCode = one+"0"+(receiptCount+1);
					}
					else 
					{
						receiptCode = one+(receiptCount+1);
					}
					customerreceiptform=new CustomerReceiptForm();
					paymentType="Registration";
					paymentAmount=registrationAmount;
					customerreceiptform.setReceiptId(receiptCode);
					customerreceiptform.setBookingId(bookingId);
					customerreceiptform.setPaymentAmount(paymentAmount);
					customerreceiptform.setPaymentType(paymentType);
					customerreceiptform.setPaymentMode(bookingpaymentRsponse.getPaymentMode());
					customerreceiptform.setBankName(bookingpaymentRsponse.getBankName());
					customerreceiptform.setBranchName(bookingpaymentRsponse.getBranchName());
					customerreceiptform.setChequeNumber(bookingpaymentRsponse.getChequeNumber());
					customerreceiptform.setNarration(bookingpaymentRsponse.getNarration());
					customerreceiptform.setDate(bookingpaymentRsponse.getDate());
					customerreceiptform.setCreationDate(todayDate);
					customerreceiptform.setStatus("Not Deposited");
					customerreceiptformrepository.save(customerreceiptform);
				}
				if(gstAmount!=0.0)
				{
					sms=sms+" GST amount "+gstAmount+"/-";
					query = new Query();
					long receiptCount = mongoTemplate.count(query.addCriteria(Criteria.where("bookingId").is(bookingId)), CustomerReceiptForm.class);
					if(receiptCount<10)
					{
						receiptCode = one+"0"+(receiptCount+1);
					}
					else 
					{
						receiptCode = one+(receiptCount+1);
					}
					customerreceiptform=new CustomerReceiptForm();
					paymentType="GST";
					paymentAmount=gstAmount;
					customerreceiptform.setReceiptId(receiptCode);
					customerreceiptform.setBookingId(bookingId);
					customerreceiptform.setPaymentAmount(paymentAmount);
					customerreceiptform.setPaymentType(paymentType);
					customerreceiptform.setPaymentMode(bookingpaymentRsponse.getPaymentMode());
					customerreceiptform.setBankName(bookingpaymentRsponse.getBankName());
					customerreceiptform.setBranchName(bookingpaymentRsponse.getBranchName());
					customerreceiptform.setChequeNumber(bookingpaymentRsponse.getChequeNumber());
					customerreceiptform.setNarration(bookingpaymentRsponse.getNarration());
					customerreceiptform.setDate(bookingpaymentRsponse.getDate());
					customerreceiptform.setCreationDate(todayDate);
					customerreceiptform.setStatus("Not Deposited");
					customerreceiptformrepository.save(customerreceiptform);
				}
				Double totalpayAggreement=0.0;
				Double totalpayStampDuty=0.0;
				Double totalpayRegistration=0.0;
				Double totalpaygstAmount=0.0;
				Query query4 = new Query();
				List<CustomerPaymentDetails> customerpaymentdetails = mongoTemplate.find(query4.addCriteria(Criteria.where("bookingId").is(bookingId)),CustomerPaymentDetails.class);
				if(customerpaymentdetails.size()!=0)
				{
					totalpayAggreement=customerpaymentdetails.get(0).getTotalpayAggreement()+aggreementAmount;
					totalpayStampDuty=customerpaymentdetails.get(0).getTotalpayStampDuty()+stampDutyAmount;
					totalpayRegistration=customerpaymentdetails.get(0).getTotalpayRegistration()+registrationAmount;
					totalpaygstAmount=customerpaymentdetails.get(0).getTotalpaygstAmount()+gstAmount;
					String amountStatus;
					if(bookingList.getAggreementValue1()==totalpayAggreement && bookingList.getStampDuty1()==totalpayStampDuty && bookingList.getRegistrationCost1()==totalpayRegistration && bookingList.getGstAmount1()==totalpaygstAmount)
					{
						amountStatus="Completed";
					}
					else
					{
						amountStatus="Pending";
					}
					try
					{
						customerpaymentdetails.get(0).setTotalpayAggreement(totalpayAggreement);
						customerpaymentdetails.get(0).setTotalpayStampDuty(totalpayStampDuty);
						customerpaymentdetails.get(0).setTotalpayRegistration(totalpayRegistration);
						customerpaymentdetails.get(0).setTotalpaygstAmount(totalpaygstAmount);
						customerpaymentdetails.get(0).setAmountStatus(amountStatus);
						customerpaymentdetails.get(0).setCreationDate(todayDate);
						customerpaymentdetails.get(0).setUpdateDate(todayDate);
						//customerpaymentdetails.getUserName();
						customerpaymentdetailsrepository.save(customerpaymentdetails);
					}
					catch(Exception ee)
					{
					}
				}
				else
				{
					String amountStatus;
					if(bookingList.getAggreementValue1()==aggreementAmount && bookingList.getStampDuty1()==stampDutyAmount && bookingList.getRegistrationCost1()==registrationAmount && bookingList.getGstAmount1()==gstAmount)
					{
						amountStatus="Completed";
					}
					else
					{
						amountStatus="Pending";
					}

					CustomerPaymentDetails customerpaymentdetails1=new CustomerPaymentDetails();
					customerpaymentdetails1.setId(customerpayAmountCode());
					customerpaymentdetails1.setBookingId(bookingId);
					customerpaymentdetails1.setTotalpayAggreement(aggreementAmount);
					customerpaymentdetails1.setTotalpayStampDuty(stampDutyAmount);
					customerpaymentdetails1.setTotalpayRegistration(registrationAmount);
					customerpaymentdetails1.setTotalpaygstAmount(gstAmount);
					customerpaymentdetails1.setAmountStatus(amountStatus);
					customerpaymentdetails1.setCreationDate(todayDate);
					customerpaymentdetails1.setUpdateDate(todayDate);
					//customerpaymentdetails.getUserName();
					customerpaymentdetailsrepository.save(customerpaymentdetails1);
				}
			}
			catch (Exception e) {
				System.out.println("Error = "+e);
				// TODO: handle exception
			}

			try {
				sms=sms+".";
				//SmsServices.sendSMS(mobileNumber,sms);
			}catch (Exception e) {
				// TODO: handle exception
			}

			response.setResponse("Customer Payment successfully added");
			response.setStatus(1);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			response.setResponse("Payment added fail");
			response.setStatus(0);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
		}
	}


}
