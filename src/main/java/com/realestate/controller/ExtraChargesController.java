package com.realestate.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.BankTransactionHistoryDetails;
import com.realestate.bean.Booking;
import com.realestate.bean.CompanyAccountPaymentDetails;
import com.realestate.bean.CustomerOtherPaymentDetails;
import com.realestate.bean.CustomerPaymentDetails;
import com.realestate.bean.CustomerPaymentStatusHistory;
import com.realestate.bean.CustomerReceiptForm;
import com.realestate.bean.ExtraCharges;
import com.realestate.bean.Item;
import com.realestate.bean.Login;
import com.realestate.bean.MaterialPurchaseRequisition;
import com.realestate.bean.MaterialPurchaseRequisitionHistory;
import com.realestate.bean.MultipleOtherCharges;
import com.realestate.bean.SubSupplierType;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.BookingRepository;
import com.realestate.repository.CustomerOtherPaymentDetailsRepository;
import com.realestate.repository.CustomerReceiptFormRepository;
import com.realestate.repository.ExtraChargeRepository;
import com.realestate.repository.LoginRepository;
import com.realestate.repository.UserAssignedProjectRepository;
import com.realestate.response.LoginDetails;
import com.realestate.response.MaterialRequisitionResponse;
import com.realestate.response.CommanResponse;
import com.realestate.response.ExtraChargesListResponse;
import com.realestate.response.ExtraChargesResponse;
import com.realestate.response.ExtraPaymentResponse;
import com.realestate.response.RequisitionResponse;
import com.realestate.response.UpdatePaymentStatusResponse;

@Controller
@RestController
@RequestMapping("extracharges")
public class ExtraChargesController {


	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired 
	BookingRepository bookingRepository;
	@Autowired 
	ExtraChargeRepository extrachargesRepository;
	@Autowired
	UserAssignedProjectRepository userprojectListRepository;
	@Autowired
	LoginRepository loginRepository;
	@Autowired
	CustomerOtherPaymentDetailsRepository customerOtherPaymentDetailsRepository;
	@Autowired
	CustomerReceiptFormRepository customerreceiptformrepository;

	String Code;
	private String extraCode()
	{
		long Count = extrachargesRepository.count();

		if(Count<10)
		{
			Code = "EC000"+(Count+1);
		}
		else if((Count>=10) && (Count<100))
		{
			Code = "EC00"+(Count+1);
		}
		else if((Count>=100) && (Count<1000))
		{
			Code = "EC0"+(Count+1);
		}
		else
		{
			Code = "EC"+(Count+1);
		}

		return Code;
	}

	String payAmountCode;
	private String customerpayAmountCode()
	{
		long Count = customerOtherPaymentDetailsRepository.count();

		if(Count<10)
		{
			payAmountCode = "TP000"+(Count+1);
		}
		else if((Count>=10) && (Count<100))
		{
			payAmountCode = "TP00"+(Count+1);
		}
		else if((Count>=100) && (Count<1000))
		{
			payAmountCode = "TP0"+(Count+1);
		}
		else if(Count>1000)
		{
			payAmountCode = "TP"+(Count+1);
		}

		return payAmountCode;
	}


	@RequestMapping(value = "/getExtraChargesList", method = RequestMethod.POST)
	public ResponseEntity<ExtraChargesListResponse> getExtraChargesList(@RequestBody LoginDetails loginDetails) {

		ExtraChargesListResponse response = new ExtraChargesListResponse();

		try {
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");  
			Login login =loginRepository.findByUserNameAndPassWordAndStatus(loginDetails.getUserName(), loginDetails.getPassWord(),"Active");

			if(login!=null)
			{

				int i=0,j=0;
				List<UserAssignedProject> userLoginProjectList=userprojectListRepository.findByEmployeeId(login.getEmployeeId());
				List<ExtraCharges> extrachargesList1 = extrachargesRepository.findAll(); 

				List<ExtraChargesResponse> extrachargesList=new ArrayList<ExtraChargesResponse>();
				ExtraChargesResponse extracharges=new ExtraChargesResponse();

				long totalExtraCharges = 0;
				long reamaingMaintainance=0, reamaingHandling=0, reamaingExtraCharges=0;
				long maintPaidAmount=0, handlingPaidAmount=0, extraChargesPaidAmount=0;

				Query query = new Query();
				for(int k=0;k<userLoginProjectList.size();k++)
				{
					query = new Query();
					List<Booking> bookingList = mongoTemplate.find(query.addCriteria(Criteria.where("bookingstatus").ne("Cancel").and("projectId").is(userLoginProjectList.get(k).getProjectId())), Booking.class);

					for(i=0;i<bookingList.size();i++)
					{
						for(j=0;j<extrachargesList1.size();j++)
						{
							if(bookingList.get(i).getBookingId().equals(extrachargesList1.get(j).getBookingId()))
							{
								query = new Query();
								ExtraCharges extraChargesDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("chargesId").is(extrachargesList1.get(j).getChargesId())), ExtraCharges.class); 

								query = new Query();
								List<MultipleOtherCharges> multipleOtherCharges = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingList.get(i).getBookingId())),MultipleOtherCharges.class);
								totalExtraCharges = 0;
								for(int m=0;m<multipleOtherCharges.size();m++)
								{
									totalExtraCharges=(long) (totalExtraCharges+multipleOtherCharges.get(m).getAmount());
								}

								maintPaidAmount=0;
								handlingPaidAmount=0;
								extraChargesPaidAmount=0;
								query = new Query();
								List<CustomerOtherPaymentDetails> customerPayment = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(bookingList.get(i).getBookingId())),CustomerOtherPaymentDetails.class);
								for(int m=0;m<customerPayment.size();m++)
								{
									maintPaidAmount=maintPaidAmount+customerPayment.get(m).getTotalPaidMaintainanceAmt();
									handlingPaidAmount=handlingPaidAmount+customerPayment.get(m).getTotalPaidHandlingCharges();
									extraChargesPaidAmount=extraChargesPaidAmount+customerPayment.get(m).getTotalPaidExtraCharges();
								}

								reamaingMaintainance=0;
								reamaingHandling=0;
								reamaingExtraCharges=0;

								reamaingMaintainance=extraChargesDetails.getMaintenanceCost().longValue()-maintPaidAmount;
								reamaingHandling=bookingList.get(i).getHandlingCharges().longValue()-handlingPaidAmount;
								reamaingExtraCharges=totalExtraCharges-extraChargesPaidAmount;

								extracharges=new ExtraChargesResponse();
								extracharges.setChargesId(extrachargesList1.get(j).getChargesId());
								extracharges.setBookingId(extrachargesList1.get(j).getBookingId());
								extracharges.setBookingDate(dateFormat.format(bookingList.get(i).getCreationDate()));
								extracharges.setCustomerName(extrachargesList1.get(j).getCustomerName());
								extracharges.setAggAmount(bookingList.get(i).getAggreementValue1().longValue());

								extracharges.setMaintTotalAmount(extraChargesDetails.getMaintenanceCost().longValue());
								extracharges.setMaintPaidAmount(maintPaidAmount);
								extracharges.setMaintRemainingAmount(reamaingMaintainance);

								extracharges.setHandlingTotalAmount(bookingList.get(i).getHandlingCharges().longValue());
								extracharges.setHandlingPaidAmount(handlingPaidAmount);
								extracharges.setHandlingRemainingAmount(reamaingHandling);

								extracharges.setExtraChargesTotalAmount(totalExtraCharges);
								extracharges.setExtraChargesPaidAmount(extraChargesPaidAmount);
								extracharges.setExtraChargesRemainingAmount(reamaingExtraCharges);

								extracharges.setCreationDate(extrachargesList1.get(j).getCreationDate());
								extracharges.setUpdateDate(extrachargesList1.get(j).getUpdateDate());

								extrachargesList.add(extracharges);

							}

						}
					}
				}
				response.setCustomerList(extrachargesList);
				response.setResponse("Customer List");
				response.setStatus(1);

			}
			else
			{
				response.setResponse("Login Fail");
				response.setStatus(0);

			}
			return new ResponseEntity<ExtraChargesListResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			ex.printStackTrace();
			response.setResponse("Login Fail");
			response.setStatus(0);

			return new ResponseEntity<ExtraChargesListResponse>(response,HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/addCustomerExtraPayment", method = RequestMethod.POST)
	public ResponseEntity<CommanResponse> updatePaymentStatus(@RequestBody ExtraPaymentResponse extrapaymentResponse) {

		CommanResponse response = new CommanResponse();
		try {

			//for getting handling charges from booking
			Query query = new Query();
			Booking bookingList = mongoTemplate.findOne(query.addCriteria((Criteria.where("bookingId").is(extrapaymentResponse.getBookingId()))), Booking.class);

			//for getting extra charges
			query = new Query();
			ExtraCharges extraCharges = mongoTemplate.findOne(query.addCriteria(Criteria.where("bookingId").is(extrapaymentResponse.getBookingId())), ExtraCharges.class);

			/*String projectName=bookingList.getProjectName();
			     String buildingName=bookingList.getBuildingName();
			     String wingName=bookingList.getWingName();*/

			String one = "REC";
			one = one +"/"+extrapaymentResponse.getBookingId()+"/";
			String receiptCode="";

			Date todayDate = new Date();  

			CustomerReceiptForm customerreceiptform = new CustomerReceiptForm();

			Double paymentAmount;
			String paymentType;

			if(extrapaymentResponse.getMaintainanceAmt()!=0.0)
			{
				query = new Query();
				long receiptCount = mongoTemplate.count(query.addCriteria(Criteria.where("bookingId").is(extrapaymentResponse.getBookingId())), CustomerReceiptForm.class);
				if(receiptCount<10)
				{
					receiptCode = one+"0"+(receiptCount+1);
				}
				else 
				{
					receiptCode = one+(receiptCount+1);
				}

				paymentType="Maintainance";
				paymentAmount=extrapaymentResponse.getMaintainanceAmt();
				customerreceiptform.setReceiptId(receiptCode);
				customerreceiptform.setBookingId(extrapaymentResponse.getBookingId());
				customerreceiptform.setPaymentAmount(paymentAmount);
				customerreceiptform.setPaymentType(paymentType);
				customerreceiptform.setPaymentMode(extrapaymentResponse.getPaymentMode());
				customerreceiptform.setBankName(extrapaymentResponse.getBankName());
				customerreceiptform.setBranchName(extrapaymentResponse.getBranchName());
				customerreceiptform.setChequeNumber(extrapaymentResponse.getChequeNumber());
				customerreceiptform.setNarration(extrapaymentResponse.getNarration());
				customerreceiptform.setCreationDate(todayDate);
				customerreceiptform.setDate(extrapaymentResponse.getDate());
				customerreceiptform.setUserName(extrapaymentResponse.getUserId());
				customerreceiptformrepository.save(customerreceiptform);
			}

			if(extrapaymentResponse.getHandlingChargesAmt()!=0.0)
			{
				query = new Query();
				long receiptCount = mongoTemplate.count(query.addCriteria(Criteria.where("bookingId").is(extrapaymentResponse.getBookingId())), CustomerReceiptForm.class);
				if(receiptCount<10)
				{
					receiptCode = one+"0"+(receiptCount+1);
				}
				else 
				{
					receiptCode = one+(receiptCount+1);
				}
				customerreceiptform = new CustomerReceiptForm();
				paymentType="Handling Charge";
				paymentAmount=extrapaymentResponse.getHandlingChargesAmt();
				customerreceiptform.setReceiptId(receiptCode);
				customerreceiptform.setBookingId(extrapaymentResponse.getBookingId());
				customerreceiptform.setPaymentAmount(paymentAmount);
				customerreceiptform.setPaymentType(paymentType);
				customerreceiptform.setPaymentMode(extrapaymentResponse.getPaymentMode());
				customerreceiptform.setBankName(extrapaymentResponse.getBankName());
				customerreceiptform.setBranchName(extrapaymentResponse.getBranchName());
				customerreceiptform.setChequeNumber(extrapaymentResponse.getChequeNumber());
				customerreceiptform.setNarration(extrapaymentResponse.getNarration());
				customerreceiptform.setCreationDate(todayDate);
				customerreceiptform.setDate(extrapaymentResponse.getDate());
				customerreceiptform.setUserName(extrapaymentResponse.getUserId());
				customerreceiptformrepository.save(customerreceiptform);
			}
			if(extrapaymentResponse.getExtraChargesAmt()!=0.0)
			{
				query = new Query();

				long receiptCount = mongoTemplate.count(query.addCriteria(Criteria.where("bookingId").is(extrapaymentResponse.getBookingId())), CustomerReceiptForm.class);

				if(receiptCount<10)
				{
					receiptCode = one+"0"+(receiptCount+1);
				}
				else 
				{
					receiptCode = one+(receiptCount+1);
				}

				customerreceiptform = new CustomerReceiptForm();

				paymentType="Extra Charge";

				paymentAmount=extrapaymentResponse.getExtraChargesAmt();

				customerreceiptform.setReceiptId(receiptCode);
				customerreceiptform.setBookingId(extrapaymentResponse.getBookingId());
				customerreceiptform.setPaymentAmount(paymentAmount);
				customerreceiptform.setPaymentType(paymentType);
				customerreceiptform.setPaymentMode(extrapaymentResponse.getPaymentMode());
				customerreceiptform.setBankName(extrapaymentResponse.getBankName());
				customerreceiptform.setBranchName(extrapaymentResponse.getBranchName());
				customerreceiptform.setChequeNumber(extrapaymentResponse.getChequeNumber());
				customerreceiptform.setNarration(extrapaymentResponse.getNarration());
				customerreceiptform.setCreationDate(todayDate);
				customerreceiptform.setDate(extrapaymentResponse.getDate());
				customerreceiptform.setUserName(extrapaymentResponse.getUserId());
				customerreceiptformrepository.save(customerreceiptform);
			}

			Double totalPaidMaintainanceAmt=0.0;
			Double totalPaidHandlingCharges=0.0;
			Double totalPaidExtraCharges=0.0;

			query = new Query();
			List<CustomerOtherPaymentDetails> customerOtherPaymentDetails = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(extrapaymentResponse.getBookingId())),CustomerOtherPaymentDetails.class);

			if(customerOtherPaymentDetails.size()!=0)
			{
				totalPaidMaintainanceAmt = customerOtherPaymentDetails.get(0).getTotalPaidMaintainanceAmt() + extrapaymentResponse.getMaintainanceAmt();
				totalPaidHandlingCharges = customerOtherPaymentDetails.get(0).getTotalPaidHandlingCharges() + extrapaymentResponse.getHandlingChargesAmt();
				totalPaidExtraCharges    = customerOtherPaymentDetails.get(0).getTotalPaidExtraCharges() + extrapaymentResponse.getExtraChargesAmt();

				long totalExtraCharges = 0;

				query = new Query();
				List<MultipleOtherCharges> multipleOtherCharges = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(extrapaymentResponse.getBookingId())), MultipleOtherCharges.class);

				for(int i=0;i<multipleOtherCharges.size();i++)
				{
					totalExtraCharges = (long) (totalExtraCharges + multipleOtherCharges.get(i).getAmount());
				}

				String amountStatus;

				if(extraCharges.getMaintenanceCost()==totalPaidMaintainanceAmt && bookingList.getHandlingCharges()==totalPaidHandlingCharges && totalExtraCharges==totalPaidExtraCharges)
				{
					amountStatus="Completed";
				}
				else
				{
					amountStatus="Pending";
				}
				try
				{
					customerOtherPaymentDetails.get(0).setTotalPaidMaintainanceAmt(totalPaidMaintainanceAmt.longValue());
					customerOtherPaymentDetails.get(0).setTotalPaidHandlingCharges(totalPaidHandlingCharges.longValue());
					customerOtherPaymentDetails.get(0).setTotalPaidExtraCharges(totalPaidExtraCharges.longValue());
					customerOtherPaymentDetails.get(0).setAmountStatus(amountStatus);
					customerOtherPaymentDetails.get(0).setCreationDate(todayDate);
					customerOtherPaymentDetails.get(0).setUpdateDate(todayDate);
					customerOtherPaymentDetailsRepository.save(customerOtherPaymentDetails);
				}
				catch(Exception ee)
				{

				}
			}
			else
			{
				long totalExtraCharges = 0;

				query = new Query();
				List<MultipleOtherCharges> multipleOtherCharges = mongoTemplate.find(query.addCriteria(Criteria.where("bookingId").is(extrapaymentResponse.getBookingId())), MultipleOtherCharges.class);

				for(int i=0;i<multipleOtherCharges.size();i++)
				{
					totalExtraCharges = (long) (totalExtraCharges + multipleOtherCharges.get(i).getAmount());
				}

				String amountStatus;
				if(extraCharges.getMaintenanceCost()==extrapaymentResponse.getMaintainanceAmt() && bookingList.getHandlingCharges()==extrapaymentResponse.getHandlingChargesAmt() && totalExtraCharges==extrapaymentResponse.getExtraChargesAmt())
				{
					amountStatus="Completed";
				}
				else
				{
					amountStatus="Pending";
				}

				CustomerOtherPaymentDetails customerpaymentdetails1 = new CustomerOtherPaymentDetails();
				customerpaymentdetails1.setPaymentId(customerpayAmountCode());
				customerpaymentdetails1.setBookingId(extrapaymentResponse.getBookingId());
				customerpaymentdetails1.setTotalPaidMaintainanceAmt((long)extrapaymentResponse.getMaintainanceAmt());;
				customerpaymentdetails1.setTotalPaidHandlingCharges((long)extrapaymentResponse.getHandlingChargesAmt());;
				customerpaymentdetails1.setTotalPaidExtraCharges((long)extrapaymentResponse.getExtraChargesAmt());;
				customerpaymentdetails1.setAmountStatus(amountStatus);
				customerpaymentdetails1.setCreationDate(todayDate);
				customerpaymentdetails1.setUpdateDate(todayDate);

				customerOtherPaymentDetailsRepository.save(customerpaymentdetails1);
			}



			response.setResponse("Payment add successfully");
			response.setStatus(1);

			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			response.setResponse("Login fail");
			response.setStatus(0);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
		}
	}

}
