package com.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.Booking;
import com.realestate.bean.Flat;
import com.realestate.bean.Floor;
import com.realestate.bean.ParkingZone;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.repository.BookingRepository;
import com.realestate.repository.ParkingZoneRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.response.BudgetResponse;
import com.realestate.response.LoginDetails;
import com.realestate.response.ParkingListResponse;
import com.realestate.response.ParkingZoneResponse;
import com.realestate.response.WingwiseParkingResponse;

@Controller
@RestController
@RequestMapping("parking")
public class ParkingZoneController {


	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	BookingRepository bookingRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	ParkingZoneRepository parkingzoneRepository;

	@RequestMapping(value = "/getParkingZoneList", method = RequestMethod.POST)
	public ResponseEntity<ParkingZoneResponse> getParkingZoneList(@RequestBody LoginDetails loginDetails) {

		ParkingZoneResponse response = new ParkingZoneResponse();
		
		try {
			List<ParkingZone> parkingZoneList=parkingzoneRepository.findAll();
			response.setParkingZoneList(parkingZoneList);
			response.setResponse("Parking Zone List");
			response.setStatus(1);
			
			return new ResponseEntity<ParkingZoneResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			response.setResponse("Login fail");
			response.setStatus(0);
			return new ResponseEntity<ParkingZoneResponse>(response,HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/getParkingList", method = RequestMethod.POST)
	public ResponseEntity<ParkingListResponse> getParkingList(@RequestBody WingwiseParkingResponse wingwiseParkingResponse) {

		ParkingListResponse response = new ParkingListResponse();
		
		try {

			//List<Booking> parkingList=bookingRepository.findAll();

			Query query = new Query();
			List<Booking> parkingList = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(wingwiseParkingResponse.getProjectId()).and("buildingId").is(wingwiseParkingResponse.getBuildingId()).and("wingId").is(wingwiseParkingResponse.getWingId())), Booking.class);

			query = new Query();
			ProjectWing wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(wingwiseParkingResponse.getWingId())), ProjectWing.class);

			query = new Query();
			ProjectBuilding buildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(wingwiseParkingResponse.getBuildingId())), ProjectBuilding.class);

			query = new Query();
			Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(wingwiseParkingResponse.getProjectId())), Project.class);

			for(int i=0;i<parkingList.size();i++)
			{
				try
				{
					Flat flatDetails = new Flat();
					ParkingZone parkingZoneDetails = new ParkingZone();
					Floor floorDetails = new Floor();

					query = new Query();
					flatDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("flatId").is(parkingList.get(i).getFlatId())), Flat.class);

					query = new Query();
					parkingZoneDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("parkingZoneId").is(parkingList.get(i).getParkingZoneId())), ParkingZone.class);

					query = new Query();
					floorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("floorId").is(parkingList.get(i).getParkingFloorId())), Floor.class);
/*
					query = new Query();
					ProjectWing wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(parkingList.get(i).getWingId())), ProjectWing.class);

					query = new Query();
					ProjectBuilding buildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(parkingList.get(i).getBuildingId())), ProjectBuilding.class);

					query = new Query();
					Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(parkingList.get(i).getProjectId())), Project.class);
*/
					if(flatDetails != null)
					{
						parkingList.get(i).setFlatNumber(""+flatDetails.getFlatNumber());	
					}
					else
					{
						parkingList.get(i).setFlatNumber("N.A.");
					}

					if(floorDetails != null)
					{
						parkingList.get(i).setParkingFloorId(""+floorDetails.getFloortypeName());	
					}
					else
					{
						parkingList.get(i).setParkingFloorId("N.A.");
					}

					if(parkingZoneDetails != null)
					{
						parkingList.get(i).setParkingZoneId(parkingZoneDetails.getParkingNumber().toString());	
					}
					else
					{
						parkingList.get(i).setParkingZoneId("N.A.");
					}

					if(wingDetails != null)
					{
						parkingList.get(i).setWingName(""+wingDetails.getWingName());	
					}
					else
					{
						parkingList.get(i).setWingName("N.A.");
					}

					if(buildingDetails != null)
					{
						parkingList.get(i).setBuildingName(""+buildingDetails.getBuildingName());	
					}
					else
					{
						parkingList.get(i).setBuildingName("N.A.");	
					}

					if(projectDetails != null)
					{
						parkingList.get(i).setProjectName(""+projectDetails.getProjectName());	
					}
					else
					{
						parkingList.get(i).setProjectName("N.A.");	
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
			}

			response.setParkingList(parkingList);
			response.setResponse("Parking Zone List");
			response.setStatus(1);
			
			return new ResponseEntity<ParkingListResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			response.setResponse("Login fail");
			response.setStatus(0);
			return new ResponseEntity<ParkingListResponse>(response,HttpStatus.OK);
		}
	}
	
}
