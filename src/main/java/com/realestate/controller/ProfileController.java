package com.realestate.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.Employee;
import com.realestate.bean.Login;
import com.realestate.repository.LoginRepository;
import com.realestate.response.CommanResponse;
import com.realestate.response.ProfileResponse;
import com.realestate.services.ImageService;

@Controller
@RestController
@RequestMapping("profile")
public class ProfileController {


	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	LoginRepository loginRepository;

	@Value("${PROFILE_IMAGE_LOCATION}")
	private String PROFILE_IMAGE_LOCATION;

	@RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
	public ResponseEntity<CommanResponse> updatePassword(@RequestBody ProfileResponse profileResponse) {

		CommanResponse response = new CommanResponse();

		try {

			Query query = new Query();
			Login loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(profileResponse.getUserId())), Login.class);

			if(loginDetails.getPassWord().equals(profileResponse.getOldPassWord()) && loginDetails!=null)
			{
				//loginDetails.setUserName(profileResponse.getNewUserName());

				loginDetails.setPassWord(profileResponse.getPassWord());
				loginRepository.save(loginDetails);
				/*
				try
				{
					query = new Query();
					Employee employeeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), Employee.class);

					String Msgbody1 = "Hi, "+employeeDetails.getEmployeefirstName()+" "+employeeDetails.getEmployeelastName();
					String Msgbody2 = "\n Your account updated successfully. Login to qaerp.genieiot.in/";
					String Msgbody3 = "\n With further updated login Credentials : ";
					String Msgbody4 =  "\n Username : "+profileResponse.getUserName()+"\n Password : "+profileResponse.getPassWord();

					String emilId = employeeDetails.getEmployeeEmailId();
					//sendLoginCredentialsMail(emilId,"Login Credentials", Msgbody1+"\n\t"+Msgbody2+"\n"+Msgbody3+"\n"+Msgbody4+" \n\n\t Please, do not reply to the mail.");

				}
				catch(Exception e)
				{
					System.out.println("Mail Sending Exception = "+e.toString());
				}
				 */

				response.setResponse("Password change successfully added");
				response.setStatus(1);
				return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);

			}
			else
			{
				response.setResponse("Old Password in wrong");
				response.setStatus(0);
				return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			response.setResponse("Password change fail");
			response.setStatus(0);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/updateProfileImage", method = RequestMethod.POST)
	public ResponseEntity<CommanResponse> updateProfileImage(@RequestBody ProfileResponse profileResponse) {

		CommanResponse response = new CommanResponse();

		try {
			String path=PROFILE_IMAGE_LOCATION;
			Query query = new Query();
			Login loginDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("userId").is(profileResponse.getUserId())), Login.class);

			//loginDetails.setUserName(profileResponse.getNewUserName());
			//query = new Query();
			//Employee employeeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("employeeId").is(loginDetails.getEmployeeId())), Employee.class);
			
			ImageService.uploadImageToServer(profileResponse.getImage(), loginDetails.getEmployeeId(), path);
			response.setResponse("Profile Image change successfully added");
			response.setStatus(1);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);


		} catch (Exception ex) {
			ex.printStackTrace();
			response.setResponse("Profile Image change fail");
			response.setStatus(0);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
		}
	}

}
