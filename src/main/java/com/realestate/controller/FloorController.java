package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.Floor;
import com.realestate.bean.Login;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.LoginRepository;
import com.realestate.repository.FloorRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.ProjectWingRepository;
import com.realestate.repository.UserAssignedProjectRepository;
import com.realestate.repository.UserModelRepository;
import com.realestate.response.FloorResponse;
import com.realestate.response.LoginDetails;
import com.realestate.response.ProjectWingResponse;

@Controller
@RestController
@RequestMapping("floor")
public class FloorController {

	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	LoginRepository loginRepository;
	@Autowired
	UserAssignedProjectRepository userprojectListRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	UserModelRepository userModelRepository;
	@Autowired
	FloorRepository floorRepository;

	@RequestMapping(value = "/getFloorList", method = RequestMethod.POST)
	public ResponseEntity<FloorResponse> getFloorList(@RequestBody LoginDetails loginDetails) {

		FloorResponse response = new FloorResponse();

		try {

			Login login =loginRepository.findByUserNameAndPassWordAndStatus(loginDetails.getUserName(), loginDetails.getPassWord(),"Active");

			if(login!=null)
			{
				List<Floor> floorList=new ArrayList<Floor>();

				List<UserAssignedProject> userLoginProjectList=userprojectListRepository.findByEmployeeId(login.getEmployeeId());

				for(int i=0;i<userLoginProjectList.size();i++)
				{
					List <Floor> list=floorRepository.findByProjectId(userLoginProjectList.get(i).getProjectId());
					floorList.addAll(list);
				}

				response.setResponse("Project Floor List");
				response.setStatus(1);

				response.setFloorList(floorList);

				return new ResponseEntity<FloorResponse>(response,HttpStatus.OK);
			}
			else
			{

				response.setResponse("Login fail");
				response.setStatus(0);

				return new ResponseEntity<FloorResponse>(response,HttpStatus.OK);
			}
		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<FloorResponse>(response,HttpStatus.OK);
		}
	}

}
