package com.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.EnquirySource;
import com.realestate.bean.Occupation;
import com.realestate.repository.OccupationRepository;
import com.realestate.response.EnquirySourceResponse;
import com.realestate.response.LoginDetails;
import com.realestate.response.OccupationResponse;

@Controller
@RestController
@RequestMapping("occupation")
public class OccupationController {

	@Autowired
	OccupationRepository occupationRepository;
	
	
	@RequestMapping(value = "/getOccupation", method = RequestMethod.POST)
	public ResponseEntity<OccupationResponse> getEnquirySource(@RequestBody LoginDetails loginDetails) {

		OccupationResponse occupationResponse = new OccupationResponse();
		
		try {
			List<Occupation> occupationList=occupationRepository.findAll();
			occupationResponse.setResponse("Occupation List");
			occupationResponse.setStatus(1);
			occupationResponse.setOccupationList(occupationList);
			return new ResponseEntity<OccupationResponse>(occupationResponse,HttpStatus.OK);

		} catch (Exception ex) {
			occupationResponse.setResponse("Login fail");
			occupationResponse.setStatus(0);
			return new ResponseEntity<OccupationResponse>(occupationResponse,HttpStatus.OK);
		}
	}

}
