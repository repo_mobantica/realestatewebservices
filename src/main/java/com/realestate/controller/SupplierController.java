package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.Login;
import com.realestate.bean.SubSupplierType;
import com.realestate.bean.Supplier;
import com.realestate.bean.SupplierType;
import com.realestate.repository.LoginRepository;
import com.realestate.repository.SubSupplierTypeRepository;
import com.realestate.repository.SupplierRepository;
import com.realestate.repository.SupplierTypeRepository;
import com.realestate.response.LoginDetails;
import com.realestate.response.SubSupplierTypeResponse;
import com.realestate.response.SupplierResponse;
import com.realestate.response.SupplierTypeResponse;

@Controller
@RestController
@RequestMapping("supplier")
public class SupplierController {

	@Autowired
	SupplierRepository supplierRepository;
	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	LoginRepository loginRepository;
	@Autowired
	SupplierTypeRepository suppliertypeRepository;
	@Autowired
	SubSupplierTypeRepository subsuppliertypeRepository;

	@RequestMapping(value = "/getSupplierTypeList", method = RequestMethod.POST)
	public ResponseEntity<SupplierTypeResponse> getSupplierTypeList(@RequestBody LoginDetails loginDetails) {

		SupplierTypeResponse response = new SupplierTypeResponse();

		try {

			Login login =loginRepository.findByUserNameAndPassWordAndStatus(loginDetails.getUserName(), loginDetails.getPassWord(),"Active");

			if(login!=null)
			{
				List<SupplierType> suppliertypeList=suppliertypeRepository.findAll();
				response.setSuppliertypeList(suppliertypeList);
				response.setResponse("Supplier Type List");
				response.setStatus(1);
				return new ResponseEntity<SupplierTypeResponse>(response,HttpStatus.OK);
			}
			else
			{
				response.setResponse("Login fail");
				response.setStatus(0);

				return new ResponseEntity<SupplierTypeResponse>(response,HttpStatus.OK);
			}
		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<SupplierTypeResponse>(response,HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/getSubSupplierTypeList", method = RequestMethod.POST)
	public ResponseEntity<SubSupplierTypeResponse> getSubSupplierTypeList(@RequestBody LoginDetails loginDetails) {

		SubSupplierTypeResponse response = new SubSupplierTypeResponse();

		try {

			Login login =loginRepository.findByUserNameAndPassWordAndStatus(loginDetails.getUserName(), loginDetails.getPassWord(),"Active");

			if(login!=null)
			{
				List<SubSupplierType> subsuppliertypeList=subsuppliertypeRepository.findAll();
				response.setSubsuppliertypeList(subsuppliertypeList);
				response.setResponse("Sub Supplier Type List");
				response.setStatus(1);
				return new ResponseEntity<SubSupplierTypeResponse>(response,HttpStatus.OK);
			}
			else
			{
				response.setResponse("Login fail");
				response.setStatus(0);

				return new ResponseEntity<SubSupplierTypeResponse>(response,HttpStatus.OK);
			}
		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<SubSupplierTypeResponse>(response,HttpStatus.OK);
		}
	}


	@RequestMapping(value = "/getSupplierList", method = RequestMethod.POST)
	public ResponseEntity<SupplierResponse> getSupplierList(@RequestBody LoginDetails loginDetails) {

		SupplierResponse response = new SupplierResponse();

		try {

			Login login =loginRepository.findByUserNameAndPassWordAndStatus(loginDetails.getUserName(), loginDetails.getPassWord(),"Active");

			if(login!=null)
			{

				List<Supplier> supplierList = supplierRepository.findAll(); 

				response.setSupplierList(supplierList);
				response.setResponse("Supplier List");
				response.setStatus(1);
				return new ResponseEntity<SupplierResponse>(response,HttpStatus.OK);
			}
			else
			{
				response.setResponse("Login fail");
				response.setStatus(0);

				return new ResponseEntity<SupplierResponse>(response,HttpStatus.OK);
			}
		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<SupplierResponse>(response,HttpStatus.OK);
		}
	}




}
