package com.realestate.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.Contractor;
import com.realestate.bean.ContractorPaymentSchedule;
import com.realestate.bean.ContractorQuotation;
import com.realestate.bean.ContractorType;
import com.realestate.bean.ContractorWorkList;
import com.realestate.bean.ContractorWorkListHistory;
import com.realestate.bean.ContractorWorkOrder;
import com.realestate.bean.Login;
import com.realestate.bean.MaterialPurchaseRequisition;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.SubContractorType;
import com.realestate.bean.Tax;
import com.realestate.repository.LoginRepository;
import com.realestate.repository.*;
import com.realestate.response.LoginDetails;
import com.realestate.response.AddContractorQuotationResponse;
import com.realestate.response.AddContractorWorkListDetailsResponse;
import com.realestate.response.CommanResponse;
import com.realestate.response.ContractorListResponse;
import com.realestate.response.ContractorQuotationDetailsResponse;
import com.realestate.response.ContractorQuotationResponse;
import com.realestate.response.ContractorTypeResponse;
import com.realestate.response.ContractorWorkListDetailsResponse;
import com.realestate.response.ContractorWorkListResponse;
import com.realestate.response.ContractorWorkOrderDetailsResponse;
import com.realestate.response.ContractorWorkOrderResponse;
import com.realestate.response.ContractorWorkResponse;

@Controller
@RestController
@RequestMapping("contractor")
public class ContractorWorkOrderController {

	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	LoginRepository loginRepository;
	@Autowired
	ContractorRepository contractorRepository;
	@Autowired
	ContractorPaymentScheduleRepository contractorpaymentscheduleRepository;
	@Autowired
	ContractorTypeRepository contractortypeRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	ContractorWorkOrderRepository contractorworkorderRepository;
	@Autowired
	ContractorWorkListRepository contractorworklistRepository;
	@Autowired
	ContractorWorkListHistoryRepository contractorworklisthistoryRepository;
	@Autowired
	SubContractorTypeRepository subcontractorTypeRepository;
	@Autowired
	ContractorQuotationRepository contractorquotationRepository;

	public long GenerateContractorPaymentId()
	{
		long id = 0;
		List<ContractorPaymentSchedule> contractorpaymentscheduleList = contractorpaymentscheduleRepository.findAll();

		int size = contractorpaymentscheduleList.size();

		if(size!=0)
		{
			id = contractorpaymentscheduleList.get(size-1).getContractorrschedulerId();
		}

		return id+1;
	}
	String workListCode;
	public String GenerateContractorWorkListId()
	{
		long cnt  = contractorworklistRepository.count();

		if(cnt<10)
		{
			workListCode = "WL000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			workListCode = "WL00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			workListCode = "WL0"+(cnt+1);
		}
		else
		{
			workListCode = "WL"+(cnt+1);
		}
		return workListCode;

	}

	public String GenerateContractorWorkOrderId()
	{
		int id = 0;
		String oderId="0";
		String workId="";
		List<ContractorWorkOrder> contractorworkOrderList = contractorworkorderRepository.findAll();

		int size = contractorworkOrderList.size();

		if(size!=0)
		{
			oderId = contractorworkOrderList.get(size-1).getWorkOrderId();
		}

		id=Integer.parseInt(oderId);

		if(id<10)
		{
			workId = "00000"+(id+1);
		}
		else if((id<100) && (id>=10))
		{
			workId = "0000"+(id+1);
		}
		else if((id<1000) && (id>=100))
		{
			workId = "000"+(id+1);
		}
		else if(id>=1000)
		{
			workId = "00"+(id+1);
		}
		else if((id<1000) && (id>=100))
		{
			workId = "0"+(id+1);
		}
		else if(id>=1000)
		{
			workId = ""+(id+1);
		}

		//id=Integer.parseInt(workId);

		return workId;
	}

	public long GenerateContractorWorkListHistoryId()
	{
		long workHistoryId = 0;

		List<ContractorWorkListHistory> contractorworklisthistory = contractorworklisthistoryRepository.findAll();

		int size = contractorworklisthistory.size();

		if(size!=0)
		{
			workHistoryId = contractorworklisthistory.get(size-1).getWorkHistoryId();
		}
		workHistoryId=workHistoryId+1;

		return workHistoryId;
	}

	String quotationCode;

	private String ContractorQuotationCode()
	{
		long cnt  = contractorquotationRepository.count();
		if(cnt<10)
		{
			quotationCode = "CQ000"+(cnt+1);
		}
		else if((cnt<100) && (cnt>=10))
		{
			quotationCode = "CQ00"+(cnt+1);
		}
		else if((cnt<1000) && (cnt>=100))
		{
			quotationCode = "CQ0"+(cnt+1);
		}
		else
		{
			quotationCode = "CQ"+(cnt+1);
		}
		return quotationCode;
	}


	@RequestMapping(value = "/getContractorList", method = RequestMethod.POST)
	public ResponseEntity<ContractorListResponse> getContractorType(@RequestBody LoginDetails loginDetails) {

		ContractorListResponse response = new ContractorListResponse();

		try {

			Login login =loginRepository.findByUserNameAndPassWordAndStatus(loginDetails.getUserName(), loginDetails.getPassWord(),"Active");

			if(login!=null)
			{
				List<Contractor> contractorList =contractorRepository.findAll();
				response.setContractorList(contractorList);
				response.setResponse("Contractor List");
				response.setStatus(1);
				return new ResponseEntity<ContractorListResponse>(response,HttpStatus.OK);
			}
			else
			{
				response.setResponse("Login fail");
				response.setStatus(0);

				return new ResponseEntity<ContractorListResponse>(response,HttpStatus.OK);
			}
		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<ContractorListResponse>(response,HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/getContractorType", method = RequestMethod.POST)
	public ResponseEntity<ContractorTypeResponse> getContractorList(@RequestBody LoginDetails loginDetails) {

		ContractorTypeResponse response = new ContractorTypeResponse();

		try {

			Login login =loginRepository.findByUserNameAndPassWordAndStatus(loginDetails.getUserName(), loginDetails.getPassWord(),"Active");

			if(login!=null)
			{
				List<ContractorType> contractorTypeList =contractortypeRepository.findAll();

				List<SubContractorType> subContractorTypeList =subcontractorTypeRepository.findAll();

				response.setContractorTypeList(contractorTypeList);
				response.setSubContractorTypeList(subContractorTypeList);
				response.setResponse("Contractor Type List");
				response.setStatus(1);
				return new ResponseEntity<ContractorTypeResponse>(response,HttpStatus.OK);
			}
			else
			{
				response.setResponse("Login fail");
				response.setStatus(0);

				return new ResponseEntity<ContractorTypeResponse>(response,HttpStatus.OK);
			}
		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<ContractorTypeResponse>(response,HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/getContractorWorkList", method = RequestMethod.POST)
	public ResponseEntity<ContractorWorkListResponse> getContractorWorkList(@RequestBody LoginDetails loginDetails) {

		ContractorWorkListResponse response = new ContractorWorkListResponse();

		try {

			Login login =loginRepository.findByUserNameAndPassWordAndStatus(loginDetails.getUserName(), loginDetails.getPassWord(),"Active");

			if(login!=null)
			{
				Query query = new Query();

				List<ContractorWorkList> contractorworkList = mongoTemplate.find(query.addCriteria(Criteria.where("status").is("Applied")), ContractorWorkList.class);

				for(int i=0;i<contractorworkList.size();i++)
				{
					try {
						query = new Query();
						Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(contractorworkList.get(i).getProjectId())), Project.class);
						query = new Query();
						ProjectBuilding buildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(contractorworkList.get(i).getBuildingId())), ProjectBuilding.class);
						query = new Query();
						ProjectWing wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(contractorworkList.get(i).getWingId())), ProjectWing.class);

						query = new Query();
						ContractorType contractortypeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("contractortypeId").is(contractorworkList.get(i).getContractortypeId())), ContractorType.class);

						contractorworkList.get(i).setProjectName(projectDetails.getProjectName());
						contractorworkList.get(i).setBuildingName(buildingDetails.getBuildingName());
						contractorworkList.get(i).setWingName(wingDetails.getWingName());
						contractorworkList.get(i).setContractortype(contractortypeDetails.getContractorType());
					}
					catch (Exception e) {
						// TODO: handle exception
					}
				}

				response.setContractorworkList(contractorworkList);
				response.setResponse("Contractor work List");
				response.setStatus(1);
				return new ResponseEntity<ContractorWorkListResponse>(response,HttpStatus.OK);
			}
			else
			{
				response.setResponse("Login fail");
				response.setStatus(0);

				return new ResponseEntity<ContractorWorkListResponse>(response,HttpStatus.OK);
			}
		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<ContractorWorkListResponse>(response,HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/getContractorWorkListDetails", method = RequestMethod.POST)
	public ResponseEntity<ContractorWorkListDetailsResponse> getContractorWorkListDetails(@RequestBody ContractorWorkList contractorworkList) {

		ContractorWorkListDetailsResponse response = new ContractorWorkListDetailsResponse();

		try {

			long totalAmount=0;
			Query query = new Query();
			ContractorWorkList contractorworkListDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("workId").is(contractorworkList.getWorkId())), ContractorWorkList.class);

			query = new Query();
			Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(contractorworkListDetails.getProjectId())), Project.class);
			query = new Query();
			ProjectBuilding buildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(contractorworkListDetails.getBuildingId())), ProjectBuilding.class);
			query = new Query();
			ProjectWing wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(contractorworkListDetails.getWingId())), ProjectWing.class);

			query = new Query();
			ContractorType contractorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("contractortypeId").is(contractorworkListDetails.getContractortypeId())), ContractorType.class);

			query = new Query();
			List<ContractorWorkListHistory> contractorworkhistoryList = mongoTemplate.find(query.addCriteria(Criteria.where("workId").is(contractorworkListDetails.getWorkId())), ContractorWorkListHistory.class);

			for(int i=0;i<contractorworkhistoryList.size();i++)
			{
				query = new Query();
				SubContractorType subcontractortypeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("subcontractortypeId").is(contractorworkhistoryList.get(i).getSubcontractortypeId())), SubContractorType.class);
				contractorworkhistoryList.get(i).setSubcontractortype(subcontractortypeDetails.getSubcontractorType());
				totalAmount=(long) (totalAmount+contractorworkhistoryList.get(i).getGrandTotal());
				contractorworkhistoryList.get(i).setWorkUnit1(""+contractorworkhistoryList.get(i).getUnit());
				contractorworkhistoryList.get(i).setWorkRate1(""+contractorworkhistoryList.get(i).getWorkRate());
				contractorworkhistoryList.get(i).setGrandTotal1(""+contractorworkhistoryList.get(i).getGrandTotal());
			}
			response.setTotalAmount(totalAmount);
			response.setWorkId(contractorworkListDetails.getWorkId());
			response.setProjectId(contractorworkListDetails.getProjectId());
			response.setBuildingId(contractorworkListDetails.getBuildingId());
			response.setWingId(contractorworkListDetails.getWingId());
			response.setProjectName(projectDetails.getProjectName());
			response.setBuildingName(buildingDetails.getBuildingName());
			response.setWingName(wingDetails.getWingName());
			response.setContractorType(contractorDetails.getContractorType());
			response.setContractorworkListhistory(contractorworkhistoryList);

			response.setResponse("Contractor work List Details");
			response.setStatus(1);
			return new ResponseEntity<ContractorWorkListDetailsResponse>(response,HttpStatus.OK);
		} catch (Exception ex) {
			ex.printStackTrace();
			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<ContractorWorkListDetailsResponse>(response,HttpStatus.OK);
		}
	}


	@RequestMapping(value = "/addContractorWorkListDetails", method = RequestMethod.POST)
	public ResponseEntity<CommanResponse> addContractorWorkListDetails(@RequestBody AddContractorWorkListDetailsResponse addcontractorworkListDetailsResponse) {

		CommanResponse response = new CommanResponse();

		try {
			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  

			String workId=GenerateContractorWorkListId();
			ContractorWorkList contractorworklist=new ContractorWorkList();
			contractorworklist.setWorkId(workId);
			contractorworklist.setProjectId(addcontractorworkListDetailsResponse.getProjectId());
			contractorworklist.setBuildingId(addcontractorworkListDetailsResponse.getBuildingId());
			contractorworklist.setWingId(addcontractorworkListDetailsResponse.getWingId());
			contractorworklist.setContractortypeId(addcontractorworkListDetailsResponse.getContractortypeId());
			contractorworklist.setCreationDate(formatter.format(date));
			contractorworklist.setUpdateDate(formatter.format(date));
			contractorworklist.setStatus("Applied");
			contractorworklist.setUserName(addcontractorworkListDetailsResponse.getUserId());
			contractorworklistRepository.save(contractorworklist);

			ContractorWorkListHistory contractorworklisthistory = new ContractorWorkListHistory();
			for(int i=0;i<addcontractorworkListDetailsResponse.getContractorworkListhistory().size();i++)
			{

				contractorworklisthistory = new ContractorWorkListHistory();
				contractorworklisthistory.setWorkHistoryId(GenerateContractorWorkListHistoryId());
				contractorworklisthistory.setWorkId(workId);
				contractorworklisthistory.setContractortypeId(addcontractorworkListDetailsResponse.getContractorworkListhistory().get(i).getContractortypeId());
				contractorworklisthistory.setSubcontractortypeId(addcontractorworkListDetailsResponse.getContractorworkListhistory().get(i).getSubcontractortypeId());
				contractorworklisthistory.setWorkType(addcontractorworkListDetailsResponse.getContractorworkListhistory().get(i).getWorkType());
				contractorworklisthistory.setType(addcontractorworkListDetailsResponse.getContractorworkListhistory().get(i).getType());
				contractorworklisthistory.setUnit(addcontractorworkListDetailsResponse.getContractorworkListhistory().get(i).getUnit());
				contractorworklisthistory.setWorkRate(addcontractorworkListDetailsResponse.getContractorworkListhistory().get(i).getWorkRate());
				contractorworklisthistory.setGrandTotal(addcontractorworkListDetailsResponse.getContractorworkListhistory().get(i).getGrandTotal());

				contractorworklisthistoryRepository.save(contractorworklisthistory);

			}

			response.setResponse("Contractor Work List successfully added");
			response.setStatus(1);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			response.setResponse("Contractor Work List added fail");
			response.setStatus(0);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
		}
	}


	@RequestMapping(value = "/getContractorQuotationList", method = RequestMethod.POST)
	public ResponseEntity<ContractorQuotationResponse> getContractorQuotationList(@RequestBody ContractorWorkList contractorworkList) {

		ContractorQuotationResponse response = new ContractorQuotationResponse();

		try {

			Query query = new Query();
			List<ContractorQuotation> contractorquotationList = mongoTemplate.find(query.addCriteria(Criteria.where("workId").is(contractorworkList.getWorkId())), ContractorQuotation.class);
			query = new Query();
			ContractorWorkList contractorWorkListDetails=mongoTemplate.findOne(query.addCriteria(Criteria.where("workId").is(contractorworkList.getWorkId())), ContractorWorkList.class);

			query = new Query();
			Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(contractorWorkListDetails.getProjectId())), Project.class);
			query = new Query();
			ProjectBuilding buildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(contractorWorkListDetails.getBuildingId())), ProjectBuilding.class);
			query = new Query();
			ProjectWing wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(contractorWorkListDetails.getWingId())), ProjectWing.class);

			query = new Query();
			ContractorType contractorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("contractortypeId").is(contractorWorkListDetails.getContractortypeId())), ContractorType.class);

			response.setWorkId(contractorWorkListDetails.getWorkId());
			response.setProjectId(contractorWorkListDetails.getProjectId());
			response.setBuildingId(contractorWorkListDetails.getBuildingId());
			response.setWingId(contractorWorkListDetails.getWingId());
			response.setContractortypeId(contractorWorkListDetails.getContractortypeId());
			response.setProjectName(projectDetails.getProjectName());
			response.setBuildingName(buildingDetails.getBuildingName());
			response.setWingName(wingDetails.getWingName());
			response.setContractortype(contractorDetails.getContractorType());

			response.setContractorquotationList(contractorquotationList);
			response.setResponse("Contractor Quotation List ");
			response.setStatus(1);
			return new ResponseEntity<ContractorQuotationResponse>(response,HttpStatus.OK);
		} catch (Exception ex) {
			ex.printStackTrace();
			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<ContractorQuotationResponse>(response,HttpStatus.OK);
		}
	}


	@RequestMapping(value = "/addContractorQuotation", method = RequestMethod.POST)
	public ResponseEntity<CommanResponse> addContractorQuotation(@RequestBody AddContractorQuotationResponse addcontractorquotationResponse) {

		CommanResponse response = new CommanResponse();

		try {

			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  

			ContractorQuotation contractorQuotation=new ContractorQuotation();
			String quotationId=ContractorQuotationCode();
			contractorQuotation.setQuotationId(quotationId);
			contractorQuotation.setWorkId(addcontractorquotationResponse.getWorkId());
			contractorQuotation.setContractorId(addcontractorquotationResponse.getContractorId());
			contractorQuotation.setTotalAmount(addcontractorquotationResponse.getTotalAmount());
			contractorQuotation.setStatus("Applied");
			contractorQuotation.setCreationDate(formatter.format(date));
			contractorQuotation.setUpdateDate(formatter.format(date));
			contractorQuotation.setUserName(addcontractorquotationResponse.getUserId());

			contractorquotationRepository.save(contractorQuotation);
			response.setResponse("Add Contractor Quotation ");
			response.setStatus(1);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
		} catch (Exception ex) {
			ex.printStackTrace();
			response.setResponse("Contractor Quotation added fail");
			response.setStatus(0);

			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/getContractorQuotationDetails", method = RequestMethod.POST)
	public ResponseEntity<ContractorQuotationDetailsResponse> getContractorQuotationDetails(@RequestBody ContractorQuotation contractorQuotation) {

		ContractorQuotationDetailsResponse response=new ContractorQuotationDetailsResponse();

		try {

			Query query = new Query();
			ContractorQuotation contractorquotationDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("quotationId").is(contractorQuotation.getQuotationId())), ContractorQuotation.class);
			query = new Query();
			ContractorWorkList contractorWorkListDetails=mongoTemplate.findOne(query.addCriteria(Criteria.where("workId").is(contractorquotationDetails.getWorkId())), ContractorWorkList.class);

			query = new Query();
			Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(contractorWorkListDetails.getProjectId())), Project.class);
			query = new Query();
			ProjectBuilding buildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(contractorWorkListDetails.getBuildingId())), ProjectBuilding.class);
			query = new Query();
			ProjectWing wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(contractorWorkListDetails.getWingId())), ProjectWing.class);
			query = new Query();
			ContractorType contractorTypeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("contractortypeId").is(contractorWorkListDetails.getContractortypeId())), ContractorType.class);
			query = new Query();
			Contractor contractorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("contractorId").is(contractorquotationDetails.getContractorId())), Contractor.class);
			query = new Query();
			List<ContractorWorkListHistory> contractorWorkList = mongoTemplate.find(query.addCriteria(Criteria.where("workId").is(contractorquotationDetails.getWorkId())), ContractorWorkListHistory.class);


			for(int i=0;i<contractorWorkList.size();i++)
			{
				query = new Query();
				SubContractorType subcontractortypeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("subcontractortypeId").is(contractorWorkList.get(i).getSubcontractortypeId())), SubContractorType.class);
				contractorWorkList.get(i).setSubcontractortype(subcontractortypeDetails.getSubcontractorType());
				contractorWorkList.get(i).setWorkUnit1(""+contractorWorkList.get(i).getUnit());
				contractorWorkList.get(i).setWorkRate1(""+contractorWorkList.get(i).getWorkRate());
				contractorWorkList.get(i).setGrandTotal1(""+contractorWorkList.get(i).getGrandTotal());
			}

			response.setQuotationId(contractorquotationDetails.getQuotationId());
			response.setWorkId(contractorquotationDetails.getWorkId());
			response.setTotalAmount(contractorquotationDetails.getTotalAmount());
			response.setProjectName(projectDetails.getProjectName());
			response.setBuildingName(buildingDetails.getBuildingName());
			response.setWingName(wingDetails.getWingName());
			response.setContractortype(contractorTypeDetails.getContractorType());
			response.setContractorworkList(contractorWorkList);
			response.setContractorName(contractorDetails.getContractorfirmName());
			response.setContractorId(contractorquotationDetails.getContractorId());
			response.setResponse("Contractor Quotation Details");
			response.setStatus(1);
			return new ResponseEntity<ContractorQuotationDetailsResponse>(response,HttpStatus.OK);
		} catch (Exception ex) {
			ex.printStackTrace();
			response.setResponse("Contractor Quotation Details fail");
			response.setStatus(0);

			return new ResponseEntity<ContractorQuotationDetailsResponse>(response,HttpStatus.OK);
		}
	}



	@RequestMapping(value = "/addContractorWorkOrder", method = RequestMethod.POST)
	public ResponseEntity<CommanResponse> addContractorWorkOrder(@RequestBody ContractorWorkOrderResponse contractorWorkOrderResponse) {

		CommanResponse response=new CommanResponse();

		try {
			ContractorWorkOrder contractorworkorder=new ContractorWorkOrder();

			Date date = new Date();  
			SimpleDateFormat formatter = new SimpleDateFormat("d/M/yyyy");  
			Query query = new Query();
			ContractorWorkList contractorWorkListDetails=mongoTemplate.findOne(query.addCriteria(Criteria.where("workId").is(contractorWorkOrderResponse.getWorkId())), ContractorWorkList.class);

			contractorworkorder.setWorkOrderId(GenerateContractorWorkOrderId());
			contractorworkorder.setQuotationId(contractorWorkOrderResponse.getQuotationId());
			contractorworkorder.setWorkId(contractorWorkOrderResponse.getWorkId());
			contractorworkorder.setProjectId(contractorWorkListDetails.getProjectId());
			contractorworkorder.setBuildingId(contractorWorkListDetails.getBuildingId());
			contractorworkorder.setWingId(contractorWorkListDetails.getWingId());
			contractorworkorder.setContractortypeId(contractorWorkListDetails.getContractortypeId());
			contractorworkorder.setContractorId(contractorWorkOrderResponse.getContractorId());
			contractorworkorder.setContractorfirmName(contractorWorkOrderResponse.getContractorfirmName());
			contractorworkorder.setTotalAmount(contractorWorkOrderResponse.getTotalAmount());
			contractorworkorder.setNoofFemaleLabour(contractorWorkOrderResponse.getNoofFemaleLabour());
			contractorworkorder.setNoofMaleLabour(contractorWorkOrderResponse.getNoofMaleLabour());
			contractorworkorder.setNoofInsuredLabour(contractorWorkOrderResponse.getNoofInsuredLabour());
			contractorworkorder.setRetentionAmount(contractorWorkOrderResponse.getRetentionAmount());
			contractorworkorder.setRetentionPer(contractorWorkOrderResponse.getRetentionPer());
			contractorworkorder.setUserName(contractorWorkOrderResponse.getUserId());
			contractorworkorder.setCreationDate(formatter.format(date));
			contractorworkorder.setUpdateDate(formatter.format(date));

			contractorworkorderRepository.save(contractorworkorder);
			response.setResponse("Contractor Quotation Details");
			response.setStatus(1);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
		} catch (Exception ex) {
			ex.printStackTrace();
			response.setResponse("Contractor Quotation Details fail");
			response.setStatus(0);

			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
		}
	}


	@RequestMapping(value = "/getContractorWorkOrderList", method = RequestMethod.POST)
	public ResponseEntity<ContractorWorkResponse> getContractorWorkOrderList(@RequestBody LoginDetails loginDetails) {

		ContractorWorkResponse response = new ContractorWorkResponse();

		try {

			Login login =loginRepository.findByUserNameAndPassWordAndStatus(loginDetails.getUserName(), loginDetails.getPassWord(),"Active");

			if(login!=null)
			{
				List<ContractorWorkOrder> contarctorworkorderList = contractorworkorderRepository.findAll(); 
				Query query = new Query();
				for(int i=0;i<contarctorworkorderList.size();i++)
				{
					try
					{
						query = new Query();
						List<Project> projectDetails = mongoTemplate.find(query.addCriteria(Criteria.where("projectId").is(contarctorworkorderList.get(i).getProjectId())), Project.class);
						query = new Query();
						List<ProjectBuilding> buildingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("buildingId").is(contarctorworkorderList.get(i).getBuildingId())), ProjectBuilding.class);
						query = new Query();
						List<ProjectWing> wingDetails = mongoTemplate.find(query.addCriteria(Criteria.where("wingId").is(contarctorworkorderList.get(i).getWingId())), ProjectWing.class);

						query = new Query();
						List<ContractorType> contractorDetails = mongoTemplate.find(query.addCriteria(Criteria.where("contractortypeId").is(contarctorworkorderList.get(i).getContractortypeId())), ContractorType.class);

						contarctorworkorderList.get(i).setProjectId(projectDetails.get(0).getProjectName());
						contarctorworkorderList.get(i).setBuildingId(buildingDetails.get(0).getBuildingName());
						contarctorworkorderList.get(i).setWingId(wingDetails.get(0).getWingName());
						contarctorworkorderList.get(i).setContractortypeId(contractorDetails.get(0).getContractorType());
					}
					catch (Exception e) {
						// TODO: handle exception
					}
				}
				response.setContractorworkOrderList(contarctorworkorderList);
				response.setResponse("Contractor work List");
				response.setStatus(1);
				return new ResponseEntity<ContractorWorkResponse>(response,HttpStatus.OK);
			}
			else
			{
				response.setResponse("Login fail");
				response.setStatus(0);

				return new ResponseEntity<ContractorWorkResponse>(response,HttpStatus.OK);
			}
		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<ContractorWorkResponse>(response,HttpStatus.OK);
		}
	}


	@RequestMapping(value = "/getViewContractorWorkOrder", method = RequestMethod.POST)
	public ResponseEntity<ContractorWorkOrderDetailsResponse> getViewContractorWorkOrder(@RequestBody ContractorWorkOrder contractorWorkOrder) {

		ContractorWorkOrderDetailsResponse response = new ContractorWorkOrderDetailsResponse();

		try {


			Query query = new Query();
			ContractorWorkOrder contractorworkorderDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("workOrderId").is(contractorWorkOrder.getWorkOrderId())),ContractorWorkOrder.class);

			query = new Query();
			List<ContractorPaymentSchedule> contractorpaymentscheduleList = mongoTemplate.find(query.addCriteria(Criteria.where("workOrderId").is(contractorWorkOrder.getWorkOrderId())), ContractorPaymentSchedule.class); 

			query = new Query();
			List<ContractorWorkListHistory> contractorworkhistoryList = mongoTemplate.find(query.addCriteria(Criteria.where("workId").is(contractorworkorderDetails.getWorkId())), ContractorWorkListHistory.class);

			for(int i=0;i<contractorworkhistoryList.size();i++)
			{
				try {
					query = new Query();
					List<SubContractorType> subcontractortypeDetails = mongoTemplate.find(query.addCriteria(Criteria.where("subcontractortypeId").is(contractorworkhistoryList.get(i).getSubcontractortypeId())), SubContractorType.class);
					contractorworkhistoryList.get(i).setSubcontractortypeId(subcontractortypeDetails.get(0).getSubcontractorType());
					contractorworkhistoryList.get(i).setWorkRate1(""+contractorworkhistoryList.get(i).getWorkRate());
					contractorworkhistoryList.get(i).setWorkUnit1(""+contractorworkhistoryList.get(i).getWorkUnit1());
					contractorworkhistoryList.get(i).setGrandTotal1(""+contractorworkhistoryList.get(i).getGrandTotal());
				}catch (Exception e) {
					// TODO: handle exception
				}
			}
			response.setWorkOrderId(contractorworkorderDetails.getWorkOrderId());
			response.setWorkId(contractorworkorderDetails.getWorkId());
			response.setQuotationId(contractorworkorderDetails.getQuotationId());
			response.setProjectId(contractorworkorderDetails.getProjectId());
			response.setBuildingId(contractorworkorderDetails.getBuildingId());
			response.setWingId(contractorworkorderDetails.getWingId());
			response.setContractortypeId(contractorworkorderDetails.getContractortypeId());
			response.setContractorId(contractorworkorderDetails.getContractorId());
			response.setTotalAmount(contractorworkorderDetails.getTotalAmount());
			response.setNoofMaleLabour(contractorworkorderDetails.getNoofMaleLabour());
			response.setNoofFemaleLabour(contractorworkorderDetails.getNoofFemaleLabour());
			response.setNoofInsuredLabour(contractorworkorderDetails.getNoofInsuredLabour());
			response.setRetentionAmount(contractorworkorderDetails.getRetentionAmount());
			response.setRetentionPer(contractorworkorderDetails.getRetentionPer());
			response.setCreationDate(contractorworkorderDetails.getCreationDate());
			response.setPaymentStatus(contractorworkorderDetails.getPaymentStatus());
			response.setContractorpaymentscheduleList(contractorpaymentscheduleList);
			response.setContractorworkList(contractorworkhistoryList);
			try {

				query = new Query();
				Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(contractorworkorderDetails.getProjectId())), Project.class);
				query = new Query();
				ProjectBuilding buildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(contractorworkorderDetails.getBuildingId())), ProjectBuilding.class);
				query = new Query();
				ProjectWing wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(contractorworkorderDetails.getWingId())), ProjectWing.class);

				query = new Query();
				ContractorType contractorTypeDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("contractortypeId").is(contractorworkorderDetails.getContractortypeId())), ContractorType.class);

				response.setProjectName(projectDetails.getProjectName());
				response.setBuildingName(buildingDetails.getBuildingName());
				response.setWingName(wingDetails.getWingName());
				response.setContractortype(contractorTypeDetails.getContractorType());
				query = new Query();
				try {
					Contractor contractorDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("contractorId").is(contractorworkorderDetails.getContractorId())), Contractor.class);
					response.setContractorfirmName(contractorDetails.getContractorfirmName());

				}catch (Exception e) {
					response.setContractorfirmName(contractorworkorderDetails.getContractorfirmName());
					e.printStackTrace();
					// TODO: handle exception
				}
			}catch (Exception e) {
				// TODO: handle exception
			}


			response.setResponse("Contractor work Details");
			response.setStatus(1);
			return new ResponseEntity<ContractorWorkOrderDetailsResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<ContractorWorkOrderDetailsResponse>(response,HttpStatus.OK);
		}
	}

}
