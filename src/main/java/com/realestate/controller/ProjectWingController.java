package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.Login;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.LoginRepository;
import com.realestate.repository.ProjectBuildingRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.ProjectWingRepository;
import com.realestate.repository.UserAssignedProjectRepository;
import com.realestate.repository.UserModelRepository;
import com.realestate.response.LoginDetails;
import com.realestate.response.ProjectWingResponse;

@Controller
@RestController
@RequestMapping("wing")
public class ProjectWingController {

	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	LoginRepository loginRepository;
	@Autowired
	UserAssignedProjectRepository userprojectListRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	UserModelRepository userModelRepository;
	@Autowired
	ProjectBuildingRepository projectbuildingRepository;
	@Autowired
	ProjectWingRepository projectWingRepository;


	@RequestMapping(value = "/getWingList", method = RequestMethod.POST)
	public ResponseEntity<ProjectWingResponse> getWingList(@RequestBody LoginDetails loginDetails) {

		ProjectWingResponse response = new ProjectWingResponse();

		try {

			Login login =loginRepository.findByUserNameAndPassWordAndStatus(loginDetails.getUserName(), loginDetails.getPassWord(),"Active");

			if(login!=null)
			{
				List<ProjectWing> wingList=new ArrayList<ProjectWing>();

				List<UserAssignedProject> userLoginProjectList=userprojectListRepository.findByEmployeeId(login.getEmployeeId());

				for(int i=0;i<userLoginProjectList.size();i++)
				{
					List <ProjectWing> list=projectWingRepository.findByProjectId(userLoginProjectList.get(i).getProjectId());
					wingList.addAll(list);
				}

				response.setResponse("Project Wing List");
				response.setStatus(1);
				response.setWingList(wingList);

				return new ResponseEntity<ProjectWingResponse>(response,HttpStatus.OK);
			}
			else
			{

				response.setResponse("Login fail");
				response.setStatus(0);

				return new ResponseEntity<ProjectWingResponse>(response,HttpStatus.OK);
			}
		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<ProjectWingResponse>(response,HttpStatus.OK);
		}
	}

}
