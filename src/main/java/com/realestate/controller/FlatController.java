package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.Booking;
import com.realestate.bean.Flat;
import com.realestate.bean.Floor;
import com.realestate.bean.Login;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.BookingRepository;
import com.realestate.repository.FlatRepository;
import com.realestate.repository.FloorRepository;
import com.realestate.repository.LoginRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.UserAssignedProjectRepository;
import com.realestate.response.FlatResponse;
import com.realestate.response.LoginDetails;

@Controller
@RestController
@RequestMapping("flat")
public class FlatController {

	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	LoginRepository loginRepository;
	@Autowired
	UserAssignedProjectRepository userprojectListRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	FlatRepository flatRepository;
	@Autowired
	BookingRepository bookingRepository;
	@Autowired
	FloorRepository floorRepository;

	@RequestMapping(value = "/getFlatList", method = RequestMethod.POST)
	public ResponseEntity<FlatResponse> getFlatList(@RequestBody LoginDetails loginDetails) {

		FlatResponse response = new FlatResponse();

		try {
			Login login =loginRepository.findByUserNameAndPassWordAndStatus(loginDetails.getUserName(), loginDetails.getPassWord(),"Active");

			int j=0;
			String bookingName="";
			if(login!=null)
			{
				List<Flat> flatList=new ArrayList<Flat>();

				List<UserAssignedProject> userLoginProjectList=userprojectListRepository.findByEmployeeId(login.getEmployeeId());

				for(int k=0;k<userLoginProjectList.size();k++)
				{
					List <Flat> list=flatRepository.findByProjectId(userLoginProjectList.get(k).getProjectId());

					List<Booking> bookingDetails  =bookingRepository.findByProjectId(userLoginProjectList.get(k).getProjectId());

					try {
						for(int i=0;i<list.size();i++)
						{
							Floor floor=floorRepository.findByFloorId(list.get(i).getFloorId());
							
							list.get(i).setFloorName(floor.getFloortypeName());
							if(list.get(i).getFlatstatus().equals("Booking Completed"))
							{

								for(j=0;j<bookingDetails.size();j++)
								{
									if(list.get(i).getFlatId().equalsIgnoreCase(bookingDetails.get(j).getFlatId()))
									{
										bookingName="Booking by "+bookingDetails.get(j).getBookingfirstname();
										list.get(i).setBookingName(bookingName);
										break;
									}
								}
							}
							else if(list.get(i).getFlatstatus().equals("Aggreement Completed"))
							{
								for(j=0;j<bookingDetails.size();j++)
								{
									if(list.get(i).getFlatId().equalsIgnoreCase(bookingDetails.get(j).getFlatId()))
									{
										bookingName="Agreement by "+bookingDetails.get(j).getBookingfirstname();
										list.get(i).setBookingName(bookingName);
										break;
									}
								}
							}
							else
							{
								bookingName="Remaninig";
								list.get(i).setBookingName(bookingName);
							}
						}
					}catch (Exception e) {
						e.printStackTrace();
						// TODO: handle exception
					}

					flatList.addAll(list);
				}

				response.setResponse("Project Flat List");
				response.setStatus(1);
				response.setFlatList(flatList);

				return new ResponseEntity<FlatResponse>(response,HttpStatus.OK);
			}
			else
			{
				response.setResponse("Login fail");
				response.setStatus(0);

				return new ResponseEntity<FlatResponse>(response,HttpStatus.OK);
			}
		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<FlatResponse>(response,HttpStatus.OK);
		}
	}


}
