package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.repository.EnquirySourceRepository;
import com.realestate.repository.SubEnquirySourceRepository;
import com.realestate.bean.EnquirySource;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.SubEnquirySource;
import com.realestate.bean.UserAssignedProject;
import com.realestate.bean.UserModel;
import com.realestate.response.EnquirySourceResponse;
import com.realestate.response.LoginDetails;
import com.realestate.response.LoginResponse;
import com.realestate.response.SubEnquirySourceResponse;
import com.realestate.response.UserProjectDetails;

@Controller
@RestController
@RequestMapping("enquirySource")
public class EnquirySourceController {

	@Autowired
	EnquirySourceRepository enquirySourceRepository;
	@Autowired
	SubEnquirySourceRepository subEnquirySourceRepository;
	
	
	@RequestMapping(value = "/getEnquirySource", method = RequestMethod.POST)
	public ResponseEntity<EnquirySourceResponse> getEnquirySource(@RequestBody LoginDetails loginDetails) {

		EnquirySourceResponse enquirySourceResponse = new EnquirySourceResponse();
		
		try {
			List<EnquirySource> enquirySourceList=enquirySourceRepository.findAll();
			enquirySourceResponse.setResponse("Enquiry Source List");
			enquirySourceResponse.setStatus(1);
			enquirySourceResponse.setEnquirySourceList(enquirySourceList);
			return new ResponseEntity<EnquirySourceResponse>(enquirySourceResponse,HttpStatus.OK);

		} catch (Exception ex) {
			enquirySourceResponse.setResponse("Login fail");
			enquirySourceResponse.setStatus(0);
			return new ResponseEntity<EnquirySourceResponse>(enquirySourceResponse,HttpStatus.OK);
		}
	}


	@RequestMapping(value = "/getSubEnquirySource", method = RequestMethod.POST)
	public ResponseEntity<SubEnquirySourceResponse> getSubEnquirySource(@RequestBody LoginDetails loginDetails) {

		SubEnquirySourceResponse subenquirySourceResponse = new SubEnquirySourceResponse();
		
		try {
			List<SubEnquirySource> subEnquirySourceList=subEnquirySourceRepository.findAll();
			subenquirySourceResponse.setResponse(" Sub Enquiry Source List");
			subenquirySourceResponse.setStatus(1);
			subenquirySourceResponse.setSubEnquirySourceList(subEnquirySourceList);
			return new ResponseEntity<SubEnquirySourceResponse>(subenquirySourceResponse,HttpStatus.OK);

		} catch (Exception ex) {
			subenquirySourceResponse.setResponse("Login fail");
			subenquirySourceResponse.setStatus(0);
			return new ResponseEntity<SubEnquirySourceResponse>(subenquirySourceResponse,HttpStatus.OK);
		}
	}


}
