package com.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.Budget;
import com.realestate.repository.BudgetRepository;
import com.realestate.repository.SubEnquirySourceRepository;
import com.realestate.response.BudgetResponse;
import com.realestate.response.LoginDetails;

@Controller
@RestController
@RequestMapping("budget")
public class BudgetController {

	@Autowired
	BudgetRepository budgetRepository;
	
	@RequestMapping(value = "/getBudget", method = RequestMethod.POST)
	public ResponseEntity<BudgetResponse> getBudget(@RequestBody LoginDetails loginDetails) {

		BudgetResponse budgetResponse = new BudgetResponse();
		
		try {
			List<Budget> budgetList=budgetRepository.findAll();
			budgetResponse.setResponse("Budget List");
			budgetResponse.setStatus(1);
			budgetResponse.setBudgetList(budgetList);
			return new ResponseEntity<BudgetResponse>(budgetResponse,HttpStatus.OK);

		} catch (Exception ex) {
			budgetResponse.setResponse("Login fail");
			budgetResponse.setStatus(0);
			return new ResponseEntity<BudgetResponse>(budgetResponse,HttpStatus.OK);
		}
	}
	
}
