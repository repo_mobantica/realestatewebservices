package com.realestate.controller;

import java.util.ArrayList;
import java.util.List;
import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;  

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.Item;
import com.realestate.bean.Login;
import com.realestate.bean.MaterialPurchaseRequisition;
import com.realestate.bean.MaterialPurchaseRequisitionHistory;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.ProjectWing;
import com.realestate.bean.SubSupplierType;
import com.realestate.repository.ItemRepository;
import com.realestate.repository.LoginRepository;
import com.realestate.repository.MaterialPurchaseRequisitionRepository;
import com.realestate.repository.ProjectBuildingRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.SubSupplierTypeRepository;
import com.realestate.repository.MaterialPurchaseRequisitionHistoryRepository;
import com.realestate.repository.UserAssignedProjectRepository;
import com.realestate.repository.UserModelRepository;
import com.realestate.response.CommanResponse;
import com.realestate.response.LoginDetails;
import com.realestate.response.MaterialPurchaseRequisitionResponse;
import com.realestate.response.MaterialRequisitionResponse;
import com.realestate.response.RequisitionItemResponse;
import com.realestate.response.RequisitionResponse;

@Controller
@RestController
@RequestMapping("requisition")
public class MaterialPurchaseRequisitionController {

	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	LoginRepository loginRepository;
	@Autowired
	UserAssignedProjectRepository userprojectListRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	UserModelRepository userModelRepository;
	@Autowired
	ProjectBuildingRepository projectbuildingRepository;
	@Autowired
	MaterialPurchaseRequisitionRepository materialpurchaseRequisitionRepository;
	@Autowired
	MaterialPurchaseRequisitionHistoryRepository materialpurchaseRequisitionHistoryRepository;
	@Autowired
	SubSupplierTypeRepository subsupplierTypeRepository;
	@Autowired
	ItemRepository itemRepository;

	String requisitionCode;
	private String RequisitionCode()
	{
		long mcount = materialpurchaseRequisitionRepository.count();

		if(mcount<10)
		{
			requisitionCode = "MPR000"+(mcount+1);
		}
		else if((mcount>=10) && (mcount<100))
		{
			requisitionCode = "MPR00"+(mcount+1);
		}
		else if((mcount>=100) && (mcount<1000))
		{
			requisitionCode = "MPR0"+(mcount+1);
		}
		else
		{
			requisitionCode = "MPR"+(mcount+1);
		}

		return requisitionCode;
	}

	String requisitionHistoryCode;
	private String RequisitionHistoryCode()
	{
		long mcount = materialpurchaseRequisitionHistoryRepository.count();

		if(mcount<10)
		{
			requisitionHistoryCode = "MPR000"+(mcount+1);
		}
		else if((mcount>=10) && (mcount<100))
		{
			requisitionHistoryCode = "MPR00"+(mcount+1);
		}
		else if((mcount>=100) && (mcount<1000))
		{
			requisitionHistoryCode = "MPR0"+(mcount+1);
		}
		else if(mcount>1000)
		{
			requisitionHistoryCode = "MPR"+(mcount+1);
		}

		return requisitionHistoryCode;
	}


	@RequestMapping(value = "/getRequisition", method = RequestMethod.POST)
	public ResponseEntity<RequisitionResponse> getRequisition(@RequestBody LoginDetails loginDetails) {

		RequisitionResponse response = new RequisitionResponse();

		try {

			Login login =loginRepository.findByUserNameAndPassWordAndStatus(loginDetails.getUserName(), loginDetails.getPassWord(),"Active");

			if(login!=null)
			{
				List<MaterialPurchaseRequisition> requisitionList=materialpurchaseRequisitionRepository.findByStatusAndEmployeeId("Applied",login.getEmployeeId());
				List<MaterialPurchaseRequisitionResponse> list=new ArrayList<MaterialPurchaseRequisitionResponse>();
				MaterialPurchaseRequisitionResponse requisitionResponse=new MaterialPurchaseRequisitionResponse();
				for(int i=0;i<requisitionList.size();i++)
				{
					try {
						requisitionResponse=new MaterialPurchaseRequisitionResponse();
						requisitionResponse.setRequisitionId(requisitionList.get(i).getRequisitionId());
						requisitionResponse.setEmployeeId(requisitionList.get(i).getEmployeeId());

						requisitionResponse.setProjectId(requisitionList.get(i).getProjectId());
						requisitionResponse.setWingId(requisitionList.get(i).getWingId());
						requisitionResponse.setBuildingId(requisitionList.get(i).getBuildingId());
						requisitionResponse.setRequiredDate(requisitionList.get(i).getRequiredDate());
						requisitionResponse.setTotalNoItem(requisitionList.get(i).getTotalNoItem());
						requisitionResponse.setTotalQuantity(requisitionList.get(i).getTotalQuantity());
						requisitionResponse.setStatus(requisitionList.get(i).getStatus());

						requisitionResponse.setCreationDate(requisitionList.get(i).getCreationDate());
						requisitionResponse.setUpdateDate(requisitionList.get(i).getUpdateDate());

						try {
							Query query = new Query();
							Project projectDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("projectId").is(requisitionList.get(i).getProjectId())),Project.class);
							query =new Query();
							ProjectBuilding buildingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("buildingId").is(requisitionList.get(i).getBuildingId())), ProjectBuilding.class);

							query =new Query();
							ProjectWing wingDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("wingId").is(requisitionList.get(i).getWingId())), ProjectWing.class);
							query =new Query();

							requisitionResponse.setProjectName(projectDetails.getProjectName());
							requisitionResponse.setBuildingName(buildingDetails.getBuildingName());
							requisitionResponse.setWingName(wingDetails.getWingName());
						}catch (Exception e) {
							// TODO: handle exception
						}
						list.add(requisitionResponse);
					}catch (Exception e) {
						// TODO: handle exception
					}
				}

				response.setResponse("Requisition List");
				response.setStatus(1);
				response.setRequisitionList(list);
				return new ResponseEntity<RequisitionResponse>(response,HttpStatus.OK);
			}
			else
			{
				response.setResponse("Login fail");
				response.setStatus(0);

				return new ResponseEntity<RequisitionResponse>(response,HttpStatus.OK);
			}
		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<RequisitionResponse>(response,HttpStatus.OK);
		}
	}


	@RequestMapping(value = "/getRequisitionItemList", method = RequestMethod.POST)
	public ResponseEntity<RequisitionItemResponse> getRequisitionItemList(@RequestBody MaterialRequisitionResponse materialrequisitionResponse) {

		RequisitionItemResponse response = new RequisitionItemResponse();

		try {

			MaterialPurchaseRequisition materialpurchaseRequisition=materialpurchaseRequisitionRepository.findByRequisitionId(materialrequisitionResponse.getRequisitionId());

			response.setRequisitionId(materialpurchaseRequisition.getRequisitionId());
			response.setProjectId(materialpurchaseRequisition.getProjectId());
			response.setBuildingId(materialpurchaseRequisition.getBuildingId());
			response.setWingId(materialpurchaseRequisition.getWingId());
			response.setEmployeeId(materialpurchaseRequisition.getEmployeeId());
			response.setTotalNoItem(materialpurchaseRequisition.getTotalNoItem());
			response.setTotalQuantity(materialpurchaseRequisition.getTotalQuantity());
			response.setApplyDate(materialpurchaseRequisition.getCreationDate());
			response.setRequiredDate(materialpurchaseRequisition.getRequiredDate());
			response.setRequisitionStatus(materialpurchaseRequisition.getStatus());

			List<MaterialPurchaseRequisitionHistory> requisitionItemList=materialpurchaseRequisitionHistoryRepository.findByRequisitionId(materialpurchaseRequisition.getRequisitionId());
			for(int j=0;j<requisitionItemList.size();j++)
			{
				Item item=itemRepository.findByItemId(requisitionItemList.get(j).getItemId());
				requisitionItemList.get(j).setItemName(item.getItemName());

				SubSupplierType subsupplierType=subsupplierTypeRepository.findBySubsuppliertypeId(item.getSubsuppliertypeId());
				requisitionItemList.get(j).setItemSubType(subsupplierType.getSubsupplierType());
				requisitionItemList.get(j).setSuppliertypeId("");
				requisitionItemList.get(j).setSubsuppliertypeId("");
			}

			response.setRequisitionItemList(requisitionItemList);

			response.setResponse("Requisition Item List");
			response.setStatus(1);

			return new ResponseEntity<RequisitionItemResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {

			response.setResponse("Requisition Item List not found");
			response.setStatus(0);

			return new ResponseEntity<RequisitionItemResponse>(response,HttpStatus.OK);
		}
	}


	@RequestMapping(value = "/addMaterialRequisition", method = RequestMethod.POST)
	public ResponseEntity<CommanResponse> addMaterialRequisition(@RequestBody MaterialRequisitionResponse materialrequisitionResponse) {

		CommanResponse response = new CommanResponse();

		try {

			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
			LocalDateTime now = LocalDateTime.now();

			String requisitionHistoryId="";
			String requisitionId=RequisitionCode();
			int totalNoItem=0;
			double totalQuantity=0;

			MaterialPurchaseRequisition materialpurchaseRequisition=new MaterialPurchaseRequisition();
			materialpurchaseRequisition.setRequisitionId(requisitionId);
			materialpurchaseRequisition.setEmployeeId(materialrequisitionResponse.getEmployeeId());
			materialpurchaseRequisition.setProjectId(materialrequisitionResponse.getProjectId());
			materialpurchaseRequisition.setBuildingId(materialrequisitionResponse.getBuildingId());
			materialpurchaseRequisition.setWingId(materialrequisitionResponse.getWingId());
			materialpurchaseRequisition.setRequiredDate(materialrequisitionResponse.getRequiredDate());
			materialpurchaseRequisition.setUserName(materialrequisitionResponse.getUserId());
			materialpurchaseRequisition.setStatus("Applied");
			materialpurchaseRequisition.setCreationDate(dtf.format(now));
			materialpurchaseRequisition.setUpdateDate(dtf.format(now));


			MaterialPurchaseRequisitionHistory materialpurchaseRequisitionHistory=new MaterialPurchaseRequisitionHistory();

			for(int i=0;i<materialrequisitionResponse.getItemList().size();i++)
			{
				try {

					requisitionHistoryId=RequisitionHistoryCode();
					materialpurchaseRequisitionHistory=new MaterialPurchaseRequisitionHistory();
					//materialpurchaseRequisitionHistory.setRequisitionHistoryId(requisitionHistoryId);
					materialpurchaseRequisitionHistory.setRequisitionId(requisitionId);
					materialpurchaseRequisitionHistory.setItemId(materialrequisitionResponse.getItemList().get(i).getItemId());
					materialpurchaseRequisitionHistory.setItemSize(materialrequisitionResponse.getItemList().get(i).getItemSize());
					materialpurchaseRequisitionHistory.setItemunitName(materialrequisitionResponse.getItemList().get(i).getItemunitName());
					materialpurchaseRequisitionHistory.setItemQuantity(materialrequisitionResponse.getItemList().get(i).getItemQuantity());
					materialpurchaseRequisitionHistoryRepository.save(materialpurchaseRequisitionHistory);
					totalNoItem=totalNoItem+1;
					totalQuantity=totalQuantity+materialrequisitionResponse.getItemList().get(i).getItemQuantity();
				}catch (Exception e) {
					e.printStackTrace();
					// TODO: handle exception
				}

			}

			materialpurchaseRequisition.setTotalNoItem(totalNoItem);
			materialpurchaseRequisition.setTotalQuantity(totalQuantity);
			materialpurchaseRequisitionRepository.save(materialpurchaseRequisition);

			response.setResponse("Requisition successfully added");
			response.setStatus(1);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			response.setResponse("Requisition added fail");
			response.setStatus(0);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
		}
	}



}
