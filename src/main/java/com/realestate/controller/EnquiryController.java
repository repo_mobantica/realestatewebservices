package com.realestate.controller;

import java.util.List;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.text.DateFormat;  
import java.text.SimpleDateFormat;  
import java.util.Date;  
import java.util.Calendar;  

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.Budget;
import com.realestate.bean.Enquiry;
import com.realestate.bean.EnquiryFollowUp;
import com.realestate.repository.EnquiryRepository;
import com.realestate.repository.EnquiryFollowUpRepository;
import com.realestate.response.CommanResponse;
import com.realestate.response.EnquiryFollowUpResponse;
import com.realestate.response.EnquiryResponse;
import com.realestate.response.EnquiryResponseDetails;
import com.realestate.response.FollowupResponse;
import com.realestate.response.LoginDetails;
import com.realestate.services.SmsServices;

@Controller
@RestController
@RequestMapping("enquiry")
public class EnquiryController {


	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	EnquiryRepository enquiryRepository;

	@Autowired
	EnquiryFollowUpRepository enquiryFollowUpRepository;


	int year = Calendar.getInstance().get(Calendar.YEAR);
	int month=Calendar.getInstance().get(Calendar.MONTH)+1;
	int y1,y2;

	String enquiryCode;
	private String EnquiryCode()
	{
		long enquiryCount = enquiryRepository.count();
		if(month>3) 
		{
			y1=year;
			y2=year+1;
			if(enquiryCount<10)
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/000"+(enquiryCount+1);
			}
			else if((enquiryCount>=10) && (enquiryCount<100))
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/00"+(enquiryCount+1);
			}
			else if((enquiryCount>=100) && (enquiryCount<1000))
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/0"+(enquiryCount+1);
			}
			else
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/"+(enquiryCount+1);
			}
		}
		else
		{
			y1=year-1;
			y2=year;
			if(enquiryCount<10)
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/000"+(enquiryCount+1);
			}
			else if((enquiryCount>=10) && (enquiryCount<100))
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/00"+(enquiryCount+1);
			}
			else if((enquiryCount>=100) && (enquiryCount<1000))
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/0"+(enquiryCount+1);
			}
			else 
			{
				enquiryCode = "ENQ/"+String.valueOf(y1).substring(2)+""+String.valueOf(y2).substring(2)+"/"+(enquiryCount+1);
			}
		}
		return enquiryCode;
	}

	@RequestMapping(value = "/addEnquiry", method = RequestMethod.POST)
	public ResponseEntity<CommanResponse> addEnquiry(@RequestBody EnquiryResponse enquiry) {

		CommanResponse response = new CommanResponse();

		try {

			EnquiryFollowUp enquiryFollowUp1=new EnquiryFollowUp();
			enquiryFollowUp1.setEnquiryId(EnquiryCode());
			enquiryFollowUp1.setFollowupDate(enquiry.getFollowupDate());

			enquiryFollowUp1.setEnqRemark(enquiry.getEnqRemark());
			enquiryFollowUp1.setEnqStatus(enquiry.getEnqStatus());
			enquiryFollowUp1.setFollowupTakenBy( enquiry.getUserId());
			enquiryFollowUp1.setCreationDate(new Date());
			enquiryFollowUp1.setUserName(enquiry.getUserId());
			enquiryFollowUpRepository.save(enquiryFollowUp1);

			//for enquiry table
			Enquiry enquiry1=new Enquiry();
			enquiry1.setEnquiryId(EnquiryCode());
			enquiry1.setEnqfirstName(enquiry.getEnqfirstName());
			enquiry1.setEnqmiddleName("");
			enquiry1.setEnqlastName("");
			enquiry1.setEnqEmail(enquiry.getEnqEmail());
			enquiry1.setEnqmobileNumber1(enquiry.getEnqmobileNumber1());
			enquiry1.setEnqmobileNumber2(enquiry.getEnqmobileNumber2());
			enquiry1.setEnqOccupation(enquiry.getEnqOccupation());
			enquiry1.setProjectName(enquiry.getProjectName());
			enquiry1.setFlatType(enquiry.getFlatType());
			enquiry1.setFlatRemark(enquiry.getFlatRemark());
			enquiry1.setFlatBudget(enquiry.getFlatBudget());
			enquiry1.setEnquirysourceId(enquiry.getEnquirysourceId());
			enquiry1.setSubsourceId(enquiry.getSubsourceId());
			enquiry1.setFollowupDate(enquiry.getFollowupDate());
			enquiry1.setEnqStatus(enquiry.getEnqStatus());
			enquiry1.setEnqRemark(enquiry.getEnqRemark());
			enquiry1.setCreationDate(new Date());
			enquiry1.setUpdateDate(new Date());
			enquiry1.setUserName(enquiry.getUserId());
			enquiryRepository.save(enquiry1);

			//Send sms to customer
			try {
				String sms="";
				sms="Dear "+enquiry.getEnqfirstName()+", Thanks for visiting us and enquiring a "+enquiry.getProjectName()+" project of Sonigara Group for "+enquiry.getFlatType()+" flat.\r\n" + 
						"Sales Ex Contact No:-09527023000"+".\r\n" +
						"Pls do visit again.";
				//	SmsServices.sendSMS(enquiry.getEnqmobileNumber1(),sms);
			}catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}


			response.setResponse("Enquiry successfully added");
			response.setStatus(1);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			response.setResponse("Enquiry added fail");
			response.setStatus(0);
			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
		}
	}


	@RequestMapping(value = "/getTodaysEnquiry", method = RequestMethod.POST)
	public ResponseEntity<EnquiryResponseDetails> getTodaysEnquiry(@RequestBody LoginDetails loginDetails) {

		EnquiryResponseDetails response = new EnquiryResponseDetails();

		try {
			//List<Enquiry> enquiryList=enquiryRepository.findByCreationDate(""+new Date());

			Date todayDate = new Date();
			final Calendar calendar = Calendar.getInstance();
			calendar.setTime(todayDate);
			calendar.add(Calendar.DAY_OF_YEAR, -1);
			Date date= calendar.getTime();

			Query query = new Query();
			List<Enquiry> enquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("creationDate").gte(date).lt(todayDate)), Enquiry.class);

			response.setResponse("Enquiry List");
			response.setStatus(1);
			response.setEnquiryList(enquiryList);
			return new ResponseEntity<EnquiryResponseDetails>(response,HttpStatus.OK);

		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<EnquiryResponseDetails>(response,HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/getTodayEnquiryFollowUp", method = RequestMethod.POST)
	public ResponseEntity<EnquiryResponseDetails> getTodayEnquiryFollowUp(@RequestBody LoginDetails loginDetails) {

		EnquiryResponseDetails response = new EnquiryResponseDetails();

		try {
			//List<Enquiry> enquiryList=enquiryRepository.findByCreationDate(""+new Date());

			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("d/M/yyyy");
			LocalDate localDate = LocalDate.now();
			String todayDate=dtf.format(localDate);

			List<Enquiry> todayenquiryList;
			//todayenquiryList = enquiryRepository.findAll();

			Query query = new Query();
			todayenquiryList = mongoTemplate.find(query.addCriteria(Criteria.where("followupDate").is(todayDate).and("enqStatus").ne("Booking")), Enquiry.class);

			response.setResponse("Enquiry List");
			response.setStatus(1);
			response.setEnquiryList(todayenquiryList);
			return new ResponseEntity<EnquiryResponseDetails>(response,HttpStatus.OK);

		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<EnquiryResponseDetails>(response,HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/enquiryFollowUp", method = RequestMethod.POST)
	public ResponseEntity<EnquiryFollowUpResponse> EnquiryFollowUp(@RequestBody Enquiry enquiry) {

		EnquiryFollowUpResponse response = new EnquiryFollowUpResponse();

		try {
			List<EnquiryFollowUp> followupList ; 
			Query query = new Query();
			followupList = mongoTemplate.find(query.addCriteria(Criteria.where("enquiryId").is(enquiry.getEnquiryId())), EnquiryFollowUp.class);
			DateFormat dateFormat = new SimpleDateFormat("d/M/yyyy"); 
			try {
				for(int i=0;i<followupList.size();i++)
				{
					try {
						followupList.get(i).setDate(""+ dateFormat.format(followupList.get(i).getCreationDate()));
					}catch (Exception e) {
						// TODO: handle exception
					}
				}

			}
			catch (Exception e) {
				// TODO: handle exception
			}

			query = new Query();
			Enquiry	enquiryDetails = mongoTemplate.findOne(query.addCriteria(Criteria.where("enquiryId").is(enquiry.getEnquiryId())), Enquiry.class);

			response.setResponse("Enquiry Details");
			response.setStatus(1);
			response.setEnquiryId(enquiryDetails.getEnquiryId());
			response.setEnqfirstName(enquiryDetails.getEnqfirstName());
			response.setEnqEmail(enquiryDetails.getEnqEmail());
			response.setEnqmobileNumber1(enquiryDetails.getEnqmobileNumber1());
			response.setEnqmobileNumber2(enquiryDetails.getEnqmobileNumber2());
			response.setFlatType(enquiryDetails.getFlatType());
			response.setProjectName(enquiryDetails.getProjectName());

			response.setFollowupList(followupList);
			return new ResponseEntity<EnquiryFollowUpResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {

			response.setResponse("fail");
			response.setStatus(0);

			return new ResponseEntity<EnquiryFollowUpResponse>(response,HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/addEnquiryFollowUp", method = RequestMethod.POST)
	public ResponseEntity<CommanResponse> addEnquiryFollowUp(@RequestBody FollowupResponse followupResponse) {

		CommanResponse response = new CommanResponse();

		try {

			EnquiryFollowUp enquiryFollowUp1=new EnquiryFollowUp();
			enquiryFollowUp1.setEnquiryId(followupResponse.getEnquiryId());
			enquiryFollowUp1.setFollowupDate(followupResponse.getFollowupDate());

			enquiryFollowUp1.setEnqRemark(followupResponse.getEnqRemark());
			enquiryFollowUp1.setEnqStatus(followupResponse.getEnqStatus());
			enquiryFollowUp1.setFollowupTakenBy(followupResponse.getUserId());
			enquiryFollowUp1.setCreationDate(new Date());
			enquiryFollowUp1.setUserName(followupResponse.getUserId());
			enquiryFollowUpRepository.save(enquiryFollowUp1);

			Enquiry enquiry = mongoTemplate.findOne(Query.query(Criteria.where("enquiryId").is(followupResponse.getEnquiryId())), Enquiry.class);
			enquiry.setFollowupDate(followupResponse.getFollowupDate());
			enquiry.setEnqStatus(followupResponse.getEnqStatus());
			enquiryRepository.save(enquiry);

			response.setResponse("Enquiry Followup add successfully");
			response.setStatus(1);

			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {

			response.setResponse("fail");
			response.setStatus(0);

			return new ResponseEntity<CommanResponse>(response,HttpStatus.OK);
		}
	}

}
