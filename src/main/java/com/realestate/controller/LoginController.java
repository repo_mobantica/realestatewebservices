package com.realestate.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.annotation.Transient;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.Department;
import com.realestate.bean.Designation;
import com.realestate.bean.Employee;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.UserAssignedProject;
import com.realestate.bean.UserModel;
import com.realestate.bean.UserProjectList;
import com.realestate.repository.DepartmentRepository;
import com.realestate.repository.DesignationRepository;
import com.realestate.repository.EmployeeRepository;
import com.realestate.repository.LoginRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.UserAssignedProjectRepository;
import com.realestate.repository.UserModelRepository;
import com.realestate.response.LoginDetails;
import com.realestate.response.LoginResponse;
import com.realestate.response.MenuListResponse;
import com.realestate.response.UserProjectDetails;
import com.realestate.services.ProjectServices;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@Controller
@RestController
@RequestMapping("")
public class LoginController {

	@Autowired
	LoginRepository loginRepository;
	@Autowired
	UserAssignedProjectRepository userprojectListRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	UserModelRepository userModelRepository;
	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	DepartmentRepository departmentRepository;
	@Autowired
	DesignationRepository designationRepository;

	@Value("${PROFILE_IMAGE_LOCATION}")
	private String PROFILE_IMAGE_LOCATION;
	
	/*@Autowired
    ProjectServices projectServices;

*/	@Autowired
	MongoTemplate mongoTemplate;

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<LoginResponse> loginDetails(@RequestBody LoginDetails loginDetails) {

		LoginResponse loginResponse = new LoginResponse();
		try {

			Login login =loginRepository.findByUserNameAndPassWordAndStatus(loginDetails.getUserName(), loginDetails.getPassWord(),"Active");
			//Login login =loginRepository.findByUserNameAndPassWord(loginDetails.getUserName(), loginDetails.getPassWord());
			
			if(login!=null)
			{

				//Query query = new Query();
				//List<UserProjectList> userLoginProjectList = mongoTemplate.find(query.addCriteria(Criteria.where("employeeId").is(login.getEmployeeId())), UserProjectList.class);

				List<UserAssignedProject> userLoginProjectList=userprojectListRepository.findByEmployeeId(login.getEmployeeId());
				Employee Employee=employeeRepository.findByEmployeeId(login.getEmployeeId());
				
				loginResponse.setEmployeeId(login.getEmployeeId());
				loginResponse.setRole(login.getRole());
				loginResponse.setUserId(login.getUserId());
				loginResponse.setUserName(login.getUserName());
				loginResponse.setPassWord(login.getPassWord());
				loginResponse.setEmployeefirstName(Employee.getEmployeefirstName());
				loginResponse.setEmployeemiddleName(Employee.getEmployeemiddleName());
				loginResponse.setEmployeelastName(Employee.getEmployeelastName());
				loginResponse.setEmployeeEmailId(Employee.getEmployeeEmailId());
				loginResponse.setEmployeeMobileno(Employee.getEmployeeMobileno());
				loginResponse.setEmployeeEducation(Employee.getEmployeeEducation());
				
				Department  department =departmentRepository.findByDepartmentId(Employee.getDepartmentId());
				Designation designation=designationRepository.findByDesignationId(Employee.getDesignationId());
				loginResponse.setDepartmentName(department.getDepartmentName());
				loginResponse.setDesignationName(designation.getDesignationName());
				loginResponse.setImagePath(""+PROFILE_IMAGE_LOCATION+Employee.getEmployeeId()+".png");
				loginResponse.setResponse("Login successfully");
				loginResponse.setStatus(1);

				UserProjectDetails project = new UserProjectDetails();

				List<UserProjectDetails> projectList=new ArrayList<UserProjectDetails>();

				for(int i=0;i<userLoginProjectList.size();i++)
				{
					project = new UserProjectDetails();
					Project project1=projectRepository.findByProjectId(userLoginProjectList.get(i).getProjectId());
					project.setProjectId(userLoginProjectList.get(i).getProjectId());
					project.setProjectName(project1.getProjectName());
					project.setGstPer(project1.getGstPer());
					project.setStampdutyPer(project1.getStampdutyPer());
					project.setRegistrationPer(project1.getRegistrationPer());
					project.setCompanyId(project1.getCompanyId());
					projectList.add(project);
				}
				
				loginResponse.setProjectList(projectList);
				MenuListResponse menulistResponse=new MenuListResponse();
				List<MenuListResponse> menuList=new ArrayList<MenuListResponse>();
				UserModel UserModel=userModelRepository.findByEmployeeId(login.getEmployeeId());
				try {
					if(UserModel.getAdministrator().equalsIgnoreCase("OK"))
					{
						menulistResponse=new MenuListResponse();
						menulistResponse.setId(1);
						menulistResponse.setMenuName("Adminstrator");
						menuList.add(menulistResponse);
						
					}
				}
				catch (Exception e) {}
				try {
					if(UserModel.getAddTask().equalsIgnoreCase("OK"))
					{
						menulistResponse=new MenuListResponse();
						menulistResponse.setId(2);
						menulistResponse.setMenuName("Task");
						menuList.add(menulistResponse);
						
					}
				}
				catch (Exception e) {}
				try {
					if(UserModel.getAgentPayment().equalsIgnoreCase("OK"))
					{
						menulistResponse=new MenuListResponse();
						menulistResponse.setId(3);
						menulistResponse.setMenuName("Agent Payment");
						menuList.add(menulistResponse);
						
					}
				}
				catch (Exception e) {}

				try {
					if(UserModel.getHrANDpayRollManagement().equalsIgnoreCase("OK"))
					{
						menulistResponse=new MenuListResponse();
						menulistResponse.setId(4);
						menulistResponse.setMenuName("Hr And Payroll");
						menuList.add(menulistResponse);
						
					}
				}
				catch (Exception e) {}

				try {
					if(UserModel.getJrEngineering().equalsIgnoreCase("OK"))
					{
						menulistResponse=new MenuListResponse();
						menulistResponse.setId(5);
						menulistResponse.setMenuName("Enginerring");
						menuList.add(menulistResponse);
						
					}
				}
				catch (Exception e) {}

				try {
					if(UserModel.getMaterialRequisitionList().equalsIgnoreCase("OK"))
					{
						menulistResponse=new MenuListResponse();
						menulistResponse.setId(6);
						menulistResponse.setMenuName("Material Requisition List");
						menuList.add(menulistResponse);
						
					}
				}
				catch (Exception e) {}

				try {
					if(UserModel.getJrSupervisor().equalsIgnoreCase("OK"))
					{
						menulistResponse=new MenuListResponse();
						menulistResponse.setId(7);
						menulistResponse.setMenuName("Jr. Supervisor");
						menuList.add(menulistResponse);
						
					}
				}
				catch (Exception e) {}

				try {
					if(UserModel.getMaterialTransferToContractor().equalsIgnoreCase("OK"))
					{
						menulistResponse=new MenuListResponse();
						menulistResponse.setId(8);
						menulistResponse.setMenuName("Material Transfer To Contractor");
						menuList.add(menulistResponse);
						
					}
				}
				catch (Exception e) {}

				try {
					if(UserModel.getPreSaleaManagment().equalsIgnoreCase("OK"))
					{
						menulistResponse=new MenuListResponse();
						menulistResponse.setId(9);
						menulistResponse.setMenuName("Pre Sale Managment");
						menuList.add(menulistResponse);
						
					}
				}
				catch (Exception e) {}

				try {
					if(UserModel.getProjectDevelopmentWork().equalsIgnoreCase("OK"))
					{
						menulistResponse=new MenuListResponse();
						menulistResponse.setId(10);
						menulistResponse.setMenuName("Project Development Work");
						menuList.add(menulistResponse);
						
					}
				}
				catch (Exception e) {}
				try {
					if(UserModel.getProjectEngineering().equalsIgnoreCase("OK"))
					{
						menulistResponse=new MenuListResponse();
						menulistResponse.setId(11);
						menulistResponse.setMenuName("Project Engineering");
						menuList.add(menulistResponse);
						
					}
				}
				catch (Exception e) {}
				
				try {
					if(UserModel.getProjectSpecificationMaster().equalsIgnoreCase("OK"))
					{
						menulistResponse=new MenuListResponse();
						menulistResponse.setId(12);
						menulistResponse.setMenuName("Project Specification Master");
						menuList.add(menulistResponse);
						
					}
				}
				catch (Exception e) {}

				try {
					if(UserModel.getContractorMagagement().equalsIgnoreCase("OK"))
					{
						menulistResponse=new MenuListResponse();
						menulistResponse.setId(13);
						menulistResponse.setMenuName("Contractor Managment");
						menuList.add(menulistResponse);
						
					}
				}
				catch (Exception e) {}
				try {
					if(UserModel.getSalesManagment().equalsIgnoreCase("OK"))
					{
						menulistResponse=new MenuListResponse();
						menulistResponse.setId(14);
						menulistResponse.setMenuName("Sale Managment");
						menuList.add(menulistResponse);
						
					}
				}
				catch (Exception e) {}
				try {
					if(UserModel.getStoreManagement().equalsIgnoreCase("OK"))
					{
						menulistResponse=new MenuListResponse();
						menulistResponse.setId(15);
						menulistResponse.setMenuName("Store Managment");
						menuList.add(menulistResponse);
						
					}
				}
				catch (Exception e) {}
				try {
					if(UserModel.getStoreStocks().equalsIgnoreCase("OK"))
					{
						menulistResponse=new MenuListResponse();
						menulistResponse.setId(16);
						menulistResponse.setMenuName("Store Stock");
						menuList.add(menulistResponse);
						
					}
				}
				catch (Exception e) {}
				try {
					if(UserModel.getSupplierPaymentBill().equalsIgnoreCase("OK"))
					{
						menulistResponse=new MenuListResponse();
						menulistResponse.setId(17);
						menulistResponse.setMenuName("Supplier Payment Bill");
						menuList.add(menulistResponse);
						
					}
				}
				catch (Exception e) {}
				
				try {
					if(UserModel.getFlatStatusReport().equalsIgnoreCase("OK"))
					{
						menulistResponse=new MenuListResponse();
						menulistResponse.setId(18);
						menulistResponse.setMenuName("Flat Status Report");
						menuList.add(menulistResponse);
						
					}
				}
				catch (Exception e) {}
				loginResponse.setMenuList(menuList);
			}
			else
			{
				loginResponse.setResponse("Invalid username Or Password");
				loginResponse.setStatus(0);
			}
			return new ResponseEntity<LoginResponse>(loginResponse,HttpStatus.OK);

		} catch (Exception ex) {
			loginResponse.setResponse("Invalid username Or Password");
			loginResponse.setStatus(0);
			return new ResponseEntity<LoginResponse>(loginResponse,HttpStatus.OK);
		}
	}



}
