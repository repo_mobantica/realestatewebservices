package com.realestate.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.Enquiry;
import com.realestate.bean.Login;
import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;
import com.realestate.bean.UserAssignedProject;
import com.realestate.repository.LoginRepository;
import com.realestate.repository.ProjectBuildingRepository;
import com.realestate.repository.ProjectRepository;
import com.realestate.repository.UserAssignedProjectRepository;
import com.realestate.repository.UserModelRepository;
import com.realestate.response.EnquiryResponseDetails;
import com.realestate.response.LoginDetails;
import com.realestate.response.ProjectBuildingResponse;
import com.realestate.response.UserProjectDetails;

@Controller
@RestController
@RequestMapping("building")
public class ProjectBuildingController {

	@Autowired
	MongoTemplate mongoTemplate;
	@Autowired
	LoginRepository loginRepository;
	@Autowired
	UserAssignedProjectRepository userprojectListRepository;
	@Autowired
	ProjectRepository projectRepository;
	@Autowired
	UserModelRepository userModelRepository;
	@Autowired
	ProjectBuildingRepository projectbuildingRepository;

	@RequestMapping(value = "/getBuildingList", method = RequestMethod.POST)
	public ResponseEntity<ProjectBuildingResponse> getBuildingList(@RequestBody LoginDetails loginDetails) {

		ProjectBuildingResponse response = new ProjectBuildingResponse();

		try {

			Login login =loginRepository.findByUserNameAndPassWordAndStatus(loginDetails.getUserName(), loginDetails.getPassWord(),"Active");

			if(login!=null)
			{
				List <ProjectBuilding> buildinglist=new ArrayList<ProjectBuilding>();
				List <Project> projectlist=new ArrayList<Project>();
				
				List<UserAssignedProject> userLoginProjectList=userprojectListRepository.findByEmployeeId(login.getEmployeeId());

				for(int i=0;i<userLoginProjectList.size();i++)
				{
					List <ProjectBuilding> list=projectbuildingRepository.findByProjectId(userLoginProjectList.get(i).getProjectId());
					buildinglist.addAll(list);
					Project Project=projectRepository.findByProjectId(userLoginProjectList.get(i).getProjectId());
					projectlist.add(Project);
				}
				
				response.setResponse("Project &  Building List");
				response.setStatus(1);
				response.setProjectbuildingList(buildinglist);
				response.setProjectList(projectlist);
				return new ResponseEntity<ProjectBuildingResponse>(response,HttpStatus.OK);
			}
			else
			{

				response.setResponse("Login fail");
				response.setStatus(0);

				return new ResponseEntity<ProjectBuildingResponse>(response,HttpStatus.OK);
			}
		} catch (Exception ex) {

			response.setResponse("Login fail");
			response.setStatus(0);

			return new ResponseEntity<ProjectBuildingResponse>(response,HttpStatus.OK);
		}
	}

}
