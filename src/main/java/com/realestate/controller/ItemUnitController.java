package com.realestate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.realestate.bean.ItemUnit;
import com.realestate.repository.ItemUnitRepository;
import com.realestate.response.ItemUnitResponse;
import com.realestate.response.LoginDetails;

@Controller
@RestController
@RequestMapping("itemUnit")
public class ItemUnitController {

	@Autowired
	MongoTemplate mongoTemplate;
	

	@Autowired
	ItemUnitRepository itemUnitRepository;
	
	@RequestMapping(value = "/getItemUnitList", method = RequestMethod.POST)
	public ResponseEntity<ItemUnitResponse> getItemUnitList(@RequestBody LoginDetails loginDetails) {

		ItemUnitResponse response = new ItemUnitResponse();
		
		try {
			List<ItemUnit> itemUnitList=itemUnitRepository.findAll();
			response.setItemUnitList(itemUnitList);
			response.setResponse("Login successfully");
			response.setStatus(1);
			return new ResponseEntity<ItemUnitResponse>(response,HttpStatus.OK);

		} catch (Exception ex) {
			response.setResponse("Login fail");
			response.setStatus(0);
			return new ResponseEntity<ItemUnitResponse>(response,HttpStatus.OK);
		}
	}
	
	
}
