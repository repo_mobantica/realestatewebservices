package com.realestate.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.ProjectBuilding;

public interface ProjectBuildingRepository  extends MongoRepository<ProjectBuilding, String>{

	List findByProjectId(String projectId);
}
