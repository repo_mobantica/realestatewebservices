package com.realestate.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.MaterialPurchaseRequisition;

public interface MaterialPurchaseRequisitionRepository extends MongoRepository<MaterialPurchaseRequisition, String> {

	List<MaterialPurchaseRequisition> findByStatusAndEmployeeId(String status,String employeeId);
	
	MaterialPurchaseRequisition findByRequisitionId(String requisitionId);
}
