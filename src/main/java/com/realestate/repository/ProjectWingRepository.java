package com.realestate.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.ProjectWing;

public interface ProjectWingRepository  extends MongoRepository<ProjectWing, String>{

	List findByProjectId(String projectId);
}
