package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.BankTransactionHistoryDetails;

public interface BankTransactionHistoryDetailsRepository extends MongoRepository<BankTransactionHistoryDetails, String>{

}
