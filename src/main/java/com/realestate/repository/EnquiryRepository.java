package com.realestate.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.Enquiry;

public interface EnquiryRepository extends MongoRepository<Enquiry, String> {

	List findByCreationDate(String creationDate);
}
