package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.Country;

public interface CountryRepository extends MongoRepository<Country, String>{

}
