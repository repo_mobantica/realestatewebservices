package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.City;

public interface CityRepository extends MongoRepository<City, String>{

}
