package com.realestate.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.Flat;

public interface FlatRepository extends MongoRepository<Flat, String>{

	List findByProjectId(String projectId);
}
