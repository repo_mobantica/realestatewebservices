package com.realestate.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.UserAssignedProject;
import com.realestate.bean.UserProjectList;

public interface UserAssignedProjectRepository extends MongoRepository<UserAssignedProject, String>{

	List<UserAssignedProject> findAll();
	
	List<UserAssignedProject> findByEmployeeId(String employeeId);
}
