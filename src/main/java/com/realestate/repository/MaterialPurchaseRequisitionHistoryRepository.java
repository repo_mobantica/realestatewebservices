package com.realestate.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.MaterialPurchaseRequisitionHistory;

public interface MaterialPurchaseRequisitionHistoryRepository extends MongoRepository<MaterialPurchaseRequisitionHistory, String>{

	List<MaterialPurchaseRequisitionHistory> findByRequisitionId(String requisitionId);
}
