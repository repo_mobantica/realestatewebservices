package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.ItemUnit;

public interface ItemUnitRepository extends MongoRepository<ItemUnit, String> {

}
