package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.CustomerPaymentStatusHistory;

public interface CustomerPaymentStatusHistoryRepository extends MongoRepository<CustomerPaymentStatusHistory, String>
{

}
