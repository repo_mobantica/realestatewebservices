package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.ContractorType;

public interface ContractorTypeRepository extends MongoRepository<ContractorType, String>{

}
