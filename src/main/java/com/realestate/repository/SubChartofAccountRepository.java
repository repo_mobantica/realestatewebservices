package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.SubChartofAccount;

public interface SubChartofAccountRepository extends MongoRepository<SubChartofAccount, String>{

}
