package com.realestate.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.SupplierQuotation;

public interface SupplierQuotationRepository extends MongoRepository<SupplierQuotation, String>{

	List<SupplierQuotation> findByRequisitionId(String requisitionId);
	
	SupplierQuotation findByQuotationId(String quotationId);
}
