package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.TaskHistory;

public interface TaskHistoryRepository extends MongoRepository<TaskHistory, String> 
{

}
