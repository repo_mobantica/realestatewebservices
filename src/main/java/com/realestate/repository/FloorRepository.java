package com.realestate.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.Floor;

public interface FloorRepository extends MongoRepository<Floor, String>{

	List findByProjectId(String projectId);
	Floor findByFloorId(String floorId);
}