package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.ContractorWorkListHistory;

public interface ContractorWorkListHistoryRepository extends MongoRepository<ContractorWorkListHistory, String>{

}
