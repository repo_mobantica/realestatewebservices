package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.MaterialsPurchased;

public interface MaterialsPurchasedRepository extends MongoRepository<MaterialsPurchased, String>{

}
