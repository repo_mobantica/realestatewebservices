package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.realestate.bean.Task;

public interface TaskRepository extends MongoRepository<Task, String>{

}
