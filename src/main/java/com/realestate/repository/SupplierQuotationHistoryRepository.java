package com.realestate.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.SupplierQuotationHistory;

public interface SupplierQuotationHistoryRepository extends  MongoRepository<SupplierQuotationHistory, String>{

	List<SupplierQuotationHistory> findByQuotationId(String quotationId);
}
