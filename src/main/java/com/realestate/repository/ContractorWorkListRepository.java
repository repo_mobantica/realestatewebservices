package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.ContractorWorkList;

public interface ContractorWorkListRepository extends MongoRepository<ContractorWorkList, String>{

}
