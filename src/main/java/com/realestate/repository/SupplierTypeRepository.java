package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.SupplierType;

public interface SupplierTypeRepository extends MongoRepository<SupplierType, String>{

}
