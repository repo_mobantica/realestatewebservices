package com.realestate.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.Booking;

public interface BookingRepository extends MongoRepository<Booking, String>{

	List findByProjectId(String projectId);
}
