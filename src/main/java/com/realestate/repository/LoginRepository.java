package com.realestate.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;

import com.realestate.bean.Booking;
import com.realestate.bean.Login;

@Transactional
public interface LoginRepository extends MongoRepository<Login, String> {

Login findByUserNameAndPassWord(String username, String passWord);
Login findByUserNameAndPassWordAndStatus(String username, String passWord,String status);
	
}