package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.transaction.annotation.Transactional;

import com.realestate.bean.Project;

@Transactional
public interface ProjectRepository extends MongoRepository<Project, String>{

	Project findByProjectId(String projectId);
}
