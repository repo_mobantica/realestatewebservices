package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.CompanyAccountPaymentDetails;

public interface CompanyAccountPaymentDetailsRepository extends MongoRepository<CompanyAccountPaymentDetails, String>
{

}
