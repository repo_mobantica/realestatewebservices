package com.realestate.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.realestate.bean.ChartofAccount;

public interface ChartofAccountRepository  extends MongoRepository<ChartofAccount, String>{

}
