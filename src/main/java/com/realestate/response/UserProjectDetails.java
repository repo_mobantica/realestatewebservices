package com.realestate.response;

public class UserProjectDetails {

	private String projectId;
	private String projectName;

	private double gstPer;
	private double stampdutyPer;
	private double registrationPer;
	private String companyId;
	
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public double getGstPer() {
		return gstPer;
	}
	public void setGstPer(double gstPer) {
		this.gstPer = gstPer;
	}
	public double getStampdutyPer() {
		return stampdutyPer;
	}
	public void setStampdutyPer(double stampdutyPer) {
		this.stampdutyPer = stampdutyPer;
	}
	public double getRegistrationPer() {
		return registrationPer;
	}
	public void setRegistrationPer(double registrationPer) {
		this.registrationPer = registrationPer;
	}
	public String getCompanyId() {
		return companyId;
	}
	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}
	
}
