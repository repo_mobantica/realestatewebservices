package com.realestate.response;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Transient;

import com.realestate.bean.TaskHistory;

public class TaskDetailsResponse {


	private String response;
	private int status;

	private String taskId;
	private String projectId;
	private String buildingId;
	private String wingId;
	private String floorId;
	private String flatId;
	private String taskName;
	private String taskassignFrom;
	private String taskassignTo;
	private String taskStatus;
    private String taskassingDate1;
    private String taskendDate1;
	List<TaskHistory> taskHistoryDetails=new ArrayList<TaskHistory>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getWingId() {
		return wingId;
	}

	public void setWingId(String wingId) {
		this.wingId = wingId;
	}

	public String getFloorId() {
		return floorId;
	}

	public void setFloorId(String floorId) {
		this.floorId = floorId;
	}

	public String getFlatId() {
		return flatId;
	}

	public void setFlatId(String flatId) {
		this.flatId = flatId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTaskassignFrom() {
		return taskassignFrom;
	}

	public void setTaskassignFrom(String taskassignFrom) {
		this.taskassignFrom = taskassignFrom;
	}

	public String getTaskassignTo() {
		return taskassignTo;
	}

	public void setTaskassignTo(String taskassignTo) {
		this.taskassignTo = taskassignTo;
	}

	public String getTaskStatus() {
		return taskStatus;
	}

	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}

	public List<TaskHistory> getTaskHistoryDetails() {
		return taskHistoryDetails;
	}

	public void setTaskHistoryDetails(List<TaskHistory> taskHistoryDetails) {
		this.taskHistoryDetails = taskHistoryDetails;
	}

	public String getTaskassingDate1() {
		return taskassingDate1;
	}

	public void setTaskassingDate1(String taskassingDate1) {
		this.taskassingDate1 = taskassingDate1;
	}

	public String getTaskendDate1() {
		return taskendDate1;
	}

	public void setTaskendDate1(String taskendDate1) {
		this.taskendDate1 = taskendDate1;
	}

	
}
