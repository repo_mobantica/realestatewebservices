package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.ContractorQuotation;

public class ContractorQuotationResponse {

	private String response;
	private int status;
	
	private String workId;
	private String projectId;
	private String buildingId;
    private String wingId;
    private String contractortypeId;
    
	private String projectName;
	private String buildingName;
    private String wingName;
    private String contractortype;
     
	List<ContractorQuotation> contractorquotationList=new ArrayList<ContractorQuotation>();

	public String getWorkId() {
		return workId;
	}

	public void setWorkId(String workId) {
		this.workId = workId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getWingId() {
		return wingId;
	}

	public void setWingId(String wingId) {
		this.wingId = wingId;
	}

	public String getContractortypeId() {
		return contractortypeId;
	}

	public void setContractortypeId(String contractortypeId) {
		this.contractortypeId = contractortypeId;
	}

	public List<ContractorQuotation> getContractorquotationList() {
		return contractorquotationList;
	}

	public void setContractorquotationList(List<ContractorQuotation> contractorquotationList) {
		this.contractorquotationList = contractorquotationList;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public String getWingName() {
		return wingName;
	}

	public void setWingName(String wingName) {
		this.wingName = wingName;
	}

	public String getContractortype() {
		return contractortype;
	}

	public void setContractortype(String contractortype) {
		this.contractortype = contractortype;
	}
	
}
