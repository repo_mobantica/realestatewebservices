package com.realestate.response;

public class EnquiryResponse {

	private String enqfirstName;
	private String enqEmail;
	private String enqmobileNumber1;
	private String enqmobileNumber2;
	private String enqOccupation;
	private String projectName;
	private String flatType;
	private String flatRemark;
	private String flatBudget;
	private String enquirysourceId;
	private String subsourceId;
	private String followupDate;
	private String enqStatus;
	private String enqRemark;
	
	private String userId;

	public String getEnqfirstName() {
		return enqfirstName;
	}

	public void setEnqfirstName(String enqfirstName) {
		this.enqfirstName = enqfirstName;
	}

	public String getEnqEmail() {
		return enqEmail;
	}

	public void setEnqEmail(String enqEmail) {
		this.enqEmail = enqEmail;
	}

	public String getEnqmobileNumber1() {
		return enqmobileNumber1;
	}

	public void setEnqmobileNumber1(String enqmobileNumber1) {
		this.enqmobileNumber1 = enqmobileNumber1;
	}

	public String getEnqmobileNumber2() {
		return enqmobileNumber2;
	}

	public void setEnqmobileNumber2(String enqmobileNumber2) {
		this.enqmobileNumber2 = enqmobileNumber2;
	}

	public String getEnqOccupation() {
		return enqOccupation;
	}

	public void setEnqOccupation(String enqOccupation) {
		this.enqOccupation = enqOccupation;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getFlatType() {
		return flatType;
	}

	public void setFlatType(String flatType) {
		this.flatType = flatType;
	}

	public String getFlatRemark() {
		return flatRemark;
	}

	public void setFlatRemark(String flatRemark) {
		this.flatRemark = flatRemark;
	}

	public String getFlatBudget() {
		return flatBudget;
	}

	public void setFlatBudget(String flatBudget) {
		this.flatBudget = flatBudget;
	}

	public String getEnquirysourceId() {
		return enquirysourceId;
	}

	public void setEnquirysourceId(String enquirysourceId) {
		this.enquirysourceId = enquirysourceId;
	}

	public String getSubsourceId() {
		return subsourceId;
	}

	public void setSubsourceId(String subsourceId) {
		this.subsourceId = subsourceId;
	}

	public String getFollowupDate() {
		return followupDate;
	}

	public void setFollowupDate(String followupDate) {
		this.followupDate = followupDate;
	}

	public String getEnqStatus() {
		return enqStatus;
	}

	public void setEnqStatus(String enqStatus) {
		this.enqStatus = enqStatus;
	}

	public String getEnqRemark() {
		return enqRemark;
	}

	public void setEnqRemark(String enqRemark) {
		this.enqRemark = enqRemark;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
}
