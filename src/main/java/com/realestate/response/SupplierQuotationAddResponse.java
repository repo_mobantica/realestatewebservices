package com.realestate.response;

import java.util.ArrayList;
import java.util.List;


public class SupplierQuotationAddResponse {

	private String supplierId;
	private String requisitionId;
	private String userId;

	List<SupplierQuotationHistoryResponse> itemList=new ArrayList<SupplierQuotationHistoryResponse>();

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getRequisitionId() {
		return requisitionId;
	}

	public void setRequisitionId(String requisitionId) {
		this.requisitionId = requisitionId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<SupplierQuotationHistoryResponse> getItemList() {
		return itemList;
	}

	public void setItemList(List<SupplierQuotationHistoryResponse> itemList) {
		this.itemList = itemList;
	}

}
