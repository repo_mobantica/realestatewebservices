package com.realestate.response;

public class UpdatePaymentStatusResponse {

	private String receiptId;
	private String bookingId;
	private String paymentType;
	private Double paymentAmount;
	private String companyBankId;
	private String paymentStatus;
	private String remark;
	private String paymentDate;
	private String chartaccountId;
	private String subchartaccountId;
    private String userId;
	
	public String getReceiptId() {
		return receiptId;
	}
	public void setReceiptId(String receiptId) {
		this.receiptId = receiptId;
	}
	public String getBookingId() {
		return bookingId;
	}
	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public Double getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(Double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public String getCompanyBankId() {
		return companyBankId;
	}
	public void setCompanyBankId(String companyBankId) {
		this.companyBankId = companyBankId;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public String getChartaccountId() {
		return chartaccountId;
	}
	public void setChartaccountId(String chartaccountId) {
		this.chartaccountId = chartaccountId;
	}
	public String getSubchartaccountId() {
		return subchartaccountId;
	}
	public void setSubchartaccountId(String subchartaccountId) {
		this.subchartaccountId = subchartaccountId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
