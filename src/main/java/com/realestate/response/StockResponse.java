package com.realestate.response;

import org.springframework.data.annotation.Transient;

public class StockResponse {

    private String itemId;
    private String storeId;
    private long noOfPieces;

    private String itemName;
    private String suppliertypeId;
    private String subsuppliertypeId;
    private Double itemQuantity;
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public long getNoOfPieces() {
		return noOfPieces;
	}
	public void setNoOfPieces(long noOfPieces) {
		this.noOfPieces = noOfPieces;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getSuppliertypeId() {
		return suppliertypeId;
	}
	public void setSuppliertypeId(String suppliertypeId) {
		this.suppliertypeId = suppliertypeId;
	}
	public String getSubsuppliertypeId() {
		return subsuppliertypeId;
	}
	public void setSubsuppliertypeId(String subsuppliertypeId) {
		this.subsuppliertypeId = subsuppliertypeId;
	}
	public Double getItemQuantity() {
		return itemQuantity;
	}
	public void setItemQuantity(Double itemQuantity) {
		this.itemQuantity = itemQuantity;
	}
    
    
}
