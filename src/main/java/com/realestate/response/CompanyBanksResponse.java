package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.CompanyBanks;

public class CompanyBanksResponse {

	private String response;
	private int status;
	
	List<CompanyBanks> companyBanksList=new ArrayList<CompanyBanks>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<CompanyBanks> getCompanyBanksList() {
		return companyBanksList;
	}

	public void setCompanyBanksList(List<CompanyBanks> companyBanksList) {
		this.companyBanksList = companyBanksList;
	}
	
}
