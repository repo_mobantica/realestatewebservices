package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.EnquiryFollowUp;

public class EnquiryFollowUpResponse {


	private String enquiryId;
	private String enqfirstName;
	private String enqEmail;
	private String enqmobileNumber1;
	private String enqmobileNumber2;
	private String projectName;
	private String flatType;
	

	private String response;
	private int status;
	
	List<EnquiryFollowUp> followupList=new ArrayList<EnquiryFollowUp>();

	public String getEnquiryId() {
		return enquiryId;
	}

	public void setEnquiryId(String enquiryId) {
		this.enquiryId = enquiryId;
	}

	public String getEnqfirstName() {
		return enqfirstName;
	}

	public void setEnqfirstName(String enqfirstName) {
		this.enqfirstName = enqfirstName;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<EnquiryFollowUp> getFollowupList() {
		return followupList;
	}

	public void setFollowupList(List<EnquiryFollowUp> followupList) {
		this.followupList = followupList;
	}

	public String getEnqEmail() {
		return enqEmail;
	}

	public void setEnqEmail(String enqEmail) {
		this.enqEmail = enqEmail;
	}

	public String getEnqmobileNumber1() {
		return enqmobileNumber1;
	}

	public void setEnqmobileNumber1(String enqmobileNumber1) {
		this.enqmobileNumber1 = enqmobileNumber1;
	}

	public String getEnqmobileNumber2() {
		return enqmobileNumber2;
	}

	public void setEnqmobileNumber2(String enqmobileNumber2) {
		this.enqmobileNumber2 = enqmobileNumber2;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getFlatType() {
		return flatType;
	}

	public void setFlatType(String flatType) {
		this.flatType = flatType;
	}

}
