package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.ContractorWorkListHistory;

public class ContractorWorkListDetailsResponse {


	private String response;
	private int status;

    private String workId;
	private String projectId;
	private String buildingId;
    private String wingId;
    private String contractortypeId;
	private String projectName;
	private String buildingName;
    private String wingName;
    private String contractorType;
    private long totalAmount;
	
	List<ContractorWorkListHistory> contractorworkListhistory=new ArrayList<ContractorWorkListHistory>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getWorkId() {
		return workId;
	}

	public void setWorkId(String workId) {
		this.workId = workId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getWingId() {
		return wingId;
	}

	public void setWingId(String wingId) {
		this.wingId = wingId;
	}

	public String getContractortypeId() {
		return contractortypeId;
	}

	public void setContractortypeId(String contractortypeId) {
		this.contractortypeId = contractortypeId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public String getWingName() {
		return wingName;
	}

	public void setWingName(String wingName) {
		this.wingName = wingName;
	}

	public String getContractorType() {
		return contractorType;
	}

	public void setContractorType(String contractorType) {
		this.contractorType = contractorType;
	}

	public long getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(long totalAmount) {
		this.totalAmount = totalAmount;
	}

	public List<ContractorWorkListHistory> getContractorworkListhistory() {
		return contractorworkListhistory;
	}

	public void setContractorworkListhistory(List<ContractorWorkListHistory> contractorworkListhistory) {
		this.contractorworkListhistory = contractorworkListhistory;
	}

}
