package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.ContractorWorkListHistory;

public class ContractorQuotationDetailsResponse {

	private String response;
	private int status;
	
    private String quotationId;
	private String workId;
    private String contractorId;
    private String contractorName;
    private long totalAmount;

	private String projectName;
	private String buildingName;
    private String wingName;
    private String contractortype;
    
	List<ContractorWorkListHistory> contractorworkList=new ArrayList<ContractorWorkListHistory>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getQuotationId() {
		return quotationId;
	}

	public void setQuotationId(String quotationId) {
		this.quotationId = quotationId;
	}

	public String getWorkId() {
		return workId;
	}

	public void setWorkId(String workId) {
		this.workId = workId;
	}

	public String getContractorId() {
		return contractorId;
	}

	public void setContractorId(String contractorId) {
		this.contractorId = contractorId;
	}

	public long getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(long totalAmount) {
		this.totalAmount = totalAmount;
	}

	public List<ContractorWorkListHistory> getContractorworkList() {
		return contractorworkList;
	}

	public void setContractorworkList(List<ContractorWorkListHistory> contractorworkList) {
		this.contractorworkList = contractorworkList;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public String getWingName() {
		return wingName;
	}

	public void setWingName(String wingName) {
		this.wingName = wingName;
	}

	public String getContractortype() {
		return contractortype;
	}

	public void setContractortype(String contractortype) {
		this.contractortype = contractortype;
	}

	public String getContractorName() {
		return contractorName;
	}

	public void setContractorName(String contractorName) {
		this.contractorName = contractorName;
	}
	
}
