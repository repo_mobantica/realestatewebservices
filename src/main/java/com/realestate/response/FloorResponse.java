package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.Floor;

public class FloorResponse {

	private String response;
	private int status;
	
	List<Floor> floorList=new ArrayList<Floor>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<Floor> getFloorList() {
		return floorList;
	}

	public void setFloorList(List<Floor> floorList) {
		this.floorList = floorList;
	}


}
