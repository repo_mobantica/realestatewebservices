package com.realestate.response;

public class BookingResponse {

	private String enquiryId;
	private String bookingName;
	private String bookingaddress;
	private String countryId;
	private String stateId;
	private String cityId;
	private String locationareaId;
	private String bookingPincode;
	private String bookingEmail;
	private String bookingmobileNumber1;
	private String bookingmobileNumber2;
	private String bookingOccupation;
	private String purposeOfFlat;
	
	private String projectId;
	private String buildingId;
	private String wingId;
	private String floorId;
	private String flatType;
	private String flatId;
	private String flatFacing;
	private Double flatareainSqFt;

	private Double flatCostwithotfloorise;
	private Double floorRise;
	
	private Double flatCost;
	private Double flatbasicCost;
	
	private String parkingFloorId;
	private String parkingZoneId;
	private String agentId;
	
	private Double infrastructureCharge;
	private Double aggreementValue1;
	private Double handlingCharges;
	private Double stampDutyPer;
	private Double registrationPer;
	private Double stampDuty1;
	private Double registrationCost1;
	private Double gstCost;
	private Double gstAmount1;
	private Double grandTotal1;
	private Double tds;
	
	private String flatBudget;
	private String enquirysourceId;
	private String subsourceId;
	private String userId;
	
	public String getEnquiryId() {
		return enquiryId;
	}
	public void setEnquiryId(String enquiryId) {
		this.enquiryId = enquiryId;
	}
	public String getBookingName() {
		return bookingName;
	}
	public void setBookingName(String bookingName) {
		this.bookingName = bookingName;
	}
	public String getBookingaddress() {
		return bookingaddress;
	}
	public void setBookingaddress(String bookingaddress) {
		this.bookingaddress = bookingaddress;
	}
	public String getCountryId() {
		return countryId;
	}
	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}
	public String getStateId() {
		return stateId;
	}
	public void setStateId(String stateId) {
		this.stateId = stateId;
	}
	public String getCityId() {
		return cityId;
	}
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}
	public String getLocationareaId() {
		return locationareaId;
	}
	public void setLocationareaId(String locationareaId) {
		this.locationareaId = locationareaId;
	}
	public String getBookingPincode() {
		return bookingPincode;
	}
	public void setBookingPincode(String bookingPincode) {
		this.bookingPincode = bookingPincode;
	}
	public String getBookingEmail() {
		return bookingEmail;
	}
	public void setBookingEmail(String bookingEmail) {
		this.bookingEmail = bookingEmail;
	}
	public String getBookingmobileNumber1() {
		return bookingmobileNumber1;
	}
	public void setBookingmobileNumber1(String bookingmobileNumber1) {
		this.bookingmobileNumber1 = bookingmobileNumber1;
	}
	public String getBookingmobileNumber2() {
		return bookingmobileNumber2;
	}
	public void setBookingmobileNumber2(String bookingmobileNumber2) {
		this.bookingmobileNumber2 = bookingmobileNumber2;
	}
	public String getBookingOccupation() {
		return bookingOccupation;
	}
	public void setBookingOccupation(String bookingOccupation) {
		this.bookingOccupation = bookingOccupation;
	}
	public String getPurposeOfFlat() {
		return purposeOfFlat;
	}
	public void setPurposeOfFlat(String purposeOfFlat) {
		this.purposeOfFlat = purposeOfFlat;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getBuildingId() {
		return buildingId;
	}
	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}
	public String getWingId() {
		return wingId;
	}
	public void setWingId(String wingId) {
		this.wingId = wingId;
	}
	public String getFloorId() {
		return floorId;
	}
	public void setFloorId(String floorId) {
		this.floorId = floorId;
	}
	public String getFlatType() {
		return flatType;
	}
	public void setFlatType(String flatType) {
		this.flatType = flatType;
	}
	public String getFlatId() {
		return flatId;
	}
	public void setFlatId(String flatId) {
		this.flatId = flatId;
	}
	public String getFlatFacing() {
		return flatFacing;
	}
	public void setFlatFacing(String flatFacing) {
		this.flatFacing = flatFacing;
	}
	public Double getFlatareainSqFt() {
		return flatareainSqFt;
	}
	public void setFlatareainSqFt(Double flatareainSqFt) {
		this.flatareainSqFt = flatareainSqFt;
	}
	public Double getFlatCostwithotfloorise() {
		return flatCostwithotfloorise;
	}
	public void setFlatCostwithotfloorise(Double flatCostwithotfloorise) {
		this.flatCostwithotfloorise = flatCostwithotfloorise;
	}
	public Double getFloorRise() {
		return floorRise;
	}
	public void setFloorRise(Double floorRise) {
		this.floorRise = floorRise;
	}
	public Double getFlatCost() {
		return flatCost;
	}
	public void setFlatCost(Double flatCost) {
		this.flatCost = flatCost;
	}
	public Double getFlatbasicCost() {
		return flatbasicCost;
	}
	public void setFlatbasicCost(Double flatbasicCost) {
		this.flatbasicCost = flatbasicCost;
	}
	public String getParkingFloorId() {
		return parkingFloorId;
	}
	public void setParkingFloorId(String parkingFloorId) {
		this.parkingFloorId = parkingFloorId;
	}
	public String getParkingZoneId() {
		return parkingZoneId;
	}
	public void setParkingZoneId(String parkingZoneId) {
		this.parkingZoneId = parkingZoneId;
	}
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public Double getInfrastructureCharge() {
		return infrastructureCharge;
	}
	public void setInfrastructureCharge(Double infrastructureCharge) {
		this.infrastructureCharge = infrastructureCharge;
	}
	public Double getAggreementValue1() {
		return aggreementValue1;
	}
	public void setAggreementValue1(Double aggreementValue1) {
		this.aggreementValue1 = aggreementValue1;
	}
	public Double getHandlingCharges() {
		return handlingCharges;
	}
	public void setHandlingCharges(Double handlingCharges) {
		this.handlingCharges = handlingCharges;
	}
	public Double getStampDutyPer() {
		return stampDutyPer;
	}
	public void setStampDutyPer(Double stampDutyPer) {
		this.stampDutyPer = stampDutyPer;
	}
	public Double getRegistrationPer() {
		return registrationPer;
	}
	public void setRegistrationPer(Double registrationPer) {
		this.registrationPer = registrationPer;
	}
	public Double getStampDuty1() {
		return stampDuty1;
	}
	public void setStampDuty1(Double stampDuty1) {
		this.stampDuty1 = stampDuty1;
	}
	public Double getRegistrationCost1() {
		return registrationCost1;
	}
	public void setRegistrationCost1(Double registrationCost1) {
		this.registrationCost1 = registrationCost1;
	}
	public Double getGstCost() {
		return gstCost;
	}
	public void setGstCost(Double gstCost) {
		this.gstCost = gstCost;
	}
	public Double getGstAmount1() {
		return gstAmount1;
	}
	public void setGstAmount1(Double gstAmount1) {
		this.gstAmount1 = gstAmount1;
	}
	public Double getGrandTotal1() {
		return grandTotal1;
	}
	public void setGrandTotal1(Double grandTotal1) {
		this.grandTotal1 = grandTotal1;
	}
	public Double getTds() {
		return tds;
	}
	public void setTds(Double tds) {
		this.tds = tds;
	}
	public String getFlatBudget() {
		return flatBudget;
	}
	public void setFlatBudget(String flatBudget) {
		this.flatBudget = flatBudget;
	}
	public String getEnquirysourceId() {
		return enquirysourceId;
	}
	public void setEnquirysourceId(String enquirysourceId) {
		this.enquirysourceId = enquirysourceId;
	}
	public String getSubsourceId() {
		return subsourceId;
	}
	public void setSubsourceId(String subsourceId) {
		this.subsourceId = subsourceId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
