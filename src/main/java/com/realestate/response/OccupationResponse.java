package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.Occupation;

public class OccupationResponse {

	private String response;
	private int status;
	
	List<Occupation> occupationList=new ArrayList<Occupation>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<Occupation> getOccupationList() {
		return occupationList;
	}

	public void setOccupationList(List<Occupation> occupationList) {
		this.occupationList = occupationList;
	}

}
