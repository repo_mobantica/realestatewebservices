package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.Item;

public class ItemResponse {

	private String response;
	private int status;
	
	List<Item> itemList=new ArrayList<Item>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<Item> getItemList() {
		return itemList;
	}

	public void setItemList(List<Item> itemList) {
		this.itemList = itemList;
	}

}
