package com.realestate.response;

public class MaterialRequisitionHistoryResponse {
	
	private String itemId;
	private String itemunitName;
	private String itemSize;
	private double itemQuantity;
	
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	public String getItemunitName() {
		return itemunitName;
	}
	public void setItemunitName(String itemunitName) {
		this.itemunitName = itemunitName;
	}
	public String getItemSize() {
		return itemSize;
	}
	public void setItemSize(String itemSize) {
		this.itemSize = itemSize;
	}
	public double getItemQuantity() {
		return itemQuantity;
	}
	public void setItemQuantity(double itemQuantity) {
		this.itemQuantity = itemQuantity;
	}
	
}
