package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.SupplierQuotationHistory;

public class SupplierQuotationDetailsResponse {

    private String quotationId;
	private String supplierId;
	private String requisitionId;
	private Double total;
	private Double discountAmount;
	private Double gstAmount;
    private Double grandTotal;
    

	private String response;
	private int status;
	
	List<SupplierQuotationHistory> itemList=new ArrayList<SupplierQuotationHistory>();

	public String getQuotationId() {
		return quotationId;
	}

	public void setQuotationId(String quotationId) {
		this.quotationId = quotationId;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getRequisitionId() {
		return requisitionId;
	}

	public void setRequisitionId(String requisitionId) {
		this.requisitionId = requisitionId;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Double getGstAmount() {
		return gstAmount;
	}

	public void setGstAmount(Double gstAmount) {
		this.gstAmount = gstAmount;
	}

	public Double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<SupplierQuotationHistory> getItemList() {
		return itemList;
	}

	public void setItemList(List<SupplierQuotationHistory> itemList) {
		this.itemList = itemList;
	}

}
