package com.realestate.response;

public class PurchaseSupplierQuotationResponse {

	    private String quotationId;
		private String supplierId;
		private String requisitionId;
		private String storeId;
		private String userId;

		private String delivery;
		private String paymentterms;
		private String afterdelivery;
		
		public String getQuotationId() {
			return quotationId;
		}
		public void setQuotationId(String quotationId) {
			this.quotationId = quotationId;
		}
		public String getSupplierId() {
			return supplierId;
		}
		public void setSupplierId(String supplierId) {
			this.supplierId = supplierId;
		}
		public String getRequisitionId() {
			return requisitionId;
		}
		public void setRequisitionId(String requisitionId) {
			this.requisitionId = requisitionId;
		}
		public String getStoreId() {
			return storeId;
		}
		public void setStoreId(String storeId) {
			this.storeId = storeId;
		}
		public String getUserId() {
			return userId;
		}
		public void setUserId(String userId) {
			this.userId = userId;
		}
		public String getDelivery() {
			return delivery;
		}
		public void setDelivery(String delivery) {
			this.delivery = delivery;
		}
		public String getPaymentterms() {
			return paymentterms;
		}
		public void setPaymentterms(String paymentterms) {
			this.paymentterms = paymentterms;
		}
		public String getAfterdelivery() {
			return afterdelivery;
		}
		public void setAfterdelivery(String afterdelivery) {
			this.afterdelivery = afterdelivery;
		}

}
