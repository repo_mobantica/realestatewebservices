package com.realestate.response;

import java.util.Date;

public class BookingViewResponse {


	private String response;
	private int status;
	private String bookingId;
	private String bookingName;
	private String bookingEmail;
	private String bookingmobileNumber1;
	private String bookingaddress;
	private String countryName;
	private String stateName;
	private String cityName;
	private String locationareaName;
	private String bookingPincode;
	private String bookingOccupation;
	private String purposeOfFlat;
	
	private String projectName;
	private String buildingName;
	private String wingName;
	private String floorName;
	private String flatType;
	private String flatNumber;
	private String flatFacing;
	private Double flatareainSqFt;

	private Double flatCostwithotfloorise;
	private Double floorRise;
	
	private Double flatCost;
	private Double flatbasicCost;
	
	private String parkingFloorName;
	private String parkingZoneName;
	private String agentName;
	
	private Double infrastructureCharge;
	private Double aggreementValue1;
	private Double handlingCharges;
	private Double stampDuty1;
	private Double registrationCost1;
	private Double gstAmount1;
	private Double grandTotal1;
	private Double tds;
	private String bookingstatus;
	private String creationDate;
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getBookingId() {
		return bookingId;
	}
	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}
	public String getBookingName() {
		return bookingName;
	}
	public void setBookingName(String bookingName) {
		this.bookingName = bookingName;
	}
	public String getBookingEmail() {
		return bookingEmail;
	}
	public void setBookingEmail(String bookingEmail) {
		this.bookingEmail = bookingEmail;
	}
	public String getBookingmobileNumber1() {
		return bookingmobileNumber1;
	}
	public void setBookingmobileNumber1(String bookingmobileNumber1) {
		this.bookingmobileNumber1 = bookingmobileNumber1;
	}
	public String getBookingaddress() {
		return bookingaddress;
	}
	public void setBookingaddress(String bookingaddress) {
		this.bookingaddress = bookingaddress;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public String getStateName() {
		return stateName;
	}
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getLocationareaName() {
		return locationareaName;
	}
	public void setLocationareaName(String locationareaName) {
		this.locationareaName = locationareaName;
	}
	public String getBookingPincode() {
		return bookingPincode;
	}
	public void setBookingPincode(String bookingPincode) {
		this.bookingPincode = bookingPincode;
	}
	public String getBookingOccupation() {
		return bookingOccupation;
	}
	public void setBookingOccupation(String bookingOccupation) {
		this.bookingOccupation = bookingOccupation;
	}
	public String getPurposeOfFlat() {
		return purposeOfFlat;
	}
	public void setPurposeOfFlat(String purposeOfFlat) {
		this.purposeOfFlat = purposeOfFlat;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getBuildingName() {
		return buildingName;
	}
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}
	public String getWingName() {
		return wingName;
	}
	public void setWingName(String wingName) {
		this.wingName = wingName;
	}
	public String getFloorName() {
		return floorName;
	}
	public void setFloorName(String floorName) {
		this.floorName = floorName;
	}
	public String getFlatType() {
		return flatType;
	}
	public void setFlatType(String flatType) {
		this.flatType = flatType;
	}
	public String getFlatNumber() {
		return flatNumber;
	}
	public void setFlatNumber(String flatNumber) {
		this.flatNumber = flatNumber;
	}
	public String getFlatFacing() {
		return flatFacing;
	}
	public void setFlatFacing(String flatFacing) {
		this.flatFacing = flatFacing;
	}
	public Double getFlatareainSqFt() {
		return flatareainSqFt;
	}
	public void setFlatareainSqFt(Double flatareainSqFt) {
		this.flatareainSqFt = flatareainSqFt;
	}
	public Double getFlatCostwithotfloorise() {
		return flatCostwithotfloorise;
	}
	public void setFlatCostwithotfloorise(Double flatCostwithotfloorise) {
		this.flatCostwithotfloorise = flatCostwithotfloorise;
	}
	public Double getFloorRise() {
		return floorRise;
	}
	public void setFloorRise(Double floorRise) {
		this.floorRise = floorRise;
	}
	public Double getFlatCost() {
		return flatCost;
	}
	public void setFlatCost(Double flatCost) {
		this.flatCost = flatCost;
	}
	public Double getFlatbasicCost() {
		return flatbasicCost;
	}
	public void setFlatbasicCost(Double flatbasicCost) {
		this.flatbasicCost = flatbasicCost;
	}
	public String getParkingFloorName() {
		return parkingFloorName;
	}
	public void setParkingFloorName(String parkingFloorName) {
		this.parkingFloorName = parkingFloorName;
	}
	public String getParkingZoneName() {
		return parkingZoneName;
	}
	public void setParkingZoneName(String parkingZoneName) {
		this.parkingZoneName = parkingZoneName;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	public Double getInfrastructureCharge() {
		return infrastructureCharge;
	}
	public void setInfrastructureCharge(Double infrastructureCharge) {
		this.infrastructureCharge = infrastructureCharge;
	}
	public Double getAggreementValue1() {
		return aggreementValue1;
	}
	public void setAggreementValue1(Double aggreementValue1) {
		this.aggreementValue1 = aggreementValue1;
	}
	public Double getHandlingCharges() {
		return handlingCharges;
	}
	public void setHandlingCharges(Double handlingCharges) {
		this.handlingCharges = handlingCharges;
	}
	public Double getStampDuty1() {
		return stampDuty1;
	}
	public void setStampDuty1(Double stampDuty1) {
		this.stampDuty1 = stampDuty1;
	}
	public Double getRegistrationCost1() {
		return registrationCost1;
	}
	public void setRegistrationCost1(Double registrationCost1) {
		this.registrationCost1 = registrationCost1;
	}
	public Double getGstAmount1() {
		return gstAmount1;
	}
	public void setGstAmount1(Double gstAmount1) {
		this.gstAmount1 = gstAmount1;
	}
	public Double getGrandTotal1() {
		return grandTotal1;
	}
	public void setGrandTotal1(Double grandTotal1) {
		this.grandTotal1 = grandTotal1;
	}
	public Double getTds() {
		return tds;
	}
	public void setTds(Double tds) {
		this.tds = tds;
	}
	public String getBookingstatus() {
		return bookingstatus;
	}
	public void setBookingstatus(String bookingstatus) {
		this.bookingstatus = bookingstatus;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	
}
