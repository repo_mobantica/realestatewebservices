package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.*;

public class ChartofAccountResponse {

	private String response;
	private int status;
	
	List<ChartofAccount> chartofaccountList=new ArrayList<ChartofAccount>();
	List<SubChartofAccount> subchartofaccountList=new ArrayList<SubChartofAccount>();
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public List<ChartofAccount> getChartofaccountList() {
		return chartofaccountList;
	}
	public void setChartofaccountList(List<ChartofAccount> chartofaccountList) {
		this.chartofaccountList = chartofaccountList;
	}
	public List<SubChartofAccount> getSubchartofaccountList() {
		return subchartofaccountList;
	}
	public void setSubchartofaccountList(List<SubChartofAccount> subchartofaccountList) {
		this.subchartofaccountList = subchartofaccountList;
	}

}
