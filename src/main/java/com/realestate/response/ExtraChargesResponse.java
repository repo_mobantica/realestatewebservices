package com.realestate.response;

public class ExtraChargesResponse {
	
	private String chargesId;
	private String bookingId;
	private String bookingDate;
	private String customerName;

	private long aggAmount;
	
	private long maintTotalAmount;
	private long maintPaidAmount;
	private long maintRemainingAmount;

	private long handlingTotalAmount;
	private long handlingPaidAmount;
	private long handlingRemainingAmount;

	private long extraChargesTotalAmount;
	private long extraChargesPaidAmount;
	private long extraChargesRemainingAmount;
	
	private String creationDate;
	private String updateDate;
	public String getChargesId() {
		return chargesId;
	}
	public void setChargesId(String chargesId) {
		this.chargesId = chargesId;
	}
	public String getBookingId() {
		return bookingId;
	}
	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public long getMaintTotalAmount() {
		return maintTotalAmount;
	}
	public void setMaintTotalAmount(long maintTotalAmount) {
		this.maintTotalAmount = maintTotalAmount;
	}
	public long getMaintPaidAmount() {
		return maintPaidAmount;
	}
	public void setMaintPaidAmount(long maintPaidAmount) {
		this.maintPaidAmount = maintPaidAmount;
	}
	public long getMaintRemainingAmount() {
		return maintRemainingAmount;
	}
	public void setMaintRemainingAmount(long maintRemainingAmount) {
		this.maintRemainingAmount = maintRemainingAmount;
	}
	public long getHandlingTotalAmount() {
		return handlingTotalAmount;
	}
	public void setHandlingTotalAmount(long handlingTotalAmount) {
		this.handlingTotalAmount = handlingTotalAmount;
	}
	public long getHandlingPaidAmount() {
		return handlingPaidAmount;
	}
	public void setHandlingPaidAmount(long handlingPaidAmount) {
		this.handlingPaidAmount = handlingPaidAmount;
	}
	public long getHandlingRemainingAmount() {
		return handlingRemainingAmount;
	}
	public void setHandlingRemainingAmount(long handlingRemainingAmount) {
		this.handlingRemainingAmount = handlingRemainingAmount;
	}
	public long getExtraChargesTotalAmount() {
		return extraChargesTotalAmount;
	}
	public void setExtraChargesTotalAmount(long extraChargesTotalAmount) {
		this.extraChargesTotalAmount = extraChargesTotalAmount;
	}
	public long getExtraChargesPaidAmount() {
		return extraChargesPaidAmount;
	}
	public void setExtraChargesPaidAmount(long extraChargesPaidAmount) {
		this.extraChargesPaidAmount = extraChargesPaidAmount;
	}
	public long getExtraChargesRemainingAmount() {
		return extraChargesRemainingAmount;
	}
	public void setExtraChargesRemainingAmount(long extraChargesRemainingAmount) {
		this.extraChargesRemainingAmount = extraChargesRemainingAmount;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public long getAggAmount() {
		return aggAmount;
	}
	public void setAggAmount(long aggAmount) {
		this.aggAmount = aggAmount;
	}
	
}
