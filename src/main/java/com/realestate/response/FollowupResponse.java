package com.realestate.response;

import java.util.Date;

public class FollowupResponse {


	private String enquiryId;
	private String followupDate;
	private String enqRemark;
	private String enqStatus;
	private String userId;
	public String getEnquiryId() {
		return enquiryId;
	}
	public void setEnquiryId(String enquiryId) {
		this.enquiryId = enquiryId;
	}
	public String getFollowupDate() {
		return followupDate;
	}
	public void setFollowupDate(String followupDate) {
		this.followupDate = followupDate;
	}
	public String getEnqRemark() {
		return enqRemark;
	}
	public void setEnqRemark(String enqRemark) {
		this.enqRemark = enqRemark;
	}
	public String getEnqStatus() {
		return enqStatus;
	}
	public void setEnqStatus(String enqStatus) {
		this.enqStatus = enqStatus;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	
}
