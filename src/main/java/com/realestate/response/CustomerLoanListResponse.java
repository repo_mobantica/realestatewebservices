package com.realestate.response;

import org.springframework.data.annotation.Transient;

public class CustomerLoanListResponse {


	private String customerloanId;
	private String bookingId;
	private String customerName;
	private Double totalAmount;	
	private Double loanAmount;
	private Double remainingAmount;
	private String bankName;
	private String fileNumber;
	private String bankPersonName;
	private String bankPersonMobNo;
	private String bankPersonEmailId;
	private String againstAggreement;
	private String againstStampDuty;
	private String againstRegistration;
	private String againstGST;
	
	private String totalAmount1;
	private String loanAmount1;
	private String remainingAmount1;
	private String againstAggreement1;
	private String againstStampDuty1;
	private String againstRegistration1;
	private String againstGST1;

	private String aggreementAmount;
	private String stampDutyAmount;
	private String registrationAmount;
	private String gstAmount;
	private String ownContributionAmount;
	public String getCustomerloanId() {
		return customerloanId;
	}
	public void setCustomerloanId(String customerloanId) {
		this.customerloanId = customerloanId;
	}
	public String getBookingId() {
		return bookingId;
	}
	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Double getLoanAmount() {
		return loanAmount;
	}
	public void setLoanAmount(Double loanAmount) {
		this.loanAmount = loanAmount;
	}
	public Double getRemainingAmount() {
		return remainingAmount;
	}
	public void setRemainingAmount(Double remainingAmount) {
		this.remainingAmount = remainingAmount;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getFileNumber() {
		return fileNumber;
	}
	public void setFileNumber(String fileNumber) {
		this.fileNumber = fileNumber;
	}
	public String getBankPersonName() {
		return bankPersonName;
	}
	public void setBankPersonName(String bankPersonName) {
		this.bankPersonName = bankPersonName;
	}
	public String getBankPersonMobNo() {
		return bankPersonMobNo;
	}
	public void setBankPersonMobNo(String bankPersonMobNo) {
		this.bankPersonMobNo = bankPersonMobNo;
	}
	public String getBankPersonEmailId() {
		return bankPersonEmailId;
	}
	public void setBankPersonEmailId(String bankPersonEmailId) {
		this.bankPersonEmailId = bankPersonEmailId;
	}
	public String getAgainstAggreement() {
		return againstAggreement;
	}
	public void setAgainstAggreement(String againstAggreement) {
		this.againstAggreement = againstAggreement;
	}
	public String getAgainstStampDuty() {
		return againstStampDuty;
	}
	public void setAgainstStampDuty(String againstStampDuty) {
		this.againstStampDuty = againstStampDuty;
	}
	public String getAgainstRegistration() {
		return againstRegistration;
	}
	public void setAgainstRegistration(String againstRegistration) {
		this.againstRegistration = againstRegistration;
	}
	public String getAgainstGST() {
		return againstGST;
	}
	public void setAgainstGST(String againstGST) {
		this.againstGST = againstGST;
	}
	public String getTotalAmount1() {
		return totalAmount1;
	}
	public void setTotalAmount1(String totalAmount1) {
		this.totalAmount1 = totalAmount1;
	}
	public String getLoanAmount1() {
		return loanAmount1;
	}
	public void setLoanAmount1(String loanAmount1) {
		this.loanAmount1 = loanAmount1;
	}
	public String getRemainingAmount1() {
		return remainingAmount1;
	}
	public void setRemainingAmount1(String remainingAmount1) {
		this.remainingAmount1 = remainingAmount1;
	}
	public String getAgainstAggreement1() {
		return againstAggreement1;
	}
	public void setAgainstAggreement1(String againstAggreement1) {
		this.againstAggreement1 = againstAggreement1;
	}
	public String getAgainstStampDuty1() {
		return againstStampDuty1;
	}
	public void setAgainstStampDuty1(String againstStampDuty1) {
		this.againstStampDuty1 = againstStampDuty1;
	}
	public String getAgainstRegistration1() {
		return againstRegistration1;
	}
	public void setAgainstRegistration1(String againstRegistration1) {
		this.againstRegistration1 = againstRegistration1;
	}
	public String getAgainstGST1() {
		return againstGST1;
	}
	public void setAgainstGST1(String againstGST1) {
		this.againstGST1 = againstGST1;
	}
	public String getAggreementAmount() {
		return aggreementAmount;
	}
	public void setAggreementAmount(String aggreementAmount) {
		this.aggreementAmount = aggreementAmount;
	}
	public String getStampDutyAmount() {
		return stampDutyAmount;
	}
	public void setStampDutyAmount(String stampDutyAmount) {
		this.stampDutyAmount = stampDutyAmount;
	}
	public String getRegistrationAmount() {
		return registrationAmount;
	}
	public void setRegistrationAmount(String registrationAmount) {
		this.registrationAmount = registrationAmount;
	}
	public String getGstAmount() {
		return gstAmount;
	}
	public void setGstAmount(String gstAmount) {
		this.gstAmount = gstAmount;
	}
	public String getOwnContributionAmount() {
		return ownContributionAmount;
	}
	public void setOwnContributionAmount(String ownContributionAmount) {
		this.ownContributionAmount = ownContributionAmount;
	}
	
}
