package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.ProjectWing;

public class ProjectWingResponse {

	private String response;
	private int status;
	
	List<ProjectWing> wingList=new ArrayList<ProjectWing>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<ProjectWing> getWingList() {
		return wingList;
	}

	public void setWingList(List<ProjectWing> wingList) {
		this.wingList = wingList;
	}

	
}
