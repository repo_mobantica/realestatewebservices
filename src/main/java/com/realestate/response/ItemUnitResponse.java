package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.ItemUnit;

public class ItemUnitResponse {

	private String response;
	private int status;
	
	List<ItemUnit> itemUnitList=new ArrayList<ItemUnit>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<ItemUnit> getItemUnitList() {
		return itemUnitList;
	}

	public void setItemUnitList(List<ItemUnit> itemUnitList) {
		this.itemUnitList = itemUnitList;
	}

}
