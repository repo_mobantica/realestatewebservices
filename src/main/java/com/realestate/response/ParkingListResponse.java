package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.Booking;

public class ParkingListResponse {

	private String response;
	private int status;

	List<Booking> parkingList=new ArrayList<Booking>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<Booking> getParkingList() {
		return parkingList;
	}

	public void setParkingList(List<Booking> parkingList) {
		this.parkingList = parkingList;
	}

}
