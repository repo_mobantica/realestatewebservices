package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.MaterialsPurchased;

public class MaterialsPurchasedResponse {

	private String response;
	private int status;
	
	List<MaterialsPurchased> materialspurchasedList=new ArrayList<MaterialsPurchased>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<MaterialsPurchased> getMaterialspurchasedList() {
		return materialspurchasedList;
	}

	public void setMaterialspurchasedList(List<MaterialsPurchased> materialspurchasedList) {
		this.materialspurchasedList = materialspurchasedList;
	}

}
