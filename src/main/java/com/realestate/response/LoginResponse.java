package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.Project;
import com.realestate.bean.ProjectDetails;
import com.realestate.bean.UserModel;

public class LoginResponse {


	private String response;
	private String userId;
	private String userName;
	private String passWord;
	private String role;
	
	private String employeeId;
	private String employeefirstName;
	private String employeemiddleName;
	private String employeelastName;
	private String employeeEducation;
	private String departmentName;
	private String designationName;
	private String employeeEmailId;
	private String employeeMobileno;
	private String imagePath;
	private int status;
	
	List<UserProjectDetails> projectList=new ArrayList<UserProjectDetails>(); 

	List<MenuListResponse> menuList=new ArrayList<MenuListResponse>(); 
	
	
	//String menuList1[]=new String[20];
	
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassWord() {
		return passWord;
	}
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public List<UserProjectDetails> getProjectList() {
		return projectList;
	}
	public void setProjectList(List<UserProjectDetails> projectList) {
		this.projectList = projectList;
	}
	public String getEmployeefirstName() {
		return employeefirstName;
	}
	public void setEmployeefirstName(String employeefirstName) {
		this.employeefirstName = employeefirstName;
	}
	public String getEmployeemiddleName() {
		return employeemiddleName;
	}
	public void setEmployeemiddleName(String employeemiddleName) {
		this.employeemiddleName = employeemiddleName;
	}
	public String getEmployeelastName() {
		return employeelastName;
	}
	public void setEmployeelastName(String employeelastName) {
		this.employeelastName = employeelastName;
	}
	public String getEmployeeEducation() {
		return employeeEducation;
	}
	public void setEmployeeEducation(String employeeEducation) {
		this.employeeEducation = employeeEducation;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getDesignationName() {
		return designationName;
	}
	public void setDesignationName(String designationName) {
		this.designationName = designationName;
	}
	public String getEmployeeEmailId() {
		return employeeEmailId;
	}
	public void setEmployeeEmailId(String employeeEmailId) {
		this.employeeEmailId = employeeEmailId;
	}
	public String getEmployeeMobileno() {
		return employeeMobileno;
	}
	public void setEmployeeMobileno(String employeeMobileno) {
		this.employeeMobileno = employeeMobileno;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public List<MenuListResponse> getMenuList() {
		return menuList;
	}
	public void setMenuList(List<MenuListResponse> menuList) {
		this.menuList = menuList;
	}
	
	
}
