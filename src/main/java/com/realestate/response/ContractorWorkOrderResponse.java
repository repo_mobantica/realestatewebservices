package com.realestate.response;

public class ContractorWorkOrderResponse {

	private String workId;
	private String quotationId;
	private String contractorId;
	private String contractorfirmName;
	private Double totalAmount;
	
	private long noofMaleLabour;
	private long noofFemaleLabour;
	private long noofInsuredLabour;
	
	private Double retentionPer;
	private Double retentionAmount;
	
	private String userId;

	public String getWorkId() {
		return workId;
	}

	public void setWorkId(String workId) {
		this.workId = workId;
	}

	public String getQuotationId() {
		return quotationId;
	}

	public void setQuotationId(String quotationId) {
		this.quotationId = quotationId;
	}

	public String getContractorId() {
		return contractorId;
	}

	public void setContractorId(String contractorId) {
		this.contractorId = contractorId;
	}

	public String getContractorfirmName() {
		return contractorfirmName;
	}

	public void setContractorfirmName(String contractorfirmName) {
		this.contractorfirmName = contractorfirmName;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public long getNoofMaleLabour() {
		return noofMaleLabour;
	}

	public void setNoofMaleLabour(long noofMaleLabour) {
		this.noofMaleLabour = noofMaleLabour;
	}

	public long getNoofFemaleLabour() {
		return noofFemaleLabour;
	}

	public void setNoofFemaleLabour(long noofFemaleLabour) {
		this.noofFemaleLabour = noofFemaleLabour;
	}

	public long getNoofInsuredLabour() {
		return noofInsuredLabour;
	}

	public void setNoofInsuredLabour(long noofInsuredLabour) {
		this.noofInsuredLabour = noofInsuredLabour;
	}

	public Double getRetentionPer() {
		return retentionPer;
	}

	public void setRetentionPer(Double retentionPer) {
		this.retentionPer = retentionPer;
	}

	public Double getRetentionAmount() {
		return retentionAmount;
	}

	public void setRetentionAmount(Double retentionAmount) {
		this.retentionAmount = retentionAmount;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
