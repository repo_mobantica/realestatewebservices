package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Transient;

import com.realestate.bean.ContractorWorkListHistory;
import com.realestate.bean.ContractorPaymentSchedule;

public class ContractorWorkOrderDetailsResponse {

	private String response;
	private int status;
	
	private String workOrderId;
	private String workId;
	private String quotationId;
	private String projectId;
	private String buildingId;
	private String wingId;
	private String contractortypeId;
	private String contractorId;
	private String contractorfirmName;
	private Double totalAmount;
	
	private long noofMaleLabour;
	private long noofFemaleLabour;
	private long noofInsuredLabour;
	
	private Double retentionPer;
	private Double retentionAmount;
	
	private String creationDate;
	
	private String paymentStatus;
	private String projectName;
	private String buildingName;
	private String wingName;
	private String contractortype;
	List<ContractorWorkListHistory> contractorworkList=new ArrayList<ContractorWorkListHistory>();
	List<ContractorPaymentSchedule> contractorpaymentscheduleList=new ArrayList<ContractorPaymentSchedule>();
	
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getWorkOrderId() {
		return workOrderId;
	}
	public void setWorkOrderId(String workOrderId) {
		this.workOrderId = workOrderId;
	}
	public String getWorkId() {
		return workId;
	}
	public void setWorkId(String workId) {
		this.workId = workId;
	}
	public String getQuotationId() {
		return quotationId;
	}
	public void setQuotationId(String quotationId) {
		this.quotationId = quotationId;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getBuildingId() {
		return buildingId;
	}
	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}
	public String getWingId() {
		return wingId;
	}
	public void setWingId(String wingId) {
		this.wingId = wingId;
	}
	public String getContractortypeId() {
		return contractortypeId;
	}
	public void setContractortypeId(String contractortypeId) {
		this.contractortypeId = contractortypeId;
	}
	public String getContractorId() {
		return contractorId;
	}
	public void setContractorId(String contractorId) {
		this.contractorId = contractorId;
	}
	public String getContractorfirmName() {
		return contractorfirmName;
	}
	public void setContractorfirmName(String contractorfirmName) {
		this.contractorfirmName = contractorfirmName;
	}
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public long getNoofMaleLabour() {
		return noofMaleLabour;
	}
	public void setNoofMaleLabour(long noofMaleLabour) {
		this.noofMaleLabour = noofMaleLabour;
	}
	public long getNoofFemaleLabour() {
		return noofFemaleLabour;
	}
	public void setNoofFemaleLabour(long noofFemaleLabour) {
		this.noofFemaleLabour = noofFemaleLabour;
	}
	public long getNoofInsuredLabour() {
		return noofInsuredLabour;
	}
	public void setNoofInsuredLabour(long noofInsuredLabour) {
		this.noofInsuredLabour = noofInsuredLabour;
	}
	public Double getRetentionPer() {
		return retentionPer;
	}
	public void setRetentionPer(Double retentionPer) {
		this.retentionPer = retentionPer;
	}
	public Double getRetentionAmount() {
		return retentionAmount;
	}
	public void setRetentionAmount(Double retentionAmount) {
		this.retentionAmount = retentionAmount;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public List<ContractorWorkListHistory> getContractorworkList() {
		return contractorworkList;
	}
	public void setContractorworkList(List<ContractorWorkListHistory> contractorworkList) {
		this.contractorworkList = contractorworkList;
	}
	public List<ContractorPaymentSchedule> getContractorpaymentscheduleList() {
		return contractorpaymentscheduleList;
	}
	public void setContractorpaymentscheduleList(List<ContractorPaymentSchedule> contractorpaymentscheduleList) {
		this.contractorpaymentscheduleList = contractorpaymentscheduleList;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getBuildingName() {
		return buildingName;
	}
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}
	public String getWingName() {
		return wingName;
	}
	public void setWingName(String wingName) {
		this.wingName = wingName;
	}
	public String getContractortype() {
		return contractortype;
	}
	public void setContractortype(String contractortype) {
		this.contractortype = contractortype;
	}
	
}
