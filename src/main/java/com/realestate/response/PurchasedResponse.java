package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Transient;

import com.realestate.bean.MaterialsPurchasedHistory;

public class PurchasedResponse {

	private String response;
	private int status;
	
	private String materialsPurchasedId;
	private String requisitionId;
	private String supplierId;
	private String storeId;
	private String employeeId;
	
	private Double totalPrice;
	private Double totalDiscount;
	private Double totalGstAmount;
	private Double totalAmount;

	private String supplierfirmName;
	private String storeName;
	private String employeeName;
	
	List<MaterialsPurchasedHistory> materialList=new ArrayList<MaterialsPurchasedHistory>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMaterialsPurchasedId() {
		return materialsPurchasedId;
	}

	public void setMaterialsPurchasedId(String materialsPurchasedId) {
		this.materialsPurchasedId = materialsPurchasedId;
	}

	public String getRequisitionId() {
		return requisitionId;
	}

	public void setRequisitionId(String requisitionId) {
		this.requisitionId = requisitionId;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Double getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	public Double getTotalGstAmount() {
		return totalGstAmount;
	}

	public void setTotalGstAmount(Double totalGstAmount) {
		this.totalGstAmount = totalGstAmount;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getSupplierfirmName() {
		return supplierfirmName;
	}

	public void setSupplierfirmName(String supplierfirmName) {
		this.supplierfirmName = supplierfirmName;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public List<MaterialsPurchasedHistory> getMaterialList() {
		return materialList;
	}

	public void setMaterialList(List<MaterialsPurchasedHistory> materialList) {
		this.materialList = materialList;
	}
	
}
