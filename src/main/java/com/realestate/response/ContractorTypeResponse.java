package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.ContractorType;
import com.realestate.bean.SubContractorType;

public class ContractorTypeResponse {

	private String response;
	private int status;
	
	List<ContractorType> contractorTypeList=new ArrayList<ContractorType>();
	List<SubContractorType> subContractorTypeList=new ArrayList<SubContractorType>();
	
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public List<ContractorType> getContractorTypeList() {
		return contractorTypeList;
	}
	public void setContractorTypeList(List<ContractorType> contractorTypeList) {
		this.contractorTypeList = contractorTypeList;
	}
	public List<SubContractorType> getSubContractorTypeList() {
		return subContractorTypeList;
	}
	public void setSubContractorTypeList(List<SubContractorType> subContractorTypeList) {
		this.subContractorTypeList = subContractorTypeList;
	}

}
