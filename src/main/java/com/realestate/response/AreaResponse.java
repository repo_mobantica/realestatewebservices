package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.*;

public class AreaResponse {

	private String response;
	private int status;

	List<Country> countryList=new ArrayList<Country>();
	List<State> stateList=new ArrayList<State>();
	List<City> cityList=new ArrayList<City>();
	List<LocationArea> locationareaList=new ArrayList<LocationArea>();
	
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public List<Country> getCountryList() {
		return countryList;
	}
	public void setCountryList(List<Country> countryList) {
		this.countryList = countryList;
	}
	public List<State> getStateList() {
		return stateList;
	}
	public void setStateList(List<State> stateList) {
		this.stateList = stateList;
	}
	public List<City> getCityList() {
		return cityList;
	}
	public void setCityList(List<City> cityList) {
		this.cityList = cityList;
	}
	public List<LocationArea> getLocationareaList() {
		return locationareaList;
	}
	public void setLocationareaList(List<LocationArea> locationareaList) {
		this.locationareaList = locationareaList;
	}

}
