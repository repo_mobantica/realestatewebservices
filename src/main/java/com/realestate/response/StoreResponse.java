package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.Store;

public class StoreResponse {

	private String response;
	private int status;
	
	List<Store> storeList=new ArrayList<Store>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<Store> getStoreList() {
		return storeList;
	}

	public void setStoreList(List<Store> storeList) {
		this.storeList = storeList;
	}

}
