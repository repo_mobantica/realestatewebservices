package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.ContractorWorkListHistory;

public class AddContractorWorkListDetailsResponse {

	private String projectId;
	private String buildingId;
    private String wingId;
    private String contractortypeId;
	private String userId;
     
	List<ContractorWorkListHistory> contractorworkListhistory=new ArrayList<ContractorWorkListHistory>();

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getWingId() {
		return wingId;
	}

	public void setWingId(String wingId) {
		this.wingId = wingId;
	}

	public String getContractortypeId() {
		return contractortypeId;
	}

	public void setContractortypeId(String contractortypeId) {
		this.contractortypeId = contractortypeId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<ContractorWorkListHistory> getContractorworkListhistory() {
		return contractorworkListhistory;
	}

	public void setContractorworkListhistory(List<ContractorWorkListHistory> contractorworkListhistory) {
		this.contractorworkListhistory = contractorworkListhistory;
	}
	
}
