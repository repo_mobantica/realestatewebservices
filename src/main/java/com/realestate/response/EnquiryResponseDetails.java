package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.Enquiry;

public class EnquiryResponseDetails {

	private String response;
	private int status;
	
	List<Enquiry> enquiryList=new ArrayList<Enquiry>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<Enquiry> getEnquiryList() {
		return enquiryList;
	}

	public void setEnquiryList(List<Enquiry> enquiryList) {
		this.enquiryList = enquiryList;
	}

}
