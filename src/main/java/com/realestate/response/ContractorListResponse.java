package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.Contractor;

public class ContractorListResponse {

	private String response;
	private int status;
	
	List<Contractor> contractorList=new ArrayList<Contractor>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<Contractor> getContractorList() {
		return contractorList;
	}

	public void setContractorList(List<Contractor> contractorList) {
		this.contractorList = contractorList;
	}
	
	
}
