package com.realestate.response;

import java.util.ArrayList;
import java.util.List;


public class CustomerLoanDetailsResponse {

	private String response;
	private int status;

	List<CustomerLoanListResponse> customerList=new ArrayList<CustomerLoanListResponse>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<CustomerLoanListResponse> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(List<CustomerLoanListResponse> customerList) {
		this.customerList = customerList;
	}
}
