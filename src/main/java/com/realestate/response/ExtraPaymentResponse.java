package com.realestate.response;

public class ExtraPaymentResponse {

	private String bookingId;
	private double  maintainanceAmt;
	private double  handlingChargesAmt;
	private double  extraChargesAmt;

	private String  paymentMode;
	private String  bankName;
	private String  branchName;
	private String  chequeNumber;
	private String  narration;

	private String  date;
	private String userId;

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public double getMaintainanceAmt() {
		return maintainanceAmt;
	}

	public void setMaintainanceAmt(double maintainanceAmt) {
		this.maintainanceAmt = maintainanceAmt;
	}

	public double getHandlingChargesAmt() {
		return handlingChargesAmt;
	}

	public void setHandlingChargesAmt(double handlingChargesAmt) {
		this.handlingChargesAmt = handlingChargesAmt;
	}

	public double getExtraChargesAmt() {
		return extraChargesAmt;
	}

	public void setExtraChargesAmt(double extraChargesAmt) {
		this.extraChargesAmt = extraChargesAmt;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getChequeNumber() {
		return chequeNumber;
	}

	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}

	public String getNarration() {
		return narration;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
