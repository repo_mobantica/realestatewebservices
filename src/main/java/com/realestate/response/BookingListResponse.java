package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.Booking;

public class BookingListResponse {

	private String response;
	private int status;

	List<Booking> bookingList=new ArrayList<Booking>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<Booking> getBookingList() {
		return bookingList;
	}

	public void setBookingList(List<Booking> bookingList) {
		this.bookingList = bookingList;
	}

}
