package com.realestate.response;

public class WingwiseParkingResponse {

	private String projectId;
	private String buildingId;
	private String wingId;
	
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getBuildingId() {
		return buildingId;
	}
	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}
	public String getWingId() {
		return wingId;
	}
	public void setWingId(String wingId) {
		this.wingId = wingId;
	}
	
}
