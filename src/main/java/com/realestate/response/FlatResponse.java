package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.Flat;

public class FlatResponse {

	private String response;
	private int status;
	
	List<Flat> flatList=new ArrayList<Flat>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<Flat> getFlatList() {
		return flatList;
	}

	public void setFlatList(List<Flat> flatList) {
		this.flatList = flatList;
	}

}
