package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.Budget;

public class BudgetResponse {

	private String response;
	private int status;
	
	List<Budget> budgetList=new ArrayList<Budget>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<Budget> getBudgetList() {
		return budgetList;
	}

	public void setBudgetList(List<Budget> budgetList) {
		this.budgetList = budgetList;
	}

	
}
