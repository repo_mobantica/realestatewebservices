package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.ContractorWorkOrder;

public class ContractorWorkResponse {

	private String response;
	private int status;
	
	List<ContractorWorkOrder> contractorworkOrderList=new ArrayList<ContractorWorkOrder>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<ContractorWorkOrder> getContractorworkOrderList() {
		return contractorworkOrderList;
	}

	public void setContractorworkOrderList(List<ContractorWorkOrder> contractorworkOrderList) {
		this.contractorworkOrderList = contractorworkOrderList;
	}

}
