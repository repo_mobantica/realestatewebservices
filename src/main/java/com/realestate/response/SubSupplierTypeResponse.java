package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.SubSupplierType;

public class SubSupplierTypeResponse {

	private String response;
	private int status;
	
	List<SubSupplierType> subsuppliertypeList=new ArrayList<SubSupplierType>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<SubSupplierType> getSubsuppliertypeList() {
		return subsuppliertypeList;
	}

	public void setSubsuppliertypeList(List<SubSupplierType> subsuppliertypeList) {
		this.subsuppliertypeList = subsuppliertypeList;
	}

}
