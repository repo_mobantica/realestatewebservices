package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.ContractorWorkList;

public class ContractorWorkListResponse {

	private String response;
	private int status;
	
	List<ContractorWorkList> contractorworkList=new ArrayList<ContractorWorkList>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<ContractorWorkList> getContractorworkList() {
		return contractorworkList;
	}

	public void setContractorworkList(List<ContractorWorkList> contractorworkList) {
		this.contractorworkList = contractorworkList;
	}

}
