package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.FlatPossessionCheckList;

public class FlatPossessionCheckListResponse {


	private String response;
	private int status;
	
	List<FlatPossessionCheckList> flatpossessionchecklistdetails=new ArrayList<FlatPossessionCheckList>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<FlatPossessionCheckList> getFlatpossessionchecklistdetails() {
		return flatpossessionchecklistdetails;
	}

	public void setFlatpossessionchecklistdetails(List<FlatPossessionCheckList> flatpossessionchecklistdetails) {
		this.flatpossessionchecklistdetails = flatpossessionchecklistdetails;
	}
	
}
