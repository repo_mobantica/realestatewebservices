package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.Task;

public class TaskResponse {


	private String response;
	private int status;
	
	List<Task> taskList=new ArrayList<Task>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<Task> getTaskList() {
		return taskList;
	}

	public void setTaskList(List<Task> taskList) {
		this.taskList = taskList;
	}

}
