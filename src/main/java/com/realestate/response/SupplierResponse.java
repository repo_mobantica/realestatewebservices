package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.Supplier;

public class SupplierResponse {

	private String response;
	private int status;
	
	List<Supplier> supplierList=new ArrayList<Supplier>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<Supplier> getSupplierList() {
		return supplierList;
	}

	public void setSupplierList(List<Supplier> supplierList) {
		this.supplierList = supplierList;
	}

}
