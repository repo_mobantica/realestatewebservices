package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.SupplierQuotation;

public class SupplierQuotationResponse {

	private String response;
	private int status;
	
	List<SupplierQuotation> supplierQuotationList=new ArrayList<SupplierQuotation>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<SupplierQuotation> getSupplierQuotationList() {
		return supplierQuotationList;
	}

	public void setSupplierQuotationList(List<SupplierQuotation> supplierQuotationList) {
		this.supplierQuotationList = supplierQuotationList;
	}

}
