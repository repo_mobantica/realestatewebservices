package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.SupplierType;

public class SupplierTypeResponse {

	private String response;
	private int status;
	
	List<SupplierType> suppliertypeList=new ArrayList<SupplierType>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<SupplierType> getSuppliertypeList() {
		return suppliertypeList;
	}

	public void setSuppliertypeList(List<SupplierType> suppliertypeList) {
		this.suppliertypeList = suppliertypeList;
	}

}
