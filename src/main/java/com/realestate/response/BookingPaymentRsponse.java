package com.realestate.response;

public class BookingPaymentRsponse {

	private String bookingId;
	private double aggreementAmount;
	private double stampDutyAmount;
	private double registrationAmount;	
	private double gstAmount;

	private String paymentMode;
	private String bankName;	
	private String branchName;
	private String chequeNumber;	
	private String narration;	
	private String date;		
	private String userId;
	public String getBookingId() {
		return bookingId;
	}
	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}
	public double getAggreementAmount() {
		return aggreementAmount;
	}
	public void setAggreementAmount(double aggreementAmount) {
		this.aggreementAmount = aggreementAmount;
	}
	public double getStampDutyAmount() {
		return stampDutyAmount;
	}
	public void setStampDutyAmount(double stampDutyAmount) {
		this.stampDutyAmount = stampDutyAmount;
	}
	public double getRegistrationAmount() {
		return registrationAmount;
	}
	public void setRegistrationAmount(double registrationAmount) {
		this.registrationAmount = registrationAmount;
	}
	public double getGstAmount() {
		return gstAmount;
	}
	public void setGstAmount(double gstAmount) {
		this.gstAmount = gstAmount;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getChequeNumber() {
		return chequeNumber;
	}
	public void setChequeNumber(String chequeNumber) {
		this.chequeNumber = chequeNumber;
	}
	public String getNarration() {
		return narration;
	}
	public void setNarration(String narration) {
		this.narration = narration;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
}
