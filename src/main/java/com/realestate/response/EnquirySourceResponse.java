package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.EnquirySource;

public class EnquirySourceResponse {


	private String response;
	private int status;
	
	List<EnquirySource> enquirySourceList=new ArrayList<EnquirySource>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<EnquirySource> getEnquirySourceList() {
		return enquirySourceList;
	}

	public void setEnquirySourceList(List<EnquirySource> enquirySourceList) {
		this.enquirySourceList = enquirySourceList;
	}    
	
	
}
