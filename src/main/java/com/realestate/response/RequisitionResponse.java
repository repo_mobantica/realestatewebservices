package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

public class RequisitionResponse {

	private String response;
	private int status;
	
	List<MaterialPurchaseRequisitionResponse> requisitionList=new ArrayList<MaterialPurchaseRequisitionResponse>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<MaterialPurchaseRequisitionResponse> getRequisitionList() {
		return requisitionList;
	}

	public void setRequisitionList(List<MaterialPurchaseRequisitionResponse> requisitionList) {
		this.requisitionList = requisitionList;
	}

}
