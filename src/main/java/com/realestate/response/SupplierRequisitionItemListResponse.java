package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.MaterialPurchaseRequisitionHistory;

public class SupplierRequisitionItemListResponse {
	private String requisitionId;
	private String employeeId;
	private String projectId;
	private String buildingId;
	private String wingId;
	private String requiredDate;
	private int totalNoItem;
	private double totalQuantity;
	private String requisitionstatus;
	
	private String response;
	private int status;
	
	List<MaterialPurchaseRequisitionHistory> itemList=new ArrayList<MaterialPurchaseRequisitionHistory>();

	public String getRequisitionId() {
		return requisitionId;
	}

	public void setRequisitionId(String requisitionId) {
		this.requisitionId = requisitionId;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getWingId() {
		return wingId;
	}

	public void setWingId(String wingId) {
		this.wingId = wingId;
	}

	public String getRequiredDate() {
		return requiredDate;
	}

	public void setRequiredDate(String requiredDate) {
		this.requiredDate = requiredDate;
	}

	public int getTotalNoItem() {
		return totalNoItem;
	}

	public void setTotalNoItem(int totalNoItem) {
		this.totalNoItem = totalNoItem;
	}

	public double getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(double totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public String getRequisitionstatus() {
		return requisitionstatus;
	}

	public void setRequisitionstatus(String requisitionstatus) {
		this.requisitionstatus = requisitionstatus;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<MaterialPurchaseRequisitionHistory> getItemList() {
		return itemList;
	}

	public void setItemList(List<MaterialPurchaseRequisitionHistory> itemList) {
		this.itemList = itemList;
	}

}
