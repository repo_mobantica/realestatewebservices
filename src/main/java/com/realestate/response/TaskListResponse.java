package com.realestate.response;

public class TaskListResponse {


	private String taskHistoryId;
	private String otherTaskStatus;

	public String getTaskHistoryId() {
		return taskHistoryId;
	}
	public void setTaskHistoryId(String taskHistoryId) {
		this.taskHistoryId = taskHistoryId;
	}
	public String getOtherTaskStatus() {
		return otherTaskStatus;
	}
	public void setOtherTaskStatus(String otherTaskStatus) {
		this.otherTaskStatus = otherTaskStatus;
	}

}
