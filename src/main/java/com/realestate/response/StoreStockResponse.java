package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.Stock;

public class StoreStockResponse {

	private String response;
	private int status;
	
	List<StockResponse> stockList=new ArrayList<StockResponse>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<StockResponse> getStockList() {
		return stockList;
	}

	public void setStockList(List<StockResponse> stockList) {
		this.stockList = stockList;
	}

}
