package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.TaskHistory;

public class UpdateTaskResponse {

    private String taskId;
    private String taskStatus;
    

	List<TaskListResponse> tasklist=new ArrayList<TaskListResponse>();


	public String getTaskId() {
		return taskId;
	}


	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}


	public String getTaskStatus() {
		return taskStatus;
	}


	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}


	public List<TaskListResponse> getTasklist() {
		return tasklist;
	}


	public void setTasklist(List<TaskListResponse> tasklist) {
		this.tasklist = tasklist;
	}

}
