package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.Project;
import com.realestate.bean.ProjectBuilding;

public class ProjectBuildingResponse {

	private String response;
	private int status;
	
	List<ProjectBuilding> projectbuildingList=new ArrayList<ProjectBuilding>();
	List<Project> projectList=new ArrayList<Project>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<ProjectBuilding> getProjectbuildingList() {
		return projectbuildingList;
	}

	public void setProjectbuildingList(List<ProjectBuilding> projectbuildingList) {
		this.projectbuildingList = projectbuildingList;
	}

	public List<Project> getProjectList() {
		return projectList;
	}

	public void setProjectList(List<Project> projectList) {
		this.projectList = projectList;
	}

}
