package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.SubEnquirySource;

public class SubEnquirySourceResponse {

	private String response;
	private int status;

	List<SubEnquirySource> subEnquirySourceList=new ArrayList<SubEnquirySource>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<SubEnquirySource> getSubEnquirySourceList() {
		return subEnquirySourceList;
	}

	public void setSubEnquirySourceList(List<SubEnquirySource> subEnquirySourceList) {
		this.subEnquirySourceList = subEnquirySourceList;
	}

}
