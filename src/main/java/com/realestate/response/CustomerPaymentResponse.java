package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.Booking;

public class CustomerPaymentResponse {


	private String response;
	private int status;
	
	private long loanAmount;
	private String loanAmountAgainst;
	
	private Double totalpayAggreement;
	private Double totalpayStampDuty;
	private Double totalpayRegistration;
	private Double totalpaygstAmount;
	
	private Double reamaingAggreement;
	private Double reamaingstampDuty;
	private Double reamaingRegistration;
	private Double reamaingGstAmount;
	private Double totalRemainingAmount;
	

	private String bookingId;
	private String bookingName;
	private Double infrastructureCharge;
	private Double aggreementValue1;
	private Double handlingCharges;
	private Double stampDutyPer;
	private Double registrationPer;
	private Double stampDuty1;
	private Double registrationCost1;
	private Double gstCost;
	private Double gstAmount1;
	private Double grandTotal1;
	private Double tds;
	

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public long getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(long loanAmount) {
		this.loanAmount = loanAmount;
	}

	public String getLoanAmountAgainst() {
		return loanAmountAgainst;
	}

	public void setLoanAmountAgainst(String loanAmountAgainst) {
		this.loanAmountAgainst = loanAmountAgainst;
	}

	public Double getTotalpayAggreement() {
		return totalpayAggreement;
	}

	public void setTotalpayAggreement(Double totalpayAggreement) {
		this.totalpayAggreement = totalpayAggreement;
	}

	public Double getTotalpayStampDuty() {
		return totalpayStampDuty;
	}

	public void setTotalpayStampDuty(Double totalpayStampDuty) {
		this.totalpayStampDuty = totalpayStampDuty;
	}

	public Double getTotalpayRegistration() {
		return totalpayRegistration;
	}

	public void setTotalpayRegistration(Double totalpayRegistration) {
		this.totalpayRegistration = totalpayRegistration;
	}

	public Double getTotalpaygstAmount() {
		return totalpaygstAmount;
	}

	public void setTotalpaygstAmount(Double totalpaygstAmount) {
		this.totalpaygstAmount = totalpaygstAmount;
	}

	public Double getReamaingAggreement() {
		return reamaingAggreement;
	}

	public void setReamaingAggreement(Double reamaingAggreement) {
		this.reamaingAggreement = reamaingAggreement;
	}

	public Double getReamaingstampDuty() {
		return reamaingstampDuty;
	}

	public void setReamaingstampDuty(Double reamaingstampDuty) {
		this.reamaingstampDuty = reamaingstampDuty;
	}

	public Double getReamaingRegistration() {
		return reamaingRegistration;
	}

	public void setReamaingRegistration(Double reamaingRegistration) {
		this.reamaingRegistration = reamaingRegistration;
	}

	public Double getReamaingGstAmount() {
		return reamaingGstAmount;
	}

	public void setReamaingGstAmount(Double reamaingGstAmount) {
		this.reamaingGstAmount = reamaingGstAmount;
	}

	public Double getTotalRemainingAmount() {
		return totalRemainingAmount;
	}

	public void setTotalRemainingAmount(Double totalRemainingAmount) {
		this.totalRemainingAmount = totalRemainingAmount;
	}

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public String getBookingName() {
		return bookingName;
	}

	public void setBookingName(String bookingName) {
		this.bookingName = bookingName;
	}

	public Double getInfrastructureCharge() {
		return infrastructureCharge;
	}

	public void setInfrastructureCharge(Double infrastructureCharge) {
		this.infrastructureCharge = infrastructureCharge;
	}

	public Double getAggreementValue1() {
		return aggreementValue1;
	}

	public void setAggreementValue1(Double aggreementValue1) {
		this.aggreementValue1 = aggreementValue1;
	}

	public Double getHandlingCharges() {
		return handlingCharges;
	}

	public void setHandlingCharges(Double handlingCharges) {
		this.handlingCharges = handlingCharges;
	}

	public Double getStampDutyPer() {
		return stampDutyPer;
	}

	public void setStampDutyPer(Double stampDutyPer) {
		this.stampDutyPer = stampDutyPer;
	}

	public Double getRegistrationPer() {
		return registrationPer;
	}

	public void setRegistrationPer(Double registrationPer) {
		this.registrationPer = registrationPer;
	}

	public Double getStampDuty1() {
		return stampDuty1;
	}

	public void setStampDuty1(Double stampDuty1) {
		this.stampDuty1 = stampDuty1;
	}

	public Double getRegistrationCost1() {
		return registrationCost1;
	}

	public void setRegistrationCost1(Double registrationCost1) {
		this.registrationCost1 = registrationCost1;
	}

	public Double getGstCost() {
		return gstCost;
	}

	public void setGstCost(Double gstCost) {
		this.gstCost = gstCost;
	}

	public Double getGstAmount1() {
		return gstAmount1;
	}

	public void setGstAmount1(Double gstAmount1) {
		this.gstAmount1 = gstAmount1;
	}

	public Double getGrandTotal1() {
		return grandTotal1;
	}

	public void setGrandTotal1(Double grandTotal1) {
		this.grandTotal1 = grandTotal1;
	}

	public Double getTds() {
		return tds;
	}

	public void setTds(Double tds) {
		this.tds = tds;
	}

}
