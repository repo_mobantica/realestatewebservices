package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.Booking;
import com.realestate.bean.ExtraCharges;

public class ExtraChargesListResponse {

	private String response;
	private int status;

	List<ExtraChargesResponse> customerList=new ArrayList<ExtraChargesResponse>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<ExtraChargesResponse> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(List<ExtraChargesResponse> customerList) {
		this.customerList = customerList;
	}

}
