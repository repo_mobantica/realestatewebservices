package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.ParkingZone;

public class ParkingZoneResponse {

	private String response;
	private int status;
	
	List<ParkingZone> parkingZoneList=new ArrayList<ParkingZone>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<ParkingZone> getParkingZoneList() {
		return parkingZoneList;
	}

	public void setParkingZoneList(List<ParkingZone> parkingZoneList) {
		this.parkingZoneList = parkingZoneList;
	}

}
