package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.Agent;

public class AgentResponse {

	private String response;
	private int status;
	
	List<Agent> agentList=new ArrayList<Agent>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<Agent> getAgentList() {
		return agentList;
	}

	public void setAgentList(List<Agent> agentList) {
		this.agentList = agentList;
	}

}
