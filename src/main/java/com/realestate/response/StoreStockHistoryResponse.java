package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.StoreStockHistory;

public class StoreStockHistoryResponse {


	private String storeId;
	private String materialsPurchasedId;
	private String challanNo;
	private String ewayBillNo;
	private String vehicleNumber;
	private Double loadedWeight;
	private Double unloadedWeight;
	private Double totalMaterialWeight;
	private String remark;

	List<StoreStockHistory> stockList=new ArrayList<StoreStockHistory>();

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getMaterialsPurchasedId() {
		return materialsPurchasedId;
	}

	public void setMaterialsPurchasedId(String materialsPurchasedId) {
		this.materialsPurchasedId = materialsPurchasedId;
	}

	public String getChallanNo() {
		return challanNo;
	}

	public void setChallanNo(String challanNo) {
		this.challanNo = challanNo;
	}

	public String getEwayBillNo() {
		return ewayBillNo;
	}

	public void setEwayBillNo(String ewayBillNo) {
		this.ewayBillNo = ewayBillNo;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public Double getLoadedWeight() {
		return loadedWeight;
	}

	public void setLoadedWeight(Double loadedWeight) {
		this.loadedWeight = loadedWeight;
	}

	public Double getUnloadedWeight() {
		return unloadedWeight;
	}

	public void setUnloadedWeight(Double unloadedWeight) {
		this.unloadedWeight = unloadedWeight;
	}

	public Double getTotalMaterialWeight() {
		return totalMaterialWeight;
	}

	public void setTotalMaterialWeight(Double totalMaterialWeight) {
		this.totalMaterialWeight = totalMaterialWeight;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public List<StoreStockHistory> getStockList() {
		return stockList;
	}

	public void setStockList(List<StoreStockHistory> stockList) {
		this.stockList = stockList;
	}
	
}
