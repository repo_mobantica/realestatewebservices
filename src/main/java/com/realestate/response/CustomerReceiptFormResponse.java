package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.CustomerReceiptForm;

public class CustomerReceiptFormResponse {

	private String response;
	private int status;
	
	List<CustomerReceiptForm> customerReceiptList=new ArrayList<CustomerReceiptForm>();

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public List<CustomerReceiptForm> getCustomerReceiptList() {
		return customerReceiptList;
	}

	public void setCustomerReceiptList(List<CustomerReceiptForm> customerReceiptList) {
		this.customerReceiptList = customerReceiptList;
	}
	
}
