package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

public class MaterialRequisitionResponse {

	private String requisitionId;
	private String employeeId;
	private String projectId;
	private String buildingId;
	private String wingId;
	private String requiredDate;
	private String userId;
	
	List<MaterialRequisitionHistoryResponse> itemList=new ArrayList<MaterialRequisitionHistoryResponse>();

	public String getRequisitionId() {
		return requisitionId;
	}

	public void setRequisitionId(String requisitionId) {
		this.requisitionId = requisitionId;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getWingId() {
		return wingId;
	}

	public void setWingId(String wingId) {
		this.wingId = wingId;
	}

	public String getRequiredDate() {
		return requiredDate;
	}

	public void setRequiredDate(String requiredDate) {
		this.requiredDate = requiredDate;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<MaterialRequisitionHistoryResponse> getItemList() {
		return itemList;
	}

	public void setItemList(List<MaterialRequisitionHistoryResponse> itemList) {
		this.itemList = itemList;
	}

	
}
