package com.realestate.response;

import java.util.ArrayList;
import java.util.List;

import com.realestate.bean.MaterialPurchaseRequisitionHistory;
public class RequisitionItemResponse {

	private String requisitionId;
	private String employeeId;
	private String projectId;
	private String buildingId;
	private String wingId;
	private String requiredDate;
	private int totalNoItem;
	private double totalQuantity;
	private String requisitionStatus;
	private String ApplyDate;

	private String response;
	private int status;

	List<MaterialPurchaseRequisitionHistory> requisitionItemList=new ArrayList<MaterialPurchaseRequisitionHistory>();


	public String getRequisitionId() {
		return requisitionId;
	}


	public void setRequisitionId(String requisitionId) {
		this.requisitionId = requisitionId;
	}


	public String getEmployeeId() {
		return employeeId;
	}


	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}


	public String getProjectId() {
		return projectId;
	}


	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}


	public String getBuildingId() {
		return buildingId;
	}


	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}


	public String getWingId() {
		return wingId;
	}


	public void setWingId(String wingId) {
		this.wingId = wingId;
	}


	public String getRequiredDate() {
		return requiredDate;
	}


	public void setRequiredDate(String requiredDate) {
		this.requiredDate = requiredDate;
	}


	public int getTotalNoItem() {
		return totalNoItem;
	}


	public void setTotalNoItem(int totalNoItem) {
		this.totalNoItem = totalNoItem;
	}


	public double getTotalQuantity() {
		return totalQuantity;
	}


	public void setTotalQuantity(double totalQuantity) {
		this.totalQuantity = totalQuantity;
	}


	public String getRequisitionStatus() {
		return requisitionStatus;
	}


	public void setRequisitionStatus(String requisitionStatus) {
		this.requisitionStatus = requisitionStatus;
	}


	public String getApplyDate() {
		return ApplyDate;
	}


	public void setApplyDate(String applyDate) {
		ApplyDate = applyDate;
	}


	public String getResponse() {
		return response;
	}


	public void setResponse(String response) {
		this.response = response;
	}


	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}


	public List<MaterialPurchaseRequisitionHistory> getRequisitionItemList() {
		return requisitionItemList;
	}


	public void setRequisitionItemList(List<MaterialPurchaseRequisitionHistory> requisitionItemList) {
		this.requisitionItemList = requisitionItemList;
	}

}
