package com.realestate.services;

import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import javax.imageio.ImageIO;

public class ImageService {

	public static String uploadImageToServer(byte[] image, String employeeId, String path) {

		try {

			String filename = employeeId + ".png";

			// byte[] imageByte=Base64.decodeBase64(imageValue);
			String directory = path + filename;
			//new FileOutputStream(new File(directory)).write(image);

			/*
			//byte[] bytes = profile_img.getBytes();
			BufferedOutputStream stream =new BufferedOutputStream(new FileOutputStream(new File(directory)));
			stream.write(image);
			stream.flush();
			stream.close();
			 */

			//  BufferedImage originalImage = ImageIO.read(new File(directory));
			BufferedImage src = ImageIO.read(new ByteArrayInputStream(image));
			File destinstion = new File(directory);
			ImageIO.write(src, "png", destinstion);


			File f = new File(directory);

			if (f.exists() && !f.isDirectory()) 
			{
				return filename;
			} 
			else 
			{
				return "";
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return "error = " + e;
		} catch (IOException e) {
			e.printStackTrace();
			return "error = " + e;
		} catch (Exception e) {
			e.printStackTrace();
			return "error = " + e;
		}

	}
}
