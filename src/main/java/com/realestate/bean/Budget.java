package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "budgets")
@CompoundIndexes({@CompoundIndex(name="budgetIndex", unique= true, def="{'minimumBudget':1}")})
public class Budget {

	@Id
    private String budgetId;
    private long minimumBudget;
    private long maximumBudget;
    private String budgetCost;
    private String creationDate;
    private String updateDate;
    private String userName;
    
    
    public Budget() 
    {
    	
    }


	public Budget(String budgetId, long minimumBudget, long maximumBudget, String budgetCost, String creationDate,
			String updateDate, String userName) 
	{
		super();
		this.budgetId = budgetId;
		this.minimumBudget = minimumBudget;
		this.maximumBudget = maximumBudget;
		this.budgetCost = budgetCost;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}


	public String getBudgetId() {
		return budgetId;
	}


	public void setBudgetId(String budgetId) {
		this.budgetId = budgetId;
	}


	public long getMinimumBudget() {
		return minimumBudget;
	}


	public void setMinimumBudget(long minimumBudget) {
		this.minimumBudget = minimumBudget;
	}


	public long getMaximumBudget() {
		return maximumBudget;
	}


	public void setMaximumBudget(long maximumBudget) {
		this.maximumBudget = maximumBudget;
	}


	public String getBudgetCost() {
		return budgetCost;
	}


	public void setBudgetCost(String budgetCost) {
		this.budgetCost = budgetCost;
	}


	public String getCreationDate() {
		return creationDate;
	}


	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}


	public String getUpdateDate() {
		return updateDate;
	}


	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}

}
