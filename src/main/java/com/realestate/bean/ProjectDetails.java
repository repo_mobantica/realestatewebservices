package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "projectdetails")
public class ProjectDetails {

	@Id
    private String projectDetailsId;
	private String projectId;
    private String landownerName;
	private String projectType;
    

    private String projectLatitude;
	private String projectLongitude;
	
    private String locationInEast;
    private String locationInWest;
	private String locationInSouth;
	private String locationInNorth;
	
    private String googleLocationInEast;
    private String googleLocationInWest;
	private String googleLocationInSouth;
	private String googleLocationInNorth;
   
	private Double landArea;
	private Double landPrice;
	
    private String creationDate;
	private String updateDate;
	private String userName;
	
	public ProjectDetails() {}

	public ProjectDetails(String projectDetailsId, String projectId, String landownerName, String projectType,
			String projectLatitude, String projectLongitude, String locationInEast, String locationInWest,
			String locationInSouth, String locationInNorth, String googleLocationInEast, String googleLocationInWest,
			String googleLocationInSouth, String googleLocationInNorth, Double landArea, Double landPrice,
			String creationDate, String updateDate, String userName) {
		super();
		this.projectDetailsId = projectDetailsId;
		this.projectId = projectId;
		this.landownerName = landownerName;
		this.projectType = projectType;
		this.projectLatitude = projectLatitude;
		this.projectLongitude = projectLongitude;
		this.locationInEast = locationInEast;
		this.locationInWest = locationInWest;
		this.locationInSouth = locationInSouth;
		this.locationInNorth = locationInNorth;
		this.googleLocationInEast = googleLocationInEast;
		this.googleLocationInWest = googleLocationInWest;
		this.googleLocationInSouth = googleLocationInSouth;
		this.googleLocationInNorth = googleLocationInNorth;
		this.landArea = landArea;
		this.landPrice = landPrice;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getProjectDetailsId() {
		return projectDetailsId;
	}

	public void setProjectDetailsId(String projectDetailsId) {
		this.projectDetailsId = projectDetailsId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getLandownerName() {
		return landownerName;
	}

	public void setLandownerName(String landownerName) {
		this.landownerName = landownerName;
	}

	public String getProjectType() {
		return projectType;
	}

	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	public String getProjectLatitude() {
		return projectLatitude;
	}

	public void setProjectLatitude(String projectLatitude) {
		this.projectLatitude = projectLatitude;
	}

	public String getProjectLongitude() {
		return projectLongitude;
	}

	public void setProjectLongitude(String projectLongitude) {
		this.projectLongitude = projectLongitude;
	}

	public String getLocationInEast() {
		return locationInEast;
	}

	public void setLocationInEast(String locationInEast) {
		this.locationInEast = locationInEast;
	}

	public String getLocationInWest() {
		return locationInWest;
	}

	public void setLocationInWest(String locationInWest) {
		this.locationInWest = locationInWest;
	}

	public String getLocationInSouth() {
		return locationInSouth;
	}

	public void setLocationInSouth(String locationInSouth) {
		this.locationInSouth = locationInSouth;
	}

	public String getLocationInNorth() {
		return locationInNorth;
	}

	public void setLocationInNorth(String locationInNorth) {
		this.locationInNorth = locationInNorth;
	}

	public String getGoogleLocationInEast() {
		return googleLocationInEast;
	}

	public void setGoogleLocationInEast(String googleLocationInEast) {
		this.googleLocationInEast = googleLocationInEast;
	}

	public String getGoogleLocationInWest() {
		return googleLocationInWest;
	}

	public void setGoogleLocationInWest(String googleLocationInWest) {
		this.googleLocationInWest = googleLocationInWest;
	}

	public String getGoogleLocationInSouth() {
		return googleLocationInSouth;
	}

	public void setGoogleLocationInSouth(String googleLocationInSouth) {
		this.googleLocationInSouth = googleLocationInSouth;
	}

	public String getGoogleLocationInNorth() {
		return googleLocationInNorth;
	}

	public void setGoogleLocationInNorth(String googleLocationInNorth) {
		this.googleLocationInNorth = googleLocationInNorth;
	}

	public Double getLandArea() {
		return landArea;
	}

	public void setLandArea(Double landArea) {
		this.landArea = landArea;
	}

	public Double getLandPrice() {
		return landPrice;
	}

	public void setLandPrice(Double landPrice) {
		this.landPrice = landPrice;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
}
