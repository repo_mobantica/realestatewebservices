package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "taxes")
//@CompoundIndexes({@CompoundIndex(name="taxIndex", unique= true, def="{'taxName':1}")})
public class Tax {

	@Id
    private String taxId;
	private String taxType;
    private String taxName;
    private Double cgstPercentage;
    private Double sgstPercentage;
    private Double taxPercentage;
    private String currencyName;
    private String wefDate;
    
    private String creationDate;
    private String updateDate;
    private String userName;
 
    public Tax() 
    {
    }

	public Tax(String taxId, String taxType, String taxName, Double cgstPercentage, Double sgstPercentage,
			Double taxPercentage, String currencyName, String wefDate, String creationDate, String updateDate,
			String userName) {
		super();
		this.taxId = taxId;
		this.taxType = taxType;
		this.taxName = taxName;
		this.cgstPercentage = cgstPercentage;
		this.sgstPercentage = sgstPercentage;
		this.taxPercentage = taxPercentage;
		this.currencyName = currencyName;
		this.wefDate = wefDate;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public String getTaxType() {
		return taxType;
	}

	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}

	public String getTaxName() {
		return taxName;
	}

	public void setTaxName(String taxName) {
		this.taxName = taxName;
	}

	public Double getCgstPercentage() {
		return cgstPercentage;
	}

	public void setCgstPercentage(Double cgstPercentage) {
		this.cgstPercentage = cgstPercentage;
	}

	public Double getSgstPercentage() {
		return sgstPercentage;
	}

	public void setSgstPercentage(Double sgstPercentage) {
		this.sgstPercentage = sgstPercentage;
	}

	public Double getTaxPercentage() {
		return taxPercentage;
	}

	public void setTaxPercentage(Double taxPercentage) {
		this.taxPercentage = taxPercentage;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public String getWefDate() {
		return wefDate;
	}

	public void setWefDate(String wefDate) {
		this.wefDate = wefDate;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

    
}
