package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "currencies")
@CompoundIndexes({@CompoundIndex(name="currencyIndex", unique= true, def="{'countryName':1}")})
public class Currency 
{

	@Id
    private String currencyId;
    private String countryId;
    private String currencyName;
    private String creationDate;
    private String updateDate;
    private String userName;
    
    public Currency()
    {
    	
    }

	public Currency(String currencyId, String countryId, String currencyName, String creationDate, String updateDate,
			String userName) {
		super();
		this.currencyId = currencyId;
		this.countryId = countryId;
		this.currencyName = currencyName;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getCurrencyName() {
		return currencyName;
	}

	public void setCurrencyName(String currencyName) {
		this.currencyName = currencyName;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
   
	
}
