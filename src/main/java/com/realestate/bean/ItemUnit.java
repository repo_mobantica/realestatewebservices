package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "itemunits")
@CompoundIndexes({@CompoundIndex(name="itemunitIndex", unique= true, def="{'itemunitName':1}")})

public class ItemUnit {

		@Id
	    private String itemunitId;
	    private String itemunitName;
	    private String itemunitSymbol;
	    private String creationDate;
	    private String updateDate;
	    private String userName;
	    
	    public ItemUnit() 
	    {
	    	
	    }

		public ItemUnit(String itemunitId, String itemunitName, String itemunitSymbol, String creationDate,
				String updateDate, String userName) {
			super();
			this.itemunitId = itemunitId;
			this.itemunitName = itemunitName;
			this.itemunitSymbol = itemunitSymbol;
			this.creationDate = creationDate;
			this.updateDate = updateDate;
			this.userName = userName;
		}

		public String getItemunitId() {
			return itemunitId;
		}

		public void setItemunitId(String itemunitId) {
			this.itemunitId = itemunitId;
		}

		public String getItemunitName() {
			return itemunitName;
		}

		public void setItemunitName(String itemunitName) {
			this.itemunitName = itemunitName;
		}

		public String getItemunitSymbol() {
			return itemunitSymbol;
		}

		public void setItemunitSymbol(String itemunitSymbol) {
			this.itemunitSymbol = itemunitSymbol;
		}

		public String getCreationDate() {
			return creationDate;
		}

		public void setCreationDate(String creationDate) {
			this.creationDate = creationDate;
		}

		public String getUpdateDate() {
			return updateDate;
		}

		public void setUpdateDate(String updateDate) {
			this.updateDate = updateDate;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

}
