package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "receipts")
public class Receipt
{
	@Id
	private long receiptId;
	private String receiptType;
	private String bookingId;
	private String receiptStatus;
	
	public Receipt() 
	{
		
	}
	
	public Receipt(long receiptId, String receiptType, String bookingId, String receiptStatus)
	{
		super();
		this.receiptId = receiptId;
		this.receiptType = receiptType;
		this.bookingId = bookingId;
		this.receiptStatus = receiptStatus;
	}

	public long getReceiptId() {
		return receiptId;
	}

	public void setReceiptId(long receiptId) {
		this.receiptId = receiptId;
	}

	public String getReceiptType() {
		return receiptType;
	}

	public void setReceiptType(String receiptType) {
		this.receiptType = receiptType;
	}

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public String getReceiptStatus() {
		return receiptStatus;
	}

	public void setReceiptStatus(String receiptStatus) {
		this.receiptStatus = receiptStatus;
	}

}
