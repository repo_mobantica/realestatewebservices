package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "subcontractortypes")
public class SubContractorType {

	@Id
    private String subcontractortypeId;
	private String subcontractorType;
    private String contractortypeId;
    private String creationDate;
    private String updateDate;
    private String userName;
    
    public SubContractorType() {}

	public SubContractorType(String subcontractortypeId, String subcontractorType, String contractortypeId, String creationDate,
			String updateDate, String userName) {
		super();
		this.subcontractortypeId = subcontractortypeId;
		this.subcontractorType = subcontractorType;
		this.contractortypeId = contractortypeId;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getSubcontractortypeId() {
		return subcontractortypeId;
	}

	public void setSubcontractortypeId(String subcontractortypeId) {
		this.subcontractortypeId = subcontractortypeId;
	}

	public String getSubcontractorType() {
		return subcontractorType;
	}

	public void setSubcontractorType(String subcontractorType) {
		this.subcontractorType = subcontractorType;
	}

	public String getContractortypeId() {
		return contractortypeId;
	}

	public void setContractortypeId(String contractortypeId) {
		this.contractortypeId = contractortypeId;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
    
    
}
