package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "supplierquotations")
public class SupplierQuotation {

	@Id
    private String quotationId;
	private String supplierId;
	private String requisitionId;
	private Double total;
	private Double discountAmount;
	private Double gstAmount;
    private Double grandTotal;
    
    private String creationDate;
	private String updateDate;
	private String userName;
	
	public SupplierQuotation() {}

	public SupplierQuotation(String quotationId, String supplierId, String requisitionId, Double total,
			Double discountAmount, Double gstAmount, Double grandTotal, String creationDate, String updateDate,
			String userName) {
		super();
		this.quotationId = quotationId;
		this.supplierId = supplierId;
		this.requisitionId = requisitionId;
		this.total = total;
		this.discountAmount = discountAmount;
		this.gstAmount = gstAmount;
		this.grandTotal = grandTotal;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getQuotationId() {
		return quotationId;
	}

	public void setQuotationId(String quotationId) {
		this.quotationId = quotationId;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getRequisitionId() {
		return requisitionId;
	}

	public void setRequisitionId(String requisitionId) {
		this.requisitionId = requisitionId;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public Double getGstAmount() {
		return gstAmount;
	}

	public void setGstAmount(Double gstAmount) {
		this.gstAmount = gstAmount;
	}

	public Double getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
}
