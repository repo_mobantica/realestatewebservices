package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "architecturalemployees")
public class ArchitecturalEmployees {

	@Id
	private int architecturalEmployeeId;
	private String architecturalId;
	private String employeeName;
	private String employeeDesignation;
	private String employeeEmail;
	private String employeeMobileno;
	
	public ArchitecturalEmployees() {}

	public ArchitecturalEmployees(int architecturalEmployeeId, String architecturalId, String employeeName,
			String employeeDesignation, String employeeEmail, String employeeMobileno) {
		super();
		this.architecturalEmployeeId = architecturalEmployeeId;
		this.architecturalId = architecturalId;
		this.employeeName = employeeName;
		this.employeeDesignation = employeeDesignation;
		this.employeeEmail = employeeEmail;
		this.employeeMobileno = employeeMobileno;
	}

	public int getArchitecturalEmployeeId() {
		return architecturalEmployeeId;
	}

	public void setArchitecturalEmployeeId(int architecturalEmployeeId) {
		this.architecturalEmployeeId = architecturalEmployeeId;
	}

	public String getArchitecturalId() {
		return architecturalId;
	}

	public void setArchitecturalId(String architecturalId) {
		this.architecturalId = architecturalId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmployeeDesignation() {
		return employeeDesignation;
	}

	public void setEmployeeDesignation(String employeeDesignation) {
		this.employeeDesignation = employeeDesignation;
	}

	public String getEmployeeEmail() {
		return employeeEmail;
	}

	public void setEmployeeEmail(String employeeEmail) {
		this.employeeEmail = employeeEmail;
	}

	public String getEmployeeMobileno() {
		return employeeMobileno;
	}

	public void setEmployeeMobileno(String employeeMobileno) {
		this.employeeMobileno = employeeMobileno;
	}
	
}
