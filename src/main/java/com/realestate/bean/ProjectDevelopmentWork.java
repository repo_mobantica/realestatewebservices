package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "projectdevelopmentworks")
public class ProjectDevelopmentWork {

	@Id
	private long projectWorkId;
	private String projectId;

	private String commonAreasAndFacilitiesAmenities;
	private String availableStatus;
	private String completionPercent;
	private String details;
	
	
	public ProjectDevelopmentWork() {}

	public ProjectDevelopmentWork(long projectWorkId, String projectId, String commonAreasAndFacilitiesAmenities,
			String availableStatus, String completionPercent, String details) {
		super();
		this.projectWorkId = projectWorkId;
		this.projectId = projectId;
		this.commonAreasAndFacilitiesAmenities = commonAreasAndFacilitiesAmenities;
		this.availableStatus = availableStatus;
		this.completionPercent = completionPercent;
		this.details = details;
	}

	public long getProjectWorkId() {
		return projectWorkId;
	}

	public void setProjectWorkId(long projectWorkId) {
		this.projectWorkId = projectWorkId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getCommonAreasAndFacilitiesAmenities() {
		return commonAreasAndFacilitiesAmenities;
	}

	public void setCommonAreasAndFacilitiesAmenities(String commonAreasAndFacilitiesAmenities) {
		this.commonAreasAndFacilitiesAmenities = commonAreasAndFacilitiesAmenities;
	}

	public String getAvailableStatus() {
		return availableStatus;
	}

	public void setAvailableStatus(String availableStatus) {
		this.availableStatus = availableStatus;
	}

	public String getCompletionPercent() {
		return completionPercent;
	}

	public void setCompletionPercent(String completionPercent) {
		this.completionPercent = completionPercent;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

}
