package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="itemsubcategories")
public class ItemSubCategory 
{
	@Id
	private String subItemCategoryId;
	private String itemMainCategoryName;
	private String itemCategoryName;
	private String subItemCategoryName;
	
	private String creationDate;
	private String updateDate;
	private String userName;
	
	public ItemSubCategory()
	{

	}

	public ItemSubCategory(String subItemCategoryId, String itemMainCategoryName, String itemCategoryName,
			String subItemCategoryName, String creationDate, String updateDate, String userName)
	{
		super();
		this.subItemCategoryId = subItemCategoryId;
		this.itemMainCategoryName = itemMainCategoryName;
		this.itemCategoryName = itemCategoryName;
		this.subItemCategoryName = subItemCategoryName;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getSubItemCategoryId() {
		return subItemCategoryId;
	}

	public void setSubItemCategoryId(String subItemCategoryId) {
		this.subItemCategoryId = subItemCategoryId;
	}

	public String getItemMainCategoryName() {
		return itemMainCategoryName;
	}

	public void setItemMainCategoryName(String itemMainCategoryName) {
		this.itemMainCategoryName = itemMainCategoryName;
	}

	public String getItemCategoryName() {
		return itemCategoryName;
	}

	public void setItemCategoryName(String itemCategoryName) {
		this.itemCategoryName = itemCategoryName;
	}

	public String getSubItemCategoryName() {
		return subItemCategoryName;
	}

	public void setSubItemCategoryName(String subItemCategoryName) {
		this.subItemCategoryName = subItemCategoryName;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
