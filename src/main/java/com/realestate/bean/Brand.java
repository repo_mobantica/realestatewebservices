package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "brands")
@CompoundIndexes({@CompoundIndex(name="brandNameIndex", unique= true, def="{'brandName':1}")})
public class Brand {

	  @Id
	    private String brandId;
	    private String suppliertypeId;
	    private String subsuppliertypeId;
	    private String brandName;
	    private String creationDate;
	    private String updateDate;
	    private String userName;
	    
	    public Brand() {}

		public Brand(String brandId, String suppliertypeId, String subsuppliertypeId, String brandName,
				String creationDate, String updateDate, String userName) {
			super();
			this.brandId = brandId;
			this.suppliertypeId = suppliertypeId;
			this.subsuppliertypeId = subsuppliertypeId;
			this.brandName = brandName;
			this.creationDate = creationDate;
			this.updateDate = updateDate;
			this.userName = userName;
		}

		public String getBrandId() {
			return brandId;
		}

		public void setBrandId(String brandId) {
			this.brandId = brandId;
		}

		public String getSuppliertypeId() {
			return suppliertypeId;
		}

		public void setSuppliertypeId(String suppliertypeId) {
			this.suppliertypeId = suppliertypeId;
		}

		public String getSubsuppliertypeId() {
			return subsuppliertypeId;
		}

		public void setSubsuppliertypeId(String subsuppliertypeId) {
			this.subsuppliertypeId = subsuppliertypeId;
		}

		public String getBrandName() {
			return brandName;
		}

		public void setBrandName(String brandName) {
			this.brandName = brandName;
		}

		public String getCreationDate() {
			return creationDate;
		}

		public void setCreationDate(String creationDate) {
			this.creationDate = creationDate;
		}

		public String getUpdateDate() {
			return updateDate;
		}

		public void setUpdateDate(String updateDate) {
			this.updateDate = updateDate;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

	    
}
