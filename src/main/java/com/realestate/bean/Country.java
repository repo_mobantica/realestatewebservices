package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "countries")
@CompoundIndexes({@CompoundIndex(name="countrynameIndex", unique= true, def="{'countryName':1}")})
public class Country 
{

    @Id
    private String countryId;
    private String countryName;
    private String creationDate;
    private String updateDate;
    private String userName;
    
    public Country() 
    {
    	
    }

	public Country(String countryId, String countryName, String creationDate, String updateDate, String userName)
	{
		super();
		this.countryId = countryId;
		this.countryName = countryName;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getCountryId()
	{
		return countryId;
	}

	public void setCountryId(String countryId)
	{
		this.countryId = countryId;
	}

	public String getCountryName() 
	{
		return countryName;
	}

	public void setCountryName(String countryName)
	{
		this.countryName = countryName;
	}

	public String getCreationDate()
	{
		return creationDate;
	}

	public void setCreationDate(String creationDate)
	{
		this.creationDate = creationDate;
	}

	public String getUpdateDate()
	{
		return updateDate;
	}

	public void setUpdateDate(String updateDate) 
	{
		this.updateDate = updateDate;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

}
