package com.realestate.bean;


public class FlatMarketPrice
{
	  private String flatType;
	  private Double flatArea;
	  private Double flatCost;   

	  private Double flatAreawithLoadingInFt;
	  
	  private long flatCostwithotfloorise;
	 
	  private Double floorRise;
	  
	  private long infrastructureCharges;
	  
	  private long aggreementAmount; 
	  private long maintenanceCharges;
	  
	  private long registeration;
	  private long stampDuty;
	  private long handlingCharge;
	  private long cgst;
	  private long sgst;
	  private long totalCost;
	  private long netAmount;
	  
	public String getFlatType() 
	{
		return flatType;
	}
	
	public void setFlatType(String flatType) {
		this.flatType = flatType;
	}
	
	public Double getFlatArea() {
		return flatArea;
	}
	
	public void setFlatArea(Double flatArea) {
		this.flatArea = flatArea;
	}
	
	public Double getFlatCost() {
		return flatCost;
	}
	
	public void setFlatCost(Double flatCost) {
		this.flatCost = flatCost;
	}
	
	public Double getFlatAreawithLoadingInFt() {
		return flatAreawithLoadingInFt;
	}
	
	public void setFlatAreawithLoadingInFt(Double flatAreawithLoadingInFt) {
		this.flatAreawithLoadingInFt = flatAreawithLoadingInFt;
	}
	
	public long getFlatCostwithotfloorise() {
		return flatCostwithotfloorise;
	}
	
	public void setFlatCostwithotfloorise(Double flatCostwithotfloorise) {
		this.flatCostwithotfloorise = flatCostwithotfloorise.longValue();
	}
	
	public Double getFloorRise() {
		return floorRise;
	}
	
	public void setFloorRise(Double floorRise) {
		this.floorRise = floorRise;
	}
	
	public long getInfrastructureCharges() {
		return infrastructureCharges;
	}
	
	public void setInfrastructureCharges(long infrastructureCharges) {
		this.infrastructureCharges = infrastructureCharges;
	}
	
	public long getAggreementAmount() {
		return aggreementAmount;
	}
	
	public void setAggreementAmount(Double aggreementAmount) {
		this.aggreementAmount = aggreementAmount.longValue();
	}
	
	public long getMaintenanceCharges() {
		return maintenanceCharges;
	}
	
	public void setMaintenanceCharges(long maintenanceCharges) {
		this.maintenanceCharges = maintenanceCharges;
	}
	
	public long getRegisteration() {
		return registeration;
	}
	
	public void setRegisteration(Double registeration) {
		this.registeration = registeration.longValue();
	}
	
	public long getStampDuty() {
		return stampDuty;
	}
	
	public void setStampDuty(double stampDuty) {
		this.stampDuty = (long)stampDuty;
	}
	
	public long getHandlingCharge() {
		return handlingCharge;
	}
	
	public void setHandlingCharge(long handlingCharge) {
		this.handlingCharge = handlingCharge;
	}
	
	public long getCgst() {
		return cgst;
	}
	
	public void setCgst(Double cgst) {
		this.cgst = cgst.longValue();
	}
	
	public long getSgst() {
		return sgst;
	}
	
	public void setSgst(Double sgst) {
		this.sgst = sgst.longValue();
	}
	
	public long getTotalCost() {
		return totalCost;
	}
	
	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost.longValue();
	}
	
	public long getNetAmount() {
		return netAmount;
	}
	
	public void setNetAmount(long netAmount) {
		this.netAmount = netAmount;
	}
}
