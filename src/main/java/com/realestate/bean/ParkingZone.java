package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "parkingzones")
public class ParkingZone 
{
	@Id
	private String parkingZoneId;
	private String projectId;
	private String buildingId;
	private String wingId;
	private String floorId;
	private String parkingType;
	private String parkingNumber;
	private double parkingLength;
	private double parkingWidth;
	private double areaInSqMtr;
	private double areaInSqFt;
	private String parkingZoneStatus;
	
	private String creationDate;
	private String updateDate;
	private String userName;
	
	
	public ParkingZone() 
	{
		
	}


	public ParkingZone(String parkingZoneId, String projectId, String buildingId, String wingId,
			String floorId, String parkingType, String parkingNumber, double parkingLength, double parkingWidth,
			double areaInSqMtr, double areaInSqFt, String parkingZoneStatus, String creationDate, String updateDate,
			String userName)
	{
		super();
		this.parkingZoneId = parkingZoneId;
		this.projectId = projectId;
		this.buildingId = buildingId;
		this.wingId = wingId;
		this.floorId = floorId;
		this.parkingType = parkingType;
		this.parkingNumber = parkingNumber;
		this.parkingLength = parkingLength;
		this.parkingWidth = parkingWidth;
		this.areaInSqMtr = areaInSqMtr;
		this.areaInSqFt = areaInSqFt;
		this.parkingZoneStatus = parkingZoneStatus;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}


	public String getParkingZoneId() {
		return parkingZoneId;
	}


	public void setParkingZoneId(String parkingZoneId) {
		this.parkingZoneId = parkingZoneId;
	}


	public String getProjectId() {
		return projectId;
	}


	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}


	public String getBuildingId() {
		return buildingId;
	}


	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}


	public String getWingId() {
		return wingId;
	}


	public void setWingId(String wingId) {
		this.wingId = wingId;
	}


	public String getFloorId() {
		return floorId;
	}


	public void setFloorId(String floorId) {
		this.floorId = floorId;
	}


	public String getParkingType() {
		return parkingType;
	}


	public void setParkingType(String parkingType) {
		this.parkingType = parkingType;
	}


	public String getParkingNumber() {
		return parkingNumber;
	}


	public void setParkingNumber(String parkingNumber) {
		this.parkingNumber = parkingNumber;
	}


	public double getParkingLength() {
		return parkingLength;
	}


	public void setParkingLength(double parkingLength) {
		this.parkingLength = parkingLength;
	}


	public double getParkingWidth() {
		return parkingWidth;
	}


	public void setParkingWidth(double parkingWidth) {
		this.parkingWidth = parkingWidth;
	}


	public double getAreaInSqMtr() {
		return areaInSqMtr;
	}


	public void setAreaInSqMtr(double areaInSqMtr) {
		this.areaInSqMtr = areaInSqMtr;
	}


	public double getAreaInSqFt() {
		return areaInSqFt;
	}


	public void setAreaInSqFt(double areaInSqFt) {
		this.areaInSqFt = areaInSqFt;
	}


	public String getParkingZoneStatus() {
		return parkingZoneStatus;
	}


	public void setParkingZoneStatus(String parkingZoneStatus) {
		this.parkingZoneStatus = parkingZoneStatus;
	}


	public String getCreationDate() {
		return creationDate;
	}


	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}


	public String getUpdateDate() {
		return updateDate;
	}


	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}

}
