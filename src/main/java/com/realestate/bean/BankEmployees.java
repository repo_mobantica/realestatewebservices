package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "bankemployees")
public class BankEmployees
{
	@Id
	private int bankEmployeeId;
	private String bankId;
	private String employeeName;
	private String employeeDesignation;
	private String employeeEmail;
	private String employeeMobileno;
	
	public BankEmployees()
	{
		
	}

	public BankEmployees(int bankEmployeeId, String bankId, String employeeName, String employeeDesignation, String employeeEmail, String employeeMobileno) 
	{
		super();
		this.bankEmployeeId = bankEmployeeId;
		this.bankId = bankId;
		this.employeeName = employeeName;
		this.employeeDesignation = employeeDesignation;
		this.employeeEmail = employeeEmail;
		this.employeeMobileno = employeeMobileno;
	}

	public int getBankEmployeeId() {
		return bankEmployeeId;
	}

	public void setBankEmployeeId(int bankEmployeeId) {
		this.bankEmployeeId = bankEmployeeId;
	}

	public String getBankId() {
		return bankId;
	}

	public void setBankId(String bankId) {
		this.bankId = bankId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmployeeDesignation() {
		return employeeDesignation;
	}

	public void setEmployeeDesignation(String employeeDesignation) {
		this.employeeDesignation = employeeDesignation;
	}

	public String getEmployeeEmail() {
		return employeeEmail;
	}

	public void setEmployeeEmail(String employeeEmail) {
		this.employeeEmail = employeeEmail;
	}

	public String getEmployeeMobileno() {
		return employeeMobileno;
	}

	public void setEmployeeMobileno(String employeeMobileno) {
		this.employeeMobileno = employeeMobileno;
	}

}
