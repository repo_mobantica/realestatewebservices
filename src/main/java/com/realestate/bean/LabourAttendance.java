package com.realestate.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "labourattendances")
public class LabourAttendance {

	@Id
    private long labourAttendanceId;
	private String labourId;
	private String todayWork;
	private String projectId;
	private Date attendanceDate;
    private String presentStatus;
    private Double todaySalary;
    private String creationDate;
    private String updateDate;
	private String userName;
	
	@Transient
    private String labourName;
	
	public LabourAttendance() {}


	public LabourAttendance(long labourAttendanceId, String labourId, String todayWork, String projectId,
			Date attendanceDate, String presentStatus, Double todaySalary, String creationDate, String updateDate,
			String userName) {
		super();
		this.labourAttendanceId = labourAttendanceId;
		this.labourId = labourId;
		this.todayWork = todayWork;
		this.projectId = projectId;
		this.attendanceDate = attendanceDate;
		this.presentStatus = presentStatus;
		this.todaySalary = todaySalary;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}


	public long getLabourAttendanceId() {
		return labourAttendanceId;
	}


	public void setLabourAttendanceId(long labourAttendanceId) {
		this.labourAttendanceId = labourAttendanceId;
	}


	public String getLabourId() {
		return labourId;
	}


	public void setLabourId(String labourId) {
		this.labourId = labourId;
	}


	public String getTodayWork() {
		return todayWork;
	}


	public void setTodayWork(String todayWork) {
		this.todayWork = todayWork;
	}


	public String getProjectId() {
		return projectId;
	}


	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}


	public Date getAttendanceDate() {
		return attendanceDate;
	}


	public void setAttendanceDate(Date attendanceDate) {
		this.attendanceDate = attendanceDate;
	}


	public String getPresentStatus() {
		return presentStatus;
	}


	public void setPresentStatus(String presentStatus) {
		this.presentStatus = presentStatus;
	}


	public Double getTodaySalary() {
		return todaySalary;
	}


	public void setTodaySalary(Double todaySalary) {
		this.todaySalary = todaySalary;
	}


	public String getCreationDate() {
		return creationDate;
	}


	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}


	public String getUpdateDate() {
		return updateDate;
	}


	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getLabourName() {
		return labourName;
	}


	public void setLabourName(String labourName) {
		this.labourName = labourName;
	}
	
}
