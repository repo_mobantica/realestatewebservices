package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "banknames")
@CompoundIndexes({@CompoundIndex(name="bankname", unique= true, def="{'bankName':1}")})
public class BankName {

	@Id
	private String bankNameId;
	private String bankName;
	private String bankType;
	
	private String creationDate;
	private String updateDate;
	private String userName;
	
	public BankName() {}

	public BankName(String bankNameId, String bankName, String bankType, String creationDate, String updateDate,
			String userName) {
		super();
		this.bankNameId = bankNameId;
		this.bankName = bankName;
		this.bankType = bankType;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getBankNameId() {
		return bankNameId;
	}

	public void setBankNameId(String bankNameId) {
		this.bankNameId = bankNameId;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankType() {
		return bankType;
	}

	public void setBankType(String bankType) {
		this.bankType = bankType;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
}
