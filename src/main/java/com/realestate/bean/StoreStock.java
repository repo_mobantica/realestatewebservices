package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="storestocks")
public class StoreStock {

	@Id
	private String storeStockId;
	private String storeId;
	private String materialsPurchasedId;
	private String challanNo;
	private String ewayBillNo;
	private String vehicleNumber;
	private Double loadedWeight;
	private Double unloadedWeight;
	private Double totalMaterialWeight;
	
	private String remark;
	
	private String creationDate;

	public StoreStock()
	{}

	public String getStoreStockId() {
		return storeStockId;
	}

	public void setStoreStockId(String storeStockId) {
		this.storeStockId = storeStockId;
	}

	public String getStoreId() {
		return storeId;
	}

	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}

	public String getMaterialsPurchasedId() {
		return materialsPurchasedId;
	}

	public void setMaterialsPurchasedId(String materialsPurchasedId) {
		this.materialsPurchasedId = materialsPurchasedId;
	}

	public String getChallanNo() {
		return challanNo;
	}

	public void setChallanNo(String challanNo) {
		this.challanNo = challanNo;
	}

	public String getEwayBillNo() {
		return ewayBillNo;
	}

	public void setEwayBillNo(String ewayBillNo) {
		this.ewayBillNo = ewayBillNo;
	}

	public String getVehicleNumber() {
		return vehicleNumber;
	}

	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}

	public Double getLoadedWeight() {
		return loadedWeight;
	}

	public void setLoadedWeight(Double loadedWeight) {
		this.loadedWeight = loadedWeight;
	}

	public Double getUnloadedWeight() {
		return unloadedWeight;
	}

	public void setUnloadedWeight(Double unloadedWeight) {
		this.unloadedWeight = unloadedWeight;
	}

	public Double getTotalMaterialWeight() {
		return totalMaterialWeight;
	}

	public void setTotalMaterialWeight(Double totalMaterialWeight) {
		this.totalMaterialWeight = totalMaterialWeight;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	
}
