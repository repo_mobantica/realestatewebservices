package com.realestate.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "companydocuments")
public class CompanyDocuments
{
   @Id
   private String documentId;
   private String companyId;
   private String documentNumber;
   private String extension;
   private Date creationDate;
   private Date updateDate;

   public CompanyDocuments() 
   {
   
   }

	public CompanyDocuments(String documentId, String companyId, String documentNumber, String extension, Date creationDate, Date updateDate) 
	{
		super();
		this.documentId = documentId;
		this.companyId = companyId;
		this.documentNumber = documentNumber;
		this.extension = extension;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
}
