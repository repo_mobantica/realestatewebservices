package com.realestate.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "employeeleavedetails")
public class EmployeeLeaveDetails {

	@Id
    private String employeeLeaveId;
	private String employeeId;
	private Date leaveFromDate;
    private Date leaveToDate;
    private long noOfDays;
    private String purposeOfLeave;
    private String leaveType;
    
	private Date creationDate;
	private Date updateDate;
	private String userName;
    
	@Transient
	private String fromDate;
	@Transient
	private String toDate;
	@Transient
	private String createDate;
	@Transient
	private int daysPresent;
	
    public EmployeeLeaveDetails() {}

	public EmployeeLeaveDetails(String employeeLeaveId, String employeeId, Date leaveFromDate, Date leaveToDate,
			long noOfDays, String purposeOfLeave, String leaveType, Date creationDate, Date updateDate,
			String userName) {
		super();
		this.employeeLeaveId = employeeLeaveId;
		this.employeeId = employeeId;
		this.leaveFromDate = leaveFromDate;
		this.leaveToDate = leaveToDate;
		this.noOfDays = noOfDays;
		this.purposeOfLeave = purposeOfLeave;
		this.leaveType = leaveType;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getEmployeeLeaveId() {
		return employeeLeaveId;
	}

	public void setEmployeeLeaveId(String employeeLeaveId) {
		this.employeeLeaveId = employeeLeaveId;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public Date getLeaveFromDate() {
		return leaveFromDate;
	}

	public void setLeaveFromDate(Date leaveFromDate) {
		this.leaveFromDate = leaveFromDate;
	}

	public Date getLeaveToDate() {
		return leaveToDate;
	}

	public void setLeaveToDate(Date leaveToDate) {
		this.leaveToDate = leaveToDate;
	}

	public long getNoOfDays() {
		return noOfDays;
	}

	public void setNoOfDays(long noOfDays) {
		this.noOfDays = noOfDays;
	}

	public String getPurposeOfLeave() {
		return purposeOfLeave;
	}

	public void setPurposeOfLeave(String purposeOfLeave) {
		this.purposeOfLeave = purposeOfLeave;
	}

	public String getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public int getDaysPresent() {
		return daysPresent;
	}

	public void setDaysPresent(int daysPresent) {
		this.daysPresent = daysPresent;
	}


}
