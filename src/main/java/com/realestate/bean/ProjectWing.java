package com.realestate.bean;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "projectwings")
public class ProjectWing {

	@Id
	private String wingId;
	private String wingName;
	private String buildingId;
	private String projectId;
	private long infrastructureCharges;
	private long maintenanceCharges;
	private long handlingCharges;
	
	private int noofBasement;
	private int noofStilts;
	private int noofPodiums;
	private int noofFloors;
	private int total;
	private String wingStatus;
	
	private String creationDate;
	private String updateDate;
	private String userName;
	
	public ProjectWing()
	{}

	public ProjectWing(String wingId, String wingName, String buildingId, String projectId,
			long infrastructureCharges, long maintenanceCharges, long handlingCharges, int noofBasement, int noofStilts,
			int noofPodiums, int noofFloors, int total, String wingStatus, String creationDate, String updateDate,
			String userName) {
		super();
		this.wingId = wingId;
		this.wingName = wingName;
		this.buildingId = buildingId;
		this.projectId = projectId;
		this.infrastructureCharges = infrastructureCharges;
		this.maintenanceCharges = maintenanceCharges;
		this.handlingCharges = handlingCharges;
		this.noofBasement = noofBasement;
		this.noofStilts = noofStilts;
		this.noofPodiums = noofPodiums;
		this.noofFloors = noofFloors;
		this.total = total;
		this.wingStatus = wingStatus;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getWingId() {
		return wingId;
	}

	public void setWingId(String wingId) {
		this.wingId = wingId;
	}

	public String getWingName() {
		return wingName;
	}

	public void setWingName(String wingName) {
		this.wingName = wingName;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public long getInfrastructureCharges() {
		return infrastructureCharges;
	}

	public void setInfrastructureCharges(long infrastructureCharges) {
		this.infrastructureCharges = infrastructureCharges;
	}

	public long getMaintenanceCharges() {
		return maintenanceCharges;
	}

	public void setMaintenanceCharges(long maintenanceCharges) {
		this.maintenanceCharges = maintenanceCharges;
	}

	public long getHandlingCharges() {
		return handlingCharges;
	}

	public void setHandlingCharges(long handlingCharges) {
		this.handlingCharges = handlingCharges;
	}

	public int getNoofBasement() {
		return noofBasement;
	}

	public void setNoofBasement(int noofBasement) {
		this.noofBasement = noofBasement;
	}

	public int getNoofStilts() {
		return noofStilts;
	}

	public void setNoofStilts(int noofStilts) {
		this.noofStilts = noofStilts;
	}

	public int getNoofPodiums() {
		return noofPodiums;
	}

	public void setNoofPodiums(int noofPodiums) {
		this.noofPodiums = noofPodiums;
	}

	public int getNoofFloors() {
		return noofFloors;
	}

	public void setNoofFloors(int noofFloors) {
		this.noofFloors = noofFloors;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public String getWingStatus() {
		return wingStatus;
	}

	public void setWingStatus(String wingStatus) {
		this.wingStatus = wingStatus;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
