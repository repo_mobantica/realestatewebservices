package com.realestate.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "customerpaymentschedule")
public class GeneratePaymentScheduler {

	@Id
	private String customerschedulerId;
	private String bookingId;
	
	private String installmentNumber;
	private String paymentDecription;
	private Double percentage;
	private Date creationDate;
	private String userName;
	
	public GeneratePaymentScheduler() {}

	public GeneratePaymentScheduler(String customerschedulerId, String bookingId, String installmentNumber,
			String paymentDecription, Double percentage, Date creationDate, String userName) {
		super();
		this.customerschedulerId = customerschedulerId;
		this.bookingId = bookingId;
		this.installmentNumber = installmentNumber;
		this.paymentDecription = paymentDecription;
		this.percentage = percentage;
		this.creationDate = creationDate;
		this.userName = userName;
	}

	public String getCustomerschedulerId() {
		return customerschedulerId;
	}

	public void setCustomerschedulerId(String customerschedulerId) {
		this.customerschedulerId = customerschedulerId;
	}

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public String getInstallmentNumber() {
		return installmentNumber;
	}

	public void setInstallmentNumber(String installmentNumber) {
		this.installmentNumber = installmentNumber;
	}

	public String getPaymentDecription() {
		return paymentDecription;
	}

	public void setPaymentDecription(String paymentDecription) {
		this.paymentDecription = paymentDecription;
	}

	public Double getPercentage() {
		return percentage;
	}

	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
