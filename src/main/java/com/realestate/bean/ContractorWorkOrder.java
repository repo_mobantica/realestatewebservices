package com.realestate.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.annotation.Transient;
@Document(collection = "contractorworkorders")
public class ContractorWorkOrder {

	@Id
	private String workOrderId;
	private String workId;
	private String quotationId;
	private String projectId;
	private String buildingId;
	private String wingId;
	private String contractortypeId;
	private String contractorId;
	private String contractorfirmName;
	private Double totalAmount;
	
	private long noofMaleLabour;
	private long noofFemaleLabour;
	private long noofInsuredLabour;
	
	private Double retentionPer;
	private Double retentionAmount;
	
	private String creationDate;
	private String updateDate;
	private String userName;
	
	private String paymentStatus;

	@Transient
    private long totalPaidAmount;

	@Transient
    private Double workAmount;
	
	public ContractorWorkOrder() {}

	public ContractorWorkOrder(String workOrderId, String workId, String quotationId, String projectId,
			String buildingId, String wingId, String contractortypeId, String contractorId, String contractorfirmName,
			Double totalAmount, long noofMaleLabour, long noofFemaleLabour, long noofInsuredLabour, Double retentionPer,
			Double retentionAmount, String creationDate, String updateDate, String userName, String paymentStatus) {
		super();
		this.workOrderId = workOrderId;
		this.workId = workId;
		this.quotationId = quotationId;
		this.projectId = projectId;
		this.buildingId = buildingId;
		this.wingId = wingId;
		this.contractortypeId = contractortypeId;
		this.contractorId = contractorId;
		this.contractorfirmName = contractorfirmName;
		this.totalAmount = totalAmount;
		this.noofMaleLabour = noofMaleLabour;
		this.noofFemaleLabour = noofFemaleLabour;
		this.noofInsuredLabour = noofInsuredLabour;
		this.retentionPer = retentionPer;
		this.retentionAmount = retentionAmount;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
		this.paymentStatus = paymentStatus;
	}

	public String getWorkOrderId() {
		return workOrderId;
	}

	public void setWorkOrderId(String workOrderId) {
		this.workOrderId = workOrderId;
	}

	public String getWorkId() {
		return workId;
	}

	public void setWorkId(String workId) {
		this.workId = workId;
	}

	public String getQuotationId() {
		return quotationId;
	}

	public void setQuotationId(String quotationId) {
		this.quotationId = quotationId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getWingId() {
		return wingId;
	}

	public void setWingId(String wingId) {
		this.wingId = wingId;
	}

	public String getContractortypeId() {
		return contractortypeId;
	}

	public void setContractortypeId(String contractortypeId) {
		this.contractortypeId = contractortypeId;
	}

	public String getContractorId() {
		return contractorId;
	}

	public void setContractorId(String contractorId) {
		this.contractorId = contractorId;
	}

	public String getContractorfirmName() {
		return contractorfirmName;
	}

	public void setContractorfirmName(String contractorfirmName) {
		this.contractorfirmName = contractorfirmName;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public long getNoofMaleLabour() {
		return noofMaleLabour;
	}

	public void setNoofMaleLabour(long noofMaleLabour) {
		this.noofMaleLabour = noofMaleLabour;
	}

	public long getNoofFemaleLabour() {
		return noofFemaleLabour;
	}

	public void setNoofFemaleLabour(long noofFemaleLabour) {
		this.noofFemaleLabour = noofFemaleLabour;
	}

	public long getNoofInsuredLabour() {
		return noofInsuredLabour;
	}

	public void setNoofInsuredLabour(long noofInsuredLabour) {
		this.noofInsuredLabour = noofInsuredLabour;
	}

	public Double getRetentionPer() {
		return retentionPer;
	}

	public void setRetentionPer(Double retentionPer) {
		this.retentionPer = retentionPer;
	}

	public Double getRetentionAmount() {
		return retentionAmount;
	}

	public void setRetentionAmount(Double retentionAmount) {
		this.retentionAmount = retentionAmount;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public long getTotalPaidAmount() {
		return totalPaidAmount;
	}

	public void setTotalPaidAmount(long totalPaidAmount) {
		this.totalPaidAmount = totalPaidAmount;
	}

	public Double getWorkAmount() {
		return workAmount;
	}

	public void setWorkAmount(Double workAmount) {
		this.workAmount = workAmount;
	}
	
}
