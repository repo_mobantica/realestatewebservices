package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="userassignedprojects")
public class UserAssignedProject 
{
    @Id
    private long   projectassignId;
    private String userModelId;
    private String projectId;
    private String employeeId;
    
    @Transient
    private String projectName;

    public UserAssignedProject() 
    {
   
    }

	public UserAssignedProject(long projectassignId, String userModelId, String projectId, String employeeId) 
	{
		this.projectassignId = projectassignId;
		this.userModelId = userModelId;
		this.projectId = projectId;
		this.employeeId = employeeId;
	}

	public long getProjectassignId() {
		return projectassignId;
	}

	public void setProjectassignId(long projectassignId) {
		this.projectassignId = projectassignId;
	}

	public String getUserModelId() {
		return userModelId;
	}

	public void setUserModelId(String userModelId) {
		this.userModelId = userModelId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	
}
