package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "projectspecifications")
public class ProjectSpecification
{
  @Id
  private long projectspecificationId;
  private String projectId;
  private String suppliertypeId;
  private String subsuppliertypeId;
  private String itemId;
  private String brand1;
  private String brand2;
  private String brand3;

  @Transient
  private String projectName;
  @Transient
  private String supplierType;
  @Transient
  private String subsupplierType;
  @Transient
  private String itemName;
  @Transient
  private String brandName1;
  @Transient
  private String brandName2;
  @Transient
  private String brandName3;
  
  public ProjectSpecification() 
  {

  }

  public ProjectSpecification(long projectspecificationId, String projectId, String suppliertypeId,
			String subsuppliertypeId, String itemId, String brand1, String brand2, String brand3) {
		super();
		this.projectspecificationId = projectspecificationId;
		this.projectId = projectId;
		this.suppliertypeId = suppliertypeId;
		this.subsuppliertypeId = subsuppliertypeId;
		this.itemId = itemId;
		this.brand1 = brand1;
		this.brand2 = brand2;
		this.brand3 = brand3;
  }

	public long getProjectspecificationId() {
		return projectspecificationId;
	}
	
	public void setProjectspecificationId(long projectspecificationId) {
		this.projectspecificationId = projectspecificationId;
	}
	
	public String getProjectId() {
		return projectId;
	}
	
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	
	public String getSuppliertypeId() {
		return suppliertypeId;
	}
	
	public void setSuppliertypeId(String suppliertypeId) {
		this.suppliertypeId = suppliertypeId;
	}
	
	public String getSubsuppliertypeId() {
		return subsuppliertypeId;
	}
	
	public void setSubsuppliertypeId(String subsuppliertypeId) {
		this.subsuppliertypeId = subsuppliertypeId;
	}
	
	public String getItemId() {
		return itemId;
	}
	
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
	
	public String getBrand1() {
		return brand1;
	}
	
	public void setBrand1(String brand1) {
		this.brand1 = brand1;
	}
	
	public String getBrand2() {
		return brand2;
	}
	
	public void setBrand2(String brand2) {
		this.brand2 = brand2;
	}
	
	public String getBrand3() {
		return brand3;
	}
	
	public void setBrand3(String brand3) {
		this.brand3 = brand3;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getSupplierType() {
		return supplierType;
	}

	public void setSupplierType(String supplierType) {
		this.supplierType = supplierType;
	}

	public String getSubsupplierType() {
		return subsupplierType;
	}

	public void setSubsupplierType(String subsupplierType) {
		this.subsupplierType = subsupplierType;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getBrandName1() {
		return brandName1;
	}

	public void setBrandName1(String brandName1) {
		this.brandName1 = brandName1;
	}

	public String getBrandName2() {
		return brandName2;
	}

	public void setBrandName2(String brandName2) {
		this.brandName2 = brandName2;
	}

	public String getBrandName3() {
		return brandName3;
	}

	public void setBrandName3(String brandName3) {
		this.brandName3 = brandName3;
	}
	  
}
