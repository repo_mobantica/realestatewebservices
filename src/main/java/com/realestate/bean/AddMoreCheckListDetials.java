package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "addmorechecklistdetials")
public class AddMoreCheckListDetials {

	@Id
    private String typeId;
	private String checkListId;
	private String bookingId;
	private String aggreementId;
    private String doorType;
    private String noOfkeySet;
    private String keyNo;
    
    public AddMoreCheckListDetials() {}

	public AddMoreCheckListDetials(String typeId, String checkListId, String bookingId, String aggreementId,
			String doorType, String noOfkeySet, String keyNo) {
		super();
		this.typeId = typeId;
		this.checkListId = checkListId;
		this.bookingId = bookingId;
		this.aggreementId = aggreementId;
		this.doorType = doorType;
		this.noOfkeySet = noOfkeySet;
		this.keyNo = keyNo;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getCheckListId() {
		return checkListId;
	}

	public void setCheckListId(String checkListId) {
		this.checkListId = checkListId;
	}

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public String getAggreementId() {
		return aggreementId;
	}

	public void setAggreementId(String aggreementId) {
		this.aggreementId = aggreementId;
	}

	public String getDoorType() {
		return doorType;
	}

	public void setDoorType(String doorType) {
		this.doorType = doorType;
	}

	public String getNoOfkeySet() {
		return noOfkeySet;
	}

	public void setNoOfkeySet(String noOfkeySet) {
		this.noOfkeySet = noOfkeySet;
	}

	public String getKeyNo() {
		return keyNo;
	}

	public void setKeyNo(String keyNo) {
		this.keyNo = keyNo;
	}
 
    
}
