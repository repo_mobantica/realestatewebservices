package com.realestate.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "agentpaymenthistories")
public class AgentPayment {

	@Id
	private String agentPaymentId;
	private String bookingId;
	private String agentId;

	private Double paymentAmount;
	private String paymentMode;
	private String companyBankId;
	private String refNumber;
	private String narration;

	private String chartaccountId;
	private String subchartaccountId;

	private Date creationDate;
	private Date updateDate;
	private String userName;

	public AgentPayment() {}

	public AgentPayment(String agentPaymentId, String bookingId, String agentId, Double paymentAmount,
			String paymentMode, String companyBankId, String refNumber, String narration,
			String chartaccountId, String subchartaccountId, Date creationDate, Date updateDate, String userName) {
		super();
		this.agentPaymentId = agentPaymentId;
		this.bookingId = bookingId;
		this.agentId = agentId;
		this.paymentAmount = paymentAmount;
		this.paymentMode = paymentMode;
		this.companyBankId = companyBankId;
		this.refNumber = refNumber;
		this.narration = narration;
		this.chartaccountId = chartaccountId;
		this.subchartaccountId = subchartaccountId;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getAgentPaymentId() {
		return agentPaymentId;
	}

	public void setAgentPaymentId(String agentPaymentId) {
		this.agentPaymentId = agentPaymentId;
	}

	public String getBookingId() {
		return bookingId;
	}

	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public Double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(Double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getCompanyBankId() {
		return companyBankId;
	}

	public void setCompanyBankId(String companyBankId) {
		this.companyBankId = companyBankId;
	}

	public String getRefNumber() {
		return refNumber;
	}

	public void setRefNumber(String refNumber) {
		this.refNumber = refNumber;
	}

	public String getNarration() {
		return narration;
	}

	public void setNarration(String narration) {
		this.narration = narration;
	}

	public String getChartaccountId() {
		return chartaccountId;
	}

	public void setChartaccountId(String chartaccountId) {
		this.chartaccountId = chartaccountId;
	}

	public String getSubchartaccountId() {
		return subchartaccountId;
	}

	public void setSubchartaccountId(String subchartaccountId) {
		this.subchartaccountId = subchartaccountId;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
