package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "items")
//@CompoundIndexes({@CompoundIndex(name="itemsIndex", unique= true, def="{'itemName':1}")})

public class Item
{
	 	@Id
	    private String itemId;
	    private String suppliertypeId;
	    private String subsuppliertypeId;
	   // private String subItemCategoryName;
	    private String itemName;
	    private double intemInitialPrice;
	    private double itemInitialDiscount;
	   // private String itemSize;
	    private double minimumStock;
	    private double currentStock;
	    private String hsnCode;
	    private double gstPer;
	    private String itemStatus;
	    private String creationDate;
	    private String updateDate;
	    private String userName;
	    
	    public Item()
	    { }

		public Item(String itemId, String suppliertypeId, String subsuppliertypeId, String itemName,
				double intemInitialPrice, double itemInitialDiscount, double minimumStock, double currentStock,
				String hsnCode, double gstPer, String itemStatus, String creationDate, String updateDate,
				String userName) {
			super();
			this.itemId = itemId;
			this.suppliertypeId = suppliertypeId;
			this.subsuppliertypeId = subsuppliertypeId;
			this.itemName = itemName;
			this.intemInitialPrice = intemInitialPrice;
			this.itemInitialDiscount = itemInitialDiscount;
			this.minimumStock = minimumStock;
			this.currentStock = currentStock;
			this.hsnCode = hsnCode;
			this.gstPer = gstPer;
			this.itemStatus = itemStatus;
			this.creationDate = creationDate;
			this.updateDate = updateDate;
			this.userName = userName;
		}

		public String getItemId() {
			return itemId;
		}

		public void setItemId(String itemId) {
			this.itemId = itemId;
		}

		public String getSuppliertypeId() {
			return suppliertypeId;
		}

		public void setSuppliertypeId(String suppliertypeId) {
			this.suppliertypeId = suppliertypeId;
		}

		public String getSubsuppliertypeId() {
			return subsuppliertypeId;
		}

		public void setSubsuppliertypeId(String subsuppliertypeId) {
			this.subsuppliertypeId = subsuppliertypeId;
		}

		public String getItemName() {
			return itemName;
		}

		public void setItemName(String itemName) {
			this.itemName = itemName;
		}

		public double getIntemInitialPrice() {
			return intemInitialPrice;
		}

		public void setIntemInitialPrice(double intemInitialPrice) {
			this.intemInitialPrice = intemInitialPrice;
		}

		public double getItemInitialDiscount() {
			return itemInitialDiscount;
		}

		public void setItemInitialDiscount(double itemInitialDiscount) {
			this.itemInitialDiscount = itemInitialDiscount;
		}

		public double getMinimumStock() {
			return minimumStock;
		}

		public void setMinimumStock(double minimumStock) {
			this.minimumStock = minimumStock;
		}

		public double getCurrentStock() {
			return currentStock;
		}

		public void setCurrentStock(double currentStock) {
			this.currentStock = currentStock;
		}

		public String getHsnCode() {
			return hsnCode;
		}

		public void setHsnCode(String hsnCode) {
			this.hsnCode = hsnCode;
		}

		public double getGstPer() {
			return gstPer;
		}

		public void setGstPer(double gstPer) {
			this.gstPer = gstPer;
		}

		public String getItemStatus() {
			return itemStatus;
		}

		public void setItemStatus(String itemStatus) {
			this.itemStatus = itemStatus;
		}

		public String getCreationDate() {
			return creationDate;
		}

		public void setCreationDate(String creationDate) {
			this.creationDate = creationDate;
		}

		public String getUpdateDate() {
			return updateDate;
		}

		public void setUpdateDate(String updateDate) {
			this.updateDate = updateDate;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

}
