package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "contractoremployees")
public class ContractorEmployees
{
	@Id
	private int contractorEmployeeId;
	private String contractorId;
	private String employeeName;
	private String employeeDesignation;
	private String employeeEmail;
	private String employeeMobileno;
	
	
	public ContractorEmployees()
	{
		
	}

	public ContractorEmployees(int contractorEmployeeId, String contractorId, String employeeName, String employeeDesignation, String employeeEmail, String employeeMobileno)
	{
		super();
		this.contractorEmployeeId = contractorEmployeeId;
		this.contractorId = contractorId;
		this.employeeName = employeeName;
		this.employeeDesignation = employeeDesignation;
		this.employeeEmail = employeeEmail;
		this.employeeMobileno = employeeMobileno;
	}

	public int getContractorEmployeeId() {
		return contractorEmployeeId;
	}

	public void setContractorEmployeeId(int contractorEmployeeId) {
		this.contractorEmployeeId = contractorEmployeeId;
	}

	public String getContractorId() {
		return contractorId;
	}

	public void setContractorId(String contractorId) {
		this.contractorId = contractorId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmployeeDesignation() {
		return employeeDesignation;
	}

	public void setEmployeeDesignation(String employeeDesignation) {
		this.employeeDesignation = employeeDesignation;
	}

	public String getEmployeeEmail() {
		return employeeEmail;
	}

	public void setEmployeeEmail(String employeeEmail) {
		this.employeeEmail = employeeEmail;
	}

	public String getEmployeeMobileno() {
		return employeeMobileno;
	}

	public void setEmployeeMobileno(String employeeMobileno) {
		this.employeeMobileno = employeeMobileno;
	}

}
