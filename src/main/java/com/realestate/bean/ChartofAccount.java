package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "chartofaccounts")
@CompoundIndexes({@CompoundIndex(name="chartofaccountIndex", unique= true, def="{'chartaccountName':1}")})

public class ChartofAccount {

	@Id
    private String chartaccountId;
    private Integer chartaccountNumber;
    private String chartaccountName;
    private String creationDate;
    private String updateDate;
    private String userName;
    
    public ChartofAccount() {}

	public ChartofAccount(String chartaccountId, Integer chartaccountNumber, String chartaccountName,
			String creationDate, String updateDate, String userName)
	{
		super();
		this.chartaccountId = chartaccountId;
		this.chartaccountNumber = chartaccountNumber;
		this.chartaccountName = chartaccountName;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getChartaccountId() {
		return chartaccountId;
	}

	public void setChartaccountId(String chartaccountId) {
		this.chartaccountId = chartaccountId;
	}

	public Integer getChartaccountNumber() {
		return chartaccountNumber;
	}

	public void setChartaccountNumber(Integer chartaccountNumber) {
		this.chartaccountNumber = chartaccountNumber;
	}

	public String getChartaccountName() {
		return chartaccountName;
	}

	public void setChartaccountName(String chartaccountName) {
		this.chartaccountName = chartaccountName;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
    
}
