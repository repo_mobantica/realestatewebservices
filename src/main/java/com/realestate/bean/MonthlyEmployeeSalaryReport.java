package com.realestate.bean;

public class MonthlyEmployeeSalaryReport {

	private String srno;
	private String employeeId;
	private String employeeName;
	
	private Double presentDay;
	
	private Double basicPay;
	private Double hra;
	
	private Double leaveDeduction;
	private Double pfAmount;
	private Double esi;
	private Double loanAmount;
	private Double prefesionTax;
	private Double tdsAmount;
	
	private String salarySatus;

	public String getSrno() {
		return srno;
	}

	public void setSrno(String srno) {
		this.srno = srno;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Double getPresentDay() {
		return presentDay;
	}

	public void setPresentDay(Double presentDay) {
		this.presentDay = presentDay;
	}

	public Double getBasicPay() {
		return basicPay;
	}

	public void setBasicPay(Double basicPay) {
		this.basicPay = basicPay;
	}

	public Double getHra() {
		return hra;
	}

	public void setHra(Double hra) {
		this.hra = hra;
	}

	public Double getLeaveDeduction() {
		return leaveDeduction;
	}

	public void setLeaveDeduction(Double leaveDeduction) {
		this.leaveDeduction = leaveDeduction;
	}

	public Double getPfAmount() {
		return pfAmount;
	}

	public void setPfAmount(Double pfAmount) {
		this.pfAmount = pfAmount;
	}

	public Double getEsi() {
		return esi;
	}

	public void setEsi(Double esi) {
		this.esi = esi;
	}

	public Double getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(Double loanAmount) {
		this.loanAmount = loanAmount;
	}

	public Double getPrefesionTax() {
		return prefesionTax;
	}

	public void setPrefesionTax(Double prefesionTax) {
		this.prefesionTax = prefesionTax;
	}

	public Double getTdsAmount() {
		return tdsAmount;
	}

	public void setTdsAmount(Double tdsAmount) {
		this.tdsAmount = tdsAmount;
	}

	public String getSalarySatus() {
		return salarySatus;
	}

	public void setSalarySatus(String salarySatus) {
		this.salarySatus = salarySatus;
	}
	
	
}
