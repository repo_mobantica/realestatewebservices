package com.realestate.bean;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="customerpaymentstatushistory")
public class CustomerPaymentStatusHistory 
{
  @Id
  private String paymentHistoryId;
  private String receiptId;
  private String bookingId;
  private String companyAcNo;
  private long   paymentAmount;
  private String transactionStatus;
  private String paymentStatus;
  private String paymentDate;
  private String remark;
  private String chartaccountId;
  private String subchartaccountId;
  private Date creationDate;
  
  public CustomerPaymentStatusHistory() 
  {
  }

public CustomerPaymentStatusHistory(String paymentHistoryId, String receiptId, String bookingId, String companyAcNo,
		long paymentAmount, String transactionStatus, String paymentStatus, String paymentDate, String remark,
		String chartaccountId, String subchartaccountId, Date creationDate) {
	super();
	this.paymentHistoryId = paymentHistoryId;
	this.receiptId = receiptId;
	this.bookingId = bookingId;
	this.companyAcNo = companyAcNo;
	this.paymentAmount = paymentAmount;
	this.transactionStatus = transactionStatus;
	this.paymentStatus = paymentStatus;
	this.paymentDate = paymentDate;
	this.remark = remark;
	this.chartaccountId = chartaccountId;
	this.subchartaccountId = subchartaccountId;
	this.creationDate = creationDate;
}

public String getPaymentHistoryId() {
	return paymentHistoryId;
}

public void setPaymentHistoryId(String paymentHistoryId) {
	this.paymentHistoryId = paymentHistoryId;
}

public String getReceiptId() {
	return receiptId;
}

public void setReceiptId(String receiptId) {
	this.receiptId = receiptId;
}

public String getBookingId() {
	return bookingId;
}

public void setBookingId(String bookingId) {
	this.bookingId = bookingId;
}

public String getCompanyAcNo() {
	return companyAcNo;
}

public void setCompanyAcNo(String companyAcNo) {
	this.companyAcNo = companyAcNo;
}

public long getPaymentAmount() {
	return paymentAmount;
}

public void setPaymentAmount(long paymentAmount) {
	this.paymentAmount = paymentAmount;
}

public String getTransactionStatus() {
	return transactionStatus;
}

public void setTransactionStatus(String transactionStatus) {
	this.transactionStatus = transactionStatus;
}

public String getPaymentStatus() {
	return paymentStatus;
}

public void setPaymentStatus(String paymentStatus) {
	this.paymentStatus = paymentStatus;
}

public String getPaymentDate() {
	return paymentDate;
}

public void setPaymentDate(String paymentDate) {
	this.paymentDate = paymentDate;
}

public String getRemark() {
	return remark;
}

public void setRemark(String remark) {
	this.remark = remark;
}

public String getChartaccountId() {
	return chartaccountId;
}

public void setChartaccountId(String chartaccountId) {
	this.chartaccountId = chartaccountId;
}

public String getSubchartaccountId() {
	return subchartaccountId;
}

public void setSubchartaccountId(String subchartaccountId) {
	this.subchartaccountId = subchartaccountId;
}

public Date getCreationDate() {
	return creationDate;
}

public void setCreationDate(Date creationDate) {
	this.creationDate = creationDate;
}

}
