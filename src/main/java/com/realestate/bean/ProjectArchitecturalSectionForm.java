package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "projectarchitecturalsectionforms")
public class ProjectArchitecturalSectionForm {

	@Id
	private String projectArchitecturalId;
	private String projectId;
	private String consultancyId;
	

 //per
	private Double aeraOfPlotAsPer7_12ExtractPer;
	private Double areaOfPlotAsPerPropetyPer;
	private Double areaOfPlotAsPerDemarcationPer;
	private Double areaAsPerSanctionedLayoutPer;
	private Double areaAsPerULCOrderPer;
	private Double areaAsPerDevelopmentAgreementPer;
	private Double areaOfPlotMinimumConsiderationPer;
	private Double areaUnderBRTCorridorPer;
	private Double areaUnder1_50MRoadWideningPer;
	private Double areaUnder3_00MRoadWideningPer;
	private Double areaUnder4_50MRoadWideningPer;
	private Double areaUnder6_00MRoadWideningPer;
	private Double areaUnder7_50MRoadWideningPer;
	private Double areaUnder9_00MRoadWideningPer;
	private Double areaUnder12_00MRoadWideningPer;
	private Double areaUnder15_00MRoadWideningPer;
	private Double areaUnder18_00MRoadWideningPer;
	private Double areaUnder24_00MRoadWideningPer;
	private Double areaUnder30_00MRoadWideningPer;
	private Double areaUnderServiceRoadPer;
	private Double areaUnderReservationPer;
	private Double areaUnderGreenBeltPer;
	private Double areaUnderNalaPer;
	
	
	private Double recreationGround10Per;
	private Double amenitySpace5Per;
	private Double internalRoadsPer;
	private Double transformerPer;
	
	private Double tdr40Per;
	private Double additionsForFAR4a_RecreationGroundPer;
	private Double additionForFAR4b_AmenitySpacePer;
	private Double additionForFAR4c_InternalsRoadPer;
	private Double additionForFAR4d_TransformerPer;
	
	private Double farPermissiblePer;
	
	private Double permissibleResidentialPer;
	private Double existingResidentialPer;
	private Double proposedResidentialPer;
	private Double permissibleCommercial_IndustrialPer;
	private Double existingCommercial_IndustrialPer;
	private Double proposedCommercial_IndustrialPer;
	
	private Double excessBalconyAreaTakenInFSIPer;

	private Double totalFSIConSumedPer;
	
	private Double permissibleBalconyPer;
	private Double proposedBalconyPer;
	
	private Double netGrossAreaOfPlot_A7_Per;
	private Double lessDeductionForNon_ResidentialPer;

	private Double tenenmentsPermissiblePer;
	private Double totalTenementsProposedPer;
	private Double netPlotAreaPer;
	private Double permissibleGroundCoveragePer;
	private Double existingGroundCoveragePer;
	private Double proposedGroundCoveragePer;
	private Double permissibleGroundCoverageInPremium15Per;

	
	//area
	private Double aeraOfPlotAsPer7_12ExtractArea;
	private Double areaOfPlotAsPerPropetyArea;
	private Double areaOfPlotAsPerDemarcationArea;
	private Double areaAsPerSanctionedLayoutArea;
	private Double areaAsPerULCOrderArea;
	private Double areaAsPerDevelopmentAgreementArea;
	private Double areaOfPlotMinimumConsiderationArea;
	
	private Double areaUnderBRTCorridorArea;
	private Double areaUnder1_50MRoadWideningArea;
	private Double areaUnder3_00MRoadWideningArea;
	private Double areaUnder4_50MRoadWideningArea;
	private Double areaUnder6_00MRoadWideningArea;
	private Double areaUnder7_50MRoadWideningArea;
	private Double areaUnder9_00MRoadWideningArea;
	private Double areaUnder12_00MRoadWideningArea;
	private Double areaUnder15_00MRoadWideningArea;
	private Double areaUnder18_00MRoadWideningArea;
	private Double areaUnder24_00MRoadWideningArea;
	private Double areaUnder30_00MRoadWideningArea;
	private Double areaUnderServiceRoadArea;
	private Double areaUnderReservationArea;
	private Double areaUnderGreenBeltArea;
	private Double areaUnderNalaArea;
	private Double total2Area;
	private Double netGrossAreaOfPlot1_2Area;
	private Double recreationGround10Area;
	private Double amenitySpace5Area;
	private Double internalRoadsArea;
	private Double transformerArea;
	private Double total4Area;
	private Double netAreaOfPlot3_4Area;
	private Double tdr40Area;
	private Double additionsForFAR4a_RecreationGroundArea;
	private Double additionForFAR4b_AmenitySpaceArea;
	private Double additionForFAR4c_InternalsRoadArea;
	private Double additionForFAR4d_TransformerArea;
	private Double total6Area;
	private Double totalArea5_6Area;
	private Double farPermissibleArea;
	private Double totalPermissibleFloorArea7_8Area;
	private Double permissibleResidentialArea;
	private Double existingResidentialArea;
	private Double proposedResidentialArea;
	private Double totalResidentialArea11_12Area;
	private Double permissibleCommercial_IndustrialArea;
	private Double existingCommercial_IndustrialArea;
	private Double proposedCommercial_IndustrialArea;
	private Double totalCommercial_IndustrialArea15_16Area;
	private Double totalExistingArea11_15;
	private Double totalProposedArea12_16Area;
	private Double totalExistingArea_Proposed18_19Area;
	private Double excessBalconyAreaTakenInFSIArea;
	private Double totalBupArea20_21Area;
	private Double totalFSIConSumedArea;
	private Double permissibleBalconyArea;
	private Double proposedBalconyArea;
	private Double excessBalconyArea;
	private Double netGrossAreaOfPlot_A7_Area;
	private Double lessDeductionForNon_ResidentialArea;
	private Double areaOfTenementsa_bArea;
	private Double tenenmentsPermissibleArea;
	private Double totalTenementsProposedArea;
	private Double netPlotAreaArea;
	private Double permissibleGroundCoverageArea;
	private Double existingGroundCoverageArea;
	private Double proposedGroundCoverageArea;
	private Double permissibleGroundCoverageInPremium15Area;
	private Double excessGroundConveraged_bArea;
	
	
	private String creationDate;
	private String updateDate;
	private String userName;
	
	@Transient
	private String projectName;
	@Transient
	private String firmName;
	
	public ProjectArchitecturalSectionForm() {}

	public ProjectArchitecturalSectionForm(String projectArchitecturalId, String projectId, String consultancyId,
			Double aeraOfPlotAsPer7_12ExtractPer, Double areaOfPlotAsPerPropetyPer,
			Double areaOfPlotAsPerDemarcationPer, Double areaAsPerSanctionedLayoutPer, Double areaAsPerULCOrderPer,
			Double areaAsPerDevelopmentAgreementPer, Double areaOfPlotMinimumConsiderationPer,
			Double areaUnderBRTCorridorPer, Double areaUnder1_50MRoadWideningPer, Double areaUnder3_00MRoadWideningPer,
			Double areaUnder4_50MRoadWideningPer, Double areaUnder6_00MRoadWideningPer,
			Double areaUnder7_50MRoadWideningPer, Double areaUnder9_00MRoadWideningPer,
			Double areaUnder12_00MRoadWideningPer, Double areaUnder15_00MRoadWideningPer,
			Double areaUnder18_00MRoadWideningPer, Double areaUnder24_00MRoadWideningPer,
			Double areaUnder30_00MRoadWideningPer, Double areaUnderServiceRoadPer, Double areaUnderReservationPer,
			Double areaUnderGreenBeltPer, Double areaUnderNalaPer, Double recreationGround10Per,
			Double amenitySpace5Per, Double internalRoadsPer, Double transformerPer, Double tdr40Per,
			Double additionsForFAR4a_RecreationGroundPer, Double additionForFAR4b_AmenitySpacePer,
			Double additionForFAR4c_InternalsRoadPer, Double additionForFAR4d_TransformerPer, Double farPermissiblePer,
			Double permissibleResidentialPer, Double existingResidentialPer, Double proposedResidentialPer,
			Double permissibleCommercial_IndustrialPer, Double existingCommercial_IndustrialPer,
			Double proposedCommercial_IndustrialPer, Double excessBalconyAreaTakenInFSIPer, Double totalFSIConSumedPer,
			Double permissibleBalconyPer, Double proposedBalconyPer, Double netGrossAreaOfPlot_A7_Per,
			Double lessDeductionForNon_ResidentialPer, Double tenenmentsPermissiblePer,
			Double totalTenementsProposedPer, Double netPlotAreaPer, Double permissibleGroundCoveragePer,
			Double existingGroundCoveragePer, Double proposedGroundCoveragePer,
			Double permissibleGroundCoverageInPremium15Per, Double aeraOfPlotAsPer7_12ExtractArea,
			Double areaOfPlotAsPerPropetyArea, Double areaOfPlotAsPerDemarcationArea,
			Double areaAsPerSanctionedLayoutArea, Double areaAsPerULCOrderArea,
			Double areaAsPerDevelopmentAgreementArea, Double areaOfPlotMinimumConsiderationArea,
			Double areaUnderBRTCorridorArea, Double areaUnder1_50MRoadWideningArea,
			Double areaUnder3_00MRoadWideningArea, Double areaUnder4_50MRoadWideningArea,
			Double areaUnder6_00MRoadWideningArea, Double areaUnder7_50MRoadWideningArea,
			Double areaUnder9_00MRoadWideningArea, Double areaUnder12_00MRoadWideningArea,
			Double areaUnder15_00MRoadWideningArea, Double areaUnder18_00MRoadWideningArea,
			Double areaUnder24_00MRoadWideningArea, Double areaUnder30_00MRoadWideningArea,
			Double areaUnderServiceRoadArea, Double areaUnderReservationArea, Double areaUnderGreenBeltArea,
			Double areaUnderNalaArea, Double total2Area, Double netGrossAreaOfPlot1_2Area,
			Double recreationGround10Area, Double amenitySpace5Area, Double internalRoadsArea, Double transformerArea,
			Double total4Area, Double netAreaOfPlot3_4Area, Double tdr40Area,
			Double additionsForFAR4a_RecreationGroundArea, Double additionForFAR4b_AmenitySpaceArea,
			Double additionForFAR4c_InternalsRoadArea, Double additionForFAR4d_TransformerArea, Double total6Area,
			Double totalArea5_6Area, Double farPermissibleArea, Double totalPermissibleFloorArea7_8Area,
			Double permissibleResidentialArea, Double existingResidentialArea, Double proposedResidentialArea,
			Double totalResidentialArea11_12Area, Double permissibleCommercial_IndustrialArea,
			Double existingCommercial_IndustrialArea, Double proposedCommercial_IndustrialArea,
			Double totalCommercial_IndustrialArea15_16Area, Double totalExistingArea11_15,
			Double totalProposedArea12_16Area, Double totalExistingArea_Proposed18_19Area,
			Double excessBalconyAreaTakenInFSIArea, Double totalBupArea20_21Area, Double totalFSIConSumedArea,
			Double permissibleBalconyArea, Double proposedBalconyArea, Double excessBalconyArea,
			Double netGrossAreaOfPlot_A7_Area, Double lessDeductionForNon_ResidentialArea,
			Double areaOfTenementsa_bArea, Double tenenmentsPermissibleArea, Double totalTenementsProposedArea,
			Double netPlotAreaArea, Double permissibleGroundCoverageArea, Double existingGroundCoverageArea,
			Double proposedGroundCoverageArea, Double permissibleGroundCoverageInPremium15Area,
			Double excessGroundConveraged_bArea, String creationDate, String updateDate, String userName) {
		super();
		this.projectArchitecturalId = projectArchitecturalId;
		this.projectId = projectId;
		this.consultancyId = consultancyId;
		this.aeraOfPlotAsPer7_12ExtractPer = aeraOfPlotAsPer7_12ExtractPer;
		this.areaOfPlotAsPerPropetyPer = areaOfPlotAsPerPropetyPer;
		this.areaOfPlotAsPerDemarcationPer = areaOfPlotAsPerDemarcationPer;
		this.areaAsPerSanctionedLayoutPer = areaAsPerSanctionedLayoutPer;
		this.areaAsPerULCOrderPer = areaAsPerULCOrderPer;
		this.areaAsPerDevelopmentAgreementPer = areaAsPerDevelopmentAgreementPer;
		this.areaOfPlotMinimumConsiderationPer = areaOfPlotMinimumConsiderationPer;
		this.areaUnderBRTCorridorPer = areaUnderBRTCorridorPer;
		this.areaUnder1_50MRoadWideningPer = areaUnder1_50MRoadWideningPer;
		this.areaUnder3_00MRoadWideningPer = areaUnder3_00MRoadWideningPer;
		this.areaUnder4_50MRoadWideningPer = areaUnder4_50MRoadWideningPer;
		this.areaUnder6_00MRoadWideningPer = areaUnder6_00MRoadWideningPer;
		this.areaUnder7_50MRoadWideningPer = areaUnder7_50MRoadWideningPer;
		this.areaUnder9_00MRoadWideningPer = areaUnder9_00MRoadWideningPer;
		this.areaUnder12_00MRoadWideningPer = areaUnder12_00MRoadWideningPer;
		this.areaUnder15_00MRoadWideningPer = areaUnder15_00MRoadWideningPer;
		this.areaUnder18_00MRoadWideningPer = areaUnder18_00MRoadWideningPer;
		this.areaUnder24_00MRoadWideningPer = areaUnder24_00MRoadWideningPer;
		this.areaUnder30_00MRoadWideningPer = areaUnder30_00MRoadWideningPer;
		this.areaUnderServiceRoadPer = areaUnderServiceRoadPer;
		this.areaUnderReservationPer = areaUnderReservationPer;
		this.areaUnderGreenBeltPer = areaUnderGreenBeltPer;
		this.areaUnderNalaPer = areaUnderNalaPer;
		this.recreationGround10Per = recreationGround10Per;
		this.amenitySpace5Per = amenitySpace5Per;
		this.internalRoadsPer = internalRoadsPer;
		this.transformerPer = transformerPer;
		this.tdr40Per = tdr40Per;
		this.additionsForFAR4a_RecreationGroundPer = additionsForFAR4a_RecreationGroundPer;
		this.additionForFAR4b_AmenitySpacePer = additionForFAR4b_AmenitySpacePer;
		this.additionForFAR4c_InternalsRoadPer = additionForFAR4c_InternalsRoadPer;
		this.additionForFAR4d_TransformerPer = additionForFAR4d_TransformerPer;
		this.farPermissiblePer = farPermissiblePer;
		this.permissibleResidentialPer = permissibleResidentialPer;
		this.existingResidentialPer = existingResidentialPer;
		this.proposedResidentialPer = proposedResidentialPer;
		this.permissibleCommercial_IndustrialPer = permissibleCommercial_IndustrialPer;
		this.existingCommercial_IndustrialPer = existingCommercial_IndustrialPer;
		this.proposedCommercial_IndustrialPer = proposedCommercial_IndustrialPer;
		this.excessBalconyAreaTakenInFSIPer = excessBalconyAreaTakenInFSIPer;
		this.totalFSIConSumedPer = totalFSIConSumedPer;
		this.permissibleBalconyPer = permissibleBalconyPer;
		this.proposedBalconyPer = proposedBalconyPer;
		this.netGrossAreaOfPlot_A7_Per = netGrossAreaOfPlot_A7_Per;
		this.lessDeductionForNon_ResidentialPer = lessDeductionForNon_ResidentialPer;
		this.tenenmentsPermissiblePer = tenenmentsPermissiblePer;
		this.totalTenementsProposedPer = totalTenementsProposedPer;
		this.netPlotAreaPer = netPlotAreaPer;
		this.permissibleGroundCoveragePer = permissibleGroundCoveragePer;
		this.existingGroundCoveragePer = existingGroundCoveragePer;
		this.proposedGroundCoveragePer = proposedGroundCoveragePer;
		this.permissibleGroundCoverageInPremium15Per = permissibleGroundCoverageInPremium15Per;
		this.aeraOfPlotAsPer7_12ExtractArea = aeraOfPlotAsPer7_12ExtractArea;
		this.areaOfPlotAsPerPropetyArea = areaOfPlotAsPerPropetyArea;
		this.areaOfPlotAsPerDemarcationArea = areaOfPlotAsPerDemarcationArea;
		this.areaAsPerSanctionedLayoutArea = areaAsPerSanctionedLayoutArea;
		this.areaAsPerULCOrderArea = areaAsPerULCOrderArea;
		this.areaAsPerDevelopmentAgreementArea = areaAsPerDevelopmentAgreementArea;
		this.areaOfPlotMinimumConsiderationArea = areaOfPlotMinimumConsiderationArea;
		this.areaUnderBRTCorridorArea = areaUnderBRTCorridorArea;
		this.areaUnder1_50MRoadWideningArea = areaUnder1_50MRoadWideningArea;
		this.areaUnder3_00MRoadWideningArea = areaUnder3_00MRoadWideningArea;
		this.areaUnder4_50MRoadWideningArea = areaUnder4_50MRoadWideningArea;
		this.areaUnder6_00MRoadWideningArea = areaUnder6_00MRoadWideningArea;
		this.areaUnder7_50MRoadWideningArea = areaUnder7_50MRoadWideningArea;
		this.areaUnder9_00MRoadWideningArea = areaUnder9_00MRoadWideningArea;
		this.areaUnder12_00MRoadWideningArea = areaUnder12_00MRoadWideningArea;
		this.areaUnder15_00MRoadWideningArea = areaUnder15_00MRoadWideningArea;
		this.areaUnder18_00MRoadWideningArea = areaUnder18_00MRoadWideningArea;
		this.areaUnder24_00MRoadWideningArea = areaUnder24_00MRoadWideningArea;
		this.areaUnder30_00MRoadWideningArea = areaUnder30_00MRoadWideningArea;
		this.areaUnderServiceRoadArea = areaUnderServiceRoadArea;
		this.areaUnderReservationArea = areaUnderReservationArea;
		this.areaUnderGreenBeltArea = areaUnderGreenBeltArea;
		this.areaUnderNalaArea = areaUnderNalaArea;
		this.total2Area = total2Area;
		this.netGrossAreaOfPlot1_2Area = netGrossAreaOfPlot1_2Area;
		this.recreationGround10Area = recreationGround10Area;
		this.amenitySpace5Area = amenitySpace5Area;
		this.internalRoadsArea = internalRoadsArea;
		this.transformerArea = transformerArea;
		this.total4Area = total4Area;
		this.netAreaOfPlot3_4Area = netAreaOfPlot3_4Area;
		this.tdr40Area = tdr40Area;
		this.additionsForFAR4a_RecreationGroundArea = additionsForFAR4a_RecreationGroundArea;
		this.additionForFAR4b_AmenitySpaceArea = additionForFAR4b_AmenitySpaceArea;
		this.additionForFAR4c_InternalsRoadArea = additionForFAR4c_InternalsRoadArea;
		this.additionForFAR4d_TransformerArea = additionForFAR4d_TransformerArea;
		this.total6Area = total6Area;
		this.totalArea5_6Area = totalArea5_6Area;
		this.farPermissibleArea = farPermissibleArea;
		this.totalPermissibleFloorArea7_8Area = totalPermissibleFloorArea7_8Area;
		this.permissibleResidentialArea = permissibleResidentialArea;
		this.existingResidentialArea = existingResidentialArea;
		this.proposedResidentialArea = proposedResidentialArea;
		this.totalResidentialArea11_12Area = totalResidentialArea11_12Area;
		this.permissibleCommercial_IndustrialArea = permissibleCommercial_IndustrialArea;
		this.existingCommercial_IndustrialArea = existingCommercial_IndustrialArea;
		this.proposedCommercial_IndustrialArea = proposedCommercial_IndustrialArea;
		this.totalCommercial_IndustrialArea15_16Area = totalCommercial_IndustrialArea15_16Area;
		this.totalExistingArea11_15 = totalExistingArea11_15;
		this.totalProposedArea12_16Area = totalProposedArea12_16Area;
		this.totalExistingArea_Proposed18_19Area = totalExistingArea_Proposed18_19Area;
		this.excessBalconyAreaTakenInFSIArea = excessBalconyAreaTakenInFSIArea;
		this.totalBupArea20_21Area = totalBupArea20_21Area;
		this.totalFSIConSumedArea = totalFSIConSumedArea;
		this.permissibleBalconyArea = permissibleBalconyArea;
		this.proposedBalconyArea = proposedBalconyArea;
		this.excessBalconyArea = excessBalconyArea;
		this.netGrossAreaOfPlot_A7_Area = netGrossAreaOfPlot_A7_Area;
		this.lessDeductionForNon_ResidentialArea = lessDeductionForNon_ResidentialArea;
		this.areaOfTenementsa_bArea = areaOfTenementsa_bArea;
		this.tenenmentsPermissibleArea = tenenmentsPermissibleArea;
		this.totalTenementsProposedArea = totalTenementsProposedArea;
		this.netPlotAreaArea = netPlotAreaArea;
		this.permissibleGroundCoverageArea = permissibleGroundCoverageArea;
		this.existingGroundCoverageArea = existingGroundCoverageArea;
		this.proposedGroundCoverageArea = proposedGroundCoverageArea;
		this.permissibleGroundCoverageInPremium15Area = permissibleGroundCoverageInPremium15Area;
		this.excessGroundConveraged_bArea = excessGroundConveraged_bArea;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getProjectArchitecturalId() {
		return projectArchitecturalId;
	}

	public void setProjectArchitecturalId(String projectArchitecturalId) {
		this.projectArchitecturalId = projectArchitecturalId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}


	public String getConsultancyId() {
		return consultancyId;
	}

	public void setConsultancyId(String consultancyId) {
		this.consultancyId = consultancyId;
	}

	public Double getAeraOfPlotAsPer7_12ExtractPer() {
		return aeraOfPlotAsPer7_12ExtractPer;
	}

	public void setAeraOfPlotAsPer7_12ExtractPer(Double aeraOfPlotAsPer7_12ExtractPer) {
		this.aeraOfPlotAsPer7_12ExtractPer = aeraOfPlotAsPer7_12ExtractPer;
	}

	public Double getAreaOfPlotAsPerPropetyPer() {
		return areaOfPlotAsPerPropetyPer;
	}

	public void setAreaOfPlotAsPerPropetyPer(Double areaOfPlotAsPerPropetyPer) {
		this.areaOfPlotAsPerPropetyPer = areaOfPlotAsPerPropetyPer;
	}

	public Double getAreaOfPlotAsPerDemarcationPer() {
		return areaOfPlotAsPerDemarcationPer;
	}

	public void setAreaOfPlotAsPerDemarcationPer(Double areaOfPlotAsPerDemarcationPer) {
		this.areaOfPlotAsPerDemarcationPer = areaOfPlotAsPerDemarcationPer;
	}

	public Double getAreaAsPerSanctionedLayoutPer() {
		return areaAsPerSanctionedLayoutPer;
	}

	public void setAreaAsPerSanctionedLayoutPer(Double areaAsPerSanctionedLayoutPer) {
		this.areaAsPerSanctionedLayoutPer = areaAsPerSanctionedLayoutPer;
	}

	public Double getAreaAsPerULCOrderPer() {
		return areaAsPerULCOrderPer;
	}

	public void setAreaAsPerULCOrderPer(Double areaAsPerULCOrderPer) {
		this.areaAsPerULCOrderPer = areaAsPerULCOrderPer;
	}

	public Double getAreaAsPerDevelopmentAgreementPer() {
		return areaAsPerDevelopmentAgreementPer;
	}

	public void setAreaAsPerDevelopmentAgreementPer(Double areaAsPerDevelopmentAgreementPer) {
		this.areaAsPerDevelopmentAgreementPer = areaAsPerDevelopmentAgreementPer;
	}

	public Double getAreaOfPlotMinimumConsiderationPer() {
		return areaOfPlotMinimumConsiderationPer;
	}

	public void setAreaOfPlotMinimumConsiderationPer(Double areaOfPlotMinimumConsiderationPer) {
		this.areaOfPlotMinimumConsiderationPer = areaOfPlotMinimumConsiderationPer;
	}

	public Double getAreaUnderBRTCorridorPer() {
		return areaUnderBRTCorridorPer;
	}

	public void setAreaUnderBRTCorridorPer(Double areaUnderBRTCorridorPer) {
		this.areaUnderBRTCorridorPer = areaUnderBRTCorridorPer;
	}

	public Double getAreaUnder1_50MRoadWideningPer() {
		return areaUnder1_50MRoadWideningPer;
	}

	public void setAreaUnder1_50MRoadWideningPer(Double areaUnder1_50MRoadWideningPer) {
		this.areaUnder1_50MRoadWideningPer = areaUnder1_50MRoadWideningPer;
	}

	public Double getAreaUnder3_00MRoadWideningPer() {
		return areaUnder3_00MRoadWideningPer;
	}

	public void setAreaUnder3_00MRoadWideningPer(Double areaUnder3_00MRoadWideningPer) {
		this.areaUnder3_00MRoadWideningPer = areaUnder3_00MRoadWideningPer;
	}

	public Double getAreaUnder4_50MRoadWideningPer() {
		return areaUnder4_50MRoadWideningPer;
	}

	public void setAreaUnder4_50MRoadWideningPer(Double areaUnder4_50MRoadWideningPer) {
		this.areaUnder4_50MRoadWideningPer = areaUnder4_50MRoadWideningPer;
	}

	public Double getAreaUnder6_00MRoadWideningPer() {
		return areaUnder6_00MRoadWideningPer;
	}

	public void setAreaUnder6_00MRoadWideningPer(Double areaUnder6_00MRoadWideningPer) {
		this.areaUnder6_00MRoadWideningPer = areaUnder6_00MRoadWideningPer;
	}

	public Double getAreaUnder7_50MRoadWideningPer() {
		return areaUnder7_50MRoadWideningPer;
	}

	public void setAreaUnder7_50MRoadWideningPer(Double areaUnder7_50MRoadWideningPer) {
		this.areaUnder7_50MRoadWideningPer = areaUnder7_50MRoadWideningPer;
	}

	public Double getAreaUnder9_00MRoadWideningPer() {
		return areaUnder9_00MRoadWideningPer;
	}

	public void setAreaUnder9_00MRoadWideningPer(Double areaUnder9_00MRoadWideningPer) {
		this.areaUnder9_00MRoadWideningPer = areaUnder9_00MRoadWideningPer;
	}

	public Double getAreaUnder12_00MRoadWideningPer() {
		return areaUnder12_00MRoadWideningPer;
	}

	public void setAreaUnder12_00MRoadWideningPer(Double areaUnder12_00MRoadWideningPer) {
		this.areaUnder12_00MRoadWideningPer = areaUnder12_00MRoadWideningPer;
	}

	public Double getAreaUnder15_00MRoadWideningPer() {
		return areaUnder15_00MRoadWideningPer;
	}

	public void setAreaUnder15_00MRoadWideningPer(Double areaUnder15_00MRoadWideningPer) {
		this.areaUnder15_00MRoadWideningPer = areaUnder15_00MRoadWideningPer;
	}

	public Double getAreaUnder18_00MRoadWideningPer() {
		return areaUnder18_00MRoadWideningPer;
	}

	public void setAreaUnder18_00MRoadWideningPer(Double areaUnder18_00MRoadWideningPer) {
		this.areaUnder18_00MRoadWideningPer = areaUnder18_00MRoadWideningPer;
	}

	public Double getAreaUnder24_00MRoadWideningPer() {
		return areaUnder24_00MRoadWideningPer;
	}

	public void setAreaUnder24_00MRoadWideningPer(Double areaUnder24_00MRoadWideningPer) {
		this.areaUnder24_00MRoadWideningPer = areaUnder24_00MRoadWideningPer;
	}

	public Double getAreaUnder30_00MRoadWideningPer() {
		return areaUnder30_00MRoadWideningPer;
	}

	public void setAreaUnder30_00MRoadWideningPer(Double areaUnder30_00MRoadWideningPer) {
		this.areaUnder30_00MRoadWideningPer = areaUnder30_00MRoadWideningPer;
	}

	public Double getAreaUnderServiceRoadPer() {
		return areaUnderServiceRoadPer;
	}

	public void setAreaUnderServiceRoadPer(Double areaUnderServiceRoadPer) {
		this.areaUnderServiceRoadPer = areaUnderServiceRoadPer;
	}

	public Double getAreaUnderReservationPer() {
		return areaUnderReservationPer;
	}

	public void setAreaUnderReservationPer(Double areaUnderReservationPer) {
		this.areaUnderReservationPer = areaUnderReservationPer;
	}

	public Double getAreaUnderGreenBeltPer() {
		return areaUnderGreenBeltPer;
	}

	public void setAreaUnderGreenBeltPer(Double areaUnderGreenBeltPer) {
		this.areaUnderGreenBeltPer = areaUnderGreenBeltPer;
	}

	public Double getAreaUnderNalaPer() {
		return areaUnderNalaPer;
	}

	public void setAreaUnderNalaPer(Double areaUnderNalaPer) {
		this.areaUnderNalaPer = areaUnderNalaPer;
	}

	public Double getRecreationGround10Per() {
		return recreationGround10Per;
	}

	public void setRecreationGround10Per(Double recreationGround10Per) {
		this.recreationGround10Per = recreationGround10Per;
	}

	public Double getAmenitySpace5Per() {
		return amenitySpace5Per;
	}

	public void setAmenitySpace5Per(Double amenitySpace5Per) {
		this.amenitySpace5Per = amenitySpace5Per;
	}

	public Double getInternalRoadsPer() {
		return internalRoadsPer;
	}

	public void setInternalRoadsPer(Double internalRoadsPer) {
		this.internalRoadsPer = internalRoadsPer;
	}

	public Double getTransformerPer() {
		return transformerPer;
	}

	public void setTransformerPer(Double transformerPer) {
		this.transformerPer = transformerPer;
	}

	public Double getTdr40Per() {
		return tdr40Per;
	}

	public void setTdr40Per(Double tdr40Per) {
		this.tdr40Per = tdr40Per;
	}

	public Double getAdditionsForFAR4a_RecreationGroundPer() {
		return additionsForFAR4a_RecreationGroundPer;
	}

	public void setAdditionsForFAR4a_RecreationGroundPer(Double additionsForFAR4a_RecreationGroundPer) {
		this.additionsForFAR4a_RecreationGroundPer = additionsForFAR4a_RecreationGroundPer;
	}

	public Double getAdditionForFAR4b_AmenitySpacePer() {
		return additionForFAR4b_AmenitySpacePer;
	}

	public void setAdditionForFAR4b_AmenitySpacePer(Double additionForFAR4b_AmenitySpacePer) {
		this.additionForFAR4b_AmenitySpacePer = additionForFAR4b_AmenitySpacePer;
	}

	public Double getAdditionForFAR4c_InternalsRoadPer() {
		return additionForFAR4c_InternalsRoadPer;
	}

	public void setAdditionForFAR4c_InternalsRoadPer(Double additionForFAR4c_InternalsRoadPer) {
		this.additionForFAR4c_InternalsRoadPer = additionForFAR4c_InternalsRoadPer;
	}

	public Double getAdditionForFAR4d_TransformerPer() {
		return additionForFAR4d_TransformerPer;
	}

	public void setAdditionForFAR4d_TransformerPer(Double additionForFAR4d_TransformerPer) {
		this.additionForFAR4d_TransformerPer = additionForFAR4d_TransformerPer;
	}

	public Double getFarPermissiblePer() {
		return farPermissiblePer;
	}

	public void setFarPermissiblePer(Double farPermissiblePer) {
		this.farPermissiblePer = farPermissiblePer;
	}

	public Double getPermissibleResidentialPer() {
		return permissibleResidentialPer;
	}

	public void setPermissibleResidentialPer(Double permissibleResidentialPer) {
		this.permissibleResidentialPer = permissibleResidentialPer;
	}

	public Double getExistingResidentialPer() {
		return existingResidentialPer;
	}

	public void setExistingResidentialPer(Double existingResidentialPer) {
		this.existingResidentialPer = existingResidentialPer;
	}

	public Double getProposedResidentialPer() {
		return proposedResidentialPer;
	}

	public void setProposedResidentialPer(Double proposedResidentialPer) {
		this.proposedResidentialPer = proposedResidentialPer;
	}

	public Double getPermissibleCommercial_IndustrialPer() {
		return permissibleCommercial_IndustrialPer;
	}

	public void setPermissibleCommercial_IndustrialPer(Double permissibleCommercial_IndustrialPer) {
		this.permissibleCommercial_IndustrialPer = permissibleCommercial_IndustrialPer;
	}

	public Double getExistingCommercial_IndustrialPer() {
		return existingCommercial_IndustrialPer;
	}

	public void setExistingCommercial_IndustrialPer(Double existingCommercial_IndustrialPer) {
		this.existingCommercial_IndustrialPer = existingCommercial_IndustrialPer;
	}

	public Double getProposedCommercial_IndustrialPer() {
		return proposedCommercial_IndustrialPer;
	}

	public void setProposedCommercial_IndustrialPer(Double proposedCommercial_IndustrialPer) {
		this.proposedCommercial_IndustrialPer = proposedCommercial_IndustrialPer;
	}

	public Double getExcessBalconyAreaTakenInFSIPer() {
		return excessBalconyAreaTakenInFSIPer;
	}

	public void setExcessBalconyAreaTakenInFSIPer(Double excessBalconyAreaTakenInFSIPer) {
		this.excessBalconyAreaTakenInFSIPer = excessBalconyAreaTakenInFSIPer;
	}

	public Double getTotalFSIConSumedPer() {
		return totalFSIConSumedPer;
	}

	public void setTotalFSIConSumedPer(Double totalFSIConSumedPer) {
		this.totalFSIConSumedPer = totalFSIConSumedPer;
	}

	public Double getPermissibleBalconyPer() {
		return permissibleBalconyPer;
	}

	public void setPermissibleBalconyPer(Double permissibleBalconyPer) {
		this.permissibleBalconyPer = permissibleBalconyPer;
	}

	public Double getProposedBalconyPer() {
		return proposedBalconyPer;
	}

	public void setProposedBalconyPer(Double proposedBalconyPer) {
		this.proposedBalconyPer = proposedBalconyPer;
	}

	public Double getNetGrossAreaOfPlot_A7_Per() {
		return netGrossAreaOfPlot_A7_Per;
	}

	public void setNetGrossAreaOfPlot_A7_Per(Double netGrossAreaOfPlot_A7_Per) {
		this.netGrossAreaOfPlot_A7_Per = netGrossAreaOfPlot_A7_Per;
	}

	public Double getLessDeductionForNon_ResidentialPer() {
		return lessDeductionForNon_ResidentialPer;
	}

	public void setLessDeductionForNon_ResidentialPer(Double lessDeductionForNon_ResidentialPer) {
		this.lessDeductionForNon_ResidentialPer = lessDeductionForNon_ResidentialPer;
	}

	public Double getTenenmentsPermissiblePer() {
		return tenenmentsPermissiblePer;
	}

	public void setTenenmentsPermissiblePer(Double tenenmentsPermissiblePer) {
		this.tenenmentsPermissiblePer = tenenmentsPermissiblePer;
	}

	public Double getTotalTenementsProposedPer() {
		return totalTenementsProposedPer;
	}

	public void setTotalTenementsProposedPer(Double totalTenementsProposedPer) {
		this.totalTenementsProposedPer = totalTenementsProposedPer;
	}

	public Double getNetPlotAreaPer() {
		return netPlotAreaPer;
	}

	public void setNetPlotAreaPer(Double netPlotAreaPer) {
		this.netPlotAreaPer = netPlotAreaPer;
	}

	public Double getPermissibleGroundCoveragePer() {
		return permissibleGroundCoveragePer;
	}

	public void setPermissibleGroundCoveragePer(Double permissibleGroundCoveragePer) {
		this.permissibleGroundCoveragePer = permissibleGroundCoveragePer;
	}

	public Double getExistingGroundCoveragePer() {
		return existingGroundCoveragePer;
	}

	public void setExistingGroundCoveragePer(Double existingGroundCoveragePer) {
		this.existingGroundCoveragePer = existingGroundCoveragePer;
	}

	public Double getProposedGroundCoveragePer() {
		return proposedGroundCoveragePer;
	}

	public void setProposedGroundCoveragePer(Double proposedGroundCoveragePer) {
		this.proposedGroundCoveragePer = proposedGroundCoveragePer;
	}

	public Double getPermissibleGroundCoverageInPremium15Per() {
		return permissibleGroundCoverageInPremium15Per;
	}

	public void setPermissibleGroundCoverageInPremium15Per(Double permissibleGroundCoverageInPremium15Per) {
		this.permissibleGroundCoverageInPremium15Per = permissibleGroundCoverageInPremium15Per;
	}

	public Double getAeraOfPlotAsPer7_12ExtractArea() {
		return aeraOfPlotAsPer7_12ExtractArea;
	}

	public void setAeraOfPlotAsPer7_12ExtractArea(Double aeraOfPlotAsPer7_12ExtractArea) {
		this.aeraOfPlotAsPer7_12ExtractArea = aeraOfPlotAsPer7_12ExtractArea;
	}

	public Double getAreaOfPlotAsPerPropetyArea() {
		return areaOfPlotAsPerPropetyArea;
	}

	public void setAreaOfPlotAsPerPropetyArea(Double areaOfPlotAsPerPropetyArea) {
		this.areaOfPlotAsPerPropetyArea = areaOfPlotAsPerPropetyArea;
	}

	public Double getAreaOfPlotAsPerDemarcationArea() {
		return areaOfPlotAsPerDemarcationArea;
	}

	public void setAreaOfPlotAsPerDemarcationArea(Double areaOfPlotAsPerDemarcationArea) {
		this.areaOfPlotAsPerDemarcationArea = areaOfPlotAsPerDemarcationArea;
	}

	public Double getAreaAsPerSanctionedLayoutArea() {
		return areaAsPerSanctionedLayoutArea;
	}

	public void setAreaAsPerSanctionedLayoutArea(Double areaAsPerSanctionedLayoutArea) {
		this.areaAsPerSanctionedLayoutArea = areaAsPerSanctionedLayoutArea;
	}

	public Double getAreaAsPerULCOrderArea() {
		return areaAsPerULCOrderArea;
	}

	public void setAreaAsPerULCOrderArea(Double areaAsPerULCOrderArea) {
		this.areaAsPerULCOrderArea = areaAsPerULCOrderArea;
	}

	public Double getAreaAsPerDevelopmentAgreementArea() {
		return areaAsPerDevelopmentAgreementArea;
	}

	public void setAreaAsPerDevelopmentAgreementArea(Double areaAsPerDevelopmentAgreementArea) {
		this.areaAsPerDevelopmentAgreementArea = areaAsPerDevelopmentAgreementArea;
	}

	public Double getAreaOfPlotMinimumConsiderationArea() {
		return areaOfPlotMinimumConsiderationArea;
	}

	public void setAreaOfPlotMinimumConsiderationArea(Double areaOfPlotMinimumConsiderationArea) {
		this.areaOfPlotMinimumConsiderationArea = areaOfPlotMinimumConsiderationArea;
	}

	public Double getAreaUnderBRTCorridorArea() {
		return areaUnderBRTCorridorArea;
	}

	public void setAreaUnderBRTCorridorArea(Double areaUnderBRTCorridorArea) {
		this.areaUnderBRTCorridorArea = areaUnderBRTCorridorArea;
	}

	public Double getAreaUnder1_50MRoadWideningArea() {
		return areaUnder1_50MRoadWideningArea;
	}

	public void setAreaUnder1_50MRoadWideningArea(Double areaUnder1_50MRoadWideningArea) {
		this.areaUnder1_50MRoadWideningArea = areaUnder1_50MRoadWideningArea;
	}

	public Double getAreaUnder3_00MRoadWideningArea() {
		return areaUnder3_00MRoadWideningArea;
	}

	public void setAreaUnder3_00MRoadWideningArea(Double areaUnder3_00MRoadWideningArea) {
		this.areaUnder3_00MRoadWideningArea = areaUnder3_00MRoadWideningArea;
	}

	public Double getAreaUnder4_50MRoadWideningArea() {
		return areaUnder4_50MRoadWideningArea;
	}

	public void setAreaUnder4_50MRoadWideningArea(Double areaUnder4_50MRoadWideningArea) {
		this.areaUnder4_50MRoadWideningArea = areaUnder4_50MRoadWideningArea;
	}

	public Double getAreaUnder6_00MRoadWideningArea() {
		return areaUnder6_00MRoadWideningArea;
	}

	public void setAreaUnder6_00MRoadWideningArea(Double areaUnder6_00MRoadWideningArea) {
		this.areaUnder6_00MRoadWideningArea = areaUnder6_00MRoadWideningArea;
	}

	public Double getAreaUnder7_50MRoadWideningArea() {
		return areaUnder7_50MRoadWideningArea;
	}

	public void setAreaUnder7_50MRoadWideningArea(Double areaUnder7_50MRoadWideningArea) {
		this.areaUnder7_50MRoadWideningArea = areaUnder7_50MRoadWideningArea;
	}

	public Double getAreaUnder9_00MRoadWideningArea() {
		return areaUnder9_00MRoadWideningArea;
	}

	public void setAreaUnder9_00MRoadWideningArea(Double areaUnder9_00MRoadWideningArea) {
		this.areaUnder9_00MRoadWideningArea = areaUnder9_00MRoadWideningArea;
	}

	public Double getAreaUnder12_00MRoadWideningArea() {
		return areaUnder12_00MRoadWideningArea;
	}

	public void setAreaUnder12_00MRoadWideningArea(Double areaUnder12_00MRoadWideningArea) {
		this.areaUnder12_00MRoadWideningArea = areaUnder12_00MRoadWideningArea;
	}

	public Double getAreaUnder15_00MRoadWideningArea() {
		return areaUnder15_00MRoadWideningArea;
	}

	public void setAreaUnder15_00MRoadWideningArea(Double areaUnder15_00MRoadWideningArea) {
		this.areaUnder15_00MRoadWideningArea = areaUnder15_00MRoadWideningArea;
	}

	public Double getAreaUnder18_00MRoadWideningArea() {
		return areaUnder18_00MRoadWideningArea;
	}

	public void setAreaUnder18_00MRoadWideningArea(Double areaUnder18_00MRoadWideningArea) {
		this.areaUnder18_00MRoadWideningArea = areaUnder18_00MRoadWideningArea;
	}

	public Double getAreaUnder24_00MRoadWideningArea() {
		return areaUnder24_00MRoadWideningArea;
	}

	public void setAreaUnder24_00MRoadWideningArea(Double areaUnder24_00MRoadWideningArea) {
		this.areaUnder24_00MRoadWideningArea = areaUnder24_00MRoadWideningArea;
	}

	public Double getAreaUnder30_00MRoadWideningArea() {
		return areaUnder30_00MRoadWideningArea;
	}

	public void setAreaUnder30_00MRoadWideningArea(Double areaUnder30_00MRoadWideningArea) {
		this.areaUnder30_00MRoadWideningArea = areaUnder30_00MRoadWideningArea;
	}

	public Double getAreaUnderServiceRoadArea() {
		return areaUnderServiceRoadArea;
	}

	public void setAreaUnderServiceRoadArea(Double areaUnderServiceRoadArea) {
		this.areaUnderServiceRoadArea = areaUnderServiceRoadArea;
	}

	public Double getAreaUnderReservationArea() {
		return areaUnderReservationArea;
	}

	public void setAreaUnderReservationArea(Double areaUnderReservationArea) {
		this.areaUnderReservationArea = areaUnderReservationArea;
	}

	public Double getAreaUnderGreenBeltArea() {
		return areaUnderGreenBeltArea;
	}

	public void setAreaUnderGreenBeltArea(Double areaUnderGreenBeltArea) {
		this.areaUnderGreenBeltArea = areaUnderGreenBeltArea;
	}

	public Double getAreaUnderNalaArea() {
		return areaUnderNalaArea;
	}

	public void setAreaUnderNalaArea(Double areaUnderNalaArea) {
		this.areaUnderNalaArea = areaUnderNalaArea;
	}

	public Double getTotal2Area() {
		return total2Area;
	}

	public void setTotal2Area(Double total2Area) {
		this.total2Area = total2Area;
	}

	public Double getNetGrossAreaOfPlot1_2Area() {
		return netGrossAreaOfPlot1_2Area;
	}

	public void setNetGrossAreaOfPlot1_2Area(Double netGrossAreaOfPlot1_2Area) {
		this.netGrossAreaOfPlot1_2Area = netGrossAreaOfPlot1_2Area;
	}

	public Double getRecreationGround10Area() {
		return recreationGround10Area;
	}

	public void setRecreationGround10Area(Double recreationGround10Area) {
		this.recreationGround10Area = recreationGround10Area;
	}

	public Double getAmenitySpace5Area() {
		return amenitySpace5Area;
	}

	public void setAmenitySpace5Area(Double amenitySpace5Area) {
		this.amenitySpace5Area = amenitySpace5Area;
	}

	public Double getInternalRoadsArea() {
		return internalRoadsArea;
	}

	public void setInternalRoadsArea(Double internalRoadsArea) {
		this.internalRoadsArea = internalRoadsArea;
	}

	public Double getTransformerArea() {
		return transformerArea;
	}

	public void setTransformerArea(Double transformerArea) {
		this.transformerArea = transformerArea;
	}

	public Double getTotal4Area() {
		return total4Area;
	}

	public void setTotal4Area(Double total4Area) {
		this.total4Area = total4Area;
	}

	public Double getNetAreaOfPlot3_4Area() {
		return netAreaOfPlot3_4Area;
	}

	public void setNetAreaOfPlot3_4Area(Double netAreaOfPlot3_4Area) {
		this.netAreaOfPlot3_4Area = netAreaOfPlot3_4Area;
	}

	public Double getTdr40Area() {
		return tdr40Area;
	}

	public void setTdr40Area(Double tdr40Area) {
		this.tdr40Area = tdr40Area;
	}

	public Double getAdditionsForFAR4a_RecreationGroundArea() {
		return additionsForFAR4a_RecreationGroundArea;
	}

	public void setAdditionsForFAR4a_RecreationGroundArea(Double additionsForFAR4a_RecreationGroundArea) {
		this.additionsForFAR4a_RecreationGroundArea = additionsForFAR4a_RecreationGroundArea;
	}

	public Double getAdditionForFAR4b_AmenitySpaceArea() {
		return additionForFAR4b_AmenitySpaceArea;
	}

	public void setAdditionForFAR4b_AmenitySpaceArea(Double additionForFAR4b_AmenitySpaceArea) {
		this.additionForFAR4b_AmenitySpaceArea = additionForFAR4b_AmenitySpaceArea;
	}

	public Double getAdditionForFAR4c_InternalsRoadArea() {
		return additionForFAR4c_InternalsRoadArea;
	}

	public void setAdditionForFAR4c_InternalsRoadArea(Double additionForFAR4c_InternalsRoadArea) {
		this.additionForFAR4c_InternalsRoadArea = additionForFAR4c_InternalsRoadArea;
	}

	public Double getAdditionForFAR4d_TransformerArea() {
		return additionForFAR4d_TransformerArea;
	}

	public void setAdditionForFAR4d_TransformerArea(Double additionForFAR4d_TransformerArea) {
		this.additionForFAR4d_TransformerArea = additionForFAR4d_TransformerArea;
	}

	public Double getTotal6Area() {
		return total6Area;
	}

	public void setTotal6Area(Double total6Area) {
		this.total6Area = total6Area;
	}

	public Double getTotalArea5_6Area() {
		return totalArea5_6Area;
	}

	public void setTotalArea5_6Area(Double totalArea5_6Area) {
		this.totalArea5_6Area = totalArea5_6Area;
	}

	public Double getFarPermissibleArea() {
		return farPermissibleArea;
	}

	public void setFarPermissibleArea(Double farPermissibleArea) {
		this.farPermissibleArea = farPermissibleArea;
	}

	public Double getTotalPermissibleFloorArea7_8Area() {
		return totalPermissibleFloorArea7_8Area;
	}

	public void setTotalPermissibleFloorArea7_8Area(Double totalPermissibleFloorArea7_8Area) {
		this.totalPermissibleFloorArea7_8Area = totalPermissibleFloorArea7_8Area;
	}

	public Double getPermissibleResidentialArea() {
		return permissibleResidentialArea;
	}

	public void setPermissibleResidentialArea(Double permissibleResidentialArea) {
		this.permissibleResidentialArea = permissibleResidentialArea;
	}

	public Double getExistingResidentialArea() {
		return existingResidentialArea;
	}

	public void setExistingResidentialArea(Double existingResidentialArea) {
		this.existingResidentialArea = existingResidentialArea;
	}

	public Double getProposedResidentialArea() {
		return proposedResidentialArea;
	}

	public void setProposedResidentialArea(Double proposedResidentialArea) {
		this.proposedResidentialArea = proposedResidentialArea;
	}

	public Double getTotalResidentialArea11_12Area() {
		return totalResidentialArea11_12Area;
	}

	public void setTotalResidentialArea11_12Area(Double totalResidentialArea11_12Area) {
		this.totalResidentialArea11_12Area = totalResidentialArea11_12Area;
	}

	public Double getPermissibleCommercial_IndustrialArea() {
		return permissibleCommercial_IndustrialArea;
	}

	public void setPermissibleCommercial_IndustrialArea(Double permissibleCommercial_IndustrialArea) {
		this.permissibleCommercial_IndustrialArea = permissibleCommercial_IndustrialArea;
	}

	public Double getExistingCommercial_IndustrialArea() {
		return existingCommercial_IndustrialArea;
	}

	public void setExistingCommercial_IndustrialArea(Double existingCommercial_IndustrialArea) {
		this.existingCommercial_IndustrialArea = existingCommercial_IndustrialArea;
	}

	public Double getProposedCommercial_IndustrialArea() {
		return proposedCommercial_IndustrialArea;
	}

	public void setProposedCommercial_IndustrialArea(Double proposedCommercial_IndustrialArea) {
		this.proposedCommercial_IndustrialArea = proposedCommercial_IndustrialArea;
	}

	public Double getTotalCommercial_IndustrialArea15_16Area() {
		return totalCommercial_IndustrialArea15_16Area;
	}

	public void setTotalCommercial_IndustrialArea15_16Area(Double totalCommercial_IndustrialArea15_16Area) {
		this.totalCommercial_IndustrialArea15_16Area = totalCommercial_IndustrialArea15_16Area;
	}

	public Double getTotalExistingArea11_15() {
		return totalExistingArea11_15;
	}

	public void setTotalExistingArea11_15(Double totalExistingArea11_15) {
		this.totalExistingArea11_15 = totalExistingArea11_15;
	}

	public Double getTotalProposedArea12_16Area() {
		return totalProposedArea12_16Area;
	}

	public void setTotalProposedArea12_16Area(Double totalProposedArea12_16Area) {
		this.totalProposedArea12_16Area = totalProposedArea12_16Area;
	}

	public Double getTotalExistingArea_Proposed18_19Area() {
		return totalExistingArea_Proposed18_19Area;
	}

	public void setTotalExistingArea_Proposed18_19Area(Double totalExistingArea_Proposed18_19Area) {
		this.totalExistingArea_Proposed18_19Area = totalExistingArea_Proposed18_19Area;
	}

	public Double getExcessBalconyAreaTakenInFSIArea() {
		return excessBalconyAreaTakenInFSIArea;
	}

	public void setExcessBalconyAreaTakenInFSIArea(Double excessBalconyAreaTakenInFSIArea) {
		this.excessBalconyAreaTakenInFSIArea = excessBalconyAreaTakenInFSIArea;
	}

	public Double getTotalBupArea20_21Area() {
		return totalBupArea20_21Area;
	}

	public void setTotalBupArea20_21Area(Double totalBupArea20_21Area) {
		this.totalBupArea20_21Area = totalBupArea20_21Area;
	}

	public Double getTotalFSIConSumedArea() {
		return totalFSIConSumedArea;
	}

	public void setTotalFSIConSumedArea(Double totalFSIConSumedArea) {
		this.totalFSIConSumedArea = totalFSIConSumedArea;
	}

	public Double getPermissibleBalconyArea() {
		return permissibleBalconyArea;
	}

	public void setPermissibleBalconyArea(Double permissibleBalconyArea) {
		this.permissibleBalconyArea = permissibleBalconyArea;
	}

	public Double getProposedBalconyArea() {
		return proposedBalconyArea;
	}

	public void setProposedBalconyArea(Double proposedBalconyArea) {
		this.proposedBalconyArea = proposedBalconyArea;
	}

	public Double getExcessBalconyArea() {
		return excessBalconyArea;
	}

	public void setExcessBalconyArea(Double excessBalconyArea) {
		this.excessBalconyArea = excessBalconyArea;
	}

	public Double getNetGrossAreaOfPlot_A7_Area() {
		return netGrossAreaOfPlot_A7_Area;
	}

	public void setNetGrossAreaOfPlot_A7_Area(Double netGrossAreaOfPlot_A7_Area) {
		this.netGrossAreaOfPlot_A7_Area = netGrossAreaOfPlot_A7_Area;
	}

	public Double getLessDeductionForNon_ResidentialArea() {
		return lessDeductionForNon_ResidentialArea;
	}

	public void setLessDeductionForNon_ResidentialArea(Double lessDeductionForNon_ResidentialArea) {
		this.lessDeductionForNon_ResidentialArea = lessDeductionForNon_ResidentialArea;
	}

	public Double getAreaOfTenementsa_bArea() {
		return areaOfTenementsa_bArea;
	}

	public void setAreaOfTenementsa_bArea(Double areaOfTenementsa_bArea) {
		this.areaOfTenementsa_bArea = areaOfTenementsa_bArea;
	}

	public Double getTenenmentsPermissibleArea() {
		return tenenmentsPermissibleArea;
	}

	public void setTenenmentsPermissibleArea(Double tenenmentsPermissibleArea) {
		this.tenenmentsPermissibleArea = tenenmentsPermissibleArea;
	}

	public Double getTotalTenementsProposedArea() {
		return totalTenementsProposedArea;
	}

	public void setTotalTenementsProposedArea(Double totalTenementsProposedArea) {
		this.totalTenementsProposedArea = totalTenementsProposedArea;
	}

	public Double getNetPlotAreaArea() {
		return netPlotAreaArea;
	}

	public void setNetPlotAreaArea(Double netPlotAreaArea) {
		this.netPlotAreaArea = netPlotAreaArea;
	}

	public Double getPermissibleGroundCoverageArea() {
		return permissibleGroundCoverageArea;
	}

	public void setPermissibleGroundCoverageArea(Double permissibleGroundCoverageArea) {
		this.permissibleGroundCoverageArea = permissibleGroundCoverageArea;
	}

	public Double getExistingGroundCoverageArea() {
		return existingGroundCoverageArea;
	}

	public void setExistingGroundCoverageArea(Double existingGroundCoverageArea) {
		this.existingGroundCoverageArea = existingGroundCoverageArea;
	}

	public Double getProposedGroundCoverageArea() {
		return proposedGroundCoverageArea;
	}

	public void setProposedGroundCoverageArea(Double proposedGroundCoverageArea) {
		this.proposedGroundCoverageArea = proposedGroundCoverageArea;
	}

	public Double getPermissibleGroundCoverageInPremium15Area() {
		return permissibleGroundCoverageInPremium15Area;
	}

	public void setPermissibleGroundCoverageInPremium15Area(Double permissibleGroundCoverageInPremium15Area) {
		this.permissibleGroundCoverageInPremium15Area = permissibleGroundCoverageInPremium15Area;
	}

	public Double getExcessGroundConveraged_bArea() {
		return excessGroundConveraged_bArea;
	}

	public void setExcessGroundConveraged_bArea(Double excessGroundConveraged_bArea) {
		this.excessGroundConveraged_bArea = excessGroundConveraged_bArea;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getFirmName() {
		return firmName;
	}

	public void setFirmName(String firmName) {
		this.firmName = firmName;
	}

}
