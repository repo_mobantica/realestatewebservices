package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "contractorpaymentschedule")
public class ContractorPaymentSchedule {


	@Id
	private long contractorrschedulerId;
	private String workOrderId;
	private String contractorId;
	private String paymentDecription;
	private Double paymentAmountper;
	private Double paymentAmount;
	private Double gstAmount;
	private Double paymentTotalAmount;
	private Double tdsAmount;
	private Double grandAmount;
	private String status;
	
	private String paymentStatus;
	
	@Transient
	private long paidPaymentAmount;
	
	public ContractorPaymentSchedule()
	{}

	public ContractorPaymentSchedule(long contractorrschedulerId, String workOrderId, String contractorId,
			String paymentDecription, Double paymentAmountper, Double paymentAmount, Double gstAmount, Double paymentTotalAmount,
			Double tdsAmount, Double grandAmount, String status) {
		super();
		this.contractorrschedulerId = contractorrschedulerId;
		this.workOrderId = workOrderId;
		this.contractorId = contractorId;
		this.paymentDecription = paymentDecription;
		this.paymentAmountper=paymentAmountper;
		this.paymentAmount = paymentAmount;
		this.gstAmount = gstAmount;
		this.paymentTotalAmount = paymentTotalAmount;
		this.tdsAmount = tdsAmount;
		this.grandAmount = grandAmount;
		this.status = status;
	}

	public long getContractorrschedulerId() {
		return contractorrschedulerId;
	}

	public void setContractorrschedulerId(long contractorrschedulerId) {
		this.contractorrschedulerId = contractorrschedulerId;
	}

	public String getWorkOrderId() {
		return workOrderId;
	}

	public void setWorkOrderId(String workOrderId) {
		this.workOrderId = workOrderId;
	}

	public String getContractorId() {
		return contractorId;
	}

	public void setContractorId(String contractorId) {
		this.contractorId = contractorId;
	}

	public String getPaymentDecription() {
		return paymentDecription;
	}

	public void setPaymentDecription(String paymentDecription) {
		this.paymentDecription = paymentDecription;
	}

	public Double getPaymentAmountper() {
		return paymentAmountper;
	}

	public void setPaymentAmountper(Double paymentAmountper) {
		this.paymentAmountper = paymentAmountper;
	}

	public Double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(Double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public Double getGstAmount() {
		return gstAmount;
	}

	public void setGstAmount(Double gstAmount) {
		this.gstAmount = gstAmount;
	}

	public Double getPaymentTotalAmount() {
		return paymentTotalAmount;
	}

	public void setPaymentTotalAmount(Double paymentTotalAmount) {
		this.paymentTotalAmount = paymentTotalAmount;
	}

	public Double getTdsAmount() {
		return tdsAmount;
	}

	public void setTdsAmount(Double tdsAmount) {
		this.tdsAmount = tdsAmount;
	}

	public Double getGrandAmount() {
		return grandAmount;
	}

	public void setGrandAmount(Double grandAmount) {
		this.grandAmount = grandAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public long getPaidPaymentAmount() {
		return paidPaymentAmount;
	}

	public void setPaidPaymentAmount(long paidPaymentAmount) {
		this.paidPaymentAmount = paidPaymentAmount;
	}

	
}
