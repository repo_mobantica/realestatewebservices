package com.realestate.bean;

public class MonthlyCollectionReport {

	private double totalEnquiry;
	private double totalBooking;
	private double totalCollection;
	
	private long todayTotalFlatBooking;
	private long todayTotalFlatAgreement;
	private long totalFlatAgreement;
	private long totalFlatBooking;
	private long totalFlats;
	
	
	public MonthlyCollectionReport() {}

	public double getTotalEnquiry() {
		return totalEnquiry;
	}

	public void setTotalEnquiry(double totalEnquiry) {
		this.totalEnquiry = totalEnquiry;
	}

	public double getTotalBooking() {
		return totalBooking;
	}

	public void setTotalBooking(double totalBooking) {
		this.totalBooking = totalBooking;
	}

	public double getTotalCollection() {
		return totalCollection;
	}

	public void setTotalCollection(double totalCollection) {
		this.totalCollection = totalCollection;
	}

	public long getTodayTotalFlatBooking() {
		return todayTotalFlatBooking;
	}

	public void setTodayTotalFlatBooking(long todayTotalFlatBooking) {
		this.todayTotalFlatBooking = todayTotalFlatBooking;
	}

	public long getTodayTotalFlatAgreement() {
		return todayTotalFlatAgreement;
	}

	public void setTodayTotalFlatAgreement(long todayTotalFlatAgreement) {
		this.todayTotalFlatAgreement = todayTotalFlatAgreement;
	}

	public long getTotalFlatAgreement() {
		return totalFlatAgreement;
	}

	public void setTotalFlatAgreement(long totalFlatAgreement) {
		this.totalFlatAgreement = totalFlatAgreement;
	}

	public long getTotalFlatBooking() {
		return totalFlatBooking;
	}

	public void setTotalFlatBooking(long totalFlatBooking) {
		this.totalFlatBooking = totalFlatBooking;
	}

	public long getTotalFlats() {
		return totalFlats;
	}

	public void setTotalFlats(long totalFlats) {
		this.totalFlats = totalFlats;
	}
	
}
