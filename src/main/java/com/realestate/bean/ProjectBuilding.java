package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "projectbuildings")
//@CompoundIndexes({@CompoundIndex(name="projectbuildingIndex", unique= true, def="{'buildingName':1}")})
public class ProjectBuilding 
{
	@Id
	private String buildingId;
	private String projectId;
	private String buildingName;
	private String numberofWing;
	private String buildingStatus;

	private String creationDate;
	private String updateDate;
	private String userName;
	
	public ProjectBuilding()
	{
	}

	public ProjectBuilding(String buildingId, String projectId, String buildingName, String numberofWing,
			String buildingStatus, String creationDate, String updateDate, String userName) {
		super();
		this.buildingId = buildingId;
		this.projectId = projectId;
		this.buildingName = buildingName;
		this.numberofWing = numberofWing;
		this.buildingStatus = buildingStatus;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public String getNumberofWing() {
		return numberofWing;
	}

	public void setNumberofWing(String numberofWing) {
		this.numberofWing = numberofWing;
	}

	public String getBuildingStatus() {
		return buildingStatus;
	}

	public void setBuildingStatus(String buildingStatus) {
		this.buildingStatus = buildingStatus;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
