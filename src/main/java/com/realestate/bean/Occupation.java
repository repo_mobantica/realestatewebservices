package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "occupations")
@CompoundIndexes({@CompoundIndex(name="occupationIndex", unique= true, def="{'occupationName':1}")})

public class Occupation {

	 	@Id
	    private String occupationId;
	    private String occupationName;
	    private String creationDate;
	    private String updateDate;
	    private String userName;
	    
	    public Occupation()
	    {
	    	
	    }

		public Occupation(String occupationId, String occupationName, String creationDate, String updateDate,
				String userName) {
			super();
			this.occupationId = occupationId;
			this.occupationName = occupationName;
			this.creationDate = creationDate;
			this.updateDate = updateDate;
			this.userName = userName;
		}

		public String getOccupationId() {
			return occupationId;
		}

		public void setOccupationId(String occupationId) {
			this.occupationId = occupationId;
		}

		public String getOccupationName() {
			return occupationName;
		}

		public void setOccupationName(String occupationName) {
			this.occupationName = occupationName;
		}

		public String getCreationDate() {
			return creationDate;
		}

		public void setCreationDate(String creationDate) {
			this.creationDate = creationDate;
		}

		public String getUpdateDate() {
			return updateDate;
		}

		public void setUpdateDate(String updateDate) {
			this.updateDate = updateDate;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}
	    
}
