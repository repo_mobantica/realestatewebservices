package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="userprojectlists")
public class UserProjectList {

	@Id
	private long userProjectListId;
	private String userModelId;
	private String employeeId;
	private String projectId;
	
	public UserProjectList() {}

	public UserProjectList(long userProjectListId, String userModelId, String employeeId, String projectId) {
		super();
		this.userProjectListId = userProjectListId;
		this.userModelId = userModelId;
		this.employeeId = employeeId;
		this.projectId = projectId;
	}

	public long getUserProjectListId() {
		return userProjectListId;
	}

	public void setUserProjectListId(long userProjectListId) {
		this.userProjectListId = userProjectListId;
	}

	public String getUserModelId() {
		return userModelId;
	}

	public void setUserModelId(String userModelId) {
		this.userModelId = userModelId;
	}

	public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

}

