package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "contractors")
public class Contractor
{
	@Id
    private String contractorId;
	private String contractorfirmName;
	private String contractorfirmType;
    private String firmpanNumber;
    private String firmgstNumber;
    private Double tdspercentage;
    
    private String insuranceStatus;
    
	private String contractorfirmAddress;
	private String countryId;
	private String stateId;
    private String cityId;
    private String locationareaId;
    private String contractorPincode;
    
    private String bankName;
    private String branchName;
    private String bankifscCode;
    private String firmbankacNumber;
    private String contractortypeId;
    private String subcontractortypeId;
    private String contractorpaymentTerms;
    private String checkPrintingName;
    
    private String creationDate;
    private String updateDate;
    private String userName;
    
	
	public Contractor()
	{
		
	}


	public Contractor(String contractorId, String contractorfirmName, String contractorfirmType, String firmpanNumber,
			String firmgstNumber, Double tdspercentage, String insuranceStatus, String contractorfirmAddress,
			String countryId, String stateId, String cityId, String locationareaId, String contractorPincode,
			String bankName, String branchName, String bankifscCode, String firmbankacNumber, String contractortypeId,
			String subcontractortypeId, String contractorpaymentTerms, String checkPrintingName, String creationDate,
			String updateDate, String userName) {
		super();
		this.contractorId = contractorId;
		this.contractorfirmName = contractorfirmName;
		this.contractorfirmType = contractorfirmType;
		this.firmpanNumber = firmpanNumber;
		this.firmgstNumber = firmgstNumber;
		this.tdspercentage = tdspercentage;
		this.insuranceStatus = insuranceStatus;
		this.contractorfirmAddress = contractorfirmAddress;
		this.countryId = countryId;
		this.stateId = stateId;
		this.cityId = cityId;
		this.locationareaId = locationareaId;
		this.contractorPincode = contractorPincode;
		this.bankName = bankName;
		this.branchName = branchName;
		this.bankifscCode = bankifscCode;
		this.firmbankacNumber = firmbankacNumber;
		this.contractortypeId = contractortypeId;
		this.subcontractortypeId = subcontractortypeId;
		this.contractorpaymentTerms = contractorpaymentTerms;
		this.checkPrintingName = checkPrintingName;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}


	public String getContractorId() {
		return contractorId;
	}


	public void setContractorId(String contractorId) {
		this.contractorId = contractorId;
	}


	public String getContractorfirmName() {
		return contractorfirmName;
	}


	public void setContractorfirmName(String contractorfirmName) {
		this.contractorfirmName = contractorfirmName;
	}


	public String getContractorfirmType() {
		return contractorfirmType;
	}


	public void setContractorfirmType(String contractorfirmType) {
		this.contractorfirmType = contractorfirmType;
	}


	public String getFirmpanNumber() {
		return firmpanNumber;
	}


	public void setFirmpanNumber(String firmpanNumber) {
		this.firmpanNumber = firmpanNumber;
	}


	public String getFirmgstNumber() {
		return firmgstNumber;
	}


	public void setFirmgstNumber(String firmgstNumber) {
		this.firmgstNumber = firmgstNumber;
	}


	public Double getTdspercentage() {
		return tdspercentage;
	}


	public void setTdspercentage(Double tdspercentage) {
		this.tdspercentage = tdspercentage;
	}


	public String getInsuranceStatus() {
		return insuranceStatus;
	}


	public void setInsuranceStatus(String insuranceStatus) {
		this.insuranceStatus = insuranceStatus;
	}


	public String getContractorfirmAddress() {
		return contractorfirmAddress;
	}


	public void setContractorfirmAddress(String contractorfirmAddress) {
		this.contractorfirmAddress = contractorfirmAddress;
	}


	public String getCountryId() {
		return countryId;
	}


	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}


	public String getStateId() {
		return stateId;
	}


	public void setStateId(String stateId) {
		this.stateId = stateId;
	}


	public String getCityId() {
		return cityId;
	}


	public void setCityId(String cityId) {
		this.cityId = cityId;
	}


	public String getLocationareaId() {
		return locationareaId;
	}


	public void setLocationareaId(String locationareaId) {
		this.locationareaId = locationareaId;
	}


	public String getContractorPincode() {
		return contractorPincode;
	}


	public void setContractorPincode(String contractorPincode) {
		this.contractorPincode = contractorPincode;
	}


	public String getBankName() {
		return bankName;
	}


	public void setBankName(String bankName) {
		this.bankName = bankName;
	}


	public String getBranchName() {
		return branchName;
	}


	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}


	public String getBankifscCode() {
		return bankifscCode;
	}


	public void setBankifscCode(String bankifscCode) {
		this.bankifscCode = bankifscCode;
	}


	public String getFirmbankacNumber() {
		return firmbankacNumber;
	}


	public void setFirmbankacNumber(String firmbankacNumber) {
		this.firmbankacNumber = firmbankacNumber;
	}


	public String getContractortypeId() {
		return contractortypeId;
	}


	public void setContractortypeId(String contractortypeId) {
		this.contractortypeId = contractortypeId;
	}


	public String getSubcontractortypeId() {
		return subcontractortypeId;
	}


	public void setSubcontractortypeId(String subcontractortypeId) {
		this.subcontractortypeId = subcontractortypeId;
	}


	public String getContractorpaymentTerms() {
		return contractorpaymentTerms;
	}


	public void setContractorpaymentTerms(String contractorpaymentTerms) {
		this.contractorpaymentTerms = contractorpaymentTerms;
	}


	public String getCheckPrintingName() {
		return checkPrintingName;
	}


	public void setCheckPrintingName(String checkPrintingName) {
		this.checkPrintingName = checkPrintingName;
	}


	public String getCreationDate() {
		return creationDate;
	}


	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}


	public String getUpdateDate() {
		return updateDate;
	}


	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}

}

