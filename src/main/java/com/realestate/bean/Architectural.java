package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "architecturals")
public class Architectural {
	@Id
	private String architecturalId;
	private String architecturalfirmName;
	private String firmType;
	private String firmpanNumber;
	private String firmgstNumber;
	private String checkPrintingName;
	
	private String architecturalAddress;
	private String countryId;
	private String stateId;
	private String cityId;
	private String locationareaId;
	private String areaPincode;
	
	private String architecturalBankName;
	private String branchName;
	private String bankifscCode;
	private String architecturalBankacno;
	private String status;
	
	private String creationDate;
	private String updateDate;
	private String userName;
	
	public Architectural()
	{}

	public Architectural(String architecturalId, String architecturalfirmName, String firmType, String firmpanNumber,
			String firmgstNumber, String checkPrintingName, String architecturalAddress, String countryId,
			String stateId, String cityId, String locationareaId, String areaPincode, String architecturalBankName,
			String branchName, String bankifscCode, String architecturalBankacno, String status, String creationDate,
			String updateDate, String userName) {
		super();
		this.architecturalId = architecturalId;
		this.architecturalfirmName = architecturalfirmName;
		this.firmType = firmType;
		this.firmpanNumber = firmpanNumber;
		this.firmgstNumber = firmgstNumber;
		this.checkPrintingName = checkPrintingName;
		this.architecturalAddress = architecturalAddress;
		this.countryId = countryId;
		this.stateId = stateId;
		this.cityId = cityId;
		this.locationareaId = locationareaId;
		this.areaPincode = areaPincode;
		this.architecturalBankName = architecturalBankName;
		this.branchName = branchName;
		this.bankifscCode = bankifscCode;
		this.architecturalBankacno = architecturalBankacno;
		this.status = status;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getArchitecturalId() {
		return architecturalId;
	}

	public void setArchitecturalId(String architecturalId) {
		this.architecturalId = architecturalId;
	}

	public String getArchitecturalfirmName() {
		return architecturalfirmName;
	}

	public void setArchitecturalfirmName(String architecturalfirmName) {
		this.architecturalfirmName = architecturalfirmName;
	}

	public String getFirmType() {
		return firmType;
	}

	public void setFirmType(String firmType) {
		this.firmType = firmType;
	}

	public String getFirmpanNumber() {
		return firmpanNumber;
	}

	public void setFirmpanNumber(String firmpanNumber) {
		this.firmpanNumber = firmpanNumber;
	}

	public String getFirmgstNumber() {
		return firmgstNumber;
	}

	public void setFirmgstNumber(String firmgstNumber) {
		this.firmgstNumber = firmgstNumber;
	}

	public String getCheckPrintingName() {
		return checkPrintingName;
	}

	public void setCheckPrintingName(String checkPrintingName) {
		this.checkPrintingName = checkPrintingName;
	}

	public String getArchitecturalAddress() {
		return architecturalAddress;
	}

	public void setArchitecturalAddress(String architecturalAddress) {
		this.architecturalAddress = architecturalAddress;
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getStateId() {
		return stateId;
	}

	public void setStateId(String stateId) {
		this.stateId = stateId;
	}

	public String getCityId() {
		return cityId;
	}

	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	public String getLocationareaId() {
		return locationareaId;
	}

	public void setLocationareaId(String locationareaId) {
		this.locationareaId = locationareaId;
	}

	public String getAreaPincode() {
		return areaPincode;
	}

	public void setAreaPincode(String areaPincode) {
		this.areaPincode = areaPincode;
	}

	public String getArchitecturalBankName() {
		return architecturalBankName;
	}

	public void setArchitecturalBankName(String architecturalBankName) {
		this.architecturalBankName = architecturalBankName;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBankifscCode() {
		return bankifscCode;
	}

	public void setBankifscCode(String bankifscCode) {
		this.bankifscCode = bankifscCode;
	}

	public String getArchitecturalBankacno() {
		return architecturalBankacno;
	}

	public void setArchitecturalBankacno(String architecturalBankacno) {
		this.architecturalBankacno = architecturalBankacno;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}


}
