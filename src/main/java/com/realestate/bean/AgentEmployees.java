package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "agentemployees")
public class AgentEmployees {

	@Id
	private int agentEmployeeId;
	private String agentId;
	private String employeeName;
	private String employeeDesignation;
	private String employeeEmail;
	private String employeeMobileno;
	
	public AgentEmployees() {}

	public AgentEmployees(int agentEmployeeId, String agentId, String employeeName, String employeeDesignation,
			String employeeEmail, String employeeMobileno) {
		super();
		this.agentEmployeeId = agentEmployeeId;
		this.agentId = agentId;
		this.employeeName = employeeName;
		this.employeeDesignation = employeeDesignation;
		this.employeeEmail = employeeEmail;
		this.employeeMobileno = employeeMobileno;
	}

	public int getAgentEmployeeId() {
		return agentEmployeeId;
	}

	public void setAgentEmployeeId(int agentEmployeeId) {
		this.agentEmployeeId = agentEmployeeId;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmployeeDesignation() {
		return employeeDesignation;
	}

	public void setEmployeeDesignation(String employeeDesignation) {
		this.employeeDesignation = employeeDesignation;
	}

	public String getEmployeeEmail() {
		return employeeEmail;
	}

	public void setEmployeeEmail(String employeeEmail) {
		this.employeeEmail = employeeEmail;
	}

	public String getEmployeeMobileno() {
		return employeeMobileno;
	}

	public void setEmployeeMobileno(String employeeMobileno) {
		this.employeeMobileno = employeeMobileno;
	}
	
	
}
