package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "floors")
public class Floor 
{
	@Id
	private String floorId;
	private String projectId;
	private String buildingId;
	private String wingId;
	private String floortypeName;
	private int floorRise;
	private String floorzoneType;
	private int numberofFlats;
	private int numberofShops;
	private int numberofOpenParking;
	private int numberofCloseParking;
	
	private String creationDate;
	private String updateDate;
	private String userName;
	
	public Floor()
	{
		
	}

	public Floor(String floorId, String projectId, String buildingId, String wingId, String floortypeName,
			int floorRise, String floorzoneType, int numberofFlats, int numberofShops, int numberofOpenParking,
			int numberofCloseParking, String creationDate, String updateDate, String userName)
	{
		super();
		this.floorId = floorId;
		this.projectId = projectId;
		this.buildingId = buildingId;
		this.wingId = wingId;
		this.floortypeName = floortypeName;
		this.floorRise = floorRise;
		this.floorzoneType = floorzoneType;
		this.numberofFlats = numberofFlats;
		this.numberofShops = numberofShops;
		this.numberofOpenParking = numberofOpenParking;
		this.numberofCloseParking = numberofCloseParking;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}

	public String getFloorId() {
		return floorId;
	}

	public void setFloorId(String floorId) {
		this.floorId = floorId;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getWingId() {
		return wingId;
	}

	public void setWingId(String wingId) {
		this.wingId = wingId;
	}

	public String getFloortypeName() {
		return floortypeName;
	}

	public void setFloortypeName(String floortypeName) {
		this.floortypeName = floortypeName;
	}

	public int getFloorRise() {
		return floorRise;
	}

	public void setFloorRise(int floorRise) {
		this.floorRise = floorRise;
	}

	public String getFloorzoneType() {
		return floorzoneType;
	}

	public void setFloorzoneType(String floorzoneType) {
		this.floorzoneType = floorzoneType;
	}

	public int getNumberofFlats() {
		return numberofFlats;
	}

	public void setNumberofFlats(int numberofFlats) {
		this.numberofFlats = numberofFlats;
	}

	public int getNumberofShops() {
		return numberofShops;
	}

	public void setNumberofShops(int numberofShops) {
		this.numberofShops = numberofShops;
	}

	public int getNumberofOpenParking() {
		return numberofOpenParking;
	}

	public void setNumberofOpenParking(int numberofOpenParking) {
		this.numberofOpenParking = numberofOpenParking;
	}

	public int getNumberofCloseParking() {
		return numberofCloseParking;
	}

	public void setNumberofCloseParking(int numberofCloseParking) {
		this.numberofCloseParking = numberofCloseParking;
	}

	public String getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
