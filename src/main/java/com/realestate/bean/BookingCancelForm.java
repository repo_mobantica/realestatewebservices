package com.realestate.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "bookingcancellist")
@CompoundIndexes({@CompoundIndex(name="cancelIndex", unique= true, def="{'bookingId':1}")})
public class BookingCancelForm {
	
	@Id
	private String cancelId;
	private String bookingId;
	private String bookingDate;
	private String cancelDate;
	private String customerName;
	private String customerEmail;
	private String customerMobile;
	private String projectId;
	private String buildingId;
	private String wingId;
	private String floorId;
	private String flatType;
	private String flatId;
	private String flatFacing;
	private Double flatareainSqFt;
	private Double flatbasicCost;
	private Double aggreementValue1;
	private Double cancelCharge;
	private Double gstTaxes;
	private Double netAmount;
	private String creationDate;
	private String updateDate;
	private String userName;
	public BookingCancelForm()
	{
		
	}
	public BookingCancelForm(String cancelId, String bookingId, String bookingDate, String cancelDate,
			String customerName, String customerEmail, String customerMobile, String projectId, String buildingId,
			String wingId, String floorId, String flatType, String flatId, String flatFacing,
			Double flatareainSqFt, Double flatbasicCost, Double aggreementValue1, Double cancelCharge, Double gstTaxes,
			Double netAmount, String creationDate, String updateDate, String userName) {
		super();
		this.cancelId = cancelId;
		this.bookingId = bookingId;
		this.bookingDate = bookingDate;
		this.cancelDate = cancelDate;
		this.customerName = customerName;
		this.customerEmail = customerEmail;
		this.customerMobile = customerMobile;
		this.projectId = projectId;
		this.buildingId = buildingId;
		this.wingId = wingId;
		this.floorId = floorId;
		this.flatType = flatType;
		this.flatId = flatId;
		this.flatFacing = flatFacing;
		this.flatareainSqFt = flatareainSqFt;
		this.flatbasicCost = flatbasicCost;
		this.aggreementValue1 = aggreementValue1;
		this.cancelCharge = cancelCharge;
		this.gstTaxes = gstTaxes;
		this.netAmount = netAmount;
		this.creationDate = creationDate;
		this.updateDate = updateDate;
		this.userName = userName;
	}
	public String getCancelId() {
		return cancelId;
	}
	public void setCancelId(String cancelId) {
		this.cancelId = cancelId;
	}
	public String getBookingId() {
		return bookingId;
	}
	public void setBookingId(String bookingId) {
		this.bookingId = bookingId;
	}
	public String getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}
	public String getCancelDate() {
		return cancelDate;
	}
	public void setCancelDate(String cancelDate) {
		this.cancelDate = cancelDate;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerEmail() {
		return customerEmail;
	}
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	public String getCustomerMobile() {
		return customerMobile;
	}
	public void setCustomerMobile(String customerMobile) {
		this.customerMobile = customerMobile;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getBuildingId() {
		return buildingId;
	}
	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}
	public String getWingId() {
		return wingId;
	}
	public void setWingId(String wingId) {
		this.wingId = wingId;
	}
	public String getFloorId() {
		return floorId;
	}
	public void setFloorId(String floorId) {
		this.floorId = floorId;
	}
	public String getFlatType() {
		return flatType;
	}
	public void setFlatType(String flatType) {
		this.flatType = flatType;
	}
	public String getFlatId() {
		return flatId;
	}
	public void setFlatId(String flatId) {
		this.flatId = flatId;
	}
	public String getFlatFacing() {
		return flatFacing;
	}
	public void setFlatFacing(String flatFacing) {
		this.flatFacing = flatFacing;
	}
	public Double getFlatareainSqFt() {
		return flatareainSqFt;
	}
	public void setFlatareainSqFt(Double flatareainSqFt) {
		this.flatareainSqFt = flatareainSqFt;
	}
	public Double getFlatbasicCost() {
		return flatbasicCost;
	}
	public void setFlatbasicCost(Double flatbasicCost) {
		this.flatbasicCost = flatbasicCost;
	}
	public Double getAggreementValue1() {
		return aggreementValue1;
	}
	public void setAggreementValue1(Double aggreementValue1) {
		this.aggreementValue1 = aggreementValue1;
	}
	public Double getCancelCharge() {
		return cancelCharge;
	}
	public void setCancelCharge(Double cancelCharge) {
		this.cancelCharge = cancelCharge;
	}
	public Double getGstTaxes() {
		return gstTaxes;
	}
	public void setGstTaxes(Double gstTaxes) {
		this.gstTaxes = gstTaxes;
	}
	public Double getNetAmount() {
		return netAmount;
	}
	public void setNetAmount(Double netAmount) {
		this.netAmount = netAmount;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
